import binascii
import hashlib
import os
import json

from chalice import Chalice

app = Chalice(app_name='sello-digital-service')


@app.route('/')
def index():
    password = "Data From Other System"
    salt = hashlib.sha3_512(os.urandom(60)).hexdigest().encode('ascii')
    print(salt)
    pashas = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                 salt, 100000)
    pashas = binascii.hexlify(pashas)
    print(pashas)

    print(pashas.__str__())

    return {'sello digital': pashas.__str__()}


@app.route('/sellodigital', methods=['POST'])
def create_sello_digital():
    password = "Data From Other System"
    request_as_json = app.current_request.json_body

    print(request_as_json)
    print(request_as_json.__str__())
    digital_value_all = json.dumps(request_as_json)

    print(digital_value_all)

    digital_seal_dict = json.loads(digital_value_all)

    # clave_de_rastreo = digital_seal_dict["Clave de Rastreo"]
    # tipo_de_pago = digital_seal_dict["Tipo de pago"]

    # print(clave_de_rastreo)
    # print(tipo_de_pago)

    # print(digital_value_all['sello digital'])
    salt = hashlib.sha3_512(os.urandom(60)).hexdigest().encode('ascii')
    # print(salt)
    new_salt = digital_value_all.encode('utf-8')
    print(new_salt)
    pashas = hashlib.pbkdf2_hmac('sha512', digital_value_all.encode('utf-8'),
                                 new_salt, 100000)
    pashas = binascii.hexlify(pashas)
    print(pashas)

    print(pashas.__str__())

    # return {'sello digital': pashas.__str__()}
    return pashas

# The view function above will return {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
# @app.route('/hello/{name}')
# def hello_name(name):
#    # '/hello/james' -> {"hello": "james"}
#    return {'hello': name}
#
# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.current_request.json_body
#     # We'll echo the json body back to the user in a 'user' key.
#     return {'user': user_as_json}
#
# See the README documentation for more examples.
#
