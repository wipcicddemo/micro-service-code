"""
Valida los parametros del modelo cda_fields
"""
import re
import logging
import datetime
import os
import yaml
import json
import sys
import hashlib
import Cryptodome
from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import AES, PKCS1_v1_5
from Cryptodome.Hash import SHA
from Cryptodome import Random
import base64


from datetime import datetime

def handler(event, context):
    ##logging.log(#logging.INFO, context)
    return post(event)

def post(body):
    # try:
        logging.log(logging.INFO,body)
        original_string = body['original_string']
        logging.log(logging.INFO,original_string)
        digest=hashlib.sha3_256(original_string.encode('ascii')).hexdigest()
        logging.log(logging.INFO,digest)

    #    #Generate keys
    #     private_key = RSA.generate(1024)
    #     private_key_gen = private_key.export_key()
    #     file_out = open("private.pem", "wb")
    #     file_out.write(private_key_gen)

    #     public_key_gen = private_key.publickey().export_key()
    #     file_out = open("receiver.pem", "wb")
    #     file_out.write(public_key_gen)

        # #Sin llave fisica
        # private_key = RSA.generate(1024)
        # public_key = private_key.publickey()

        #Con llave fisica
        public_key = RSA.import_key(open("receiver.pem").read())
        private_key = RSA.import_key(open("private.pem").read())


        #set message
        message = digest.encode()

        #Cifrado
        
        cipher = PKCS1_v1_5.new(public_key)
        ciphertext = cipher.encrypt(message)
        

        #Pasar a base 64

        logging.log(logging.INFO,ciphertext)
        base644 = base64.b64encode(ciphertext)

        logging.log(logging.INFO,base644)

        #secifrar para validar

        dsize = SHA.digest_size
        cipherPerro = PKCS1_v1_5.new(private_key)
        sentinel = Random.new().read(15+dsize)  

        text = cipherPerro.decrypt(ciphertext,sentinel)

        logging.log(logging.INFO,text)

        decoded = base64.b64decode(base644)

        logging.log(logging.INFO,decoded)
        
        return {"digital_stamp": base644.decode()}

    # except:
    #     return "Error desconocido", 400
