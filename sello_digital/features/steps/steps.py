from behave import given, when, then
import requests
from compare import expect
import Cryptodome
from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import AES, PKCS1_v1_5
from Cryptodome.Hash import SHA
from Cryptodome import Random
import base64
import hashlib

# @given(u'a transaction')
# def set_transaction(context):
#     context.call_data = {
#         "data": "002073876415879467"
#     }

# @given(u'the DATA is {data}')
# def set_clabe(context, data):
#     context.call_data['data'] = data

# @when(u'the service is called')
# def call_mock(context):
#     request_data = context.call_data
#     url = context.config.userdata['base_url']
#     context.response = requests.put(url, json=request_data)

# @then(u'status code is {status_code}')
# def assert_status_code(context, status_code):
#     expect(status_code).to_equal(str(context.response.status_code))

# @then(u'the message is {message}')
# def assert_transfer_account(context, message):
#     expect(message).to_equal(context.response.json()['message'])

# @given('a digital stamp')
# def get_stamp(context):
#     context.stamp = "hola"

# @when('the digital stamp is generated')
# def generate_stamp_test(context):
#     context.stamp2 = "hola"

# @then('the digital stamp ins different to the last one')
# def compare_stamp(context):
#     print(context)
#     expect(context.stamp).to_equal(context.stamp2)

@given('two different orignial strings')
def set_original_strings(context):
    context.stamp1= {
        "original_string": "||3|13122019|23112019|123456|40002|BANORTE|Dan huerta|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert||"
    }
    context.stamp2= {
        "original_string": "||3|13122019|23112019|123456|40002|BANORTE|Juan Carlos|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert||"
    }

@when('the digital stamp is generated for the two original strings')
def generate_digital_stamp(context):
    #url = "http://sello-digital-service-blacklist.apps-ilab-ctigtdc01u.nam.nsroot.net/digitalStampGen"
    url = "http://localhost:8080/digitalStampGen"
    context.response1 = requests.post(url, json=context.stamp1)
    context.response2 = requests.post(url, json=context.stamp2)

@then('the two digital stamps are different from each other')
def compare_differet_stamps(context):
    
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("***   the two digital stamps are different from each other     ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print ("***                      Digital Stamp 1                       ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print (context.response1.json()['digital_stamp'])
    print ("*******************************************************************")

    print ("*******************************************************************")
    print ("***                      Digital Stamp 2                       ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print (context.response2.json()['digital_stamp'])
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")


    not expect(context.response1.json()['digital_stamp']).to_equal(context.response1.json()['digital_stamp'])

@given('a original string')
def set_original_string(context):
    context.stamp= {
        "original_string": "||3|13122019|23112019|123456|40002|BANORTE|Dan huerta|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert||"
    }
    context.stampString = "||3|13122019|23112019|123456|40002|BANORTE|Dan huerta|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert||"

@when('the digital stamp is generated')
def generate_stamp(context):
    #url = "http://sello-digital-service-blacklist.apps-ilab-ctigtdc01u.nam.nsroot.net/digitalStampGen"
    url = "http://localhost:8080/digitalStampGen"
    context.response = requests.post(url, json=context.stamp)
    print(context.response)

@then('the digital stamp generated lenght is less than 464 characteres')
def compare_lenght_stamp(context):

    print ("****************************************************************************")
    print ("****************************************************************************")
    print ("****************************************************************************")
    print ("****************************************************************************")
    print ("****************************************************************************")
    print ("****************************************************************************")
    print ("***   the digital stamp generated lenght is less than 464 characteres   ****")
    print ("****************************************************************************")

    print ("*******************************************************************")
    print ("***                      Digital Stamp                         ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print (context.response.json()['digital_stamp'])
    print ("*******************************************************************")

    print ("*******************************************************************")
    print ("***                      Digital Stamp lenght                  ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print (len(context.response.json()['digital_stamp']))
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")



    expect(len(context.response.json()['digital_stamp'])).to_be_less_than(464)

@then('the digital stamp encryted and decrypted are the same')
def compare_encryption(context):
    original=hashlib.sha3_256(context.stampString.encode('ascii')).hexdigest()
    print(original)

    private_key = RSA.import_key(open("private.pem").read())
    dsize = SHA.digest_size
    cipher = PKCS1_v1_5.new(private_key)
    sentinel = Random.new().read(15+dsize)  

    base64ToNormal = base64.b64decode(context.response.json()['digital_stamp'])

    response = cipher.decrypt(base64ToNormal,sentinel).decode()

    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("***   the digital stamp encryted and decrypted are the same     ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print ("***                 Digital Encrypted Input                    ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print (original)
    print ("*******************************************************************")

    print ("*******************************************************************")
    print ("***                 Digital Stamp encrypted                    ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print (context.response.json()['digital_stamp'])
    print ("*******************************************************************")
    
    print ("*******************************************************************")
    print ("***                 Digital Stamp decrypted                    ****")
    print ("*******************************************************************")

    print ("*******************************************************************")
    print (response)
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    print ("*******************************************************************")
    
    expect(response).to_equal(original)

