Feature: MMD-1691 Validación de RFC o CURP del Ordenante para Transacciones Entrantes

    # Scenario: digital stamp working
    #     Given a digital stamp
    #     When the digital stamp is generated
    #     Then the digital stamp ins different to the last one

    Scenario: digital stamp working
        Given two different orignial strings
        When the digital stamp is generated for the two original strings
        Then the two digital stamps are different from each other

    Scenario: max length digital stamp
        Given a original string
        When the digital stamp is generated
        Then the digital stamp generated lenght is less than 464 characteres

    Scenario: encryted vs decrypted
        Given a original string
        When the digital stamp is generated
        Then the digital stamp encryted and decrypted are the same

        

    # Scenario: Valid RFC
    #     Given a transaction
    #         and the DATA is CUPU800825569
    #     When the service is called
    #     Then the message is RFC válido
    #         and status code is 200

    # Scenario: Valid CURP
    #     Given a transaction
    #         and the DATA is BADD110313HCMLNS09
    #     When the service is called
    #     Then the message is CURP válido
    #         and status code is 200

    # Scenario: ND field
    #     Given a transaction
    #         and the DATA is ND
    #     When the service is called
    #     Then the message is Dato no presente
    #         and status code is 200

    # Scenario: Invalid DATA
    #     Given a transaction
    #         and the DATA is 002073565115879463
    #     When the service is called
    #     Then status code is 400
    #         and the message is Dato Inválido

    # Scenario Outline: Valid Data serveral range [<data>]
    #     Given a transaction
    #         and the DATA is <data>
    #     When the service is called
    #     Then the message is <message>

    # Examples:
    #         | data               | message          |
    #         | CUPU800825569      | RFC válido       |
    #         | BADD110313HCMLNS09 | CURP válido      |
    #         | 002073560300817413 | Dato Inválido    |
    #         | ND                 | Dato no presente |