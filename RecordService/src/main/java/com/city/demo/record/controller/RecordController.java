package com.city.demo.record.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class RecordController {
	@RequestMapping(value="/v1/records", method = RequestMethod.GET)
	public String sendRecordsForQuery(@RequestParam String id, @RequestParam Integer monto, @RequestParam Integer aliasNumeroDeCelular,
			                                         @RequestParam Integer digitoVerificador1, @RequestParam Integer aliasDelNumeroDeCelular, @RequestParam Integer digitoVerificador2) {
		Integer identificadorDeTipoDeAviso = 10;
		Integer identificadorDeSubtipoDeAviso = 20;
		String estampaDeTiempoDeFinDeProcesamiento = "Estampa De Tiempo De Fin De Procesamiento";
		String claveDeRastreo = "Clave De Rastreo";
		Integer banco = 30;
		Integer tipoDeCuenta = 40;
		Integer cuenta = 50;
		String nombre = "666";
		
		
		System.out.println("Service Execution Start....");
		
		System.out.println("Parameters Recieved are " + id + " " + monto + " " + aliasNumeroDeCelular + " " + digitoVerificador1 +" " + aliasDelNumeroDeCelular + " " + digitoVerificador2);
		System.out.println("Processing your Request....");
		
		System.out.println("Service Execution End....");	
		
		return "Id : "  + id + ", Monto : " + monto + ", Alias numero de celular " + aliasNumeroDeCelular + ", Digito verificador : " + digitoVerificador1 +", Alias del numero de celular : " + aliasDelNumeroDeCelular + ", Digito verificador " + digitoVerificador2
				                           + " , Identificador de Tipo de Aviso " + identificadorDeTipoDeAviso +  ", Identificador de subtipo de Aviso " + identificadorDeSubtipoDeAviso + ", Estampa de tiempo de fin de procesamiento " + estampaDeTiempoDeFinDeProcesamiento + ", Clave de rastreo " +  claveDeRastreo 
				                           + ", Banco " + banco +  ", Tipo de cuenta " + tipoDeCuenta + ", Cuenta " + cuenta + ", Nombre " + nombre ;	
		
	}
}