package com.city.demo.service.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Users {
	
	@Id
	private Integer id;
	private String otherInfo1;
	private String otherInfo2;
	private String otherInfo3;
	private String otherInfo4;
	private String otherInfo5;
	private String otherInfo6;
	private String otherInfo7;
	private String otherInfo8;
	
	public Users (Integer id, String otherInfo1, String otherInfo2, String otherInfo3, String otherInfo4, String otherInfo5, String otherInfo6, String otherInfo7, String otherInfo8) {
		this.id=id;
		this.otherInfo1=otherInfo1;
		this.otherInfo2=otherInfo2;
		this.otherInfo3=otherInfo3;
		this.otherInfo4=otherInfo4;
		this.otherInfo5=otherInfo5;
		this.otherInfo6=otherInfo6;
		this.otherInfo7=otherInfo7;
		this.otherInfo8=otherInfo8;		
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getOtherInfo1() {
		return otherInfo1;
	}
	
	public void setOtherInfo1(String otherInfo1) {
		this.otherInfo1 = otherInfo1;
	}
	
	public String getOtherInfo2() {
		return otherInfo2;
	}
	
	public void setOtherInfo2(String otherInfo2) {
		this.otherInfo2 = otherInfo2;
	}
	
	public String getOtherInfo3() {
		return otherInfo3;
	}
	
	public void setOtherInfo3(String otherInfo3) {
		this.otherInfo3 = otherInfo3;
	}
	
	public String getOtherInfo4() {
		return otherInfo4;
	}
	
	public void setOtherInfo4(String otherInfo4) {
		this.otherInfo4 = otherInfo4;
	}
	
	public String getOtherInfo5() {
		return otherInfo5;
	}
	
	public void setOtherInfo5(String otherInfo5) {
		this.otherInfo5 = otherInfo5;
	}
	
	public String getOtherInfo6() {
		return otherInfo6;
	}
	
	public void setOtherInfo6(String otherInfo6) {
		this.otherInfo6 = otherInfo6;
	}
	
	public String getOtherInfo7() {
		return otherInfo7;
	}
	
	public void setOtherInfo7(String otherInfo7) {
		this.otherInfo7 = otherInfo7;
	}
	
	public String getOtherInfo8() {
		return otherInfo8;
	}
	
	public void setOtherInfo8(String otherInfo8) {
		this.otherInfo8 = otherInfo8;
	}	
	
}