package com.city.demo.service.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.city.demo.service.document.Users;

public interface UsersRepository extends MongoRepository<Users, Integer> {
	
}