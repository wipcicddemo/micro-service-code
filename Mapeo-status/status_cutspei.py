import random
from pymongo import MongoClient

rdmcode = random.choice(["LT","LE"])
rdmstat = random.choice(["Aplicada"])

def getStatus(status):
    client = MongoClient('mongodb://localhost:27017/')
    db = client.Status
    col = db.CUT_SPEI_STATUS
    code = rdmcode
    SEARCH_ARRAY = (col.find({"code":code},{"_id":0,"status":1}))
    estados = []
    for DICT_ELEM in SEARCH_ARRAY:
        status = (DICT_ELEM['status'])
        estados.append(status)
    return(estados) 

def getCode(code):
    client = MongoClient('mongodb://localhost:27017/')
    db = client.Status
    col = db.CUT_SPEI_STATUS
    status = rdmstat
    SEARCH_ARRAY = (col.find({"status":status},{"_id":0,"code":1}))
    codigos = []
    for DICT_ELEM in SEARCH_ARRAY:
        code = (DICT_ELEM['code'])
        codigos.append(code)
    return (codigos)
    
try:
    print("Estatus interno ingresado","---->",rdmcode)
    print("Estatus externo obtenido:","---->",getStatus("No existe el elemento"))
    print("Codigo o Codigos obtenidos:","-->",getCode("No existe el elemento"))

    
except Exception as ex:
    print('Error:  -----', ex )