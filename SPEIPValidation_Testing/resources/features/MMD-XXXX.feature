Feature: MMD-2463 CDA Validation 

    Scenario: Insert new CDA 
        Given a dictionary
        """
        {
            "Clave de Rastreo": "31",
            "Tipo de pago": "12",
            "Fecha de operación del SPEI": "13122019",
            "Fecha calendario del abono": "22112019",
            "Hora calendario del abono": "123456",
            "Clave SPEI del Participante Emisor de la CDA": 40002,
            "Nombre del Participante Emisor de la Orden de Transferencia": "BANORTE",
            "Nombre Ordenante": "Juan Carlos Cocoletzi",
            "Tipo de Cuenta Ordenante": 40,
            "Cuenta Ordenante": "400000400000400000",
            "RFC o CURP Ordenante": "MOPS870925AI1",
            "Tipo de Cuenta Beneficiario": 40,
            "Concepto del Pago": "Concepto del Pago",
            "Importe del IVA": 100.09,
            "Monto del pago": 100.09,
            "Tipo de Cuenta Beneficiario 2": 40,
            "Folio del Esquema Cobro Digital": "Esquema",
            "Pago de la comisión por la transferencia": "2",
            "Monto de la comisión por la transferencia": "9999999999",
            "Alias del número celular del comprador": "321",
            "Digito verificador del comprador": "123",
            "Nombre del participante receptor": "Citi Banamex",
            "Nombre del beneficiario": "FRANCISCO MORALES GARCIA",
            "Cuenta del beneficiario ": "012180001135124014",
            "RFC/CURP del beneficiario": "AYSA871101YH1",
            "Nombre del beneficiario 2 ": "3rd NombreBenefic2",
            "RFC/CURP del beneficiario 2": "3rd  rFCOCurpBene2",
            "Cuenta del beneficiario 2": 987654,
            "Alias Del Numero Celular Del Vendedor": "987",
            "Digito Verificador Del Vendedor": "561",
            "Numero De Serie Del Certificado": "1271238askdjfhkdeert"
        }
        """
        When the service is called
        Then the status code is 200

        Scenario: Duplicated CDA 
        Given a dictionary
        """
        {
            "Clave de Rastreo": "501",
            "Tipo de pago": "12",
            "Fecha de operación del SPEI": "13122019",
            "Fecha calendario del abono": "22112019",
            "Hora calendario del abono": "123456",
            "Clave SPEI del Participante Emisor de la CDA": 40002,
            "Nombre del Participante Emisor de la Orden de Transferencia": "BANORTE",
            "Nombre Ordenante": "Juan Carlos Cocoletzi",
            "Tipo de Cuenta Ordenante": 40,
            "Cuenta Ordenante": "400000400000400000",
            "RFC o CURP Ordenante": "MOPS870925AI1",
            "Tipo de Cuenta Beneficiario": 40,
            "Concepto del Pago": "Concepto del Pago",
            "Importe del IVA": 100.09,
            "Monto del pago": 100.09,
            "Tipo de Cuenta Beneficiario 2": 40,
            "Folio del Esquema Cobro Digital": "Esquema",
            "Pago de la comisión por la transferencia": "2",
            "Monto de la comisión por la transferencia": "9999999999",
            "Alias del número celular del comprador": "321",
            "Digito verificador del comprador": "123",
            "Nombre del participante receptor": "Citi Banamex",
            "Nombre del beneficiario": "FRANCISCO MORALES GARCIA",
            "Cuenta del beneficiario ": "012180001135124014",
            "RFC/CURP del beneficiario": "AYSA871101YH1",
            "Nombre del beneficiario 2 ": "3rd NombreBenefic2",
            "RFC/CURP del beneficiario 2": "3rd  rFCOCurpBene2",
            "Cuenta del beneficiario 2": 987654,
            "Alias Del Numero Celular Del Vendedor": "987",
            "Digito Verificador Del Vendedor": "561",
            "Numero De Serie Del Certificado": "1271238askdjfhkdeert"
        }
        """
        When the service is called
        Then the status code is 422
        and the message is El registro ya se encuentra en Base de Datos
    
        Scenario: DigitalStamp Api Is Down
        Given a dictionary
        """
        {
            "Clave de Rastreo": "508",
            "Tipo de pago": "12",
            "Fecha de operación del SPEI": "13122019",
            "Fecha calendario del abono": "22112019",
            "Hora calendario del abono": "123456",
            "Clave SPEI del Participante Emisor de la CDA": 40002,
            "Nombre del Participante Emisor de la Orden de Transferencia": "BANORTE",
            "Nombre Ordenante": "Juan Carlos Cocoletzi",
            "Tipo de Cuenta Ordenante": 40,
            "Cuenta Ordenante": "400000400000400000",
            "RFC o CURP Ordenante": "MOPS870925AI1",
            "Tipo de Cuenta Beneficiario": 40,
            "Concepto del Pago": "Concepto del Pago",
            "Importe del IVA": 100.09,
            "Monto del pago": 100.09,
            "Tipo de Cuenta Beneficiario 2": 40,
            "Folio del Esquema Cobro Digital": "Esquema",
            "Pago de la comisión por la transferencia": "2",
            "Monto de la comisión por la transferencia": "9999999999",
            "Alias del número celular del comprador": "321",
            "Digito verificador del comprador": "123",
            "Nombre del participante receptor": "Citi Banamex",
            "Nombre del beneficiario": "FRANCISCO MORALES GARCIA",
            "Cuenta del beneficiario ": "012180001135124014",
            "RFC/CURP del beneficiario": "AYSA871101YH1",
            "Nombre del beneficiario 2 ": "3rd NombreBenefic2",
            "RFC/CURP del beneficiario 2": "3rd  rFCOCurpBene2",
            "Cuenta del beneficiario 2": 987654,
            "Alias Del Numero Celular Del Vendedor": "987",
            "Digito Verificador Del Vendedor": "561",
            "Numero De Serie Del Certificado": "1271238askdjfhkdeert"
        }
        """
        When the service is called
        Then the status code is 422
        and the message is Hubo problemas al generar sello digital, por lo que no es  posible generar CDA


        Scenario: Compare values
        Given a dictionary
        """
        {
            "Clave de Rastreo": "32",
            "Tipo de pago": "12",
            "Fecha de operación del SPEI": "13122019",
            "Fecha calendario del abono": "22112019",
            "Hora calendario del abono": "123456",
            "Clave SPEI del Participante Emisor de la CDA": 40002,
            "Nombre del Participante Emisor de la Orden de Transferencia": "BANORTE",
            "Nombre Ordenante": "Juan Carlos Cocoletzi",
            "Tipo de Cuenta Ordenante": 40,
            "Cuenta Ordenante": "400000400000400000",
            "RFC o CURP Ordenante": "MOPS870925AI1",
            "Tipo de Cuenta Beneficiario": 40,
            "Concepto del Pago": "Concepto del Pago",
            "Importe del IVA": 100.09,
            "Monto del pago": 100.09,
            "Tipo de Cuenta Beneficiario 2": 40,
            "Folio del Esquema Cobro Digital": "Esquema",
            "Pago de la comisión por la transferencia": "2",
            "Monto de la comisión por la transferencia": "9999999999",
            "Alias del número celular del comprador": "321",
            "Digito verificador del comprador": "123",
            "Nombre del participante receptor": "Citi Banamex",
            "Nombre del beneficiario": "FRANCISCO MORALES GARCIA",
            "Cuenta del beneficiario ": "012180001135124014",
            "RFC/CURP del beneficiario": "AYSA871101YH1",
            "Nombre del beneficiario 2 ": "3rd NombreBenefic2",
            "RFC/CURP del beneficiario 2": "3rd  rFCOCurpBene2",
            "Cuenta del beneficiario 2": 987654,
            "Alias Del Numero Celular Del Vendedor": "987",
            "Digito Verificador Del Vendedor": "561",
            "Numero De Serie Del Certificado": "1271238askdjfhkdeert"
        }
        """
        When the service is called
        Then the status code is 200
        and compare digital stamp
