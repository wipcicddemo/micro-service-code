from behave import given, when, then
import requests
import json
from compare import expect


@given(u'a dictionary')
def set_movement(context):
    # print (json.loads(context.text))
    context.dictionary = json.loads(context.text)


@when(u'the service is called')
def call_mock(context):
    request_data = context.dictionary
   
    url = "http://localhost:8081/v1/sepip"
    context.response = requests.post(url, json=request_data)

# @when(u'the service is called')
# def findCDA(context):
#     request_data = context.dictionary
#     print("-----------------------------------------------------------Datos de busqueda-----------------------------------------------------------")
#     print("Clave de Rastreo: ------", request_data['Clave de Rastreo'])
#     print("Fecha de operación del SPEI: ------", request_data['Fecha de operación del SPEI'])
#     strDateTosearch = str(request_data['Fecha de operación del SPEI'])
#     year = strDateTosearch[4] + strDateTosearch[5] + strDateTosearch[6] + strDateTosearch[7]
#     month = strDateTosearch[3] + strDateTosearch[2] 
#     day = strDateTosearch[1] + strDateTosearch[0] 
#     dateTosearch = year + '-' + month + '-' + day

#     print("fecha de busqueda: ------", dateTosearch)
#     url = "http://localhost:8081/v1/sepip"
#     context.response = requests.post(url, json=request_data)



@then(u'the status code is {status_code}')
def assert_status_code(context, status_code):
    print("Estatus: ------", context.response.status_code)
    expect(status_code).to_equal(str(context.response.status_code))

@then(u'the message is {message}')
def assert_transfer_account(context, message):
    print("-----------------------------------------------------------Mensaje 1-----------------------------------------------------------")
    print("RESPUESTA: ------", context.response.json()['message'])
    print("ERROR: ------", context.dictionary)
    print("MENSAJE: ------", message)
    expect(message).to_equal(context.response.json()['message'])


@then(u'compare digital stamp')
def assert_transfer_account(context):
    request_data = context.dictionary
    print("-----------------------------------------------------------Mensaje 2-----------------------------------------------------------")
    print("Clave de Rastreo: ------", request_data['Clave de Rastreo'])
    print("Fecha de operación del SPEI: ------", request_data['Fecha calendario del abono'])
    strDateTosearch = str(request_data['Fecha calendario del abono'])
    year = strDateTosearch[4] + strDateTosearch[5] + strDateTosearch[6] + strDateTosearch[7]
    month = strDateTosearch[2] + strDateTosearch[3] 
    day = strDateTosearch[0] + strDateTosearch[1] 
    dateTosearch = year + '-' + month + '-' + day
    data = {}
    data['clave_de_rastreo'] = request_data['Clave de Rastreo']
    data['fecha_calendario_abono'] = dateTosearch
    request_data = data
   
    url = "http://localhost:8084/search-service"
    res = requests.post(url, json=request_data)
    #CDA de la insercion en BD
    originalRequestDigitalStamp = context.response.json()['digitalStamp']
    #Para forzar la falla se pone un CDA diferente al de BD
    #originalRequestDigitalStamp = "ex410CPnon1OPPcRrDF4EJjRiH3D1j8v7a7ZYfYzsJLC0TB1dokZqBDmARsISXOt+PekqcqGFAEKMcf+8JZtdtbm7EzAl14+PVdUINeVmSV9xUfZIq1GVJ95M9uIWi/2ZB+oAYJWfe2NAWm++AXmmKkXaziAGCRgs+CayilBZEw="
    
    dbCDADigitalStamp= res.json()[0]['selloDigital']

    print("Sello Digital obtenido de servicio de busqueda : ---> ", dbCDADigitalStamp )
    print("Sello Digital que genera el servicio de creacion : ---> ", originalRequestDigitalStamp)

    expect(dbCDADigitalStamp).to_equal(originalRequestDigitalStamp)
    #expect(dbCDADigitalStamp[0]['selloDigital']).to_equal("ex410CPnon1OPPcRrDF4EJjRiH3D1j8v7a7ZYfYzsJLC0TB1dokZqBDmARsISXOt+PekqcqGFAEKMcf+8JZtdtbm7EzAl14+PVdUINeVmSV9xUfZIq1GVJ95M9uIWi/2ZB+oAYJWfe2NAWm++AXmmKkXaziAGCRgs+CayilBZEw=")





