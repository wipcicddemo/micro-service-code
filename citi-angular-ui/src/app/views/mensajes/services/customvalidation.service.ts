import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl } from '@angular/forms';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';



@Injectable({
  providedIn: 'root'
})
export class CustomvalidationService {

  constructor() { }

  patternValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp('^[0-9]+$');
      const valid = regex.test(control.value);
      //if doesn´t exist invalid characters return true 
      return valid ? null : { invalidAccount: true };
    };
  }

  moneyValidator():ValidatorFn{
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp(/^(?!0\.00)[1-9]\d{0,2}(,\d{3})*(\.\d\d)?$/gm);
      const valid = regex.test(control.value);
      debugger;
      //if doesn´t exist invalid characters return true 
      return valid ? null : { invalidMoney: true };
    };
  }

  timeValidator():ValidatorFn{
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp(/^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/gm);
      const valid = regex.test(control.value);
      //if doesn´t exist invalid characters return true 
      return valid ? null : { invalidTime: true };
    };
  }

  dateValidator():ValidatorFn{
    let month = "", day ="";
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp(/^(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/gm);
      if (control.value.month < 10 ) 
        month = "0" + control.value.month
      if(control.value.day < 10 )
        day = "0" + control.value.day
debugger;
      const valid = regex.test(control.value.year + '-' + month +'-'+ day);
      //if doesn´t exist invalid characters return true 
      return valid ? null : { invalidDate: true };
    };
  }



}
