// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapseModule } from 'ngx-bootstrap/collapse';

// Alert Component
import { AlertModule } from 'ngx-bootstrap/alert';

// Tabs Module
import { TabsModule } from 'ngx-bootstrap/tabs';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { MensajesComponent } from './mensajes.component';
import { MensajesRoutingModule } from './mensajes-rounting.module';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Response, RequestOptions, Headers } from '@angular/http';




//datatable
import { GenericTableModule } from '@angular-generic-table/core';


@NgModule({
  imports: [
    CommonModule,
    MensajesRoutingModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    CollapseModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    GenericTableModule    
  ],
  declarations: [ MensajesComponent
  ]
})
export class MensajesModule { }
