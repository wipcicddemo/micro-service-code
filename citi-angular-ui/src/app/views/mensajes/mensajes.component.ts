import { Component, OnInit, Renderer2 } from '@angular/core';
import { MensajesService } from './mensajes.service';
import { cda } from './cda';
import { cdaRequest } from './cdaRequest';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CustomvalidationService } from './services/customvalidation.service';
import { Observable } from 'rxjs';
import 'rxjs/Rx';


import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


import { GtConfig, GenericTableComponent } from '@angular-generic-table/core';

import { TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Http, HttpModule, Response, RequestOptions, Headers } from '@angular/http';


import * as XLSX from 'xlsx';


@Component({
  templateUrl: 'mensajes.component.html'
})

export class MensajesComponent implements OnInit {

  ColapseDG: any;
  ColapseDO: any;
  ColapseDB1: any;
  ColapseDB2: any;
  isCollapsed1: boolean = true;
  isCollapsed2: boolean = true;
  isCollapsed3: boolean = true;
  isCollapsed4: boolean = true;
  amountSwitch: boolean = false;
  errorMessage: boolean = true;
  errorMessageCat: string;
  errorFromBody: string;
  templateModel: any;

  isCollapsedMain: boolean = true;

  cda: cda[] = [];
  headers: any;
  trackingKey: any;
  startDate: NgbDateStruct;
  startDateNat: Date;

  cdaForm: FormGroup;
  submitted = false;
  /*
  
    constructor(
      private menServ: MensajesService, 
      private calendar: NgbCalendar, 
      private customValidator: CustomvalidationService,
      private fb: FormBuilder
      ) {
        this.cdaForm = this.fb.group({
          trackingKey:['', Validators.maxLength(30)]
          // ,orderingAccount:['',Validators.maxLength(20)]
          ,beneficiaryAccount: ['',Validators.compose([Validators.maxLength(20),this.customValidator.patternValidator()]) ]
          ,ammount: ['', Validators.compose([Validators.maxLength(19), this.customValidator.moneyValidator()])]
          ,endingHour: ['',Validators.compose([Validators.maxLength(8),this.customValidator.timeValidator()])]
        });
       }
  
  
      onSubmit() {
        this.submitted = true;
        if (this.cdaForm.valid) {
          alert('Form Submitted succesfully!!!\n Check the values in browser console.');
          console.table(this.cdaForm.value);
        }
      }
  
    ngOnInit(): void {
      let request = {} as cdaRequest;
      request.claveRastreo = "93";
      request.rfcCurp = "AYSA871101YH1";
  
      //this.getCDA();
      //this.searchCDA(request);
    }
  
    get registerFormControl() {
      return this.cdaForm.controls;
    }
  
  */
  cdaScreen: any = {
    paymentType: '',
    cuentaBeneficiario: '',
    orderingName: '',
    rfcOrCurpPayer: ''
  }

  modalRef: BsModalRef;
  items: any[];
  @ViewChild('lgModal') ventana;
  @ViewChild(GenericTableComponent)
  private myTable: GenericTableComponent<any, any>;
  excelArray: any[];
  disableExport: boolean;

  public configObject: GtConfig<any>;

  public data: Array<{
    id: number,
    name: string,
    lucky_number: number
  }> = [];


  collapsed1(event: any): void {
    this.ColapseDG = true;
  }

  expanded1(event: any): void {
    // console.log(event);
    this.ColapseDG = false;
  }

  collapsed2(event: any): void {
    this.ColapseDO = true;
  }

  expanded2(event: any): void {
    // console.log(event);
    this.ColapseDO = false;
  }
  collapsed3(event: any): void {
    this.ColapseDB1 = true;
  }

  expanded3(event: any): void {
    // console.log(event);
    this.ColapseDB1 = false;
  }
  collapsed4(event: any): void {
    this.ColapseDB2 = true;
  }

  expanded4(event: any): void {
    // console.log(event);
    this.ColapseDB2 = false;
  }

  collapsedMain(event: any): void {
  }

  expandedMain(event: any): void {
    // console.log(event);
  }

  changeSwitchAmount(): void {
    this.amountSwitch = !this.amountSwitch;
  }

  /*   getCDA() {
      this.menServ.getCDA().subscribe(data => {
          for (const d of (data as any)) {
            this.cda.push({
              paymentType: d.customerHubModel.paymentType,
              cuentaBeneficiario: d.customerHubModel.cuentaBeneficiario,
              rfcOrCurpPayer: d.customerHubModel.rfcOrCurpPayer,
              orderingName: d.customerHubModel.orderingName
            });
  
            this.cdaScreen.paymentType = d.customerHubModel.paymentType;
            this.cdaScreen.cuentaBeneficiario =  d.customerHubModel.cuentaBeneficiario;
            this.cdaScreen.rfcOrCurpPayer = d.customerHubModel.rfcOrCurpPayer;
            this.cdaScreen.orderingName = d.customerHubModel.orderingName;
          }
          console.log(this.cda);
          console.log(data);
  
        });
    } */

  getCDA() {
    this.menServ.getCDA().subscribe(resp => {
      console.log(resp);
      const keys = resp.headers.keys();
      this.headers = keys.map(key =>
        `${key}: ${resp.headers.get(key)}`);

      for (const data of resp.body) {
        this.cda.push(data);

        this.cdaScreen.ordererAccountType = data.ordererAccountType;
        this.cdaScreen.orderingAccount = data.orderingAccount;
        this.cdaScreen.rfcOrCurpPayer = data.rfcOrCurpPayer;
        this.cdaScreen.orderingName = data.orderingName;

        this.cdaScreen.ordererAccountType = data.ordererAccountType;
        this.cdaScreen.orderingAccount = data.orderingAccount;
        this.cdaScreen.rfcOrCurpPayer = data.rfcOrCurpPayer;
        this.cdaScreen.orderingName = data.orderingName;

        this.cdaScreen.ordererAccountType = data.ordererAccountType;
        this.cdaScreen.cuentaBeneficiario = data.cuentaBeneficiario;
        this.cdaScreen.rFCOCurpBeneficiario = data.rFCOCurpBeneficiario;
        this.cdaScreen.nombreBeneficiario = data.nombreBeneficiario;

        this.cdaScreen.ordererAccountType = data.ordererAccountType;
        this.cdaScreen.cuentaBeneficiario2 = data.cuentaBeneficiario2;
        this.cdaScreen.rFCOCurpBeneficiario2 = data.rFCOCurpBeneficiario2;
        this.cdaScreen.nombreBeneficiario2 = data.nombreBeneficiario2;

        this.cdaScreen.selloDigital = data.selloDigital;
        this.cdaScreen.originalString = data.originalString;
        this.cdaScreen.CDA = data.CDA;
      }
      console.log(this.cda);
      console.log(this.headers);
    });
  }

  getCDAById(id: any) {
    this.menServ.getCDAById(id)
      .subscribe(data => {
        console.log(data);
      });
  }

  searchCDA(request: cdaRequest) {
    this.menServ.searchCDA(request)
      .subscribe(data => {
        console.log("------- " + data);
        ////debugger;


        this.configObject = {
          settings: [
            {
              objectKey: 'checkbox',
              columnOrder: 0
              // sort: 'disable'
            }, {
              objectKey: 'speiOperationDateFormat',
              sort: 'desc',
              columnOrder: 1
            }, {
              objectKey: 'trackingKey',
              sort: 'enable',
              columnOrder: 2
            }, {
              objectKey: 'amountOfPayment',
              sort: 'enable',
              columnOrder: 3
            }, {
              objectKey: 'detalles',
              sort: 'disable',
              columnOrder: 4
            }],
          fields: [{
            name: '',
            objectKey: 'checkbox',
            columnClass: 'text-right',
            columnComponent: {
              type: 'checkbox'
            },
            value: row => this.myTable.isRowSelected(row)
          }, {
            name: 'Fecha',
            objectKey: 'speiOperationDateFormat'
          }, {
            name: 'Clave de Rastreo',
            objectKey: 'trackingKey'
          }, {
            name: 'Monto',
            objectKey: 'amountOfPayment'
          }, {
            name: '',
            columnClass: 'gt-button',
            objectKey: 'detalles',
            value: () => {
              return 'up';
            },
            render: row => {
              return (
                '<button class="btn btn-sm btn-primary ' +
                (row.order === 1 ? 'disabled' : '') +
                '"(click)="lgModal.show()">Detalles</button>'
              );
            },
            click: row => {
              //this.openModal(this.modalRef.content);
              this.ventana.show();

              this.cdaScreen.paymentType = row.paymentType;
              this.cdaScreen.nameOfTransferOrderIssuerParticipant = row.nameOfTransferOrderIssuerParticipant;
              this.cdaScreen.amountOfPayment = row.amountOfPayment;
              this.cdaScreen.trackingKey = row.trackingKey;
              this.cdaScreen.conceptOfPayment = row.conceptOfPayment;

              this.cdaScreen.ordererAccountType = row.ordererAccountType;
              this.cdaScreen.orderingAccount = row.orderingAccount;
              this.cdaScreen.rfcOrCurpPayer = row.rfcOrCurpPayer;
              this.cdaScreen.orderingName = row.orderingName;

              this.cdaScreen.ordererAccountType = row.ordererAccountType;
              this.cdaScreen.cuentaBeneficiario = row.cuentaBeneficiario;
              this.cdaScreen.rFCOCurpBeneficiario = row.rFCOCurpBeneficiario;
              this.cdaScreen.nombreBeneficiario = row.nombreBeneficiario;

              this.cdaScreen.ordererAccountType = row.ordererAccountType;
              this.cdaScreen.cuentaBeneficiario2 = row.cuentaBeneficiario2;
              this.cdaScreen.rFCOCurpBeneficiario2 = row.rFCOCurpBeneficiario2;
              this.cdaScreen.nombreBeneficiario2 = row.nombreBeneficiario2;

              this.cdaScreen.CDA = row.CDA;
              this.cdaScreen.selloDigital = row.selloDigital;
              this.cdaScreen.originalString = row.originalString;
              return null;
            }
          }],
          data: data
        };

        /* this.startDateNat = this.toDateJs(this.startDate);
        console.log(this.startDateNat);
        console.log(this.startDate.day); */

        if (data.error === "" || data.error === null || data.error === undefined) {

          this.isCollapsedMain = false;

          this.cdaScreen.paymentType = data.paymentType;
          this.cdaScreen.nameOfTransferOrderIssuerParticipant = data.nameOfTransferOrderIssuerParticipant;
          this.cdaScreen.amountOfPayment = data.amountOfPayment;
          this.cdaScreen.trackingKey = data.trackingKey;
          this.cdaScreen.conceptOfPayment = data.conceptOfPayment;

          this.cdaScreen.ordererAccountType = data.ordererAccountType;
          this.cdaScreen.orderingAccount = data.orderingAccount;
          this.cdaScreen.rfcOrCurpPayer = data.rfcOrCurpPayer;
          this.cdaScreen.orderingName = data.orderingName;

          this.cdaScreen.ordererAccountType = data.ordererAccountType;
          this.cdaScreen.cuentaBeneficiario = data.cuentaBeneficiario;
          this.cdaScreen.rFCOCurpBeneficiario = data.rFCOCurpBeneficiario;
          this.cdaScreen.nombreBeneficiario = data.nombreBeneficiario;

          this.cdaScreen.ordererAccountType = data.ordererAccountType;
          this.cdaScreen.cuentaBeneficiario2 = data.cuentaBeneficiario2;
          this.cdaScreen.rFCOCurpBeneficiario2 = data.rFCOCurpBeneficiario2;
          this.cdaScreen.nombreBeneficiario2 = data.nombreBeneficiario2;

          this.cdaScreen.CDA = data.CDA;
          this.cdaScreen.selloDigital = data.selloDigital;
          this.cdaScreen.originalString = data.originalString;


        } else {
          //debugger;
          this.errorMessage = false;
          this.errorMessageCat = data.error;
        }




      }, error => {
        //debugger;
        this.errorMessage = false;
        this.errorMessageCat = "No se encontraron resultados";
      });
  }

  searchCDABtn() {
    let request = {} as cdaRequest;

    console.log(this.validate());

    if (this.validate() === null || this.validate() === undefined || this.validate() === "") {
      //request.claveRastreo = this.trackingKey;
      //request.speiOperationDate = this.startDate.year + "-" + this.startDate.month + "-" + this.startDate.day;
      request.fecha_calendario_abono = "2019-11-22"
      console.log(this.startDate.day + "" + this.startDate.month + this.startDate.year);
      console.log(this.trackingKey);

      this.errorMessage = true;
      this.searchCDA(request);
      //request.rfcCurp = "AYSA871101YH1";
    } else {
      this.errorMessage = false;
      this.errorMessageCat = " Campos obligatorios: " + this.validate();
    }


  }

  validate() {
    let validationString = "";
    let validationArr = [];
    /*     if (this.trackingKey === null || this.trackingKey === undefined ||) {
          validationArr.push("Ingrese Clave de Rastreo");
        } */
    if (this.startDate === null || this.startDate === undefined) {
      validationArr.push("Ingrese Fecha Inicial");
    }

    for (let i = 0; i < validationArr.length; i++) {
      validationString = validationString + validationArr[i];
      if (i < validationArr.length - 1) {
        validationString = validationString + ", ";
      }
      console.log("---------" + i);
    }


    return validationString;

  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  public trigger = function ($event) {
    switch ($event.name) {
      case 'gt-row-select':
        console.log($event.value);
        this.excelArray = $event.value.selectedRows;
        if (this.excelArray.length === 0) {
          this.activateExport = !true;
        } else {
          this.activateExport = !false;
        }
        //this.exportAsExcelFile($event.value.selectedRows, "archivooo");
        break;
      case 'gt-row-select-all':
        console.log($event.value);
        this.excelArray = $event.value.selectedRows;
        if (this.excelArray.length === 0) {
          this.activateExport = !true;
        } else {
          this.activateExport = !false;
        }
        break;
      case 'gt-row-deselect':
        console.log($event.value);
        this.excelArray = $event.value.selectedRows;
        if (this.excelArray.length === 0) {
          this.activateExport = !true;
        } else {
          this.activateExport = !false;
        }
        break;
      case 'gt-row-deselect-all':
        console.log($event.value);
        this.excelArray = $event.value.selectedRows;
        if (this.excelArray.length === 0) {
          this.activateExport = !true;
        } else {
          this.activateExport = !false;
        }
        break;
    }
  };

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    XLSX.writeFile(workbook, MensajesComponent.toExportFileName(excelFileName));
  }

  public exportSelectedExcel() {
    this.exportAsExcelFile(this.excelArray, "Hola");
  }

  private startingHourValidator = [Validators.maxLength(8), this.customValidator.timeValidator()];

  constructor(
    private modalService: BsModalService,
    private menServ: MensajesService,
    private calendar: NgbCalendar,
    private customValidator: CustomvalidationService,
    private fb: FormBuilder,
    private http: Http) {

    this.cdaForm = this.fb.group({
      toDate: ['', Validators.compose([Validators.required])]
      , trackingKey: ['', Validators.compose([Validators.maxLength(30), this.customValidator.patternValidator()])]
      , orderingAccount: ['', Validators.compose([Validators.maxLength(20), this.customValidator.patternValidator()])]
      , beneficiaryAccount: ['', Validators.compose([Validators.maxLength(20), this.customValidator.patternValidator()])]
      , ammount: ['', Validators.compose([Validators.maxLength(19), this.customValidator.moneyValidator()])]
      , endingHour: ['', Validators.compose([Validators.maxLength(8), this.customValidator.timeValidator()])]
      , startingHour: ['', Validators.compose(this.startingHourValidator)]
      , rfcCurp: ['', Validators.maxLength(18)]
      , switchAmount: ['']
    });

    //     //debugger;
    //     this.cdaForm.get('switchAmount').valueChanges
    //     .subscribe(value => {//debugger;
    //         this.cdaForm.get('startingHour').setValidators(this.startingHourValidator.concat(this.conditionalRequired(value)))
    //     }
    // );



    this.configObject = {
      settings: [{
        objectKey: 'checkbox'
        // sort: 'disable'
      },{
        objectKey: 'speiOperationDateFormat',
        sort: 'desc',
        columnOrder: 0
      }, {
        objectKey: 'trackingKey',
        sort: 'enable',
        columnOrder: 1
      }, {
        objectKey: 'amountOfPayment',
        sort: 'enable',
        columnOrder: 2
      }, {
        objectKey: 'detalles',
        sort: 'disable',
        columnOrder: 3
      }
      ],
      fields: [{
        name: 'Fecha',
        objectKey: 'speiOperationDateFormat'
      }, {
        name: 'Clave de Rastreo',
        objectKey: 'trackingKey'
      }, {
        name: 'Monto',
        objectKey: 'amountOfPayment'
      }, {
        name: '',
        columnClass: 'gt-button',
        objectKey: 'detalles',
        value: () => {
          return 'up';
        },
        render: row => {
          return (
            '<button class="btn btn-sm btn-primary ' +
            (row.order === 1 ? 'disabled' : '') +
            '">Detalles</button>'
          );
        },
        click: row => {
          return null;
        }
      },
      {
        name: '',
        objectKey: 'checkbox',
        columnClass: 'text-right',
        columnComponent: {
          type: 'checkbox'
        },
        value: row => this.myTable.isRowSelected(row)
      }],
      data: this.data
    };






  }

  conditionalRequired(condition) {
    return condition ? Validators.required : Validators.nullValidator;
  }

  ngOnInit(): void {
    let request = {} as cdaRequest;
    request.claveRastreo = "93";
    request.rfcCurp = "AYSA871101YH1";
    this.disableExport = true;

    //this.getCDA();
    //this.searchCDA(request);
  }


  onSubmit() {
    this.submitted = true;
    if (this.cdaForm.valid) {
      let data = this.dataToSend(this.cdaForm.controls);
      this.validatingFields(data);
      this.isCollapsedMain = true;
      this.searchCDABtn();
      console.table(this.cdaForm.value);
    }
  }


  get registerFormControl() {
    return this.cdaForm.controls;
  }

  dataToSend(data) {
    var obj: any = {};
    obj.fecha_calendario_abono = data.toDate.value;
    obj.fecha_calendario_abono = obj.fecha_calendario_abono.year + '-' + obj.fecha_calendario_abono.month + '-' + obj.fecha_calendario_abono.day;
    if (data.trackingKey.value != "")
      obj.clave_de_rastreo = data.trackingKey.value;
    if (data.orderingAccount.value != "")
      obj.cuenta_ordenante = data.orderingAccount.value;
    if (data.beneficiaryAccount.value != "")
      obj.cuenta_beneficiario = data.beneficiaryAccount.value;
    if (data.ammount.value != "")
      obj.monto_de_pago = data.ammount.value;
    if (data.endingHour.value != ""){
      //debugger;
      let time = this.timeToMilliseconds(data.endingHour.value.split(":"));
      obj.hora_abono_fin = time;
    }
     
    if (data.startingHour.value != ""){
      let time = this.timeToMilliseconds(data.startingHour.value.split(":"));
      obj.hora_abono_inicio = time;
    }

    if (data.rfcCurp.value != "")
      obj.rfc_curp = data.rfcCurp.value;
    console.log(obj);
    //debugger;
    return obj;
    //this.cdaForm.controls.beneficiaryAccount.value

  }

  validatingFields(data) {
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("Content-Type", "application/json");
    console.log("POST--------------------------");
    let url = "http://localhost:8090/fieldValidator";
    this.http.post(url, data, {headers: headers}).subscribe(
      res => {
        if( res.json().Error != undefined ||  res.json().Error != "undefined"){
          console.log("Aqui se ejecuta la busqueda");
          let request = {} as cdaRequest;
          request.clave_de_rastreo = res.json().clave_de_rastreo;
          request.cuenta_beneficiario = res.json().cuenta_beneficiario;
          request.cuenta_ordenante = res.json().cuenta_ordenante;
          request.fecha_calendario_abono = res.json().fecha_calendario_abono;
          request.hora_abono_fin = res.json().hora_abono_fin;
          request.hora_abono_inicio = res.json().hora_abono_inicio;
          request.monto_de_pago = res.json().monto_de_pago;
          request.rfc_curp = res.json().rfc_curp;

          this.searchCDA(request);

        }else{ 
          alert(JSON.stringify(res.json().Errores));
        console.log(res.json());
          }
      },
      error => {
        console.log(error);
      },
      () => {
         console.log("se termino");
      }
      );
  }

  timeToMilliseconds(data: any)
  {
    let result = 0;
    result = (data[0]*3600000) + (data[1]*60000)+ + (data[2]*1000);
    return result;
  }

}
