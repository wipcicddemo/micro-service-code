export interface cdaRequest {

    clave_de_rastreo: string;
    cuenta_beneficiario: string;
    cuenta_ordenante: string;
    fecha_calendario_abono: string;
    hora_abono_fin: number;
    hora_abono_inicio: number;
    monto_de_pago: string;
    rfc_curp: string;
    

    
        claveRastreo: string;
        rfcCurp: string;
    

    trackingKey: string;
    paymentType: string;
    speiOperationDate: string;
    calenderDatePayment: number;
    calenderTimePayment: string;
    cdaParticipientSPEIKey: number;
    nameOfTransferOrderIssuerParticipant: string;
    orderingName: string;
    ordererAccountType: number;
    orderingAccount: string;
    rfcOrCurpPayer: string;
    typeOfBeneficiaryAccount: number;
    conceptOfPayment: string;
    ivaAmount: string;
    amountOfPayment: string;
    BeneficiaryAccountType2: number;
    digitalCollectionSchemeFolio: string;
    paymentOfTheTransferFee: number;
    transferFeeAmount: string;
    buyerCellNumberAliases: string;
    digitBuyersVerifier: string;
    mombreDelParticipanteReceptor: string;
    nombreBeneficiario: string;
    cuentaBeneficiario: string;
    rFCOCurpBeneficiario: string;
    nombreBeneficiario2: string;
    rFCOCurpBeneficiario2: string;
    cuentaBeneficiario2: number;
    aliasDelNumeroCelularDelVendedor: string;
    digitoVerificadorDelVendedor: string;
    numeroDeSerieDelCertificado: string;
    selloDigital: string;
    strSpeiOperationDate: string;
    strCalenderDatePayment: string;
    CDA: string;
    originalString: string;
}