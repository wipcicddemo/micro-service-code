from behave import given, when, then
import requests
from compare import expect

@given(u'a transaction')
def set_transaction(context):
    context.call_data = {
        "data": "002073876415879467"
    }

@given(u'the DATA is {data}')
def set_clabe(context, data):
    context.call_data['data'] = data

@when(u'the service is called')
def call_mock(context):
    request_data = context.call_data
    url = context.config.userdata['base_url']
    context.response = requests.put(url, json=request_data)

@then(u'status code is {status_code}')
def assert_status_code(context, status_code):
    expect(status_code).to_equal(str(context.response.status_code))

@then(u'the message is {message}')
def assert_transfer_account(context, message):
    expect(message).to_equal(context.response.json()['message'])