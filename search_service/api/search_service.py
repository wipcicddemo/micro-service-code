"""
search_service.py
Service that search CDA with the parametrs sended
Author: Daniel Huerta
"""
import re
import logging
import datetime
import os
import yaml
from pymongo import MongoClient
#from sshtunnel import SSHTunnelForwarder
# Local Files

# from query_params_mongo import QueryParams

def handler(event, context):
    """
    AWS Lambda Handler
    """
    logging.log(logging.INFO, context)
    return post(event)

def coolVibes():
    return 'Cool Vibes'        

def post(body, context = None):
    config = get_config()
    logging.log(logging.INFO, "||||||||||Entrada||||||||||")
    logging.log(logging.INFO, body)
    logging.log(logging.INFO, "||||||||||Entrada||||||||||")

    bodySquema ={
        "clave_de_rastreo": "",
        "cuenta_beneficiario": "",
        "cuenta_ordenante": "",
        #"fecha_calendario_abono_fin": "",
        "fecha_calendario_abono": "",
        "hora_abono_fin": 0,
        "hora_abono_inicio": 0,
        "monto_de_pago": "",
        "rfc_curp": ""
    }

    for itera in body:
        bodySquema[itera] = body[itera]

    logging.log(logging.INFO, "########################################################")
    logging.log(logging.INFO, bodySquema)
    logging.log(logging.INFO, "########################################################")

    try:
        datos_params = bodySquema
        #logging.log(logging.INFO, datos_params)
        #query_params = build_query_parameters(datos_params)

        
        client = MongoClient('mongodb://{user}:{password}@{host}:{port}/{database}'.format(
            host=config.get('host'),
            port=config.get('port'),
            user=config.get('user'),
            password=config.get('password'),
            database=config.get('database')
        ))
        print("****Created MongoClient****")

        with client:
            logging.log(logging.INFO, "****Mongo conexion created****")
            return query_mongodb(client, config, datos_params)

    except Exception as exception:
        return {"error": str(exception)}


def query_mongodb(client, config, query_params):
    db = client[config.get('database')]
    collection = db[config.get('collection')]
    consulta = build_query(query_params)
    resultados = []
    for res in collection.find(consulta, {"_id": 0, "_class": 0}):
        resultados.append(res['customerHubModel'])

    #resultados = collection.find(consulta, {"_id": 0, "_class": 0})
    #logging.log(logging.INFO, resultados)

    if resultados:
        return resultados
    else:
        return { 'mensaje': 'No se encontraron registros'}


def build_query(query_params):
    consulta = {}

    #logging.log(logging.INFO, query_params['trackingKey'] )

    if query_params['cuenta_beneficiario']:
        consulta["customerHubModel.cuentaBeneficiario"] = query_params['cuenta_beneficiario']
    if query_params['cuenta_ordenante']:
        consulta["customerHubModel.orderingAccount"] = query_params['cuenta_ordenante']
    if query_params['clave_de_rastreo']:
        consulta["customerHubModel.trackingKey"] = query_params['clave_de_rastreo']
    if query_params['rfc_curp']:
        consulta["$or"] = [{ 'customerHubModel.rfcOrCurpPayer': query_params['rfc_curp']},{'customerHubModel.rFCOCurpBeneficiario': query_params['rfc_curp']},
        {'customerHubModel.rFCOCurpBeneficiario2': query_params['rfc_curp']}]
    # if query_params['fecha_calendario_abono_inicio'] and query_params['fecha_calendario_abono_fin']:
    #     split_startDate = query_params['fecha_calendario_abono_inicio'].split("-")
    #     start_date = datetime.datetime(int(split_startDate[0]), int(split_startDate[1]), int(split_startDate[2]))
    #     #logging.log(logging.INFO, start_date)

    #     split_endDate = query_params['fecha_calendario_abono_fin'].split("-")
    #     end_date = datetime.datetime(int(split_endDate[0]), int(split_endDate[1]), int(split_endDate[2]))
        
    #     consulta["customerHubModel.speiOperationDateFormat"] = {"$gte": start_date, "$lte": end_date}
    #     #logging.log(logging.INFO, consulta)
    if query_params['fecha_calendario_abono']:
        split_startDate = query_params['fecha_calendario_abono'].split("-")
        start_date = datetime.datetime(int(split_startDate[0]), int(split_startDate[1]), int(split_startDate[2]))
        end_date = datetime.datetime(int(split_startDate[0]), int(split_startDate[1]), int(split_startDate[2]),23,59,59)
        consulta["customerHubModel.speiOperationDateFormat"] = {"$gte": start_date, "$lte": end_date}
    if query_params['hora_abono_inicio'] and query_params['hora_abono_fin']:
        consulta["customerHubModel.calenderTimePaymentMS"] = {"$gte": query_params['hora_abono_inicio'], "$lte": query_params['hora_abono_fin']}
    if query_params['monto_de_pago']:
        consulta["customerHubModel.amountOfPayment"] = query_params['monto_de_pago']

    logging.log(logging.INFO, "||||||||||Consulta||||||||||")
    logging.log(logging.INFO, consulta)
    logging.log(logging.INFO, "||||||||||Consulta||||||||||")
    return consulta

def get_config():
    virtual_folder = os.path.dirname(os.path.abspath(__file__))
    config_address = os.path.join(virtual_folder, 'config.yml')
    with open(config_address, 'r') as yml_file:
        config = yaml.load(yml_file, Loader=yaml.FullLoader)
        return config['mongodb']

