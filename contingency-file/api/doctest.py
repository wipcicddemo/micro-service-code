import doctest
import from pymongo import MongoClient
from datetime import date
from datetime import datetime
import time
import json
import sys

with open(r'./api/config.yaml') as file:
    properties_list = yaml.load(file, Loader=yaml.FullLoader)
connectTo =  properties_list['mongoDBConn']
conn = MongoClient( connectTo )
db = conn.paquetes
col = db.cda
mycol = db.contingencia

def post(n, context = None):
    '''
    post(n, context) recibe los datos para el archivo de Contingencia
    >>> post("speiOperationDate":13122019,"trackingKey":"503"}, context = None)
    [{"speiOperationDate":13122019,"trackingKey":"503"}]
    '''
        connectTo =  properties_list['mongoDBConn']
        conn = MongoClient( connectTo )
        db = conn.paquetes
        col = db.cda
        mycol = db.contingencia
        return query_mongodb(conn, db, col, mycol,body)


def query_mongodb(conn, db, col, mycol,n):
    '''
    query_mongodb(conn, db,col, mycol, n)
    >>> query_mongodb(conn, db,col, mycol, "speiOperationDate":13122019,"trackingKey":"503")
    123456712345400020000000000000000000000000000000000000503||02|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||
    '''
    #Constantes
    XXXXX = "4002"
    Z = 0
    x= []
    id= 0
    FILE_SIZE = 0

    #Mock Variables
    RCDA1 = "1234567"
    RCDA1_LEN = len(RCDA1)
    if RCDA1_LEN <= 7:
        RCDA1_COMPLEMENT = 7 - RCDA1_LEN
        RCDA1_FULL = ("0" * RCDA1_COMPLEMENT) + RCDA1
    RCDA2 = "12345"
    RCDA2_LEN = len(RCDA2)
    if RCDA2_LEN <= 5:
        RCDA2_COMPLEMENT = 5 - RCDA2_LEN
        RCDA2_FULL = ("0" * RCDA2_COMPLEMENT) + RCDA2

        INSERT_INTO_FILE = ""
        DATA = json.loads(n)
        for ELEMENTS in DATA:
            #Variables
            today = date.today()
            now = datetime.now()
            DDMMAAAA = today.strftime("%d%m%Y")
            DD_MM_AAAA = today.strftime("%d/%m/%Y")
            HHMMSSmmm = now.strftime("%H%M%S%f")[:-3]
            HHMMSS = now.strftime("%H:%M:%S")
            trackingKey = ELEMENTS['trackingKey']
            speiOperationDate = ELEMENTS['speiOperationDate']
            id +=1
            SEARCH_ARRAY = (col.find_one({"customerHubModel.trackingKey":trackingKey,
                                          "customerHubModel.speiOperationDate":speiOperationDate},
                                         {"_id":0,
                                          "customerHubModel.CDA":1,
                                          "customerHubModel.cdaParticipientSPEIKey":1,
                                          "customerHubModel.trackingKey":1}))
            col.update({"customerHubModel.trackingKey":trackingKey,
                        "customerHubModel.speiOperationDate":speiOperationDate},
                        {"$set" : {"customerHubModel.CDAStatus" : "Contingencia","customerHubModel.HoraStatus": HHMMSS, "customerHubModel.FechaStatus": DD_MM_AAAA}},True,True)
            #print("<><><><><><><>",SEARCH_ARRAY)
            for DICT_ELEM in SEARCH_ARRAY:
                #print(DICT_ELEM)
                DICT_VAR = (SEARCH_ARRAY[DICT_ELEM])
                print(DICT_VAR)
                CDA = DICT_VAR["CDA"]
                CDA_LEN = len(CDA)
                CDA_FULL = ""
                if CDA_LEN <= 1832:
                    CDA_COMPLEMENT = 1832 - CDA_LEN
                    CDA_FULL = str(CDA) + (" " * CDA_COMPLEMENT)

                cdaParticipientSPEIKey = int(DICT_VAR["cdaParticipientSPEIKey"])
                cdaParticipientSPEIKey_LEN = len(str(cdaParticipientSPEIKey))
                cdaParticipientSPEIKey_FULL = ""
                if cdaParticipientSPEIKey_LEN <= 5:
                    cdaParticipientSPEIKey_COMPLEMENT = 5 - cdaParticipientSPEIKey_LEN
                    cdaParticipientSPEIKey_FULL = ("0" * cdaParticipientSPEIKey_COMPLEMENT) + str(int(cdaParticipientSPEIKey))
                trackingKey= DICT_VAR["trackingKey"]
                trackingKey_LEN = len(str(trackingKey))
                trackingKey_FULL = ""
                if trackingKey_LEN <= 40:
                    trackingKey_COMPLEMENT = 40 - trackingKey_LEN
                    trackingKey_FULL = ("0" * trackingKey_COMPLEMENT) + str(trackingKey)

                cdaToMongo = CDA
                x.append(cdaToMongo)
                print(cdaParticipientSPEIKey_FULL)
                INSERT_ = RCDA1_FULL + RCDA2_FULL + cdaParticipientSPEIKey_FULL + trackingKey_FULL + (CDA_FULL + " " * 2)
                INSERT_INTO_FILE += INSERT_
                FILE_SIZE = sys.getsizeof(INSERT_INTO_FILE)
                prueba = ""

                Z = int((INSERT_INTO_FILE.count("||"))/2)
                ZZ_len = len(str(Z))
                if ZZ_len <= 4:
                    ZZZ = 4 - ZZ_len
                    ZZZZ = ("0" * ZZZ) + str(Z)
                XXXXX = cdaParticipientSPEIKey_FULL
                file_name = DDMMAAAA + HHMMSSmmm + XXXXX + ZZZZ
            print("Tamano del archivo",FILE_SIZE)
            #print(len(resultado))
            if FILE_SIZE >= 7990000:
                prueba = INSERT_INTO_FILE
                print("Nombre del archivo:",file_name)
                f= open('/data/'+ file_name+".acda","a", encoding="utf-8")
                f.writelines(prueba)
                print("ELEMETOS INSERTADOS EN EL ARCHIVO:",INSERT_INTO_FILE)
                INSERT_INTO_FILE=""
                Z = 0
        else:
            print("No se alcanzo el tamaño")
            prueba = INSERT_INTO_FILE
            print("Nombre del archivo:",file_name)
            f= open('/data/'+ file_name+".acda","a", encoding="utf-8")
            f.writelines(prueba)
            print("ELEMETOS INSERTADOS EN EL ARCHIVO:",INSERT_INTO_FILE)
            Z = 0

        mongoUpdate( prueba, file_name, DD_MM_AAAA, x, conn, db , mycol)
        return {"contingencyFile": prueba+"/"}


def mongoUpdate( prueba, file_name, DD_MM_AAAA, x, conn, db , mycol):
    '''
    mongoUpdate(prueba, file_name, DD_MM_AAAA, x, conn, db , mycol) recibe los datos para el archivo de Contingencia
    >>> mongoUpdate(04052020155114648400020001, 04052020155114648400020001, 04/05/2020, ||02|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||, conn, db , mycol)
    { fileName": 04052020155114648400020001, 04052020155114648400020001", "fileDate": 04/05/2020, "contingencyFile": ||02|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||, "cdas": ||02|13122019|22112019|123456|40002|BANORTE|Juan CarlosCocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||, "retries": 0 }
    '''
    dataRequest =  {
            "fileName": file_name+ ".acda",
            "fileDate": DD_MM_AAAA,
            "contingencyFile": prueba,
            "cdas": x,
            "retries": 0
            }

    mycol.insert_one(dataRequest)

    return dataRequest





if __name__ == '__main__':
    import doctest
    doctest.testmod()
