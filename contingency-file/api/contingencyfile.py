'''
contingency-file.py
Create a Contingency file with the name and extension ""DDMMAAAAHHMMSSmmmXXXXXZZZZ.acda""
Author: Alejandro Sagastegui
'''

import re
import logging
import os
from pymongo import MongoClient
from datetime import date
from datetime import datetime
import time
import json
import sys
import os.path
import requests
import yaml


def handler(event, context):
    logging.log(logging.INFO, context)
    return post(event)


def post(body, context = None):

    logging.log(logging.INFO, "||||||||||Entrada||||||||||")
    logging.log(logging.INFO, body)
    logging.log(logging.INFO, "||||||||||Entrada||||||||||")

    try:
        with open(r'./api/config.yaml') as file:
            properties_list = yaml.load(file, Loader=yaml.FullLoader)
        connectTo =  properties_list['mongoDBConn']
        conn = MongoClient( connectTo )
        db = conn.admin
        col = db.sepipDB
        mycol = db.contingencia
        print("****Created MongoClient****")
        return query_mongodb(conn, db, col, mycol,body)

    except Exception as exception:
        return {"error": str(exception)}

def query_mongodb(conn, db, col, mycol,body):

    #Constantes
    XXXXX = "4002"
    Z = 0
    x= []
    id= 0
    FILE_SIZE = 0

    #Mock Variables
    RCDA1 = "1234567"
    RCDA1_LEN = len(RCDA1)
    if RCDA1_LEN <= 7:
        RCDA1_COMPLEMENT = 7 - RCDA1_LEN
        RCDA1_FULL = ("0" * RCDA1_COMPLEMENT) + RCDA1
    RCDA2 = "12345"
    RCDA2_LEN = len(RCDA2)
    if RCDA2_LEN <= 5:
        RCDA2_COMPLEMENT = 5 - RCDA2_LEN
        RCDA2_FULL = ("0" * RCDA2_COMPLEMENT) + RCDA2
    try:
        INSERT_INTO_FILE = ""
        DATA = json.loads(body)
        for ELEMENTS in DATA:
            #Variables
            today = date.today()
            now = datetime.now()
            DDMMAAAA = today.strftime("%d%m%Y")
            DD_MM_AAAA = today.strftime("%d/%m/%Y")
            HHMMSSmmm = now.strftime("%H%M%S%f")[:-3]
            HHMMSS = now.strftime("%H:%M:%S")
            trackingKey = ELEMENTS['trackingKey']
            speiOperationDate = ELEMENTS['speiOperationDate']
            id +=1
            SEARCH_ARRAY = (col.find_one({"customerHubModel.trackingKey":trackingKey,
                                          "customerHubModel.speiOperationDate":speiOperationDate},
                                         {"_id":0,
                                          "customerHubModel.CDA":1,
                                          "customerHubModel.cdaParticipientSPEIKey":1,
                                          "customerHubModel.trackingKey":1}))
            col.update({"customerHubModel.trackingKey":trackingKey,
                        "customerHubModel.speiOperationDate":speiOperationDate},
                        {"$set" : {"customerHubModel.CDAStatus" : "Contingencia","customerHubModel.HoraStatus": HHMMSS, "customerHubModel.FechaStatus": DD_MM_AAAA}},True,True)
            #print("<><><><><><><>",SEARCH_ARRAY)
            for DICT_ELEM in SEARCH_ARRAY:
                #print(DICT_ELEM)
                DICT_VAR = (SEARCH_ARRAY[DICT_ELEM])
                print(DICT_VAR)
                CDA = DICT_VAR["CDA"]
                CDA_LEN = len(CDA)
                CDA_FULL = ""
                if CDA_LEN <= 1832:
                    CDA_COMPLEMENT = 1832 - CDA_LEN
                    CDA_FULL = str(CDA) + (" " * CDA_COMPLEMENT)

                cdaParticipientSPEIKey = int(DICT_VAR["cdaParticipientSPEIKey"])
                cdaParticipientSPEIKey_LEN = len(str(cdaParticipientSPEIKey))
                cdaParticipientSPEIKey_FULL = ""
                if cdaParticipientSPEIKey_LEN <= 5:
                    cdaParticipientSPEIKey_COMPLEMENT = 5 - cdaParticipientSPEIKey_LEN
                    cdaParticipientSPEIKey_FULL = ("0" * cdaParticipientSPEIKey_COMPLEMENT) + str(int(cdaParticipientSPEIKey))
                trackingKey= DICT_VAR["trackingKey"]
                trackingKey_LEN = len(str(trackingKey))
                trackingKey_FULL = ""
                if trackingKey_LEN <= 40:
                    trackingKey_COMPLEMENT = 40 - trackingKey_LEN
                    trackingKey_FULL = ("0" * trackingKey_COMPLEMENT) + str(trackingKey)

                cdaToMongo = CDA
                x.append(cdaToMongo)
                print(cdaParticipientSPEIKey_FULL)
                INSERT_ = RCDA1_FULL + RCDA2_FULL + cdaParticipientSPEIKey_FULL + trackingKey_FULL + (CDA_FULL + " " * 2)
                INSERT_INTO_FILE += INSERT_
                FILE_SIZE = sys.getsizeof(INSERT_INTO_FILE)
                prueba = ""

                Z = int((INSERT_INTO_FILE.count("||"))/2)
                ZZ_len = len(str(Z))
                if ZZ_len <= 4:
                    ZZZ = 4 - ZZ_len
                    ZZZZ = ("0" * ZZZ) + str(Z)
                XXXXX = cdaParticipientSPEIKey_FULL
                file_name = DDMMAAAA + HHMMSSmmm + XXXXX + ZZZZ
            print("Tamano del archivo",FILE_SIZE)
            #print(len(resultado))
            if FILE_SIZE >= 7990000:
                prueba = INSERT_INTO_FILE
                print("Nombre del archivo:",file_name)
                f= open('/app/archivosContingencia/'+ file_name+".acda","a", encoding="utf-8")
                f.writelines(prueba)
                print("ELEMETOS INSERTADOS EN EL ARCHIVO:",INSERT_INTO_FILE)
                INSERT_INTO_FILE=""
                Z = 0
        else:
            print("No se alcanzo el tamaño")
            prueba = INSERT_INTO_FILE
            print("Nombre del archivo:",file_name)
            f= open('/app/archivosContingencia/'+ file_name+".acda","a", encoding="utf-8")
            f.writelines(prueba)
            print("ELEMETOS INSERTADOS EN EL ARCHIVO:",INSERT_INTO_FILE)
            Z = 0

        mongoUpdate( prueba, file_name, DD_MM_AAAA, x, conn, db , mycol)
        return {"contingencyFile": prueba}

    except Exception as ex:
        return {"message": ex}

#Se agrega guardado de archivo de baja a bd en mongo
#Mejia Regalado Hugo Alejandro, TATA Consultancy Services

def mongoUpdate( prueba, file_name, DD_MM_AAAA, x, conn, db , mycol):

    dataRequest =  {
            #"_id": id,
            "fileName": file_name+ ".acda",
            "fileDate": DD_MM_AAAA,
            "contingencyFile": prueba,
            "cdas": x,
            "retries": 0
            }

    mycol.insert_one(dataRequest)

    return dataRequest
