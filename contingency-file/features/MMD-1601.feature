Feature: MMD-1601_Generacion_de_archivos_de_CDA_de_contingencia

    #Scenario: Validate file name
    #    Given a file with the name 09032020213820189600020009.acda 
    #    When the format name must be DDMMAAAAHHMMSSmmmXXXXXZZZZ
    #    """
    #    [
    #        {
    #            DDMMAAAA : 09032020
    #            HHMMSSmmm : 21213820189
    #            XXXXX : 60002
    #            ZZZZ : 0009
    #            extension: .acda 
    #            encryption: utf-8
    #    }
    #    ]
    #    """
    #    Then the file was created succesfully
    #Scenario: Generate file
    #    Given a dictionary
    #    """
    #    [
    #      {
    #          "trackingKey": "1",
    #          "speiOperationDate": 8032020
    #      },
    #      {
    #          "trackingKey": "12",
    #          "speiOperationDate": 9032020
    #      }
    #    ]
    #    """
    #    When the service is called
    #    Then generate file
#
    #Scenario: Generate many file
    #    Given a dictionary for many files
    #    """
    #    [
    #      {
    #          "trackingKey": "1",
    #          "speiOperationDate": 8032020
    #      },
    #      {
    #          "trackingKey": "12",
    #          "speiOperationDate": 9032020
    #      },
    #      {
    #          "trackingKey": "123",
    #          "speiOperationDate": 9032020
    #      },
    #      {
    #          "trackingKey": "1234",
    #          "speiOperationDate": 9032020
    #      },
    #      {
    #          "trackingKey": "123456789",
    #          "speiOperationDate": 10032020
    #      }
    #    ]
    #    """
    #    When the service is called for many files
    #    Then generate many files 

    Scenario Outline: Invalid DATA
        Given a dictionary
        """
        [
          {
              "trackingKey": "",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "47",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "51",
              "speiOperationDate": 13122019
          }
        ]
        """
        When the service is called
        Then the <status_code> error <message> 
    
    Examples:
    | status_code | message                                                                                                                      |
    | 400         | Datos erroneos, se requiere la clave de rastreo de las CDAs a considerar en el archivo de Contingencia                       |
    #| 400         | Datos erroneos, se requiere fecha de operacion de las CDAs a considerar en el archivo de Contingencia                        |
    #| 400         | Datos erroneos, se requiere la clave de rastreo  y fecha de operacion de las CDAs a considerar en el archivo de Contingencia |
