from behave import given, when, then
import requests
import json
import os
from compare import expect
from hamcrest import assert_that, equal_to
from pymongo import MongoClient

@given(u'a file with the name 09032020213820189600020009.acda')
def open_file(context):
    with open("C:/Users/52556/Desktop/micro-service-code/contingency-file/09032020213820189600020009.acda", "r") as archivo:
        for linea in archivo:
            print(linea)
    pass

@when(u'the format name must be DDMMAAAAHHMMSSmmmXXXXXZZZZ')
def call_mock(context):
    file_name = "09032020213820189600020009.acda"
    DDMMAAAA= file_name[0:8]
    HHMMSSmmm= file_name[9:17]
    XXXXX= file_name[17:22]
    ZZZZ= file_name[22:26]
    extension = file_name[26:]
    print("Fecha",DDMMAAAA)
    print("Hora",HHMMSSmmm)
    print("Clave bancaria",XXXXX)
    print("Contador de CDAs introducidos",ZZZZ)
    print("Extension",extension)
    print("Nombre para el archivo:",DDMMAAAA,"+",HHMMSSmmm,"+",XXXXX,"+",ZZZZ,"+",extension)
    print("Nombre del archivo:",DDMMAAAA+HHMMSSmmm+XXXXX+ZZZZ+extension)
    longitud = (len(DDMMAAAA+HHMMSSmmm+XXXXX+ZZZZ))
    print (longitud)
    return("Nombre del archivo:",DDMMAAAA+HHMMSSmmm+XXXXX+ZZZZ+extension)
    assert_that (longitud, equal_to(25))
    
@then(u'the file was created succesfully')
def file_created(context):
    file_name = "09032020213820189600020009.acda"
    DDMMAAAA= file_name[0:8]
    HHMMSSmmm= file_name[9:17]
    XXXXX= file_name[17:22]
    ZZZZ= file_name[22:26]
    extension = file_name[26:]
    print("Fecha",DDMMAAAA)
    print("Hora",HHMMSSmmm)
    print("Clave bancaria",XXXXX)
    print("Contador de CDAs introducidos",ZZZZ)
    print("Extension",extension)
    print("Nombre para el archivo:",DDMMAAAA,"+",HHMMSSmmm,"+",XXXXX,"+",ZZZZ,"+",extension)
    print("Nombre del archivo:",DDMMAAAA+HHMMSSmmm+XXXXX+ZZZZ+extension)
    longitud = (len(DDMMAAAA+HHMMSSmmm+XXXXX+ZZZZ))
    print (longitud)
    return("Nombre del archivo:",DDMMAAAA+HHMMSSmmm+XXXXX+ZZZZ+extension)
    file_ = DDMMAAAA+HHMMSSmmm+XXXXX+ZZZZ+extension
    print(file_)
   #expect(file_name).equal_to(file_)
    assert_that((file_name).equal_to(file_))
@given (u'a dictionary')
def data_loaded(context):
    context.dictionary = json.loads(context.text)

@when(u'the service is called')
def call_mock_service(context):
    request_data = context.dictionary
    url = "http://localhost:8084/contingencyfile"
    context.response = requests.post(url, json=request_data)

@then('generate file')
def assert_status_code(context):
    with open("C:/Users/52556/Desktop/micro-service-code/contingency-file/09032020213820189600020009.acda", "r") as archivo:
        for trescda in archivo:
            print(trescda)
    pass

@given (u'a dictionary for many files')
def data_loaded(context):
    context.dictionary = json.loads(context.text)

@when(u'the service is called for many files')
def call_mock_service(context):
    request_data = context.dictionary
    url = "http://localhost:8084/contingencyfile"
    context.response = requests.post(url, json=request_data)

@then(u'generate many files')
def assert_status_code(context):
    with open("C:/Users/52556/Desktop/micro-service-code/contingency-file/09032020213820189600020009.acda", "r") as archivo:
        for trescda in archivo:
            print(trescda)
    client = MongoClient('mongodb://localhost:27017/')
    db = client.cda
    col = db.CDAs
    request_data = context.dictionary
    SEARCH_ARRAY = (col.find_one({"customerHubModel.trackingKey":"1",
                                          "customerHubModel.speiOperationDate":8032020},
                                         {"_id":0,"customerHubModel.CDA":1,"customerHubModel.CDAStatus" : 1,"customerHubModel.HoraStatus": 1, 
                                          "customerHubModel.FechaStatus": 1}))
    print(SEARCH_ARRAY)

@then(u'the status code is {status_code}')
def assert_status_code(context, status_code):
    print("Estatus: ------", context.response.status_code)
    expect(status_code).to_equal(str(context.response.status_code))