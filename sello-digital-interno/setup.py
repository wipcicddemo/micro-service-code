# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='digitalstampgen',
    version='1.0.0',
    description='This app provides the digital stamp to the CDA chain.  This app is called by SPEIPValidation api',
    author='Ricardo Daniel Huerta Garrido',
    author_email='ricardo.daniel.huertagarrido@citi.com',
    url='https://cedt-gct-bitbucket.nam.nsroot.net/bitbucket/users/RicardoDanielHuertaGarrido/digitalstampgen',
    license=license,
    packages=find_packages('api')
)
