import re
import logging
import datetime
import os
import yaml
import json
import sys
import hashlib
import Cryptodome
from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import AES, PKCS1_v1_5
from Cryptodome.Hash import SHA
from Cryptodome import Random
import base64

def handler(event, context):
    return post(event)

def post(body):
    original_string = body['original_string']
    digest=hashlib.sha3_512(original_string.encode('ascii')).hexdigest()
    private_key = RSA.generate(2048)
    public_key = private_key.publickey()
    message = digest.encode()
    cipher = PKCS1_v1_5.new(public_key)
    ciphertext = cipher.encrypt(message)
    base644 = base64.b64encode(ciphertext)
    logging.log(logging.INFO,(base644))
    return {"digital_stamp": base644.decode()}