#!/bin/ksh

unset SA_CALL_SECRET
# CertOrderId - Certificate Order Id returned when creating a new dynamic certificate or CertOrderId - Ticket created via cmp request
. /opt/secretagent/scripts/SecretAgentClient -i DOWNLOAD_CERT -k ${CERT}
# $SA_CALL_SECRET will be populated with the plain text password after the following commands is successful
. /opt/secretagent/scripts/SecretAgentClient -i DECRYPT_PRIVATE_PASSWORD -k ${CERT}
# replace CertOrderId with the id
# ./opt/secretagent/scripts/SecretAgentClient -i DOWNLOAD_CERTCREATED_VIA_SECRETMGMT -k CertOrderId
# once downloaded it will be in /tmp/cortosis-certs API_C-84adf154-400f-40c9-bd76-533cc83022de_cert.pem                API_C-84adf154-400f-40c9-bd76-533cc83022de_private.key
if [ "$SA_CALL_SUCCESS" = "1" ]; then
  cp -rf /tmp/cortosis-certs /app
  echo secret is $SA_CALL_SECRET
  openssl rsa -in /app/cortosis-certs/${CERT}_private.key -out /app/cortosis-certs/private.key  -passin pass:$SA_CALL_SECRET
  echo Secret retrieved successfully
  gunicorn --certfile /app/cortosis-certs/${CERT}_cert.pem --keyfile /app/cortosis-certs/private.key -b 0.0.0.0:8080 main:app
  #gunicorn --certfile /app/cortosis-certs/API_C-84adf154-400f-40c9-bd76-533cc83022de_cert.pem --keyfile /app/cortosis-certs/API_C-84adf154-400f-40c9-bd76-533cc83022de_private.key -b 0.0.0.0:8080 main:app
else

  echo "ERROR: SecretAgentClient returned $SA_CALL_RET_VAL"

  exit 1

fi