import pytest
from unittest import mock
from unittest.mock import patch
#from main 
import main
import sys

def test_main():
    module = main
    with patch.object(module, "main", return_value=42):
        with patch.object(module, "__name__", "__main__"):
            with patch.object(module.sys,'exit') as mock_exit:
                module.init()
                assert mock_exit.call_args[0][0] == 42
