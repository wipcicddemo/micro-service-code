import re
import logging
import os
import json
import sys
from api.digitalstampgen import post

def test_digital_stamp_generated():
    body = {"original_string": "2"}
    assert (post(body)) != (" ")

def test_len_digital_stamp_generated():
    body = {"original_string": "2"}
    digitalstamp = (post(body)['digital_stamp'])
    assert len(digitalstamp) == 344