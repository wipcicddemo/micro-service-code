import connexion
import logging
import logging.config
import sys
from config.logging_config import setup_logging
from connexion import NoContent
from connexion.resolver import RestyResolver
from connexion.decorators.uri_parsing import OpenAPIURIParser
from waitress import serve
import ssl
from werkzeug import serving    

def main():
    options = {
        'uri_parser_class': OpenAPIURIParser,
        'swagger_ui': True
    }
    setup_logging()
    logger = logging.getLogger(__name__)
    app = connexion.FlaskApp(__name__, specification_dir='openapi/')
    app.add_api('swagger.yaml',
        options=options,
        resolver=RestyResolver('api'))
    application = app.app
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain("cert.pem", "key.key")
    serving.run_simple("127.0.0.1", 8080, application, ssl_context=context)
    logger.info("DIGITAL STAMP GENERATION")

def init():
    if __name__ == '__main__': \
        sys.exit(main())

init()