import os
import logging.config
from logging import Logger
from config.logging_config import setup_logging

def test_setup_logging_fail_1():
    """ Function that runs the unit test of the setup_logging function """
    setup_logging()
    assert isinstance(logging.getLogger(), Logger)
def test_setup_logging_fail_2():
    """ Function that runs the unit test of the setup_logging function """
    os.environ["LOG_CFG_TEST"] = "TEST_LOG"
    setup_logging(default_path='config/logging.yaml', \
                  default_level=logging.INFO, env_key='LOG_CFG_TEST')
    result = isinstance(logging.getLogger(), Logger)
    os.unsetenv("LOG_CFG_TEST")
    assert result
def test_setup_logging():
    """ Function that runs the unit test of the setup_logging function """
    setup_logging(default_path='config/test_error.yaml', \
                  default_level=logging.INFO, env_key='LOG_CFG')
    assert isinstance(logging.getLogger(), Logger)
