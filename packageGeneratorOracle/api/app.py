import random
import time
import sys
import requests
import json
import logging
import os
import yaml
from datetime import datetime,timedelta
#constants
from const import Const
#Variables
from var import Variables
#messages
from i18n.messages import Messages
#Models
from cda_model import CB_SPEICDA,CB_SPEICDAPCKTS,CB_SPEI_CDAPCKTSRL, Base
#Connection
from conn import Session
# BD Alchemy
from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.orm import sessionmaker



def updateExistPackage(updateFolioPackage):
    session = Session()
    for i in updateFolioPackage:
        session.query(CB_SPEICDA)\
            .filter(CB_SPEICDA.PAYMENTTYPE == i)\
            .update({"PAYMENTTYPE": "CDA Empaquetado"})
        session.commit()
    return i


def packageGenerator():
    session = Session()
    const = Const()
    var = Variables()
    msg = Messages()
    start_time = time.time()

    while (True):
        var.end_time = time.time()
        if(var.start==0):
            start_time = time.time()
        array = []
        array = session.query(CB_SPEICDA).filter(CB_SPEICDA.PAYMENTTYPE == '19').all()
        logging.log(logging.INFO,msg.SEARCHING)
        var.counter = 0
        var.packageCounter = len(var.packages)
        while(var.id < len(array)):
            if(var.start==0):
                start_time = time.time()
            for x in array:
                CDA = x.CDA
                TRACKINGKEY = x.TRACKINGKEY
                CDAPARTICIPANTSPEIKEY = x.CDAPARTICIPANTSPEIKEY
                FOLIOCDA = x.FOLIOCDA
                PACKAGEINVOICE = x.PACKAGEINVOICE
                TRANSACTIONINVOICE = x.TRANSACTIONINVOICE
                ELEMENTS = x.PAYMENTTYPE

                var.package.append(CDA)
                var.updateFolioPackage.append(ELEMENTS)
                var.trackingKeyArray.append(TRACKINGKEY)

                var.id+=1
                if (abs(var.end_time-start_time)<const.maximumTime):
                        var.counter+=1
                        packageSize = sys.getsizeof(var.package)
                        var.end_time = time.time()
                        print(var.end_time-start_time)
                        var.start=1

                        if len(var.package)%const.quantityCdas==0 or packageSize >=const.sizeLimit:
                            var.packages.append(var.package)
                            var.arrays.append(var.trackingKeyArray)
                            updateExistPackage(var.updateFolioPackage)
                            var.end_time = time.time()
                            var.start=0
                            var.package = []
                            var.trackingKeyArray = []
                            logging.log(logging.INFO,msg.PACKAGEDATE)
                else:
                    logging.log(logging.INFO,msg.TIME_LIMIT)
                    var.packages.append(var.package)
                    var.arrays.append(var.trackingKeyArray)
                    updateExistPackage(var.updateFolioPackage)
                    var.start=0
                    var.package = []
                    var.trackingKeyArray= []
        else:
            if (abs(var.end_time-start_time)>const.maximumTime and len(var.package)!=0):
                logging.log(logging.INFO,msg.TIMEOUT)
                var.packages.append(var.package)
                var.arrays.append(var.trackingKeyArray)
                updateExistPackage(var.updateFolioPackage)
                var.start=0
                var.package = []
                var.trackingKeyArray= []
        for paq in range(var.packageCounter,len(var.packages)):
            dtNow = datetime.now()
            logging.log(logging.INFO,(var.packages[paq]))
            print(var.packages[paq])
            cdaQuantity = len(var.packages)
            packageWeight = sys.getsizeof(str(var.packages))
            packageCont = ' '.join(map(str, var.packages[paq]))
            arraysCont = ' '.join(map(str, var.arrays[paq]))
            dataRequestOracle = (paq+1,dtNow,dtNow,40002,cdaQuantity,145872,packageWeight,packageCont,dtNow,
                                                2,dtNow,"NA",arraysCont)
            dataRequestOracle = CB_SPEICDAPCKTS(paq+1,dtNow,dtNow,40002,cdaQuantity,145872,packageWeight,packageCont,dtNow,
                                                2,dtNow,"NA",arraysCont)
            session.add(dataRequestOracle)
            session.commit()
            session.close()
            



packageGenerator()
