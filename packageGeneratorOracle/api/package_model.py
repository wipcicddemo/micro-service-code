import os
import sys
import datetime
from conn import Base
from sqlalchemy import Column, ForeignKey, Integer, String, Text, DateTime
from sqlalchemy.dialects import oracle
from sqlalchemy.orm import relationship

#Models
#from cda_intermedia_model import CB_SPEI_CDAPCKTSRLTN, Base



class OracleText(oracle.CLOB):
    def dialect_impl(self, dialect):
        return self

class CB_SPEICDAPCKTS(Base):
    __tablename__ = 'CB_SPEI_CDAPCKTS'
    PACKAGEID = Column( Integer, primary_key = True)
    PACKAGETIME = Column( DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow, primary_key = True)
    DATEOPERATIONSYSTEM = Column( DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow, primary_key = True)
    CDAPARTICIPANTSPEIKEY = Column( Integer)
    CDAQUANTITY = Column( Integer)
    PACKAGEFOLIO = Column( Integer, primary_key = True)
    PACKAGEWEIGHT = Column( Integer)
    PACKAGECONT = Column( Text().with_variant(OracleText(), "oracle"))
    EXPIREAT = Column( DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    RETRIES = Column( Integer)
    DELIVERYDATE = Column( DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    STATUS = Column( String(50))
    ARRAYS = Column( Text().with_variant(OracleText(), "oracle"))
    PACKAGES = relationship('CB_SPEICDA', secondary='CB_SPEI_CDAPCKTSRLTN')

    def __init__(self,
    PACKAGEID,PACKAGETIME,DATEOPERATIONSYSTEM,CDAPARTICIPANTSPEIKEY,CDAQUANTITY,
    PACKAGEFOLIO,PACKAGEWEIGHT,PACKAGECONT,EXPIREAT,RETRIES,DELIVERYDATE,STATUS,ARRAYS):
        self.PACKAGEID = PACKAGEID
        self.PACKAGETIME = PACKAGETIME
        self.DATEOPERATIONSYSTEM = DATEOPERATIONSYSTEM
        self.CDAPARTICIPANTSPEIKEY = CDAPARTICIPANTSPEIKEY
        self.CDAQUANTITY = CDAQUANTITY
        self.PACKAGEFOLIO = PACKAGEFOLIO
        self.PACKAGEWEIGHT = PACKAGEWEIGHT
        self.PACKAGECONT = PACKAGECONT
        self.EXPIREAT = EXPIREAT
        self.RETRIES = RETRIES
        self.DELIVERYDATE = DELIVERYDATE
        self.STATUS = STATUS
        self.ARRAYS = ARRAYS

        super(CB_SPEICDAPCKTS, self).__init__()
