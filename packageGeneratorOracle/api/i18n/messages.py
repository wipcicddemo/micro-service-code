class Messages(object):

    @property
    def TIME_LIMIT(self):
        return "TIME EXPIRED"

    @property
    def SEARCHING(self):
        return "SEARCHING"

    @property
    def TIMEOUT(self):
        return "TIME EXPIRED NOT MORE ELEMENTS FOUND"

    @property
    def WRONG_DATE(self):
        return "ELEMENTS TO JSON"

    @property
    def SEARCH(self):
        return "NEXT SEARCH IN:"

    @property
    def PACKAGEDATE(self):
        return "PACKAGE COMPLETED"

    @property
    def WRONG_DATE(self):
        return "START REQUEST"
