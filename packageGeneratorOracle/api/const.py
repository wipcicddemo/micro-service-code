class Const(object):

    @property
    def maximumTime(self):
        return 35

    @property
    def timeTillSearch(self):
        return 5

    @property
    def quantityCdas(self):
        return 35

    @property
    def sizeLimit(self):
        return 30000
