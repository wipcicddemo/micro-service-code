import os
import sys
import datetime
from conn import Base
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Text, Date
from sqlalchemy.orm import relationship


class CB_SPEI_CDAPCKTSRL(Base):
    __tablename__ = 'CB_SPEI_CDAPCKTSRLTN'
    PQCDARELID = Column( Integer, primary_key = True)
    PACKAGEID = Column( Integer, ForeignKey('TRACKINGKEY'))
    PACKAGEFOLIO = Column( Integer, ForeignKey('PACKAGEFOLIO'))
    PACKAGETIME = Column( DateTime, ForeignKey('PACKAGETIME'))
    TRACKINGKEY = Column( String(20), ForeignKey('TRACKINGKEY'))
    SPEIOPERATIONDATE = Column( Integer, ForeignKey('SPEIOPERATIONDATE'))

    def __init__(self,
    PQCDARELID,PACKAGEID,PACKAGETIME,TRACKINGKEY,SPEIOPERATIONDATE):
        self.PQCDARELID = PQCDARELID
        self.PACKAGEID = PACKAGEID
        self.PACKAGEFOLIO = PACKAGEFOLIO
        self.PACKAGETIME = PACKAGETIME
        self.TRACKINGKEY = TRACKINGKEY
        self.SPEIOPERATIONDATE = SPEIOPERATIONDATE

        super(CB_SPEI_CDAPCKTSRL, self).__init__()
