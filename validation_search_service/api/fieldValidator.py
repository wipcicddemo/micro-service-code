"""
Valida los parametros del modelo cda_fields
"""
import re
import logging
import datetime
import os
import yaml
import json
import sys
#modelo de parametros 
from .cda_fields import CdaFields
#constantes 
from .const import Const
#mensajes 
from .i18n.messages import Messages

from datetime import datetime

def handler(event, context):
    ##logging.log(#logging.INFO, context)
    return post(event)

def post(body):
    try:
        # fieldsValidation = createModel(body )
        fieldsValidation = cdaValidator(body )
        ##logging.log(#logging.INFO,"Validaciones: ***************************************")
        ##logging.log(#logging.INFO,fieldsValidation)
        return fieldsValidation,fieldsValidation['Code']
        # return json.dumps(fieldsValidation.__dict__), 200
    except:
        return "Error desconocido", 400

"""
#Deprecated
def createModel(datos_cda):
    fields = None
    ##logging.log(#logging.INFO, "Validando CDA")
    cdaFields = CdaFields(datos_cda['clave_de_rastreo'],
                      datos_cda['fecha_calendario_abono'],
                      datos_cda['rfc_curp'],
                      datos_cda['cuenta_ordenante'],
                      datos_cda['cuenta_beneficiario'],
                      datos_cda['monto_de_pago'],
                      datos_cda['hora_abono_inicio'],
                      datos_cda['hora_abono_fin'])
    try:
        fields = json.dumps(cdaFields.__dict__)
    except:
        fields = {"Error:": sys.exc_info()[0]}
        ##logging.log(#logging.ERROR,"***************************************")
        ##logging.log(#logging.ERROR,fields)
    return fields

"""

def cdaValidator(datos_cda):
    isTimeSelected = timeFields(datos_cda)
    #logging.log(logging.INFO,isTimeSelected)
    charLength = 0
    emptyFields =0 
    errorMsg = {}
    errors=0
    response = {}
    const = Const()
    msg = Messages()
    isHourSelected = False

    logging.log(logging.ERROR,"Entra a fecha")
    #ToDo
    #Campo Requerido
    try:
        if datos_cda['fecha_calendario_abono'] is None: 
            logging.log(logging.ERROR,"No trae fecha ")
        else:
            logging.log(logging.INFO,"Si trae fecha")
        ##logging.log(#logging.INFO,"Entro fecha_calendario_abono_inicio: ***************************************")
        dateIsValid = dateValidator(datos_cda['fecha_calendario_abono'] )
        # logging.log(logging.INFO,dateIsValid['msg'])
        if dateIsValid['result']:
            fecha_calendario_abono = datos_cda['fecha_calendario_abono']
            response["fecha_calendario_abono"] = fecha_calendario_abono
        else:
            fecha_calendario_abono_inicio = dateIsValid['msg']
            errorMsg["fecha_calendario_abono"] = fecha_calendario_abono_inicio
            # errorMsg += fecha_calendario_abono_inicio
            errors=errors+1
    except:
        fecha_calendario_abono_inicio = msg.EMPTY_FIELDS_TIME
        logging.log(logging.ERROR,"No trae fecha 2 ")
        errorMsg["fecha_calendario_abono"] = msg.EMPTY_FIELDS_TIME
        errors=errors+1





   

    # #Campo Requerido
    # ##logging.log(#logging.INFO,"Entro fecha_calendario_abono_fin: ***************************************")
    # dateIsValid = dateValidator(datos_cda['fecha_calendario_abono_fin'] )
    # ##logging.log(#logging.INFO,dateIsValid)
    # if dateIsValid:
    #     fecha_calendario_abono_fin = datos_cda['fecha_calendario_abono_fin']
    #     response["fecha_calendario_abono_fin"] = fecha_calendario_abono_fin
    # else:
    #     fecha_calendario_abono_fin = "fecha_calendario_abono_fin formato incorrecto \n"
    #     errorMsg["fecha_calendario_abono_fin"] = fecha_calendario_abono_fin
    #     # errorMsg += fecha_calendario_abono_fin
    #     errors=errors+1

    #Campo opcional, es requerido si en el FE se habilita el campo Hora 
    ##logging.log(#logging.INFO,"Entro hora_abono_inicio: ***************************************")
    if isTimeSelected["fields"] == 2:
        if (datos_cda['hora_abono_inicio'] > const.TIME_IN_SECONDS ) or (datos_cda['hora_abono_inicio'] <= 0):
            hora_abono_inicio = msg.TIME__FORMAT_ERROR_INIT
            errorMsg["hora_abono_inicio"] = hora_abono_inicio
            # errorMsg += hora_abono_inicio
            errors=errors+1
        else:
            hora_abono_inicio = datos_cda['hora_abono_inicio']
            response["hora_abono_inicio"] = hora_abono_inicio
        
        if (datos_cda['hora_abono_fin'] > const.TIME_IN_SECONDS ) or (datos_cda['hora_abono_fin'] <= 0):
            hora_abono_fin = msg.TIME__FORMAT_ERROR_END
            errorMsg["hora_abono_fin"] = hora_abono_fin
            # errorMsg += hora_abono_fin
            errors=errors+1
        else:
            hora_abono_fin = datos_cda['hora_abono_fin']
            response["hora_abono_fin"] = hora_abono_fin
        isHourSelected = True    
    else:
        if isTimeSelected["fields"] == 1:
            errorMsg["hora_abono"] = isTimeSelected["error"]
            errors=errors+100
        if isTimeSelected["fields"] == 3:
            errorMsg["Diferencia_horas"] = isTimeSelected["error"]
            errors=errors+100



    # #Campo opcional, es requerido si en el FE se habilita el campo Hora 
    # ##logging.log(#logging.INFO,"Entro hora_abono_fin: ***************************************")
    # if (datos_cda['hora_abono_fin'] > const.TIME_IN_SECONDS ) or (datos_cda['hora_abono_fin'] <= 0):
    #     hora_abono_fin = msg.TIME__FORMAT_ERROR_END
    #     errorMsg["hora_abono_fin"] = hora_abono_fin
    #     # errorMsg += hora_abono_fin
    #     errors=errors+1
    # else:
    #     hora_abono_fin = datos_cda['hora_abono_fin']
    #     response["hora_abono_fin"] = hora_abono_fin


    try:
        ##logging.log(#logging.INFO,"Entro clave_de_rastreo: ***************************************")
        charLength = len(datos_cda['clave_de_rastreo'])
        if charLength > const.TRACKING_KEY_MAX_SIZE or charLength < 0:
            # logging.log(logging.ERROR,"IF clave_de_rastreo: ***************************************")
            clave_de_rastreo = msg.TRACKING_KEY_MAXSIZE_ERROR
            errorMsg["clave_de_rastreo"] = clave_de_rastreo
            # errorMsg  +=  clave_de_rastreo
            errors=errors+1
        else:
            ##logging.log(logging.INFO,"Else clave_de_rastreo: ***************************************")
            clave_de_rastreo = datos_cda['clave_de_rastreo']
            isAValidTK = alphanumericValidator(clave_de_rastreo)
            if isAValidTK == True:
                response["clave_de_rastreo"] = clave_de_rastreo
            else:
                # logging.log(logging.INFO, msg.TRACKING_KEY_ALPHANUMERIC_ERROR)
                clave_de_rastreo = msg.TRACKING_KEY_ALPHANUMERIC_ERROR
                errorMsg["clave_de_rastreo"] = clave_de_rastreo
                # logging.log(logging.ERROR, errorMsg["clave_de_rastreo"])
                errors=errors+1
    except:
        emptyFields = emptyFields + 1 
        ##logging.log(#logging.INFO,"emptyField clave_de_rastreo: ***************************************")


    try:
        ##logging.log(#logging.INFO,"Entro rfc_curp: ***************************************")
        charLength = len(datos_cda['rfc_curp'])
        if charLength > const.RFC_CURP_MAX_SIZE:
            rfc_curp = msg.RFC_CURP_MAXSIZE_ERROR
            errorMsg["rfc_curp"] = rfc_curp
            # errorMsg  +=  rfc_curp
            errors=errors+1
        else:
            rfc_curp = datos_cda['rfc_curp']
            response["rfc_curp"] = rfc_curp
    except:
        emptyFields = emptyFields + 1 
        ##logging.log(#logging.INFO,"emptyField rfc_curp: ***************************************")


    try:
        ##logging.log(#logging.INFO,"Entro cuenta_ordenante: ***************************************")
        isValid = re.match('^ *\d[\d ]*$',  datos_cda['cuenta_ordenante'])
        ##logging.log(#logging.INFO,isValid)
        if isValid is None:
            cuenta_ordenante = msg.ORDER_ACCOUNT
            errorMsg["cuenta_ordenante"] = cuenta_ordenante
            # errorMsg  +=  cuenta_ordenante
            errors=errors+1
        else:
            num = len(datos_cda['cuenta_ordenante']) 
            if num > const.ACCOUNT_MAX_SIZE:
                cuenta_ordenante = msg.ORDER_ACCOUNT_MAX_SIZE
                errorMsg["cuenta_ordenante"] = cuenta_ordenante
                # errorMsg  +=  cuenta_ordenante
                errors=errors+1
            else:
                cuenta_ordenante = datos_cda['cuenta_ordenante']
                response["cuenta_ordenante"] = cuenta_ordenante
            # else:
            #     cuenta_ordenante = "cuenta_ordenante no es un número valido\n"
            #     errorMsg  +=  cuenta_ordenante
            #     errors=errors+1
    except:
        emptyFields = emptyFields + 1 
        ##logging.log(#logging.INFO,"emptyField cuenta_ordenante: ***************************************")
 
    
    try:
        ##logging.log(#logging.INFO,"Entro cuenta_beneficiario: ***************************************")
        isValid = re.match('^ *\d[\d ]*$',  datos_cda['cuenta_beneficiario'])
        if isValid is None:
            cuenta_beneficiario = msg.BENEFICIARY_ACCOUNT
            errorMsg["cuenta_beneficiario"] = cuenta_beneficiario
            # errorMsg  +=  cuenta_beneficiario
            errors=errors+1
        else:
            num = len(datos_cda['cuenta_beneficiario'])
            # if num.isdigit():
            if num > const.ACCOUNT_MAX_SIZE:
                cuenta_beneficiario = msg.BENEFICIARY_ACCOUNT_MAX_SIZE
                errorMsg["cuenta_beneficiario"] = cuenta_beneficiario
                # errorMsg  +=  cuenta_beneficiario
                errors=errors+1
            else:
                cuenta_beneficiario = datos_cda['cuenta_beneficiario']
                response["cuenta_beneficiario"] = cuenta_beneficiario
            # else:
            #     cuenta_ordenante = "cuenta_beneficiario no es un número valido\n"
            #     errorMsg  +=  cuenta_beneficiario
            #     errors=errors+1
    except:
        emptyFields = emptyFields + 1 
        ##logging.log(#logging.INFO,"emptyField cuenta_beneficiario: ***************************************")

    try:
        ##logging.log(#logging.INFO,"Entro monto_de_pago_min: ***************************************")
        ##isValid = re.match('^ *\d[\d ]*$',  datos_cda['monto_de_pago'])
        isValid = re.match(r"^([0-9]*[.])[0-9]+$",  datos_cda['monto_de_pago'])
        if isValid is None:
            monto_de_pago = msg.PAYMENT_AMMOUNT
            errorMsg["monto_de_pago"] = monto_de_pago
            # errorMsg  +=  monto_de_pago
            errors=errors+1
        else:
            num = len(datos_cda['monto_de_pago'])
            if num > const.AMMOUNT_MAX_SIZE:
                monto_de_pago = msg.PAYMENT_AMMOUNT_MAX
                errorMsg["monto_de_pago"] = monto_de_pago
                # errorMsg  +=  monto_de_pago
                errors=errors+1
            else:
                monto_de_pago = datos_cda['monto_de_pago']
                response["monto_de_pago"] = monto_de_pago
    except:
        emptyFields = emptyFields + 1 
        ##logging.log(#logging.INFO,"emptyField monto_de_pago: ***************************************")

    if emptyFields > const.EMPTY_FIELDS or emptyFields >= 100 :
        if isHourSelected == True:
            errorMsg["Campos_Insuficientes"] = msg.EMPTY_FIELDS_TIME
        else:
            errorMsg["Campos_Insuficientes"] = msg.EMPTY_FIELDS
        # errorMsg  += "No hay suficientes campos para realizar la busqueda, se requieren al menos 5"
        errors=errors+1
    if errors>=1:
        ##logging.log(#logging.INFO,"hay errores: ***************************************")
        response["Errores"] = errorMsg
        response["Code"] = 400
    else:
        response["Code"] = 200



        
    # ##logging.log(#logging.INFO,"Validaciones: ***************************************")
    # ##logging.log(#logging.INFO,response)
    return response
"""
Valida los campos de hora, si alguno de los dos viene  entonces se convierten 
en requeridos, de lo contrario no son requeridos 
"""
def timeFields(datos_cda):
    start = 0
    end = 0
    result = 0
    results = {}
    msg=Messages()
    errors=''
    try:
        start = datos_cda["hora_abono_inicio"] 
        result+=1
    except:
        errors = msg.START_HOUR_REQUIRED
    try:
        end = datos_cda["hora_abono_fin"]
        result+=1
    except:
        errors += msg.END_HOUR_REQUIRED
    try:
        timediff = end - start
        logging.log(logging.INFO,timediff)
        if timediff < 0:
            raise Exception(msg.DIFF_FIELDS_TIME)    
        logging.log(logging.INFO,"Paso diferencia de tiempo")
    except:
        result=3
        logging.log(logging.INFO,"NO Paso diferencia de tiempo")
        errors += msg.DIFF_FIELDS_TIME
        
    results['fields'] = result
    results["error"] = errors
    return results

def dateValidator(date):
    const = Const()
    newDate = None
    #3logging.log(logging.INFO,date)
    result = False
    msg = Messages()
    returnMsg = '' 
    results = {}
    try:
        # logging.log(logging.INFO,"Validando dateValidator")
        try:
            newDate = date.split("-")
            # logging.log(logging.INFO,newDate)
            if len(newDate) < 3: 
                returnMsg = msg.INVALID_FORMAT_DATE
                raise Exception(msg.INVALID_FORMAT_DATE)
        except:
            returnMsg = msg.INVALID_FORMAT_DATE
            raise Exception(msg.INVALID_FORMAT_DATE)
        try:
            # logging.log(logging.INFO,"Validacion de fechas")
            year = int(newDate[0])
            month = int(newDate[1])
            days = int(newDate[2])
        except:
            result = False
            returnMsg = msg.INVALID_FORMAT_DATE_CHARACTERS
        ##logging.log(#logging.INFO,newDate[1])
        if (int(newDate[0]) < const.YEAR_MAX) and (int(newDate[0]) > const.YEAR_MIN):
            logging.log(logging.INFO,"AÑO")
            result = True
        else: 
            ##logging.log(#logging.INFO,"AÑO Error")
            returnMsg = msg.WRONG_DATE
            result = False
            raise Exception(msg.WRONG_DATE)

        ##logging.log(#logging.INFO,"Se termino Año")
        if (int(newDate[1]) <= const.MONTH_MAX_ALLOWED) and (int(newDate[1]) >= const.MONTH_MIN_ALLOWED):
            # logging.log(logging.INFO,"Mes")
            result = True
        else: 
            returnMsg = msg.WRONG_DATE
            result = False
            raise Exception(msg.WRONG_DATE)

        if (int(newDate[2]) <= const.DAYS_MAX_ALLOWED ) and (int(newDate[2]) >= const.DAYS_MIN_ALLOWED):
            # logging.log(logging.INFO,"Día Funciono")
            result = True
        else:
            returnMsg = msg.WRONG_DATE
            result = False
            raise Exception(msg.WRONG_DATE)
        try:
                # logging.log(logging.INFO,"datetime(int(newDate[0]), int(newDate[1]), int(newDate[2]))")
                datetime(year, month, days)
        except:
                result = False
                returnMsg = msg.INVALID_FORMAT_DATE
    except:
        # logging.log(logging.INFO,"Error")
        newDate = None
        result = False
        # returnMsg = msg.INVALID_FORMAT_DATE
        # returnMsg = returnMsg
    
    results['result'] = result
    results['msg'] = returnMsg

    logging.log(logging.INFO,results)

    return results

# Validate if is an alphanumeric string or not 
# Params: text to validate 
# Return: True if is a valid string
#         False if is an invalids string 
def alphanumericValidator(text):
    regex = r"^[A-Za-z0-9]+$"
    matches = re.match(regex, text)
    result = False
    if matches:
        result = True
    
    # logging.log(logging.INFO,"Resultado REGEX:")
    # logging.log(logging.INFO,result)

    return result

