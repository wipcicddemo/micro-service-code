
class Messages(object):
     
    
    # Mensajes para campo de fecha 
    # Date fields` messages 
    @property
    def WRONG_DATE(self):
        return "Dato inválido, fecha no valida"
    
    @property
    def INVALID_FORMAT_DATE(self):
        return "Formato de fecha inválido, validar debe ser aaaa-mm-dd"

    @property
    def INVALID_FORMAT_DATE_CHARACTERS(self):
        return "Dato inválido, no se aceptan letras o caracteres especiales"

    @property
    def DATE__FORMAT_ERROR(self):
        return "Dato inválido, fecha no valida"
    
    @property
    def TIME__FORMAT_ERROR_INIT(self):
        return "El campo hora_abono_inicio no es una hora valida"
    
    @property
    def TIME__FORMAT_ERROR_END(self):
        return "El campo hora_abono_fin no es una hora valida"
    
    # Mensajes para tracking key
    # TrackingKey field`s Messages 
    @property
    def TRACKING_KEY_MAXSIZE_ERROR(self):
        return "Dato inválido, no puede ser mayor a 30 posiciones"
    
    @property
    def TRACKING_KEY_ALPHANUMERIC_ERROR(self):
        return "Dato inválido, la clave de rastreo no acepta caracteres especiales"
    
    # Mensajes para RFC/CURP
    # RFC/CURP Fields`validator 
    @property
    def RFC_CURP_MAXSIZE_ERROR(self):
        return "Dato inválido, el RFC/CURP no puede ser mayor a 18 posiciones"
    
    # Mensajes para ORDER_ACCOUNT
    # ORDER_ACCOUNT Fields`validator 
    @property
    def ORDER_ACCOUNT(self):
        return "Dato inválido, para la cuenta ordenante no aceptan letras o caracteres especiales"

    @property
    def ORDER_ACCOUNT_MAX_SIZE(self):
        return "Dato inválido, la cuenta ordenante no puede ser mayor a 20 posiciones"

    # Mensajes para BENEFICIARY_ACCOUNT
    # BENEFICIARY_ACCOUNT Fields`validator 
    @property
    def BENEFICIARY_ACCOUNT(self):
        return "Dato inválido, para la cuenta del beneficiario no aceptan letras o caracteres especiales"
   
    @property
    def BENEFICIARY_ACCOUNT_MAX_SIZE(self):
        return "Dato inválido, la cuenta del beneficiario no puede ser mayor a 20 posiciones"

    # Mensajes para PAYMENT_AMMOUNT
    # PAYMENT_AMMOUNT Fields' validator 
    @property
    def PAYMENT_AMMOUNT(self):
        return "Dato inválido, el monto no se aceptan letras o caracteres especiales"

    @property
    def PAYMENT_AMMOUNT_MAX(self):
        return "Dato inválido, el monto del depósito  no puede ser mayor a 19 posiciones. Considerar 12 enteros, un punto decimal y dos decimales o el valor máximo indicado para el campo"
    
    @property
    def START_HOUR_REQUIRED(self):
        return "Datos inválidos, se debe digitar una hora inicial y una hora final"

    @property
    def END_HOUR_REQUIRED(self):
        return "Datos inválidos, se debe digitar una hora inicial y una hora final"
    
    @property
    def DIFF_FIELDS_TIME(self):
        return "Datos inválidos, la hora inicial no puede ser mayor que la hora final"
    
    @property
    def EMPTY_FIELDS(self):
        return "Consulta incorrecta, se debe tener al menos dos criterios de búsqueda la fecha y otro criterio"
    
    @property
    def EMPTY_FIELDS_TIME(self):
        return "Consulta incorrecta. si se desea consultar por hora se debe tener al menos dos criterios de búsqueda la fecha y otro criterio"

    @property
    def REQUIRED_DATE(self):
        return "Criterio de búsqueda inválido, el campo de fecha es obligatorio para cualquier consulta"