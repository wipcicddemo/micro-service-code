# -*- coding: utf-8 -*-

import logging
import operator

class CdaFields(object):
    def __init__(self, 
                clave_de_rastreo, 
                fecha_calendario_abono_inicio, 
                fecha_calendario_abono_fin,
                rfc_curp, 
                cuenta_ordenante,
                cuenta_beneficiario,
                monto_de_pago_min,
                monto_de_pago_max,
                hora_abono_inicio,
                hora_abono_fin):
        self.clave_de_rastreo = clave_de_rastreo
        self.fecha_calendario_abono_inicio = fecha_calendario_abono_inicio
        self.fecha_calendario_abono_fin = fecha_calendario_abono_fin
        self.rfc_curp = rfc_curp
        self.cuenta_ordenante = cuenta_ordenante
        self.cuenta_beneficiario = cuenta_beneficiario
        self.monto_de_pago_min = monto_de_pago_min
        self.monto_de_pago_max  = monto_de_pago_max
        self.hora_abono_inicio = hora_abono_inicio
        self.hora_abono_fin = hora_abono_fin

    @property
    def clave_de_rastreo(self):
        return self._clave_de_rastreo
    
    @clave_de_rastreo.setter
    def clave_de_rastreo(self, clave):
        if not clave:
            raise Exception("|| Clave de rastreo no puede estar vacío ||")
        self._clave_de_rastreo = clave
    
    @property
    def fecha_calendario_abono_inicio(self):
        return self._fecha_calendario_abono_inicio

    @fecha_calendario_abono_inicio.setter
    def fecha_calendario_abono_inicio(self, fecha):
        if not fecha:
            raise Exception("|Fecha de inicio es requrida|")
        self._fecha_calendario_abono_inicio = fecha
    
    @property
    def fecha_calendario_abono_fin(self):
        return self._fecha_calendario_abono_fin
    
    @fecha_calendario_abono_fin.setter
    def fecha_calendario_abono_fin(self, fechaFin):
        if not fechaFin:
            raise Exception("|Fecha de fin es requrida|")
        self._fecha_calendario_abono_fin = fechaFin
    
    @property
    def rfc_curp(self):
        return self._rfc_curp
    
    @rfc_curp.setter
    def rfc_curp(self, rfc_curp):
        if not rfc_curp:
            raise Exception("|rfc_curp es requrida|")
        self._rfc_curp = rfc_curp
    
    
    @property
    def cuenta_ordenante(self):
        return self._cuenta_ordenante
    
    @cuenta_ordenante.setter
    def cuenta_ordenante(self, cuenta_ordenante):
        if cuenta_ordenante is None:
            raise Exception("|cuenta_ordenante es requrida|")
        self._cuenta_ordenante = cuenta_ordenante
    

    @property
    def cuenta_beneficiario(self):
        return self._cuenta_beneficiario

    @cuenta_beneficiario.setter
    def cuenta_beneficiario(self, cuenta_beneficiario):
        if not cuenta_beneficiario:
            raise Exception("|cuenta_beneficiario es requrida|")
        self._cuenta_beneficiario = cuenta_beneficiario
    

    @property
    def monto_de_pago_min(self):
        return self._monto_de_pago_min

    @monto_de_pago_min.setter
    def monto_de_pago_min(self, monto_de_pago_min):
        if not monto_de_pago_min:
            raise Exception("|monto_de_pago_min es requrida|")
        self._monto_de_pago_min = monto_de_pago_min
    
    @property
    def monto_de_pago_max(self):
        return self._monto_de_pago_max
    
    @monto_de_pago_max.setter
    def monto_de_pago_max(self, monto_de_pago_max):
        if not monto_de_pago_max:
            raise Exception("|monto_de_pago_max es requrida|")
        self._monto_de_pago_max = monto_de_pago_max
    

    @property
    def hora_abono_inicio(self):
        return self._hora_abono_inicio

    @hora_abono_inicio.setter
    def hora_abono_inicio(self, hora_abono_inicio):
        if not hora_abono_inicio:
            raise Exception("|hora_abono_inicio es requrida|")
        self._hora_abono_inicio = hora_abono_inicio
    
    @property
    def hora_abono_fin(self):
        return self._hora_abono_fin

    @hora_abono_fin.setter
    def hora_abono_fin(self, hora_abono_fin):
        if not hora_abono_fin:
            raise Exception("|hora_abono_fin es requrida|")
        else:
            if (hora_abono_fin > 86399) or (hora_abono_fin <= 0)  :
                raise Exception("|hora_abono_fin no cumple los requisitos de forma|")
        self._hora_abono_fin = hora_abono_fin
    
    # def get(self):
    #     return [self.clave_de_rastreo, self.fecha_calendario_abono_inicio,
    #             self.fecha_calendario_abono_fin, self.rfc_curp,
    #             self.cuenta_ordenante, self.cuenta_beneficiario,
    #             self.monto_de_pago_max,
    #             self.monto_de_pago_min,
    #             self.hora_abono_inicio,
    #             self.hora_abono_fin]