#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import connexion
import logging
import doctest

from connexion import NoContent
from connexion.resolver import RestyResolver
from connexion.decorators.uri_parsing import OpenAPIURIParser
from waitress import serve

logging.basicConfig(level=logging.INFO)

def main():
    options = {
        'uri_parser_class': OpenAPIURIParser,
        'swagger_ui': True
    }

    app = connexion.FlaskApp(__name__, specification_dir='openapi/')
    app.add_api('swagger.yaml',
        options=options,
        resolver=RestyResolver('api'))
    application = app.app
    serve(application, host='0.0.0.0', port=8080)

if __name__ == '__main__':
    # doctest.testmod()
    main()