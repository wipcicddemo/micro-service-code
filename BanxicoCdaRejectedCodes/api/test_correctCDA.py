#test_correctCDA

import pytest
import os
from .cdaRejectedCodes import successfulPackage, saveSuccessfulCDA, saveSuccessfulContingencyPackage, splitRow, getContingencyFileCodeInsert, mongoConn

def test_successfulPackage_true(monkeypatch):
    # def mockreturn():
    #     return "Hola"

    # def mockSplitRow():
    #     return "None"


    # monkeypatch.setitem(os.open, 'open', mockreturn)
    # monkeypatch.setitem(splitRow, 'splitRow', mockSplitRow)

    successfulPackageVar = successfulPackage('C:/Users/RH11770/Downloads/archivo_correcto/', '28042020130355925400020002.out')
    assert successfulPackageVar == True

def test_successfulPackage_false(monkeypatch):
    successfulPackageVar = successfulPackage('C:/Users/RH11770/Downloads/archivo_correcto/', '18052020195553537400020001.out')
    assert successfulPackageVar == False


def test_saveSuccessfulCDA(monkeypatch):
    saveSuccessfulCDA('28042020130355925400020002.out')

def test_saveSuccessfulContingencyPackage():
    saveSuccessfulContingencyPackage('28042020130355925400020002.out')
    




