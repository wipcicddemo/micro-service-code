def switchCodes(argument):
    switcher = {
        1: "Al menos una CDA tiene datos incompletos o erróneos",
        2: "Fecha de operación incorrecta",
        3: "Nombre del archivo incorrecto",
        34: "Cantidad de registros mayor al permitido",
        5: "Participante Emisor de la CDA y Participante Emisor de la Orden de Transferencia son iguales.",
        12: "Formato de estampa de tiempo incorrecto",
        37: "Falla en la clave de Participante Emisor de la CDA",
        38: "Falla en la clave del Participante Emisor de la Orden de Transferencia contenida en la CDA",
        43: "No existe la Orden de Transferencia a la cual asignar la CDA",
        44: "CDA mal armada",
        45: "Formato de monto y/o IVA incorrecto",
        46: "Fecha de la CDA no corresponde con la fecha del nombre de archivo",
        47: "Sello digital inválido",
        54: "Registrada previamente",
        55: "Monto incorrecto del CDA",
        56: "Tipo de pago incorrecto ",
        57: "Fecha calendario incorrecta"
    }
    return switcher.get(argument, "Invalid code")