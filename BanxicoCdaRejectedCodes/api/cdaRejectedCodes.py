
import re
import logging
import datetime
import os
import yaml
import json
import sys
import pymongo
import doctest

from flask import request

# from .codes import switchCodes

with open(r'./api/config.yml') as file:
    config_list = yaml.load(file, Loader=yaml.FullLoader)

with open(r'./api/i18n/messages.yaml') as file:
    message_list = yaml.load(file, Loader=yaml.FullLoader)



def handler(event, context):
    
    return post()

def post():
    result = {}
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def post --------- ")
    try:
        isRequestFile(request)
        uploaded_files = request.files['file']
        isRequestFileExtension(uploaded_files.filename.split('.')[1])
        uploaded_files.save(config_list['files']['path'] + uploaded_files.filename)
        if successfulPackage(config_list['files']['path'], uploaded_files.filename):
            saveSuccessfulContingencyPackage(uploaded_files.filename)
            saveSuccessfulCDA(uploaded_files.filename)
        else:
            if verifyIfCorrectedFile(uploaded_files.filename):
                writeReturnedCorrectedFile(config_list['files']['pathRejected'], uploaded_files.filename)
            else:
                readFile(config_list['files']['path'], uploaded_files.filename)
        result['message'] =  message_list['messages']['confirmation'] + ' ' + str(uploaded_files.filename)
        if __name__ == 'api.cdaRejectedCodes':
            print(" --------- Fin def post Correcto --------- ")
        return result
    except Exception as ex:
        result['message'] = str(ex)
        if __name__ == 'api.cdaRejectedCodes':
            print(" --------- Fin def post Error --------- ")
        return result, 400

def mongoConn():
    """
    Genera la conexion a base de datos de mongo
    >>> mongoConn()
    MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=True)
    """   
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def mongoConn --------- ")

    try:
        connectTo = 'mongodb://{user}:{password}@{host}:{port}/{db}'.format(
        user=config_list['mongodb']['user'],
        password=config_list['mongodb']['password'],
        host=config_list['mongodb']['host'],
        port=config_list['mongodb']['port'],
        db=config_list['mongodb']['database']
        )
        conn = pymongo.MongoClient( connectTo )
    except Exception as ex:
        if __name__ == 'api.cdaRejectedCodes':
            print("Error-----> ",ex)
    
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Fin def mongoConn --------- ")
    return conn

def isRequestFile(request):
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def isRequestFile --------- ")
    if 'file' not in request.files:
        if __name__ == 'api.cdaRejectedCodes':
            print(" --------- fin def isRequestFile Error --------- ")
        raise Exception(message_list['messages']['noFile'])
    else:
        if __name__ == 'api.cdaRejectedCodes': 
            print(" --------- fin def isRequestFile Correcto --------- ")
    return True

def isRequestFileExtension(extension):
    """
    Valida la extension del archivo que se envia 
    >>> isRequestFileExtension('out')
    True

    """
    if __name__ == 'api.cdaRejectedCodes': 
        print(" --------- Inicio def isRequestFileExtension --------- ")
    if  extension != config_list['files']['extension']:
        if __name__ == 'api.cdaRejectedCodes': 
            print(" --------- fin def isRequestFileExtension Error --------- ")
        raise Exception(message_list['messages']['wrongExtension'])
    else:
        if __name__ == 'api.cdaRejectedCodes':  
            print(" --------- fin def isRequestFileExtension Correcto --------- ")
    return True 

def readFile(pathFile, fileName):
    """
    Lee el archivo que se subio y guardo en el server 
    >>> pathFile = '/Users/cocoletzi/Downloads/Citi/2978/'
    >>> fileName = '31012020010808009400029310.out'
    >>> readFile(pathFile, fileName)
    """
    pathFile = pathFile+fileName
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def readFile --------- ")
    outFile= open(pathFile, "r" )
    headerFile = outFile.readline().strip()
    header = splitRow(headerFile)
    generalFileStatus = header[0] + header[1]
    fileNameACDA = fileName.replace('.out', '.acda')
    contingencyFile = getContingencyFileCodeInsert(fileNameACDA)
    retries = contingencyFile['retries'] + 1 
    if int(generalFileStatus) == 1 or int(generalFileStatus) == 2 or int(generalFileStatus) == 3 or int(generalFileStatus) == 34 or retries >= 2:
        headerMessageCode = switchCodes(int(generalFileStatus))
        postContingencyFileCodeInsert(generalFileStatus, headerMessageCode, fileNameACDA, retries )
    if int(generalFileStatus) != 1 and  int(generalFileStatus) != 3 and int(generalFileStatus) != 34 or int(generalFileStatus) == 2 or retries >= 2:
        readContentFile(outFile, retries)
        if int(generalFileStatus) != 2 and retries <= 1:
            updateRetries(retries,fileNameACDA)
    outFile.close()
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- fin def readFile Correcto --------- ")

def splitRow(headerFile):
    """
    Divide un renglon dentro de los archivos 
    >>> headerFile = '0193100001'
    >>> splitRow(headerFile)
    ['0', '1', '9', '3', '1', '0', '0', '0', '0', '1']
    """
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def splitRow --------- ")

    charArray = []
    for character in headerFile: 
        charArray.append(character)
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Fin def splitRow --------- ")
    return charArray

def readContentFile(outFile, retries):
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def readContentFile --------- ")
    for line in  outFile:
        line = line.strip()
        content = splitRow(line)
        originalPaymentFolio = content[11]+content[12]+content[13]+content[14]+content[15]
        errorCode = content[16]+content[17]
        messageCode = switchCodes(int(errorCode))
        postCDAContingencyCodeInsert(errorCode, messageCode, int(originalPaymentFolio),retries)
        if __name__ == 'api.cdaRejectedCodes':    
            print ("Linea rechazada:        --> ",line)
            print ("Longitud:               --> ",len(line))
            print ("Folio Original de pago: --> ",originalPaymentFolio)
            print ("Codigo de error:        --> ",errorCode)
            print ("Mensaje de error:       --> ",messageCode)
    if __name__ == 'api.cdaRejectedCodes':    
        print(" --------- Fin def readContentFile --------- ")



def postContingencyFileCodeInsert(generalFileStatus, headerMessageCode, fileName, retries ):
    """
    Inserta la coleccion de archivos de contingencia en mongo el error con su descripcion
    >>> generalFileStatus = '01'
    >>> headerMessageCode = 'Al menos una CDA tiene datos incompletos o erróneos'
    >>> fileName = '31012020010808009400029310.acda'
    >>> retries = 2
    >>> postContingencyFileCodeInsert (generalFileStatus, headerMessageCode, fileName, retries )
    {'n': 1, 'nModified': 0, 'ok': 1.0, 'updatedExisting': True}
    """
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def postContingencyFileCodeInsert --------- ")
    conn = mongoConn()
    db = conn.admin
    contingencyFile = db.contingencia
    data = {}
    if retries == 1 :
        data["retries"] = retries
        data["errorCode"] = generalFileStatus
        data["errorMessage"] = headerMessageCode
    else : 
        data["retries"] = retries
        data["errorCode2"] = generalFileStatus
        data["errorMessage2"] = headerMessageCode
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Fin def postContingencyFileCodeInsert --------- ")
    return contingencyFile.update(
        {
            "fileName" : fileName
        },
        { '$set': data}
    )

def getContingencyFileCodeInsert(fileName):
    """
    Obtiene de base de datos el numero de intentos hechos del archivo de contingencia 
    >>> fileName = '31012020010808009400029310.acda'
    >>> getContingencyFileCodeInsert(fileName)
    {'_id': ObjectId('5ea9ccf5230a1f4d192e7eea'), 'fileName': '31012020010808009400029310.acda', 'retries': 2, 'fileDate': '28:04:2020', 'contingencyFile': '123456712345400020000000000000000000000000000000000000045||9|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   123456712345400020000000000000000000000000000000000000043||7|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ', 'cdas': ['||9|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||', '||7|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||'], 'errorCode': '01', 'errorMessage': 'Al menos una CDA tiene datos incompletos o erróneos', 'errorCode2': '01', 'errorMessage2': 'Al menos una CDA tiene datos incompletos o erróneos'}
    """
    if __name__ == 'api.cdaRejectedCodes':
        print(" --------- Inicio def getContingencyFileCodeInsert --------- ")
    conn = mongoConn()
    db = conn.admin
    contingencyFile = db.contingencia
    results = contingencyFile.find({"fileName" : fileName})
    # if results is None:
    for result in results:
        if __name__ == 'api.cdaRejectedCodes':
            print(" --------- Fin def getContingencyFileCodeInsert --------- ")
        return result
    # else:
    #     print (" --------- Fin def getContingencyFileCodeInsert Error --------- ")
    #     raise Exception(message_list['messages']['noContingencyData'])

    
        

def postCDAContingencyCodeInsert( errorCode, messageCode, originalPaymentFolio, retries ):
    """
    Inserta el mensaje y codigo de error en la BD de archivos de contingencia 
    >>> errorCode = '43' 
    >>> messageCode = 'No existe la Orden de Transferencia a la cual asignar la CDA' 
    >>> originalPaymentFolio = 30 
    >>> retries = 4
    >>> postCDAContingencyCodeInsert( errorCode, messageCode, originalPaymentFolio, retries )
    {'n': 1, 'nModified': 0, 'ok': 1.0, 'updatedExisting': True}

    """
    if __name__ == 'api.cdaRejectedCodes':    
        print(" --------- Inicio def postContingencyFileCodeInsert --------- ")
    conn = mongoConn()
    db = conn.admin
    contingencyCDAFile = db.sepipDB
    data = {}
    if retries == 1 :
        data["errorCode"] = errorCode
        data["errorMessage"] = messageCode
    else : 
        data["errorCode2"] = errorCode
        data["errorMessage2"] = messageCode
    if __name__ == 'api.cdaRejectedCodes':    
        print(" --------- Fin def postContingencyFileCodeInsert --------- ")
    return contingencyCDAFile.update(
        {
            "customerHubModel.originalPaymentFolio" : originalPaymentFolio
        },
        { '$set': data}
    )

def updateRetries(retries,fileName):
    """
    Actualiza el número de reintentos la base de datos del archivo de contingencia

    >>> retries = 2
    >>> fileName = '31012020010808009400029310.acda'
    >>> updateRetries(retries,fileName)
    {'n': 1, 'nModified': 1, 'ok': 1.0, 'updatedExisting': True}

    """
    if __name__ == 'api.cdaRejectedCodes':    
        print(" --------- Inicio def updateRetries --------- ")
    conn = mongoConn()
    db = conn.admin
    contingencyFile = db.contingencia
    data = {}
    data["retries"] = retries
    if __name__ == 'api.cdaRejectedCodes':    
        print(" --------- Fin def updateRetries --------- ")
    return contingencyFile.update(
        {
            "fileName" : fileName
        },
        { '$set': data}
    )

def switchCodes(argument):
    """
    Regresa el mensaje dependientdo del codigo que se le pase 
    >>> argument = 57 
    >>> switchCodes(argument)
    'Fecha calendario incorrecta'
    """
    switcher = {
        1: "Al menos una CDA tiene datos incompletos o erróneos",
        2: "Fecha de operación incorrecta",
        3: "Nombre del archivo incorrecto",
        34: "Cantidad de registros mayor al permitido",
        5: "Participante Emisor de la CDA y Participante Emisor de la Orden de Transferencia son iguales.",
        12: "Formato de estampa de tiempo incorrecto",
        37: "Falla en la clave de Participante Emisor de la CDA",
        38: "Falla en la clave del Participante Emisor de la Orden de Transferencia contenida en la CDA",
        43: "No existe la Orden de Transferencia a la cual asignar la CDA",
        44: "CDA mal armada",
        45: "Formato de monto y/o IVA incorrecto",
        46: "Fecha de la CDA no corresponde con la fecha del nombre de archivo",
        47: "Sello digital inválido",
        54: "Registrada previamente",
        55: "Monto incorrecto del CDA",
        56: "Tipo de pago incorrecto ",
        57: "Fecha calendario incorrecta"
    }
    return switcher.get(argument, "Invalid code")
    
def successfulPackage(pathFile, fileName):
    """
    Regresa el booleano dependientdo del codigo de archivo que se le pase 
    >>> pathFile = 'C:/Users/RH11770/Downloads/archivo_correcto/'
    >>> fileName = '28042020130355925400020002.out'
    >>> successfulPackage(pathFile, fileName)
     --------- Inicio def successfulPackage --------- 
     --------- Fin def successfulPackage True --------- 
    True
    """
    pathFile = pathFile+fileName
    print(" --------- Inicio def successfulPackage --------- ")
    outFile= open(pathFile, "r" )
    headerFile = outFile.readline().strip()
    header = splitRow(headerFile)
    hasError = header[6] + header[7] + header[8] + header[9]
    if hasError == '0000':
        print(" --------- Fin def successfulPackage True --------- ")
        return True
    else:
        print(" --------- Fin def successfulPackage False --------- ")
        return False



def saveSuccessfulCDA(fileName):
    """
    Edita los CDAs de un packete por contingencia con estado exitoso
    >>> fileName = '28042020130355925400020002.out'
    >>> saveSuccessfulCDA(fileName)
     --------- Inicio def saveSuccessfulCDA --------- 
     --------- Fin def saveSuccessfulCDA --------- 
    """
    print(" --------- Inicio def saveSuccessfulCDA --------- ")
    fileNameACDA = fileName.replace('.out', '.acda')
    contingencyFile = getContingencyFileCodeInsert(fileNameACDA)
    cdasContingencia = contingencyFile['cdas']
    conn = mongoConn()
    db = conn.admin
    contingencyCDAFile = db.sepipDB
    data = {}
    data["statusCode"] = 'CDA exitoso'
    for cda in cdasContingencia:
        contingencyCDAFile.update(
            {
                "customerHubModel.CDA" : cda
            },
            { '$set': data}
        )
    print(" --------- Fin def saveSuccessfulCDA --------- ")


def saveSuccessfulContingencyPackage(fileName):
    """
    Edita un packete por contingencia con estado exitoso
    >>> fileName = '28042020130355925400020002.out'
    >>> saveSuccessfulContingencyPackage(fileName)
     --------- Inicio def saveSuccessfulContingencyPackage --------- 
     --------- Fin def saveSuccessfulContingencyPackage --------- 
    {'n': 1, 'nModified': 0, 'ok': 1.0, 'updatedExisting': True}
    """
    print(" --------- Inicio def saveSuccessfulContingencyPackage --------- ")
    fileNameACDA = fileName.replace('.out', '.acda')
    conn = mongoConn()
    db = conn.admin
    contingencyFile = db.contingencia
    data = {}
    data["statusCode"] = 'Archivo correcto'
    print(" --------- Fin def saveSuccessfulContingencyPackage --------- ")
    return contingencyFile.update(
        {
            "fileName" : fileNameACDA
        },
        { '$set': data}
    )

def verifyIfCorrectedFile(fileName):
    fileNameACDA = fileName.replace('.out', '.acda')
    conn = mongoConn()
    db = conn.admin
    contingencyFile = db.contingencia
    results = contingencyFile.find({"fileName" : fileNameACDA})

    for result in results:
        datos = result

    bodySquema ={
            "contingecyCorrected": False
        }

    for itera in datos:
        bodySquema[itera] = datos[itera]

    if bodySquema['contingecyCorrected'] == True:
        return True
    else:
        return False



def writeReturnedCorrectedFile(pathFile, fileName):
    fileNameACDA = fileName.replace('.out', '.acda')
    conn = mongoConn()
    db = conn.admin
    contingencyFile = db.contingencia
    results = contingencyFile.find({"fileName" : fileNameACDA})
    for result in results:
        datos = result

    f = open(pathFile+fileNameACDA, "x")
    f.write(datos['contingencyFile'])
    f.close()




if __name__ == '__main__':
    doctest.testmod()


