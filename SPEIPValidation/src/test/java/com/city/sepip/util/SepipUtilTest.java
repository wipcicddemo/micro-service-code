package com.city.sepip.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SepipUtilTest extends SepipUtil {

	
SepipUtil sepipUtil;
	

	@Test
	public void getSepipValues_negative_sepipModelError() {
		
	
		assertThat(1, is( SepipUtil.getNoOfDigits(BigDecimal.valueOf(123.2))));
		
	}
}
