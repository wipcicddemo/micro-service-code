/**
 * 
 */
package com.city.sepip.util;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import com.city.sepip.controller.SepipController;
import com.city.sepip.model.SepipDB;
import com.city.sepip.model.SepipModel;
//import com.city.sepip.repo.SepipRepo;


/**
 * @author Dan
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SepipControllerTest extends SepipController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationUtilTest.class);
	
	SepipModel sepipModel;
	
//	@Mock
//	SepipRepo sepipRepoMock;
	
	@Mock
	RestTemplate restTemplateMock;
	
	@Mock
	private Environment env;
	
	@Mock
	ValidateOutputField vof;
	
	@InjectMocks
	SepipController sepipController;
	
	MockMvc mockMvc;
	@Before
	public void setUpClass() {
		sepipModel = new SepipModel(
				"1",
				"12",
				Integer.valueOf(26032020),
				Integer.valueOf(26032020),
				"232927",
				Integer.valueOf(40002),
				"X",
				"Ordenante ingresado",
				Integer.valueOf(40),
				"002180098683980590",
				"TATV730615HDFPCC01",
				Integer.valueOf(40),
				"CONCEPTO DE PAGO CONCEPTO DE PAGO D11257",
				BigDecimal.valueOf(1),
				BigDecimal.valueOf(325),
				Integer.valueOf(40),
				"X",
				Integer.valueOf(2),
				BigDecimal.valueOf(9999999),
				"321",
				"123",
				"X",
				"X",
				"Beneficiario 1",
				"002180098683980590",
				"TATV730615HDFPCC01",
				"X",
				"X",
				"X",
				"123",
				"X",
				"X",
				123,
				123
				);
		
		MockitoAnnotations.initMocks(this);
	    mockMvc = MockMvcBuilders.standaloneSetup(sepipController).setHandlerExceptionResolvers(new ExceptionHandlerExceptionResolver()).build();
		
	}
	
	
	@Test
	public void getSepipValues_negative_sepipModelError() {
		
		sepipModel.setBeneficiaryAccountType2(2);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.unprocessableEntity()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
	            .body("{\"message\":" +   "\"|| " + "Valor inválido, Tipo de Cuenta Beneficiario 2 no está catalogado"  + "||\" }");
		
		//System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		//System.out.println("Compara--------> "+response);
		assertThat(sepipController.getSepipValues(sepipModel), is( response));
		
	}
	
//	@Test
//	public void getSepipValues_negative_isInDataBAse() {
//		
//		when(sepipRepoMock.findSepipModelByTrackingKeyAndcalenderDatePayment(sepipModel.getTrackingKey(),sepipModel.getCalenderDatePayment())).thenReturn(new SepipDB());
//		
//		ResponseEntity<String> response;
//		
//		response = ResponseEntity
//				.unprocessableEntity()
//	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
//	            .body("{\"message\": \"El registro ya se encuentra en Base de Datos\"}");
//		
//		//System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
//		//System.out.println("Compara--------> "+response);
//		assertThat(sepipController.getSepipValues(sepipModel), is( response));
//		
//	}
	
	@Test
	public void getSepipValues_negative_noDigitalStamp() {
		
//		when(env.getProperty("spring.data.selloDigital.url")).thenReturn(ArgumentMatchers.anyString());
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.unprocessableEntity()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"message\": \"Hubo problemas al generar sello digital, por lo que no es  posible generar CDA\"}");
		
		//System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		//System.out.println("Compara--------> "+response);
		assertThat(sepipController.getSepipValues(sepipModel), is( response));
		
	}
	
	@Test
	public void getSepipValues_negative_siDigitalStamp() {
		
		when(env.getProperty("spring.data.selloDigital.url")).thenReturn("hola");
		
		Mockito.when(restTemplateMock.exchange(
				ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())
				).thenReturn(
				ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"digital_stamp\":" +   "\"" + "digital_stamp"  + "\" }")
				
				);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"cda\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
        				+ "\"certificateSerialNumber\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	            		+ "\"originalString\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	    	            + "\"digitalStamp\":" +   "\"" + ArgumentMatchers.anyString()  + "\""
        				+ " }");
		
		//System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		//System.out.println("Compara--------> "+response);

		assertThat(sepipController.getSepipValues(sepipModel).getStatusCode(), is( response.getStatusCode()));
		
	}
	
	
	@Test
	public void getSepipValues_negative_siDigitalStampAndValidationPaymentType22() {
		
	
		sepipModel.setPaymentType("22");
		
		when(env.getProperty("spring.data.selloDigital.url")).thenReturn("hola");
		
		Mockito.when(restTemplateMock.exchange(
				ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())
				).thenReturn(
				ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"digital_stamp\":" +   "\"" + "digital_stamp"  + "\" }")
				
				);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"cda\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
        				+ "\"certificateSerialNumber\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	            		+ "\"originalString\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	    	            + "\"digitalStamp\":" +   "\"" + ArgumentMatchers.anyString()  + "\""
        				+ " }");
		
		System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		System.out.println("Compara--------> "+response);

		assertThat(sepipController.getSepipValues(sepipModel).getStatusCode(), is( response.getStatusCode()));
		
	}
	
	@Test
	public void getSepipValues_negative_siDigitalStampAndValidationPaymentType19() {
		
	
		sepipModel.setPaymentType("19");
		
		when(env.getProperty("spring.data.selloDigital.url")).thenReturn("hola");
		
		Mockito.when(restTemplateMock.exchange(
				ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())
				).thenReturn(
				ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"digital_stamp\":" +   "\"" + "digital_stamp"  + "\" }")
				
				);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"cda\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
        				+ "\"certificateSerialNumber\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	            		+ "\"originalString\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	    	            + "\"digitalStamp\":" +   "\"" + ArgumentMatchers.anyString()  + "\""
        				+ " }");
		
		System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		System.out.println("Compara--------> "+response);

		assertThat(sepipController.getSepipValues(sepipModel).getStatusCode(), is( response.getStatusCode()));
		
	}
	
	@Test
	public void getSepipValues_negative_siDigitalStampAndValidationPaymentType20() {
		
	
		sepipModel.setPaymentType("20");
		
		when(env.getProperty("spring.data.selloDigital.url")).thenReturn("hola");
		
		Mockito.when(restTemplateMock.exchange(
				ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())
				).thenReturn(
				ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"digital_stamp\":" +   "\"" + "digital_stamp"  + "\" }")
				
				);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"cda\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
        				+ "\"certificateSerialNumber\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	            		+ "\"originalString\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	    	            + "\"digitalStamp\":" +   "\"" + ArgumentMatchers.anyString()  + "\""
        				+ " }");
		
		System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		System.out.println("Compara--------> "+response);

		assertThat(sepipController.getSepipValues(sepipModel).getStatusCode(), is( response.getStatusCode()));
		
	}
	
	@Test
	public void getSepipValues_negative_siDigitalStampAndValidationPaymentType21() {
		
	
		sepipModel.setPaymentType("21");
		
		when(env.getProperty("spring.data.selloDigital.url")).thenReturn("hola");
		
		Mockito.when(restTemplateMock.exchange(
				ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())
				).thenReturn(
				ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"digital_stamp\":" +   "\"" + "digital_stamp"  + "\" }")
				
				);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"cda\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
        				+ "\"certificateSerialNumber\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	            		+ "\"originalString\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	    	            + "\"digitalStamp\":" +   "\"" + ArgumentMatchers.anyString()  + "\""
        				+ " }");
		
		System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		System.out.println("Compara--------> "+response);

		assertThat(sepipController.getSepipValues(sepipModel).getStatusCode(), is( response.getStatusCode()));
		
	}
	
	@Test
	public void getSepipValues_negative_siSpeiOperDateEq7() {
		
	
		sepipModel.setSpeiOperationDate(Integer.valueOf(1012020));
		
		when(env.getProperty("spring.data.selloDigital.url")).thenReturn("hola");
		
		Mockito.when(restTemplateMock.exchange(
				ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())
				).thenReturn(
				ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"digital_stamp\":" +   "\"" + "digital_stamp"  + "\" }")
				
				);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"cda\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
        				+ "\"certificateSerialNumber\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	            		+ "\"originalString\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	    	            + "\"digitalStamp\":" +   "\"" + ArgumentMatchers.anyString()  + "\""
        				+ " }");
		
		System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		System.out.println("Compara--------> "+response);

		assertThat(sepipController.getSepipValues(sepipModel).getStatusCode(), is( response.getStatusCode()));
		
	}
	
	@Test
	public void getSepipValues_negative_siCalendarDateEq7() {
		
	
		sepipModel.setCalenderDatePayment(Integer.valueOf(1012020));
		
		when(env.getProperty("spring.data.selloDigital.url")).thenReturn("hola");
		
		Mockito.when(restTemplateMock.exchange(
				ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())
				).thenReturn(
				ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"digital_stamp\":" +   "\"" + "digital_stamp"  + "\" }")
				
				);
		
		ResponseEntity<String> response;
		
		response = ResponseEntity
				.ok()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
        		.body("{\"cda\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
        				+ "\"certificateSerialNumber\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	            		+ "\"originalString\":" +   "\"" + ArgumentMatchers.anyString()  + "\","
	    	            + "\"digitalStamp\":" +   "\"" + ArgumentMatchers.anyString()  + "\""
        				+ " }");
		
		System.out.println("Salida--------> "+sepipController.getSepipValues(sepipModel));
		System.out.println("Compara--------> "+response);

		assertThat(sepipController.getSepipValues(sepipModel).getStatusCode(), is( response.getStatusCode()));
		
	}
	
//	@Test
//	public void getSepipValues_negative_siCalendarDateEq7err() throws Exception {
//		
//		String error = mockMvc.perform(post("/v1/sepip").contentType(MediaType.APPLICATION_JSON).content(
//				
//				"{\r\n" + 
//				"  \"trackingKey\": \"55\",\r\n" + 
//				"  \"beneficiaryAccount\": \"002180098683980590\",\r\n" + 
//				"  \"beneficiaryAccountType\": 40,\r\n" + 
//				"  \"beneficiaryName\": \"Beneficiario 1\",\r\n" + 
//				"  \"beneficiaryRfcOrCurp\": \"TATV730615HDFPCC01\",\r\n" + 
//				"  \"beneficiaryTwoAccount\": \"X\",\r\n" + 
//				"  \"beneficiaryTwoAccountType\": 40,\r\n" + 
//				"  \"beneficiaryTwoName\": \"X\",\r\n" + 
//				"  \"beneficiaryTwoRfcOrCurp\": \"X\",\r\n" + 
//				"  \"buyerCellNumberLabel\": \"321\",\r\n" + 
//				"  \"buyerVerificationDigit\": \"123\",\r\n" + 
//				"  \"cdaParticipientSPEIKey\": 40002,\r\n" + 
//				"  \"digitalPaymentSchemeFolio\": \"X\",\r\n" + 
//				"  \"digitalStampSerialNumber\": \"X\",\r\n" + 
//				"  \"idTransaccion\": \"X\",\r\n" + 
//				"  \"ivaAmount\": 1,\r\n" + 
//				"  \"payerAccount\": \"002180098683980590\",\r\n" + 
//				"  \"payerAccountType\": 40,\r\n" + 
//				"  \"payerName\": \"Ordenante ingresado\",\r\n" + 
//				"  \"payerRfcOrCurp\": \"TATV730615HDFPCC01\",\r\n" + 
//				"  \"paymentAmount\": 325,\r\n" + 
//				"  \"paymentCalendarDate\": 26032020,\r\n" + 
//				"  \"paymentCalendarTime\": \"232927\",\r\n" + 
//				"  \"paymentConcept\": \"CONCEPTO DE PAGO CONCEPTO DE PAGO D11257\",\r\n" + 
//				"  \"paymentOfTheTransferFee\": 2,\r\n" + 
//				"  \"paymentType\": \"12\",\r\n" + 
//				"  \"receiverParticipantName\": \"X\",\r\n" + 
//				"  \"sellerCellNumberLabel\": \"123\",\r\n" + 
//				"  \"sellerVerificationDigit\": \"X\",\r\n" + 
//				"  \"speiOperationDate\": 26032020,\r\n" + 
//				"  \"transferFeeAmount\": 9999999999,\r\n" + 
//				"  \"transferOrderIssuerParticipantName\": \"X\",\r\n" + 
//				"  \"originalFolioPackage\": 123,\r\n" + 
//				"  \"originalPaymentFolio\": 123,\r\n" + 
//				"\r\n" + 
//				"}"
//				
//				
//				).accept(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden()).andReturn().getResolvedException().getMessage();
//		
//		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<"+error);
//		
//	}
	
	
	

	

}
