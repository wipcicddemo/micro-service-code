package com.city.sepip.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.city.sepip.model.SepipModel;

@RunWith(MockitoJUnitRunner.class)
public class ValidationUtilTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationUtilTest.class);

//	@Mock
//	SepipModel sepipModel;
	
	@InjectMocks
	ValidationUtil validationUtil;
	
	static SepipModel sepipModel;
	
	@Before
	public void setUpClass() {
		sepipModel = new SepipModel(
				"1",
				"12",
				Integer.valueOf(26032020),
				Integer.valueOf(26032020),
				"232927",
				Integer.valueOf(40002),
				"X",
				"Ordenante ingresado",
				Integer.valueOf(40),
				"002180098683980590",
				"TATV730615HDFPCC01",
				Integer.valueOf(40),
				"CONCEPTO DE PAGO CONCEPTO DE PAGO D11257",
				BigDecimal.valueOf(1),
				BigDecimal.valueOf(325),
				Integer.valueOf(40),
				"X",
				Integer.valueOf(2),
				BigDecimal.valueOf(9999999),
				"321",
				"123",
				"X",
				"X",
				"Beneficiario 1",
				"002180098683980590",
				"TATV730615HDFPCC01",
				"X",
				"X",
				"X",
				"123",
				"X",
				"X",
				123,
				123
				);
		
	}
	
	
	
	/////////////////////validateInputFields - INICIO/////////////////////
	
	@Test
	public void testvalidateInputFields_validateAllPaymentTypeInfo_positive() {
		sepipModel.setPaymentType("12");
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAllPaymentTypeInfo_negative_numeric() {
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAllPaymentTypeInfo_negative_empty() {
		sepipModel.setPaymentType("");
		assertEquals("Tipo de Pago diferente a tercero a tercero, no puede estar vacío", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAllPaymentTypeInfo_negative_greaterThanTwo() {
		sepipModel.setPaymentType("123");
		assertEquals("Valor inválido, el Tipo de pago  tiene un longitud máxima de 2 digitos.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAllPaymentTypeInfo_negative_notAllowedPatmentType() {
		sepipModel.setPaymentType("14");
		assertEquals("Tipo de Pago diferente a tercero a tercero", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateBeneficiaryAccountType2_positive() {
		sepipModel.setBeneficiaryAccountType2(40);
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateBeneficiaryAccountType2_negative_greaterThanTwo() {
		sepipModel.setBeneficiaryAccountType2(123);
		assertEquals("Valor inválido, Tipo de Cuenta Beneficiario 2 no puede ser mayor a dos dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateBeneficiaryAccountType2_negative_notAllowedType() {
		sepipModel.setBeneficiaryAccountType2(2);
		assertEquals("Valor inválido, Tipo de Cuenta Beneficiario 2 no está catalogado", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateBeneficiaryAccountType2_negative_validationResultAndVPOTF() {
		sepipModel.setBeneficiaryAccountType2(2);
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, Tipo de Cuenta Beneficiario 2 no está catalogado", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_positive() {
		sepipModel.setCalenderTimePayment("123456");
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_negative_onlyNumbers() {
		sepipModel.setCalenderTimePayment("1234AB");
		assertEquals("Hora inválida, no se permiten caracteres alfabéticos o especiales", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_negative_greaterThanSix() {
		sepipModel.setCalenderTimePayment("1234123");
		assertEquals("Hora inválida, mayor a 6 dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_negative_lowerThanSix() {
		sepipModel.setCalenderTimePayment("123");
		assertEquals("Hora inválida, menor a 6 dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_negative_invalidHour() {
		sepipModel.setCalenderTimePayment("993456");
		assertEquals("Hora inválida, dígitos de hora fuera de rango", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_negative_invalidMinutes() {
		sepipModel.setCalenderTimePayment("129456");
		assertEquals("Hora inválida, dígitos de minutos fuera de rango", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_negative_invalidSeconds() {
		sepipModel.setCalenderTimePayment("123466");
		assertEquals("Hora inválida, dígitos de segundos fuera de rango", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCalenderTimePayment_negative_validationResultNotNull() {
		sepipModel.setCalenderTimePayment("123466");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Hora inválida, dígitos de segundos fuera de rango", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_positive() {
		sepipModel.setOrderingAccount("002180098683980590");
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_specialChar() {
		sepipModel.setOrderingAccount("0021800986839805AB");
		assertEquals("Valor inválidoa, Cuenta Ordenante no puede tener caracteres alfabéticos o especiales", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_specialCharsp() {
		sepipModel.setOrderingAccount("0021800986839805#$");
		assertEquals("Valor inválidoa, Cuenta Ordenante no puede tener caracteres alfabéticos o especiales", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_empty() {
		sepipModel.setOrderingAccount("");
		assertEquals("Valor inválido, Cuenta Ordenante no puede estar vacío es un dato obligatorio", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_greatherThan20() {
		sepipModel.setOrderingAccount("00218009868398059012345");
		assertEquals("Valor inválido, Cuenta Ordenante no puede ser mayor a 20 dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_40andlengthDifferent18() {
		sepipModel.setOrdererAccountType(40);
		sepipModel.setOrderingAccount("002180098683");
		assertEquals("Valor inválido, el Tipo de cuenta Ordenante  Clabe no corresponde a la cuenta Ordenante recibida", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_3andlengthDifferent16() {
		sepipModel.setOrdererAccountType(3);
		sepipModel.setOrderingAccount("002180098683");
		assertEquals("Valor inválido, el Tipo de cuenta Ordenante  TD no corresponde a la cuenta Ordenante recibida", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_3andlengthEQ16() {
		sepipModel.setOrdererAccountType(3);
		sepipModel.setOrderingAccount("1234567890123456");
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_10andlengthDifferent10() {
		sepipModel.setOrdererAccountType(10);
		sepipModel.setOrderingAccount("002180098683");
		assertEquals("Valor inválido, el Tipo de cuenta Ordenante  linea telefonica movil no corresponde a la cuenta Ordenante recibida", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_10andlengthEQ10() {
		sepipModel.setOrdererAccountType(10);
		sepipModel.setOrderingAccount("1234567890");
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingAccount_negative_validationResultNotNull() {
		sepipModel.setOrdererAccountType(10);
		sepipModel.setOrderingAccount("002180098683");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, el Tipo de cuenta Ordenante  linea telefonica movil no corresponde a la cuenta Ordenante recibida", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateRfc_positive() {
		sepipModel.setRfcOrCurpPayer("TATV730615HDFPCC01");
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateRfc_negative_empty() {
		sepipModel.setRfcOrCurpPayer("");
		assertEquals("Valor inválido, RFC o CURP Ordenante es un dato obligatorio, no puede estar vacío", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateRfc_negative_greatherThan18() {
		sepipModel.setRfcOrCurpPayer("TATV730615HDFPCC012");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, RFC o CURP Ordenante no puede ser mayor a 18 posiciones", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateRfc_negative_validationResultNotNull() {
		sepipModel.setRfcOrCurpPayer("TATV730615HDFPCC012");
		assertEquals("Valor inválido, RFC o CURP Ordenante no puede ser mayor a 18 posiciones", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_positive() {
		sepipModel.setCalenderDatePayment(25012019);
		assertNull(ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_empty() {
		sepipModel.setCalenderDatePayment(0);
		assertEquals("Valor inválido, la fecha de Operación de SPEI o la fecha calendario de Abono es obligatoria, no puede estar el dato vacío.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_greaterThan8() {
		sepipModel.setCalenderDatePayment(123456786);
		assertEquals("Fecha inválida, mayor 8 dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_lessThan7() {
		sepipModel.setCalenderDatePayment(123123);
		assertEquals("Fecha inválida, menor 8 dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_yearGreatherThan3000() {
		sepipModel.setCalenderDatePayment(25013019);
		assertEquals("Fecha inválida, año no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_yearLowerThan1000() {
		sepipModel.setCalenderDatePayment(25010019);
		assertEquals("Fecha inválida, año no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_monthGreaterThan12() {
		sepipModel.setCalenderDatePayment(25142019);
		assertEquals("Fecha inválida, mes incorrecto", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_monthLowerThan1() {
		sepipModel.setCalenderDatePayment(25002019);
		assertEquals("Fecha inválida, mes incorrecto", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_dayGreaterThan31() {
		sepipModel.setCalenderDatePayment(32012019);
		assertEquals("Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_february28Days() {
		sepipModel.setCalenderDatePayment(29022019);
		assertEquals("Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_februaryLeapYear() {
		sepipModel.setCalenderDatePayment(30022020);
		assertEquals("Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_validationResultNotNull() {
		sepipModel.setCalenderDatePayment(30022020);
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_validationResultNotNullSpeiOperationDate() {
		sepipModel.setSpeiOperationDate(30022020);
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_SpeiOperationDate() {
		sepipModel.setSpeiOperationDate(30022020);
		assertEquals("Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_positive_februaryLeapYear() {
		sepipModel.setCalenderDatePayment(29022020);
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_Month31DaysGreater() {
		sepipModel.setCalenderDatePayment(32012020);
		assertEquals("Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateProperDate_negative_Month30DaysGreater() {
		sepipModel.setCalenderDatePayment(31042020);
		assertEquals("Fecha inválida, día no válido", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAmountOfPayment_positive() {
		sepipModel.setAmountOfPayment(BigDecimal.valueOf(20191));
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAmountOfPayment_negative_differentFrom0() {
		sepipModel.setAmountOfPayment(BigDecimal.valueOf(0));
		assertEquals("Monto Inválido, no puede ser 0", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAmountOfPayment_negative_greaterThan12() {
		sepipModel.setAmountOfPayment(BigDecimal.valueOf(9999999999999L));
		assertEquals("Monto Inválido, no puede ser  mayor a 999,999,999,999.99", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAmountOfPayment_negative_greaterThan12WithPoint() {
		sepipModel.setAmountOfPayment(BigDecimal.valueOf(9999999999999.22));
		assertEquals("Monto Inválido, no puede ser  mayor a 999,999,999,999.99", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateAmountOfPayment_negative_validationResultNotNull() {
		sepipModel.setAmountOfPayment(BigDecimal.valueOf(9999999999999.22));
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Monto Inválido, no puede ser  mayor a 999,999,999,999.99", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateConceptOfPayment_positive() {
		sepipModel.setConceptOfPayment("Concepto");
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateConceptOfPayment_negative_greaterThan40() {
		sepipModel.setConceptOfPayment("12345678901234567890123456789012345678901");
		assertEquals("Valor inválido, Concepto del Pago no puede ser mayor a 40 posiciones", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateConceptOfPayment_negative_validationResultNotNull() {
		sepipModel.setPaymentType("AA");
		sepipModel.setConceptOfPayment("12345678901234567890123456789012345678901");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, Concepto del Pago no puede ser mayor a 40 posiciones", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTypeOfBeneficiaryAccount_positive() {
		sepipModel.setTypeOfBeneficiaryAccount(40);
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTypeOfBeneficiaryAccount_negative_greaterThan2() {
		sepipModel.setTypeOfBeneficiaryAccount(123);
		assertEquals("Valor inválido, Tipo de Cuenta Beneficiario no puede ser mayor a dos digitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTypeOfBeneficiaryAccount_negative_notInCatalog() {
		sepipModel.setTypeOfBeneficiaryAccount(2);
		assertEquals("Valor inválido, Tipo de Cuenta Beneficiario  no está catalogado", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTypeOfBeneficiaryAccount_negative_validationResultNotNull() {
		sepipModel.setTypeOfBeneficiaryAccount(2);
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, Tipo de Cuenta Beneficiario  no está catalogado", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateNameOfTransferOrderIssuerParticipant_positive() {
		sepipModel.setNameOfTransferOrderIssuerParticipant("Nombre");
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateNameOfTransferOrderIssuerParticipant_negative_greaterThan40() {
		sepipModel.setNameOfTransferOrderIssuerParticipant("Nombre12345678901234567890123456789012345678901");
		assertEquals("Valor inválido en el campo  Nombre del Participante Emisor de la Orden de Transferencia, la longitud máxima es de 40 posiciones.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateNameOfTransferOrderIssuerParticipant_negative_validationResultNotNull() {
		sepipModel.setNameOfTransferOrderIssuerParticipant("Nombre12345678901234567890123456789012345678901");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido en el campo  Nombre del Participante Emisor de la Orden de Transferencia, la longitud máxima es de 40 posiciones.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingName_positive() {
		sepipModel.setOrderingName("Nombre");
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingName_negative_greaterThan40() {
		sepipModel.setOrderingName("Nombre12345678901234567890123456789012345678901");
		assertEquals("Valor inválido en el campo  Nombre Ordenante, la longitud máxima es de 40 posiciones.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateOrderingName_negative_validationResultNotNull() {
		sepipModel.setOrderingName("Nombre12345678901234567890123456789012345678901");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido en el campo  Nombre Ordenante, la longitud máxima es de 40 posiciones.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_typeOfAccountOrd_positive() {
		sepipModel.setOrdererAccountType(40);
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_typeOfAccountOrd_negative_greaterThan0() {
		sepipModel.setOrdererAccountType(0);
		assertEquals("Valor inválido, el Tipo de Cuenta Ordenante es obligatorio, no puede estar vacío.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_typeOfAccountOrd_negative_greaterThan2() {
		sepipModel.setOrdererAccountType(123);
		assertEquals("Valor inválido, Tipo de Cuenta Ordenante no puede ser mayor a dos digitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_typeOfAccountOrd_negative_notInCAtalog() {
		sepipModel.setOrdererAccountType(12);
		assertEquals("Valor inválido, Tipo de Cuenta Ordenante  no está catalogado", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_typeOfAccountOrd_negative_validationResultNotNull() {
		sepipModel.setOrdererAccountType(12);
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, Tipo de Cuenta Ordenante  no está catalogado", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateSPEIKey_positive() {
		sepipModel.setCdaParticipientSPEIKey(40002);
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateSPEIKey_negative_lowerThan2() {
		sepipModel.setCdaParticipientSPEIKey(1);
		assertEquals("Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta  debe ser un valor numérico, no puede estár vacío", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateSPEIKey_negative_greaterThan5() {
		sepipModel.setCdaParticipientSPEIKey(1123456);
		assertEquals("Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta  debe ser un valor numérico de longitud máxima de 5 dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateSPEIKey_negative_notInCatalog() {
		sepipModel.setCdaParticipientSPEIKey(11234);
		assertEquals("Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta de acuerdo con catálogo.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateSPEIKey_negative_validationResultNotNull() {
		sepipModel.setCdaParticipientSPEIKey(11234);
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta de acuerdo con catálogo.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validatePaymentOfFee_positive() {
		sepipModel.setPaymentOfTheTransferFee(1);
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validatePaymentOfFee_negative_greaterThan1() {
		sepipModel.setPaymentOfTheTransferFee(12);
		assertEquals("Valor inválido, para el Campo de la Comisión por la transferencia, el valor no puede ser mayor a un dígito", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validatePaymentOfFee_negative_zeroIfPayTypeNot1or2() {
		sepipModel.setPaymentType("3");
		sepipModel.setPaymentOfTheTransferFee(3);
		assertEquals("Valor inválido para Campo Pago de la Comision por la transferencia, el valor debe ser 0", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validatePaymentOfFee_negative_validationResultNotNull() {
		sepipModel.setPaymentType("3");
		sepipModel.setPaymentOfTheTransferFee(3);
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido para Campo Pago de la Comision por la transferencia, el valor debe ser 0", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTransferFeeAmt_positive() {
		sepipModel.setTransferFeeAmount(BigDecimal.valueOf(1));
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTransferFeeAmt_negative_greaterThan19() {
		sepipModel.setTransferFeeAmount(BigDecimal.valueOf(12345678901234567890D));
		assertEquals("Valor inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido de acuerdo al monto máximo que se pude recibir.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTransferFeeAmt_negative_greaterThan12() {
		sepipModel.setPaymentType("3");
		sepipModel.setPaymentOfTheTransferFee(2);
		sepipModel.setTransferFeeAmount(BigDecimal.valueOf(1234567891234D));
		assertEquals("Valor inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido de acuerdo al monto máximo que se pude recibir.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTransferFeeAmt_negative_greaterThan0() {
		sepipModel.setPaymentType("18");
		sepipModel.setPaymentOfTheTransferFee(2);
		sepipModel.setTransferFeeAmount(BigDecimal.valueOf(0));
		assertEquals("Monto Inválido, no puede ser 0.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateTransferFeeAmt_negative_validationResultNotNull() {
		sepipModel.setTransferFeeAmount(BigDecimal.valueOf(12345678901234567890D));
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido de acuerdo al monto máximo que se pude recibir.", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCellAlias_positive() {
		sepipModel.setBuyerCellNumberAliases("123");
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCellAlias_negative_isNumericSpecialChar() {
		sepipModel.setBuyerCellNumberAliases("#$%");
		assertEquals("Valor inválido, el Campo Alias del Número Celular del Comprador debe ser numérico", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCellAlias_negative_greaterThan0() {
		sepipModel.setBuyerCellNumberAliases("");
		assertEquals("Valor inválido, el Campo Alias del Número Celular del Comprador está vacío o es diferente de 0", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCellAlias_negative_lowerThan10() {
		sepipModel.setBuyerCellNumberAliases("123456789123");
		assertEquals("Valor inválido, el Campo Alias del Número Celular del Comprador no debe ser mayor a 10 dígitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCellAlias_negative_isNumericChar() {
		sepipModel.setBuyerCellNumberAliases("ASD");
		assertEquals("Valor inválido, el Campo Alias del Número Celular del Comprador debe ser numérico", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateCellAlias_negative_validationResultNotNull() {
		sepipModel.setBuyerCellNumberAliases("ASD");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor inválido, el Campo Alias del Número Celular del Comprador debe ser numérico", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalBuyer_positive() {
		sepipModel.setDigitBuyersVerifier("123");
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalBuyer_negative_isEmpty() {
		sepipModel.setDigitBuyersVerifier("");
		assertEquals("Valor inválido, el campo está vacío o es diferente de 0", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalBuyer_negative_specialChar() {
		sepipModel.setDigitBuyersVerifier("#$%");
		assertEquals("Valor inválido, el campo Digito Verificador del Comprador debe ser numérico", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalBuyer_negative_Char() {
		sepipModel.setDigitBuyersVerifier("AC");
		assertEquals("Valor inválido, el campo Dígito Verificador del Comprador debe ser numérico", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalBuyer_negative_greaterTehan3() {
		sepipModel.setDigitBuyersVerifier("1234");
		assertEquals("Valor Inválido, el campo Digito Verificador del comprador no debe ser mayor a 3 digitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalBuyer_negative_validationResultNotNull() {
		sepipModel.setDigitBuyersVerifier("1234");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor Inválido, el campo Digito Verificador del comprador no debe ser mayor a 3 digitos", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateIvaAmount_positive() {
		sepipModel.setIvaAmount(BigDecimal.valueOf(123));
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateIvaAmount_negative_lowerThan1() {
		sepipModel.setIvaAmount(BigDecimal.valueOf(0));
		assertEquals("Monto Inválido, no puede ser 0", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateIvaAmount_negative_greaterThan12() {
		sepipModel.setIvaAmount(BigDecimal.valueOf(9999999999999L));
		assertEquals("Monto Inválido, no puede ser mayor a 999,999,999,999.99", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateIvaAmount_negative_greaterThan12withPoint() {
		sepipModel.setIvaAmount(BigDecimal.valueOf(9999999999999.2));
		assertEquals("Monto Inválido, no puede ser mayor a 999,999,999,999.99", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateIvaAmount_negative_validationResultNotNull() {
		sepipModel.setIvaAmount(BigDecimal.valueOf(9999999999999.2));
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Monto Inválido, no puede ser mayor a 999,999,999,999.99", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalCollectionSchemeFolio_positive() {
		sepipModel.setDigitalCollectionSchemeFolio("123");
		assertNull( ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalCollectionSchemeFolio_negative_greaterThan20() {
		sepipModel.setDigitalCollectionSchemeFolio("123456789012345678909");
		assertEquals("Valor Inválido, Folio del Esquema Cobro Digital, no puede ser mayor a 20 posiciones", ValidationUtil.validateInputFields(sepipModel));
	}
	
	@Test
	public void testvalidateInputFields_validateDigitalCollectionSchemeFolio_negative_validationResultNotNull() {
		sepipModel.setDigitalCollectionSchemeFolio("123456789012345678909");
		sepipModel.setPaymentType("AA");
		assertEquals("Tipo de Pago diferente a tercero a tercero ,Valor Inválido, Folio del Esquema Cobro Digital, no puede ser mayor a 20 posiciones", ValidationUtil.validateInputFields(sepipModel));
	}
	
	/////////////////////validateInputFields - FIN/////////////////////
	
	
	

}
