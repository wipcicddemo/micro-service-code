package com.city.sepip.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import com.city.sepip.model.CustomerHubModel;
import com.city.sepip.model.SepipModel;

@RunWith(MockitoJUnitRunner.class)
public class ValidateOutputFieldTest extends ValidateOutputField {
	
	SepipModel sepipModel;
	
	CustomerHubModel customerHubModel;
	
	ValidateOutputField validateOutputField;
	
	@Before
	public void setUpClass() {
		sepipModel = new SepipModel(
				"1",
				"12",
				Integer.valueOf(26032020),
				Integer.valueOf(26032020),
				"232927",
				Integer.valueOf(40002),
				"X",
				"Ordenante ingresado",
				Integer.valueOf(40),
				"002180098683980590",
				"TATV730615HDFPCC01",
				Integer.valueOf(40),
				"CONCEPTO DE PAGO CONCEPTO DE PAGO D11257",
				BigDecimal.valueOf(1),
				BigDecimal.valueOf(325),
				Integer.valueOf(40),
				"X",
				Integer.valueOf(2),
				BigDecimal.valueOf(9999999),
				"321",
				"123",
				"X",
				"X",
				"Beneficiario 1",
				"002180098683980590",
				"TATV730615HDFPCC01",
				"X",
				"X",
				"X",
				"123",
				"X",
				"X",
				123,
				123
				);
		
	   
		
	}
	
	
	@Test
	public void getSepipValues_negative_sepipModelError() {
		
		customerHubModel = new CustomerHubModel();
		customerHubModel.setrFCOCurpBeneficiario2("abbcd");
		customerHubModel.setrFCOCurpBeneficiario("abbcd");
		customerHubModel.setCuentaBeneficiario("123");
		
		assertThat("Valor inválido, el Tipo de cuenta Beneficiario Clabe no corresponde a la Cuenta Beneficiario recibida", is( ValidateOutputField.validateOutputFieldValues(sepipModel, customerHubModel)));
		
	}
	
	
}
