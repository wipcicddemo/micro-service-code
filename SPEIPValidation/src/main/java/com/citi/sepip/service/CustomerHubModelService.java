package com.citi.sepip.service;

import java.util.Optional;

import com.city.sepip.model.CustomerHubModel;
import com.city.sepip.model.CustomerHubModelEntity;
import com.city.sepip.model.CustomerHubModelId;

public interface CustomerHubModelService{
	public void addCustomerHubModel(CustomerHubModel cda);
	public Optional<CustomerHubModelEntity> findById(CustomerHubModelId id);
}
