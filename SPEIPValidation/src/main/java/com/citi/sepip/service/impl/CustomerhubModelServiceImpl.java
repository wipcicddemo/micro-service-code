package com.citi.sepip.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.sepip.service.CustomerHubModelService;
import com.city.sepip.model.CustomerHubModel;
import com.city.sepip.model.CustomerHubModelEntity;
import com.city.sepip.model.CustomerHubModelId;
import com.city.sepip.repo.CustomerhubModelRepository;




@Service
public class CustomerhubModelServiceImpl implements CustomerHubModelService {
	
	@Autowired
	CustomerhubModelRepository customerhubModelRepository ;

	
	public void addCustomerHubModel(CustomerHubModel cda) {
		
		CustomerHubModelEntity customerHubModelEntity = new CustomerHubModelEntity ();
		
		CustomerHubModelId customerHubModelId = new CustomerHubModelId(cda.getTrackingKey(), cda.getSpeiOperationDate());
		
		customerHubModelEntity.setCustomerHubModelId(customerHubModelId);
//		customerHubModelEntity.setTrackingKey(cda.getTrackingKey());
		customerHubModelEntity.setPaymentType(cda.getPaymentType());
//		customerHubModelEntity.setSpeiOperationDate(cda.getSpeiOperationDate());
		customerHubModelEntity.setCalenderDatePayment(cda.getCalenderDatePayment());
		customerHubModelEntity.setCalenderTimePayment(cda.getCalenderTimePayment());
		customerHubModelEntity.setCdaParticipientSPEIKey(cda.getCdaParticipientSPEIKey());
		customerHubModelEntity.setNameOfTransferOrderIssuerParticipant(cda.getNameOfTransferOrderIssuerParticipant());
		customerHubModelEntity.setOrderingName(cda.getOrderingName());
		customerHubModelEntity.setOrdererAccountType(cda.getOrdererAccountType());
		customerHubModelEntity.setOrderingAccount(cda.getOrderingAccount());
		customerHubModelEntity.setRfcOrCurpPayer(cda.getRfcOrCurpPayer());
		customerHubModelEntity.setTypeOfBeneficiaryAccount(cda.getTypeOfBeneficiaryAccount());
		customerHubModelEntity.setConceptOfPayment(cda.getConceptOfPayment());
		customerHubModelEntity.setIvaAmount(cda.getIvaAmount());
		customerHubModelEntity.setAmountOfPayment(cda.getAmountOfPayment());
		customerHubModelEntity.setBeneficiaryAccountType2(cda.getBeneficiaryAccountType2());
		customerHubModelEntity.setDigitalCollectionSchemeFolio(cda.getDigitalCollectionSchemeFolio());
		customerHubModelEntity.setPaymentOfTheTransferFee(cda.getPaymentOfTheTransferFee());
		customerHubModelEntity.setTransferFeeAmount(cda.getTransferFeeAmount());
		customerHubModelEntity.setBuyerCellNumberAliases(cda.getBuyerCellNumberAliases());
		customerHubModelEntity.setDigitBuyersVerifier(cda.getDigitBuyersVerifier());
		customerHubModelEntity.setMombreDelParticipanteReceptor(cda.getMombreDelParticipanteReceptor());
		customerHubModelEntity.setNombreBeneficiario(cda.getNombreBeneficiario());
		customerHubModelEntity.setCuentaBeneficiario(cda.getCuentaBeneficiario());
		customerHubModelEntity.setrFCOCurpBeneficiario(cda.getrFCOCurpBeneficiario());
		customerHubModelEntity.setNombreBeneficiario2(cda.getNombreBeneficiario2());
		customerHubModelEntity.setrFCOCurpBeneficiario2(cda.getrFCOCurpBeneficiario2());
		customerHubModelEntity.setCuentaBeneficiario2(cda.getCuentaBeneficiario2());
		customerHubModelEntity.setAliasDelNumeroCelularDelVendedor(cda.getAliasDelNumeroCelularDelVendedor());
		customerHubModelEntity.setDigitoVerificadorDelVendedor(cda.getDigitoVerificadorDelVendedor());
		customerHubModelEntity.setNumeroDeSerieDelCertificado(cda.getNumeroDeSerieDelCertificado());
		customerHubModelEntity.setSelloDigital(cda.getSelloDigital());
		customerHubModelEntity.setStrSpeiOperationDate(cda.getStrSpeiOperationDate());
		customerHubModelEntity.setStrCalenderDatePayment(cda.getStrCalenderDatePayment());
		customerHubModelEntity.setSpeiOperationDateFormat(cda.getSpeiOperationDateFormat());
		customerHubModelEntity.setOriginalString(cda.getOriginalString());
		customerHubModelEntity.setCDA(cda.getCDA());
		customerHubModelEntity.setCalenderTimePaymentMS(cda.getCalenderTimePaymentMS());
		customerHubModelEntity.setIdTransaccion(cda.getIdTransaccion());
		customerHubModelEntity.setFolioCDA(123L);
		customerHubModelEntity.setOriginalPaymentFolio(cda.getOriginalPaymentFolio());
		customerHubModelEntity.setOriginalFolioPackage(cda.getOriginalFolioPackage());

		
		
		customerhubModelRepository.save(customerHubModelEntity);

	}


	@Override
	public Optional<CustomerHubModelEntity> findById(CustomerHubModelId id) {
		return customerhubModelRepository.findById(id);
		
	}

}
