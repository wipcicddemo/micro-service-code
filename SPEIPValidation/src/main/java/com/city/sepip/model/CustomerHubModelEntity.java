package com.city.sepip.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "CB_SPEI_CDA")
public class CustomerHubModelEntity {
	
	//@JsonProperty("Clave de Rastreo")
//	@Id
//	@Column(name = "TRACKINGKEY")
//	public String trackingKey;
//	
//	//@JsonProperty("Fecha de operación del SPEI")    
//	@Column(name = "SPEIOPERATIONDATE")
//	public Integer speiOperationDate;
	
	@EmbeddedId
	@AttributeOverrides({
        @AttributeOverride(name = "trackingKey", column = @Column(name = "TRACKINGKEY")),                       
        @AttributeOverride(name = "speiOperationDate", column = @Column(name = "SPEIOPERATIONDATE"))
    })
	CustomerHubModelId customerHubModelId;
	
	//@JsonProperty("Tipo de pago")
	@Column(name = "PAYMENTTYPE")
	public String paymentType; 
	
	
	
	//@JsonProperty("Fecha calendario del abono")
	@Column(name = "CALENDERDATEPAYMENT")
	public Integer calenderDatePayment;
	
	//@JsonProperty("Hora calendario del abono")
	@Column(name = "CALENDERTIMEPAYMENT")
	public String calenderTimePayment;
	
	//@JsonProperty("Clave SPEI del Participante Emisor de la CDA")
	@Column(name = "CDAPARTICIPANTSPEIKEY")
	public Integer cdaParticipientSPEIKey;
	
	//@JsonProperty("Nombre del Participante Emisor de la Orden de Transferencia")
	@Column(name = "TRNSORDRISSRPARTNAME")
	public String nameOfTransferOrderIssuerParticipant;
	
	//@JsonProperty("Nombre Ordenante")
	@Column(name = "ORDERINGNAME")
	public String orderingName;
	
	//@JsonProperty("Tipo de Cuenta Ordenante")
	@Column(name = "ORDERERACCOUNTTYPE")
	public Integer ordererAccountType;
	
	//@JsonProperty("Cuenta Ordenante")
	@Column(name = "ORDERINGACCOUNT")
	public String orderingAccount;
	
	//@JsonProperty("RFC o CURP Ordenante")
	@Column(name = "RFCORCURPPAYER")
	public String rfcOrCurpPayer;
	
	//@JsonProperty("Tipo de Cuenta Beneficiario")
	@Column(name = "TYPEOFBENEFICIARYACCOUNT")
	public Integer typeOfBeneficiaryAccount;
	
	//@JsonProperty("Concepto del Pago")
	@Column(name = "CONCEPTOFPAYMENT")
	public String conceptOfPayment;
	
	//@JsonProperty("Importe del IVA")
	@Column(name = "IVAAMOUNT")
	public BigDecimal ivaAmount;
	
	//@JsonProperty("Monto del pago")
	@Column(name = "AMOUNTOFPAYMENT")
	public BigDecimal amountOfPayment;
	
	//@JsonProperty("Tipo de Cuenta Beneficiario 2")
	@Column(name = "BENEFICIARYACCOUNTTYPE2")
	public Integer BeneficiaryAccountType2;
	
	//@JsonProperty("Folio del Esquema Cobro Digital")
	@Column(name = "DIGITALSCHEMEFOLIO")
	public String digitalCollectionSchemeFolio;
	
	//@JsonProperty("Pago de la comisión por la transferencia")
	@Column(name = "PAYMENTOFTHETRANSFERFEE")
	public Integer paymentOfTheTransferFee;
	
	//@JsonProperty("Monto de la comisión por la transferencia")
	@Column(name = "TRANSFERFEEAMOUNT")
	public BigDecimal transferFeeAmount;
	
	//@JsonProperty("Alias del número celular del comprador")
	@Column(name = "BUYERCELLNUMBERALIASES")
	public String buyerCellNumberAliases;
	
	//@JsonProperty("Digito verificador del comprador")
	@Column(name = "DIGITBUYERSVERIFIER")
	public String  digitBuyersVerifier;
		
	//Datos del beneficiario
	//@JsonProperty("Nombre del participante receptor")
	@Column(name = "RECEIVERPARTICIPANTNAME")
	public String mombreDelParticipanteReceptor;
	
	//@JsonProperty("Nombre del beneficiario")
	@Column(name = "BENEFICIARYNAME")
	public String nombreBeneficiario;
	
	//@JsonProperty("Cuenta del beneficiario ")
	@Column(name = "BENEFICIARYACCOUNT")
	public String cuentaBeneficiario;
	
	//@JsonProperty("RFC/CURP del beneficiario")
	@Column(name = "RFCORCURPBENEFICIARY")
	public String rFCOCurpBeneficiario;
	
	//@JsonProperty("Nombre del beneficiario 2 ")
	@Column(name = "BENEFICIARYNAME2")
	public String nombreBeneficiario2;
	
	//@JsonProperty("RFC/CURP del beneficiario 2")
	@Column(name = "RFCORCURPBENEFICIARY2")
	public String rFCOCurpBeneficiario2;
	
	//@JsonProperty("Cuenta del beneficiario 2")
	@Column(name = "BENEFICIARYACCOUNT2")
	public String cuentaBeneficiario2;
	
	//@JsonProperty("Alias Del Numero Celular Del Vendedor")
	@Column(name = "SELLERPHONENUMBERALIAS")
	public String aliasDelNumeroCelularDelVendedor;
	
	//@JsonProperty("Digito Verificador Del Vendedor")
	@Column(name = "SELLERVERIFIERNUMBER")
	public String digitoVerificadorDelVendedor;
	
	//@JsonProperty("Numero De Serie Del Certificado")
	@Column(name = "CERTIFICATESERIALNUMBER")
	public String numeroDeSerieDelCertificado;
	
	@Column(name = "DIGITALSTAMP")
	public String selloDigital;
	
	@Column(name = "STRSPEIOPERATIONDATE")
	public String strSpeiOperationDate;
	
	@Column(name = "STRCALENDERDATEPAYMENT")
	public String strCalenderDatePayment;
	
	@Column(name = "SPEIOPERATIONDATEFORMAT")
	public Date speiOperationDateFormat;
	
	@Column(name = "ORIGINALSTRING")
	public String originalString;
	
	@Column(name = "CDA")
	public String CDA;
	
	@Column(name = "CALENDERTIMEPAYMENTMS")
	public Integer calenderTimePaymentMS;
	
	//@JsonProperty("ID de la transacción")
	@Column(name = "IDTRANSACCION")
	public String idTransaccion;
	
	@Column(name = "FOLIOCDA")
	public long folioCDA;
	
	//@JsonProperty("originalPaymentFolio")
	@Column(name = "TRANSACTIONINVOICE")
	public Integer originalPaymentFolio;
	
	//@JsonProperty("originalFolioPackage")
	@Column(name = "PACKAGEINVOICE")
	public Integer originalFolioPackage;
	
	@Column(name = "ERRORCODE")
	public String errorCode;
	
	@Column(name = "ERRORMESSAGE")
	public String errorMessage;
	
	@Column(name = "ERRORCODE2")
	public String errorCode2;
	
	@Column(name = "ERRORMESSAGE2")
	public String errorMessage2;
	
	public CustomerHubModelEntity() {
		
	}

	public CustomerHubModelId getCustomerHubModelId() {
		return customerHubModelId;
	}

	public void setCustomerHubModelId(CustomerHubModelId customerHubModelId) {
		this.customerHubModelId = customerHubModelId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getCalenderDatePayment() {
		return calenderDatePayment;
	}

	public void setCalenderDatePayment(Integer calenderDatePayment) {
		this.calenderDatePayment = calenderDatePayment;
	}

	public String getCalenderTimePayment() {
		return calenderTimePayment;
	}

	public void setCalenderTimePayment(String calenderTimePayment) {
		this.calenderTimePayment = calenderTimePayment;
	}

	public Integer getCdaParticipientSPEIKey() {
		return cdaParticipientSPEIKey;
	}

	public void setCdaParticipientSPEIKey(Integer cdaParticipientSPEIKey) {
		this.cdaParticipientSPEIKey = cdaParticipientSPEIKey;
	}

	public String getNameOfTransferOrderIssuerParticipant() {
		return nameOfTransferOrderIssuerParticipant;
	}

	public void setNameOfTransferOrderIssuerParticipant(String nameOfTransferOrderIssuerParticipant) {
		this.nameOfTransferOrderIssuerParticipant = nameOfTransferOrderIssuerParticipant;
	}

	public String getOrderingName() {
		return orderingName;
	}

	public void setOrderingName(String orderingName) {
		this.orderingName = orderingName;
	}

	public Integer getOrdererAccountType() {
		return ordererAccountType;
	}

	public void setOrdererAccountType(Integer ordererAccountType) {
		this.ordererAccountType = ordererAccountType;
	}

	public String getOrderingAccount() {
		return orderingAccount;
	}

	public void setOrderingAccount(String orderingAccount) {
		this.orderingAccount = orderingAccount;
	}

	public String getRfcOrCurpPayer() {
		return rfcOrCurpPayer;
	}

	public void setRfcOrCurpPayer(String rfcOrCurpPayer) {
		this.rfcOrCurpPayer = rfcOrCurpPayer;
	}

	public Integer getTypeOfBeneficiaryAccount() {
		return typeOfBeneficiaryAccount;
	}

	public void setTypeOfBeneficiaryAccount(Integer typeOfBeneficiaryAccount) {
		this.typeOfBeneficiaryAccount = typeOfBeneficiaryAccount;
	}

	public String getConceptOfPayment() {
		return conceptOfPayment;
	}

	public void setConceptOfPayment(String conceptOfPayment) {
		this.conceptOfPayment = conceptOfPayment;
	}

	public BigDecimal getIvaAmount() {
		return ivaAmount;
	}

	public void setIvaAmount(BigDecimal ivaAmount) {
		this.ivaAmount = ivaAmount;
	}

	public BigDecimal getAmountOfPayment() {
		return amountOfPayment;
	}

	public void setAmountOfPayment(BigDecimal amountOfPayment) {
		this.amountOfPayment = amountOfPayment;
	}

	public Integer getBeneficiaryAccountType2() {
		return BeneficiaryAccountType2;
	}

	public void setBeneficiaryAccountType2(Integer beneficiaryAccountType2) {
		BeneficiaryAccountType2 = beneficiaryAccountType2;
	}

	public String getDigitalCollectionSchemeFolio() {
		return digitalCollectionSchemeFolio;
	}

	public void setDigitalCollectionSchemeFolio(String digitalCollectionSchemeFolio) {
		this.digitalCollectionSchemeFolio = digitalCollectionSchemeFolio;
	}

	public Integer getPaymentOfTheTransferFee() {
		return paymentOfTheTransferFee;
	}

	public void setPaymentOfTheTransferFee(Integer paymentOfTheTransferFee) {
		this.paymentOfTheTransferFee = paymentOfTheTransferFee;
	}

	public BigDecimal getTransferFeeAmount() {
		return transferFeeAmount;
	}

	public void setTransferFeeAmount(BigDecimal transferFeeAmount) {
		this.transferFeeAmount = transferFeeAmount;
	}

	public String getBuyerCellNumberAliases() {
		return buyerCellNumberAliases;
	}

	public void setBuyerCellNumberAliases(String buyerCellNumberAliases) {
		this.buyerCellNumberAliases = buyerCellNumberAliases;
	}

	public String getDigitBuyersVerifier() {
		return digitBuyersVerifier;
	}

	public void setDigitBuyersVerifier(String digitBuyersVerifier) {
		this.digitBuyersVerifier = digitBuyersVerifier;
	}

	public String getMombreDelParticipanteReceptor() {
		return mombreDelParticipanteReceptor;
	}

	public void setMombreDelParticipanteReceptor(String mombreDelParticipanteReceptor) {
		this.mombreDelParticipanteReceptor = mombreDelParticipanteReceptor;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getCuentaBeneficiario() {
		return cuentaBeneficiario;
	}

	public void setCuentaBeneficiario(String cuentaBeneficiario) {
		this.cuentaBeneficiario = cuentaBeneficiario;
	}

	public String getrFCOCurpBeneficiario() {
		return rFCOCurpBeneficiario;
	}

	public void setrFCOCurpBeneficiario(String rFCOCurpBeneficiario) {
		this.rFCOCurpBeneficiario = rFCOCurpBeneficiario;
	}

	public String getNombreBeneficiario2() {
		return nombreBeneficiario2;
	}

	public void setNombreBeneficiario2(String nombreBeneficiario2) {
		this.nombreBeneficiario2 = nombreBeneficiario2;
	}

	public String getrFCOCurpBeneficiario2() {
		return rFCOCurpBeneficiario2;
	}

	public void setrFCOCurpBeneficiario2(String rFCOCurpBeneficiario2) {
		this.rFCOCurpBeneficiario2 = rFCOCurpBeneficiario2;
	}

	public String getCuentaBeneficiario2() {
		return cuentaBeneficiario2;
	}

	public void setCuentaBeneficiario2(String cuentaBeneficiario2) {
		this.cuentaBeneficiario2 = cuentaBeneficiario2;
	}

	public String getAliasDelNumeroCelularDelVendedor() {
		return aliasDelNumeroCelularDelVendedor;
	}

	public void setAliasDelNumeroCelularDelVendedor(String aliasDelNumeroCelularDelVendedor) {
		this.aliasDelNumeroCelularDelVendedor = aliasDelNumeroCelularDelVendedor;
	}

	public String getDigitoVerificadorDelVendedor() {
		return digitoVerificadorDelVendedor;
	}

	public void setDigitoVerificadorDelVendedor(String digitoVerificadorDelVendedor) {
		this.digitoVerificadorDelVendedor = digitoVerificadorDelVendedor;
	}

	public String getNumeroDeSerieDelCertificado() {
		return numeroDeSerieDelCertificado;
	}

	public void setNumeroDeSerieDelCertificado(String numeroDeSerieDelCertificado) {
		this.numeroDeSerieDelCertificado = numeroDeSerieDelCertificado;
	}

	public String getSelloDigital() {
		return selloDigital;
	}

	public void setSelloDigital(String selloDigital) {
		this.selloDigital = selloDigital;
	}

	public String getStrSpeiOperationDate() {
		return strSpeiOperationDate;
	}

	public void setStrSpeiOperationDate(String strSpeiOperationDate) {
		this.strSpeiOperationDate = strSpeiOperationDate;
	}

	public String getStrCalenderDatePayment() {
		return strCalenderDatePayment;
	}

	public void setStrCalenderDatePayment(String strCalenderDatePayment) {
		this.strCalenderDatePayment = strCalenderDatePayment;
	}

	public Date getSpeiOperationDateFormat() {
		return speiOperationDateFormat;
	}

	public void setSpeiOperationDateFormat(Date speiOperationDateFormat) {
		this.speiOperationDateFormat = speiOperationDateFormat;
	}

	public String getOriginalString() {
		return originalString;
	}

	public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}

	public String getCDA() {
		return CDA;
	}

	public void setCDA(String cDA) {
		CDA = cDA;
	}

	public Integer getCalenderTimePaymentMS() {
		return calenderTimePaymentMS;
	}

	public void setCalenderTimePaymentMS(Integer calenderTimePaymentMS) {
		this.calenderTimePaymentMS = calenderTimePaymentMS;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public long getFolioCDA() {
		return folioCDA;
	}

	public void setFolioCDA(long folioCDA) {
		this.folioCDA = folioCDA;
	}

	public Integer getOriginalPaymentFolio() {
		return originalPaymentFolio;
	}

	public void setOriginalPaymentFolio(Integer originalPaymentFolio) {
		this.originalPaymentFolio = originalPaymentFolio;
	}

	public Integer getOriginalFolioPackage() {
		return originalFolioPackage;
	}

	public void setOriginalFolioPackage(Integer originalFolioPackage) {
		this.originalFolioPackage = originalFolioPackage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode2() {
		return errorCode2;
	}

	public void setErrorCode2(String errorCode2) {
		this.errorCode2 = errorCode2;
	}

	public String getErrorMessage2() {
		return errorMessage2;
	}

	public void setErrorMessage2(String errorMessage2) {
		this.errorMessage2 = errorMessage2;
	}

	@Override
	public String toString() {
		return "CustomerHubModelEntity [customerHubModelId=" + customerHubModelId + ", paymentType=" + paymentType
				+ ", calenderDatePayment=" + calenderDatePayment + ", calenderTimePayment=" + calenderTimePayment
				+ ", cdaParticipientSPEIKey=" + cdaParticipientSPEIKey + ", nameOfTransferOrderIssuerParticipant="
				+ nameOfTransferOrderIssuerParticipant + ", orderingName=" + orderingName + ", ordererAccountType="
				+ ordererAccountType + ", orderingAccount=" + orderingAccount + ", rfcOrCurpPayer=" + rfcOrCurpPayer
				+ ", typeOfBeneficiaryAccount=" + typeOfBeneficiaryAccount + ", conceptOfPayment=" + conceptOfPayment
				+ ", ivaAmount=" + ivaAmount + ", amountOfPayment=" + amountOfPayment + ", BeneficiaryAccountType2="
				+ BeneficiaryAccountType2 + ", digitalCollectionSchemeFolio=" + digitalCollectionSchemeFolio
				+ ", paymentOfTheTransferFee=" + paymentOfTheTransferFee + ", transferFeeAmount=" + transferFeeAmount
				+ ", buyerCellNumberAliases=" + buyerCellNumberAliases + ", digitBuyersVerifier=" + digitBuyersVerifier
				+ ", mombreDelParticipanteReceptor=" + mombreDelParticipanteReceptor + ", nombreBeneficiario="
				+ nombreBeneficiario + ", cuentaBeneficiario=" + cuentaBeneficiario + ", rFCOCurpBeneficiario="
				+ rFCOCurpBeneficiario + ", nombreBeneficiario2=" + nombreBeneficiario2 + ", rFCOCurpBeneficiario2="
				+ rFCOCurpBeneficiario2 + ", cuentaBeneficiario2=" + cuentaBeneficiario2
				+ ", aliasDelNumeroCelularDelVendedor=" + aliasDelNumeroCelularDelVendedor
				+ ", digitoVerificadorDelVendedor=" + digitoVerificadorDelVendedor + ", numeroDeSerieDelCertificado="
				+ numeroDeSerieDelCertificado + ", selloDigital=" + selloDigital + ", strSpeiOperationDate="
				+ strSpeiOperationDate + ", strCalenderDatePayment=" + strCalenderDatePayment
				+ ", speiOperationDateFormat=" + speiOperationDateFormat + ", originalString=" + originalString
				+ ", CDA=" + CDA + ", calenderTimePaymentMS=" + calenderTimePaymentMS + ", idTransaccion="
				+ idTransaccion + ", folioCDA=" + folioCDA + ", originalPaymentFolio=" + originalPaymentFolio
				+ ", originalFolioPackage=" + originalFolioPackage + ", errorCode=" + errorCode + ", errorMessage="
				+ errorMessage + ", errorCode2=" + errorCode2 + ", errorMessage2=" + errorMessage2 + "]";
	}
	
	

	

	

}
