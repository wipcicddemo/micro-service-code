package com.city.sepip.model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ErrorDetail {
	String uuid;
	String type;
	String message;
	Map<String, String> params;
	String bussinesCode;
	String location;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	LocalDateTime timestamp;
	Map<String, String> aditionalInformation;
	
	public ErrorDetail() {
		timestamp = LocalDateTime.now();
	}
	
	
	public ErrorDetail(String uuid, String type, String message, Map<String, String> params, String bussinesCode,
			String location, Map<String, String> aditionalInformation) {
		super();
		this.uuid = uuid;
		this.type = type;
		this.message = message;
		this.params = params;
		this.bussinesCode = bussinesCode;
		this.location = location;
		this.aditionalInformation = aditionalInformation;
	}


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Map<String, String> getParams() {
		return params;
	}


	public void setParams(Map<String, String> params) {
		this.params = params;
	}


	public String getBussinesCode() {
		return bussinesCode;
	}


	public void setBussinesCode(String bussinesCode) {
		this.bussinesCode = bussinesCode;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public LocalDateTime getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}


	public Map<String, String> getAditionalInformation() {
		return aditionalInformation;
	}


	public void setAditionalInformation(Map<String, String> aditionalInformation) {
		this.aditionalInformation = aditionalInformation;
	}
	
	
	
	

}
