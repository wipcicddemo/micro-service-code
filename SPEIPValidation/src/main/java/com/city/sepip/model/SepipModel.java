/**
 *
 */
package com.city.sepip.model;

import java.math.BigDecimal;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author SU20053232
 *
 */
public class SepipModel {

	
	@JsonProperty("trackingKey")
	@NotNull(message = "trackingKey: Clave de Rastreo es requerido")
	public String trackingKey;

	@JsonProperty("paymentType")
	@NotNull(message = "paymentType: Tipo de pago es requerido")
	public String paymentType;

	@JsonProperty("speiOperationDate")
	@NotNull(message = "speiOperationDate: Fecha de operación del SPEI es requerido")
	public Integer speiOperationDate;

	@JsonProperty("paymentCalendarDate")
	@NotNull(message = "paymentCalendarDate: Fecha calendario del abono es requerido")
	public Integer calenderDatePayment;

	@JsonProperty("paymentCalendarTime")
	@NotNull(message = "paymentCalendarTime: Hora calendario del abono es requerido")
	public String calenderTimePayment;

	@JsonProperty("cdaParticipantSPEIKey")
	@NotNull(message = "cdaParticipantSPEIKey: Clave SPEI del Participante Emisor de la CDA es requerido")
	public Integer cdaParticipientSPEIKey;

	@JsonProperty("transferOrderIssuerParticipantName")
	@NotNull(message = "transferOrderIssuerParticipantName: Nombre del Participante Emisor de la Orden de Transferencia es requerido")
	public String nameOfTransferOrderIssuerParticipant;

	@JsonProperty("payerName")
	@NotNull(message = "payerName: Nombre Ordenante es requerido")
	public String orderingName;

	@JsonProperty("payerAccountType")
	@NotNull(message = "payerAccountType: Tipo de Cuenta Ordenante es requerido")
	public Integer ordererAccountType;

	@JsonProperty("payerAccount")
	@NotNull(message = "payerAccount: Cuenta Ordenante es requerido")
	public String orderingAccount;

	@JsonProperty("payerRfcOrCurp")
	@NotNull(message = "payerRfcOrCurp: RFC o CURP Ordenante es requerido")
	public String rfcOrCurpPayer;

	@JsonProperty("beneficiaryAccountType")
	@NotNull(message = "beneficiaryAccountType: Tipo de Cuenta Beneficiario es requerido")
	public Integer typeOfBeneficiaryAccount;

	@JsonProperty("paymentConcept")
	@NotNull(message = "paymentConcept: Concepto del Pago es requerido")
	public String conceptOfPayment;

	@JsonProperty("ivaAmount")
	@NotNull(message = "ivaAmount: Importe del IVA es requerido")
	public BigDecimal ivaAmount;

	@JsonProperty("paymentAmount")
	@NotNull(message = "paymentAmount: Monto del pago es requerido")
	public BigDecimal amountOfPayment;

	@JsonProperty("beneficiaryTwoAccountType")
	@NotNull(message = "beneficiaryTwoAccountType: Tipo de Cuenta Beneficiario 2 es requerido")
	public Integer beneficiaryAccountType2;

	@JsonProperty("digitalPaymentSchemeFolio")
	@NotNull(message = "digitalPaymentSchemeFolio: Folio del Esquema Cobro Digital es requerido")
	public String digitalCollectionSchemeFolio;

	@JsonProperty("paymentOfTheTransferFee")
	@NotNull(message = "paymentOfTheTransferFee: Pago de la comisión por la transferencia es requerido")
	public Integer paymentOfTheTransferFee;

	@JsonProperty("transferFeeAmount")
	@NotNull(message = "transferFeeAmount: Monto de la comisión por la transferencia es requerido")
	public BigDecimal transferFeeAmount;

	@JsonProperty("buyerCellNumberLabel")
	@NotNull(message = "buyerCellNumberLabel: Alias del número celular del comprador es requerido")
	public String buyerCellNumberAliases;

	@JsonProperty("buyerVerificationDigit")
	@NotNull(message = "buyerVerificationDigit: Digito verificador del comprador es requerido")
	public String  digitBuyersVerifier;
	
	@JsonProperty("idTransaccion")
	//@NotNull(message = "ID de la transacción es requerido")
	public String  idTransaccion;
	

		
	//Datos del beneficiario
	@JsonProperty("receiverParticipantName")
	@NotNull(message = "receiverParticipantName: Nombre del participante receptor es requerido")
	public String mombreDelParticipanteReceptor;
	
	@JsonProperty("beneficiaryName")
	@NotNull(message = "beneficiaryName: Nombre del beneficiario es requerido")
	public String nombreBeneficiario;
	
	@JsonProperty("beneficiaryAccount")
	@NotNull(message = "beneficiaryAccount: Cuenta del beneficiario es requerido")
	public String cuentaBeneficiario;
	
	@JsonProperty("beneficiaryRfcOrCurp")
	@NotNull(message = "beneficiaryRfcOrCurp: RFC/CURP del beneficiario es requerido")
	public String rFCOCurpBeneficiario;
	
	@JsonProperty("beneficiaryTwoName")
	@NotNull(message = "beneficiaryTwoName: Nombre del beneficiario 2 es requerido")
	public String nombreBeneficiario2;
	
	@JsonProperty("beneficiaryTwoRfcOrCurp")
	@NotNull(message = "beneficiaryTwoRfcOrCurp: RFC/CURP del beneficiario 2 es requerido")
	public String rFCOCurpBeneficiario2;
	
	@JsonProperty("beneficiaryTwoAccount")
	@NotNull(message = "beneficiaryTwoAccount: Cuenta del beneficiario 2 es requerido")
	public String cuentaBeneficiario2;
	
	@JsonProperty("sellerCellNumberLabel")
	@NotNull(message = "sellerCellNumberLabel: Alias Del Numero Celular Del Vendedor es requerido")
	public String aliasDelNumeroCelularDelVendedor;
	
	@JsonProperty("sellerVerificationDigit")
	@NotNull(message = "sellerVerificationDigit: Digito Verificador Del Vendedor es requerido")
	public String digitoVerificadorDelVendedor;
	
	@JsonProperty("digitalStampSerialNumber")
	@NotNull(message = "digitalStampSerialNumber: Numero De Serie Del Certificado es requerido")
	public String numeroDeSerieDelCertificado;
	
	@JsonProperty("originalPaymentFolio")
	@NotNull(message = "originalPaymentFolio: Folio del pago original es requerido")
	@Max(99999)
	public Integer originalPaymentFolio;
	
	@JsonProperty("originalFolioPackage")
	@NotNull(message = "originalFolioPackage: Folio de pacquete original es requerido")
	@Max(9999999)
	public Integer originalFolioPackage;
	
	
	
	

	public SepipModel(@NotNull(message = "trackingKey: Clave de Rastreo es requerido") String trackingKey,
			@NotNull(message = "paymentType: Tipo de pago es requerido") String paymentType,
			@NotNull(message = "speiOperationDate: Fecha de operación del SPEI es requerido") Integer speiOperationDate,
			@NotNull(message = "paymentCalendarDate: Fecha calendario del abono es requerido") Integer calenderDatePayment,
			@NotNull(message = "paymentCalendarTime: Hora calendario del abono es requerido") String calenderTimePayment,
			@NotNull(message = "cdaParticipientSPEIKey: Clave SPEI del Participante Emisor de la CDA es requerido") Integer cdaParticipientSPEIKey,
			@NotNull(message = "transferOrderIssuerParticipantName: Nombre del Participante Emisor de la Orden de Transferencia es requerido") String nameOfTransferOrderIssuerParticipant,
			@NotNull(message = "payerName: Nombre Ordenante es requerido") String orderingName,
			@NotNull(message = "payerAccountType: Tipo de Cuenta Ordenante es requerido") Integer ordererAccountType,
			@NotNull(message = "payerAccount: Cuenta Ordenante es requerido") String orderingAccount,
			@NotNull(message = "payerRfcOrCurp: RFC o CURP Ordenante es requerido") String rfcOrCurpPayer,
			@NotNull(message = "beneficiaryAccountType: Tipo de Cuenta Beneficiario es requerido") Integer typeOfBeneficiaryAccount,
			@NotNull(message = "paymentConcept: Concepto del Pago es requerido") String conceptOfPayment,
			@NotNull(message = "ivaAmount: Importe del IVA es requerido") BigDecimal ivaAmount,
			@NotNull(message = "paymentAmount: Monto del pago es requerido") BigDecimal amountOfPayment,
			@NotNull(message = "beneficiaryTwoAccountType: Tipo de Cuenta Beneficiario 2 es requerido") Integer beneficiaryAccountType2,
			@NotNull(message = "digitalPaymentSchemeFolio: Folio del Esquema Cobro Digital es requerido") String digitalCollectionSchemeFolio,
			@NotNull(message = "paymentOfTheTransferFee: Pago de la comisión por la transferencia es requerido") Integer paymentOfTheTransferFee,
			@NotNull(message = "transferFeeAmount: Monto de la comisión por la transferencia es requerido") BigDecimal transferFeeAmount,
			@NotNull(message = "buyerCellNumberLabel: Alias del número celular del comprador es requerido") String buyerCellNumberAliases,
			@NotNull(message = "buyerVerificationDigit: Digito verificador del comprador es requerido") String digitBuyersVerifier,
			String idTransaccion,
			@NotNull(message = "receiverParticipantName: Nombre del participante receptor es requerido") String mombreDelParticipanteReceptor,
			@NotNull(message = "beneficiaryName: Nombre del beneficiario es requerido") String nombreBeneficiario,
			@NotNull(message = "beneficiaryAccount: Cuenta del beneficiario es requerido") String cuentaBeneficiario,
			@NotNull(message = "beneficiaryRfcOrCurp: RFC/CURP del beneficiario es requerido") String rFCOCurpBeneficiario,
			@NotNull(message = "beneficiaryTwoName: Nombre del beneficiario 2 es requerido") String nombreBeneficiario2,
			@NotNull(message = "beneficiaryTwoRfcOrCurp: RFC/CURP del beneficiario 2 es requerido") String rFCOCurpBeneficiario2,
			@NotNull(message = "beneficiaryTwoAccount: Cuenta del beneficiario 2 es requerido") String cuentaBeneficiario2,
			@NotNull(message = "sellerCellNumberLabel: Alias Del Numero Celular Del Vendedor es requerido") String aliasDelNumeroCelularDelVendedor,
			@NotNull(message = "sellerVerificationDigit: Digito Verificador Del Vendedor es requerido") String digitoVerificadorDelVendedor,
			@NotNull(message = "digitalStampSerialNumber: Numero De Serie Del Certificado es requerido") String numeroDeSerieDelCertificado,
			@NotNull(message = "originalPaymentFolio: Folio del pago original es requerido") @Max(99999) Integer originalPaymentFolio,
			@NotNull(message = "originalFolioPackage: Folio de pacquete original es requerido") @Max(9999999) Integer originalFolioPackage) {
		super();
		this.trackingKey = trackingKey;
		this.paymentType = paymentType;
		this.speiOperationDate = speiOperationDate;
		this.calenderDatePayment = calenderDatePayment;
		this.calenderTimePayment = calenderTimePayment;
		this.cdaParticipientSPEIKey = cdaParticipientSPEIKey;
		this.nameOfTransferOrderIssuerParticipant = nameOfTransferOrderIssuerParticipant;
		this.orderingName = orderingName;
		this.ordererAccountType = ordererAccountType;
		this.orderingAccount = orderingAccount;
		this.rfcOrCurpPayer = rfcOrCurpPayer;
		this.typeOfBeneficiaryAccount = typeOfBeneficiaryAccount;
		this.conceptOfPayment = conceptOfPayment;
		this.ivaAmount = ivaAmount;
		this.amountOfPayment = amountOfPayment;
		this.beneficiaryAccountType2 = beneficiaryAccountType2;
		this.digitalCollectionSchemeFolio = digitalCollectionSchemeFolio;
		this.paymentOfTheTransferFee = paymentOfTheTransferFee;
		this.transferFeeAmount = transferFeeAmount;
		this.buyerCellNumberAliases = buyerCellNumberAliases;
		this.digitBuyersVerifier = digitBuyersVerifier;
		this.idTransaccion = idTransaccion;
		this.mombreDelParticipanteReceptor = mombreDelParticipanteReceptor;
		this.nombreBeneficiario = nombreBeneficiario;
		this.cuentaBeneficiario = cuentaBeneficiario;
		this.rFCOCurpBeneficiario = rFCOCurpBeneficiario;
		this.nombreBeneficiario2 = nombreBeneficiario2;
		this.rFCOCurpBeneficiario2 = rFCOCurpBeneficiario2;
		this.cuentaBeneficiario2 = cuentaBeneficiario2;
		this.aliasDelNumeroCelularDelVendedor = aliasDelNumeroCelularDelVendedor;
		this.digitoVerificadorDelVendedor = digitoVerificadorDelVendedor;
		this.numeroDeSerieDelCertificado = numeroDeSerieDelCertificado;
		this.originalPaymentFolio = originalPaymentFolio;
		this.originalFolioPackage = originalFolioPackage;
	}

	public String getTrackingKey() {
		return trackingKey;
	}

	public void setTrackingKey(String trackingKey) {
		this.trackingKey = trackingKey;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public int getSpeiOperationDate() {
		return speiOperationDate;
	}

	public void setSpeiOperationDate(int speiOperationDate) {
		this.speiOperationDate = speiOperationDate;
	}

	public int getCalenderDatePayment() { return calenderDatePayment;}

	public void setCalenderDatePayment(int calenderDatePayment) {
		this.calenderDatePayment = calenderDatePayment;
	}

	public String getCalenderTimePayment() {
		return calenderTimePayment;
	}

	public void setCalenderTimePayment(String calenderTimePayment) {
		this.calenderTimePayment = calenderTimePayment;
	}

	public int getCdaParticipientSPEIKey() {
		return cdaParticipientSPEIKey;
	}

	public void setCdaParticipientSPEIKey(int cdaParticipientSPEIKey) {
		this.cdaParticipientSPEIKey = cdaParticipientSPEIKey;
	}

	public String getNameOfTransferOrderIssuerParticipant() {
		return nameOfTransferOrderIssuerParticipant;
	}

	public void setNameOfTransferOrderIssuerParticipant(String nameOfTransferOrderIssuerParticipant) {
		this.nameOfTransferOrderIssuerParticipant = nameOfTransferOrderIssuerParticipant;
	}

	public String getOrderingName() {
		return orderingName;
	}

	public void setOrderingName(String orderingName) {
		this.orderingName = orderingName;
	}

	public int getOrdererAccountType() {
		return ordererAccountType;
	}

	public void setOrdererAccountType(int ordererAccountType) {
		this.ordererAccountType = ordererAccountType;
	}

	public String getOrderingAccount() {
		return orderingAccount;
	}

	public void setOrderingAccount(String orderingAccount) {
		this.orderingAccount = orderingAccount;
	}

	public String getRfcOrCurpPayer() {
		return rfcOrCurpPayer;
	}

	public void setRfcOrCurpPayer(String rfcOrCurpPayer) {
		this.rfcOrCurpPayer = rfcOrCurpPayer;
	}

	public int getTypeOfBeneficiaryAccount() {
		return typeOfBeneficiaryAccount;
	}

	public void setTypeOfBeneficiaryAccount(int typeOfBeneficiaryAccount) {
		this.typeOfBeneficiaryAccount = typeOfBeneficiaryAccount;
	}

	public String getConceptOfPayment() {
		return conceptOfPayment;
	}

	public void setConceptOfPayment(String conceptOfPayment) {
		this.conceptOfPayment = conceptOfPayment;
	}

	public BigDecimal getIvaAmount() { return ivaAmount; }

	public void setIvaAmount(BigDecimal ivaAmount) {
		this.ivaAmount = ivaAmount;
	}

	public BigDecimal getAmountOfPayment() {
		return amountOfPayment;
	}

	public void setAmountOfPayment(BigDecimal amountOfPayment) {
		this.amountOfPayment = amountOfPayment;
	}

	public int getBeneficiaryAccountType2() {
		return beneficiaryAccountType2;
	}

	public void setBeneficiaryAccountType2(int beneficiaryAccountType2) {
		this.beneficiaryAccountType2 = beneficiaryAccountType2;
	}

	public String getDigitalCollectionSchemeFolio() {
		return digitalCollectionSchemeFolio;
	}

	public void setDigitalCollectionSchemeFolio(String digitalCollectionSchemeFolio) {
		this.digitalCollectionSchemeFolio = digitalCollectionSchemeFolio;
	}

	public int getPaymentOfTheTransferFee() {
		return paymentOfTheTransferFee;
	}

	public void setPaymentOfTheTransferFee(int paymentOfTheTransferFee) {
		this.paymentOfTheTransferFee = paymentOfTheTransferFee;
	}

	public BigDecimal getTransferFeeAmount() {
		return transferFeeAmount;
	}

	public void setTransferFeeAmount(BigDecimal transferFeeAmount) {
		this.transferFeeAmount = transferFeeAmount;
	}

	public String getBuyerCellNumberAliases() {
		return buyerCellNumberAliases;
	}

	public void setBuyerCellNumberAliases(String buyerCellNumberAliases) {
		this.buyerCellNumberAliases = buyerCellNumberAliases;
	}

	public String getDigitBuyersVerifier() {
		return digitBuyersVerifier;
	}

	public void setDigitBuyersVerifier(String digitBuyersVerifier) {
		this.digitBuyersVerifier = digitBuyersVerifier;
	}
	public String getMombreDelParticipanteReceptor() {
		return mombreDelParticipanteReceptor;
	}

	public void setMombreDelParticipanteReceptor(String mombreDelParticipanteReceptor) {
		this.mombreDelParticipanteReceptor = mombreDelParticipanteReceptor;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getCuentaBeneficiario() {
		return cuentaBeneficiario;
	}

	public void setCuentaBeneficiario(String cuentaBeneficiario) {
		this.cuentaBeneficiario = cuentaBeneficiario;
	}

	public String getrFCOCurpBeneficiario() {
		return rFCOCurpBeneficiario;
	}

	public void setrFCOCurpBeneficiario(String rFCOCurpBeneficiario) {
		this.rFCOCurpBeneficiario = rFCOCurpBeneficiario;
	}

	public String getNombreBeneficiario2() {
		return nombreBeneficiario2;
	}

	public void setNombreBeneficiario2(String nombreBeneficiario2) {
		this.nombreBeneficiario2 = nombreBeneficiario2;
	}

	public String getrFCOCurpBeneficiario2() {
		return rFCOCurpBeneficiario2;
	}

	public void setrFCOCurpBeneficiario2(String rFCOCurpBeneficiario2) {
		this.rFCOCurpBeneficiario2 = rFCOCurpBeneficiario2;
	}

	public String getCuentaBeneficiario2() {
		return cuentaBeneficiario2;
	}

	public void setCuentaBeneficiario2(String cuentaBeneficiario2) {
		this.cuentaBeneficiario2 = cuentaBeneficiario2;
	}

	public String getAliasDelNumeroCelularDelVendedor() {
		return aliasDelNumeroCelularDelVendedor;
	}

	public void setAliasDelNumeroCelularDelVendedor(String aliasDelNumeroCelularDelVendedor) {
		this.aliasDelNumeroCelularDelVendedor = aliasDelNumeroCelularDelVendedor;
	}

	public String getDigitoVerificadorDelVendedor() {
		return digitoVerificadorDelVendedor;
	}

	public void setDigitoVerificadorDelVendedor(String digitoVerificadorDelVendedor) {
		this.digitoVerificadorDelVendedor = digitoVerificadorDelVendedor;
	}
	
	public String getNumeroDeSerieDelCertificado() {
		return numeroDeSerieDelCertificado;
	}

	public void setNumeroDeSerieDelCertificado(String numeroDeSerieDelCertificado) {
		this.numeroDeSerieDelCertificado = numeroDeSerieDelCertificado;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}
	
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}


	public Integer getOriginalPaymentFolio() {
		return originalPaymentFolio;
	}


	public void setOriginalPaymentFolio(Integer originalPaymentFolio) {
		this.originalPaymentFolio = originalPaymentFolio;
	}


	public Integer getOriginalFolioPackage() {
		return originalFolioPackage;
	}


	public void setOriginalFolioPackage(Integer originalFolioPackage) {
		this.originalFolioPackage = originalFolioPackage;
	}
	
	

}
