/**
 * 
 */
package com.city.sepip.model;

import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author SU20053232
 *
 */
//@Document
public class SepipDB {
	
	@Id
	public String trackingKey;
	
	public CustomerHubModel customerHubModel;
	
	public SepipDB() {
		
	}
	
	public SepipDB(String trackingKey, CustomerHubModel customerHubModel) {
		super();
		this.trackingKey = trackingKey;
		this.customerHubModel = customerHubModel;
	}
	
	public String getTrackingKey() {
		return trackingKey;
	}

	public void setTrackingKey(String trackingKey) {
		this.trackingKey = trackingKey;
	}

	public CustomerHubModel getCustomerHubModel() {
		return customerHubModel;
	}

	public void setCustomerHubModel(CustomerHubModel customerHubModel) {
		this.customerHubModel = customerHubModel;
	}	
}
