/**
 * 
 */
package com.city.sepip.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author SU20053232
 *
 */
public class CustomerHubModel {
	
	@JsonProperty("Clave de Rastreo")
	public String trackingKey;
	
	@JsonProperty("Tipo de pago")
	public String paymentType; 
	
	@JsonProperty("Fecha de operación del SPEI")    
	public Integer speiOperationDate;
	
	@JsonProperty("Fecha calendario del abono")
	public Integer calenderDatePayment;
	
	@JsonProperty("Hora calendario del abono")
	public String calenderTimePayment;
	
	@JsonProperty("Clave SPEI del Participante Emisor de la CDA")
	public Integer cdaParticipientSPEIKey;
	
	@JsonProperty("Nombre del Participante Emisor de la Orden de Transferencia")
	public String nameOfTransferOrderIssuerParticipant;
	
	@JsonProperty("Nombre Ordenante")
	public String orderingName;
	
	@JsonProperty("Tipo de Cuenta Ordenante")
	public Integer ordererAccountType;
	
	@JsonProperty("Cuenta Ordenante")
	public String orderingAccount;
	
	@JsonProperty("RFC o CURP Ordenante")
	public String rfcOrCurpPayer;
	
	@JsonProperty("Tipo de Cuenta Beneficiario")
	public Integer typeOfBeneficiaryAccount;
	
	@JsonProperty("Concepto del Pago")
	public String conceptOfPayment;
	
	@JsonProperty("Importe del IVA")
	public BigDecimal ivaAmount;
	
	@JsonProperty("Monto del pago")
	public BigDecimal amountOfPayment;
	
	@JsonProperty("Tipo de Cuenta Beneficiario 2")
	public Integer BeneficiaryAccountType2;
	
	@JsonProperty("Folio del Esquema Cobro Digital")
	public String digitalCollectionSchemeFolio;
	
	@JsonProperty("Pago de la comisión por la transferencia")
	public Integer paymentOfTheTransferFee;
	
	@JsonProperty("Monto de la comisión por la transferencia")
	public BigDecimal transferFeeAmount;
	
	@JsonProperty("Alias del número celular del comprador")
	public String buyerCellNumberAliases;
	
	@JsonProperty("Digito verificador del comprador")
	public String  digitBuyersVerifier;
	
	
	//Datos del beneficiario
	@JsonProperty("Nombre del participante receptor")
	public String mombreDelParticipanteReceptor;
	
	@JsonProperty("Nombre del beneficiario")
	public String nombreBeneficiario;
	
	@JsonProperty("Cuenta del beneficiario ")
	public String cuentaBeneficiario;
	
	@JsonProperty("RFC/CURP del beneficiario")
	public String rFCOCurpBeneficiario;
	
	@JsonProperty("Nombre del beneficiario 2 ")
	public String nombreBeneficiario2;
	
	@JsonProperty("RFC/CURP del beneficiario 2")
	public String rFCOCurpBeneficiario2;
	
	@JsonProperty("Cuenta del beneficiario 2")
	public String cuentaBeneficiario2;
	
	@JsonProperty("Alias Del Numero Celular Del Vendedor")
	public String aliasDelNumeroCelularDelVendedor;
	
	@JsonProperty("Digito Verificador Del Vendedor")
	public String digitoVerificadorDelVendedor;
	
	@JsonProperty("Numero De Serie Del Certificado")
	public String numeroDeSerieDelCertificado;
	
	public String selloDigital;
	
	public String strSpeiOperationDate;
	
	public String strCalenderDatePayment;
	
	public Date speiOperationDateFormat;
	
	public String originalString;
	
	public String CDA;
	
	public Integer calenderTimePaymentMS;
	
	@JsonProperty("ID de la transacción")
	public String idTransaccion;
	
	public long folioCDA;
	
	@JsonProperty("originalPaymentFolio")
	public Integer originalPaymentFolio;
	
	@JsonProperty("originalFolioPackage")
	public Integer originalFolioPackage;
	

	public String getTrackingKey() {
		return trackingKey;
	}

	public void setTrackingKey(String trackingKey) {
		this.trackingKey = trackingKey;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public int getSpeiOperationDate() {
		return speiOperationDate;
	}

	public void setSpeiOperationDate(int speiOperationDate) {
		this.speiOperationDate = speiOperationDate;
	}

	public int getCalenderDatePayment() {
		return calenderDatePayment;
	}

	public void setCalenderDatePayment(int calenderDatePayment) {
		this.calenderDatePayment = calenderDatePayment;
	}

	public String getCalenderTimePayment() {
		return calenderTimePayment;
	}

	public void setCalenderTimePayment(String calenderTimePayment) {
		this.calenderTimePayment = calenderTimePayment;
	}

	public int getCdaParticipientSPEIKey() {
		return cdaParticipientSPEIKey;
	}

	public void setCdaParticipientSPEIKey(int cdaParticipientSPEIKey) {
		this.cdaParticipientSPEIKey = cdaParticipientSPEIKey;
	}

	public String getNameOfTransferOrderIssuerParticipant() {
		return nameOfTransferOrderIssuerParticipant;
	}

	public void setNameOfTransferOrderIssuerParticipant(String nameOfTransferOrderIssuerParticipant) {
		this.nameOfTransferOrderIssuerParticipant = nameOfTransferOrderIssuerParticipant;
	}

	public String getOrderingName() {
		return orderingName;
	}

	public void setOrderingName(String orderingName) {
		this.orderingName = orderingName;
	}

	public int getOrdererAccountType() {
		return ordererAccountType;
	}

	public void setOrdererAccountType(int ordererAccountType) {
		this.ordererAccountType = ordererAccountType;
	}

	public String getOrderingAccount() {
		return orderingAccount;
	}

	public void setOrderingAccount(String orderingAccount) {
		this.orderingAccount = orderingAccount;
	}

	public String getRfcOrCurpPayer() {
		return rfcOrCurpPayer;
	}

	public void setRfcOrCurpPayer(String rfcOrCurpPayer) {
		this.rfcOrCurpPayer = rfcOrCurpPayer;
	}

	public int getTypeOfBeneficiaryAccount() {
		return typeOfBeneficiaryAccount;
	}

	public void setTypeOfBeneficiaryAccount(int typeOfBeneficiaryAccount) {
		this.typeOfBeneficiaryAccount = typeOfBeneficiaryAccount;
	}

	public String getConceptOfPayment() {
		return conceptOfPayment;
	}

	public void setConceptOfPayment(String conceptOfPayment) {
		this.conceptOfPayment = conceptOfPayment;
	}

	public BigDecimal getIvaAmount() {
		return ivaAmount;
	}

	public void setIvaAmount(BigDecimal ivaAmount) { this.ivaAmount = ivaAmount; }

	public BigDecimal getAmountOfPayment() { return amountOfPayment;}

	public void setAmountOfPayment(BigDecimal amountOfPayment) {
		this.amountOfPayment = amountOfPayment;
	}

	public int getBeneficiaryAccountType2() {
		return BeneficiaryAccountType2;
	}

	public void setBeneficiaryAccountType2(int beneficiaryAccountType2) {
		BeneficiaryAccountType2 = beneficiaryAccountType2;
	}

	public String getDigitalCollectionSchemeFolio() {
		return digitalCollectionSchemeFolio;
	}

	public void setDigitalCollectionSchemeFolio(String digitalCollectionSchemeFolio) {
		this.digitalCollectionSchemeFolio = digitalCollectionSchemeFolio;
	}

	public int getPaymentOfTheTransferFee() {
		return paymentOfTheTransferFee;
	}

	public void setPaymentOfTheTransferFee(int paymentOfTheTransferFee) {
		this.paymentOfTheTransferFee = paymentOfTheTransferFee;
	}

	public BigDecimal getTransferFeeAmount() {
		return transferFeeAmount;
	}

	public void setTransferFeeAmount(BigDecimal transferFeeAmount) {
		this.transferFeeAmount = transferFeeAmount;
	}

	public String getBuyerCellNumberAliases() {
		return buyerCellNumberAliases;
	}

	public void setBuyerCellNumberAliases(String buyerCellNumberAliases) {
		this.buyerCellNumberAliases = buyerCellNumberAliases;
	}

	public String getDigitBuyersVerifier() {
		return digitBuyersVerifier;
	}

	public void setDigitBuyersVerifier(String digitBuyersVerifier) {
		this.digitBuyersVerifier = digitBuyersVerifier;
	}

	public String getMombreDelParticipanteReceptor() {
		return mombreDelParticipanteReceptor;
	}

	public void setMombreDelParticipanteReceptor(String mombreDelParticipanteReceptor) {
		this.mombreDelParticipanteReceptor = mombreDelParticipanteReceptor;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getCuentaBeneficiario() {
		return cuentaBeneficiario;
	}

	public void setCuentaBeneficiario(String cuentaBeneficiario) {
		this.cuentaBeneficiario = cuentaBeneficiario;
	}

	public String getrFCOCurpBeneficiario() {
		return rFCOCurpBeneficiario;
	}

	public void setrFCOCurpBeneficiario(String rFCOCurpBeneficiario) {
		this.rFCOCurpBeneficiario = rFCOCurpBeneficiario;
	}

	public String getNombreBeneficiario2() {
		return nombreBeneficiario2;
	}

	public void setNombreBeneficiario2(String nombreBeneficiario2) {
		this.nombreBeneficiario2 = nombreBeneficiario2;
	}

	public String getrFCOCurpBeneficiario2() {
		return rFCOCurpBeneficiario2;
	}

	public void setrFCOCurpBeneficiario2(String rFCOCurpBeneficiario2) {
		this.rFCOCurpBeneficiario2 = rFCOCurpBeneficiario2;
	}

	public String getCuentaBeneficiario2() {
		return cuentaBeneficiario2;
	}

	public void setCuentaBeneficiario2(String cuentaBeneficiario2) {
		this.cuentaBeneficiario2 = cuentaBeneficiario2;
	}

	public String getAliasDelNumeroCelularDelVendedor() {
		return aliasDelNumeroCelularDelVendedor;
	}

	public void setAliasDelNumeroCelularDelVendedor(String aliasDelNumeroCelularDelVendedor) {
		this.aliasDelNumeroCelularDelVendedor = aliasDelNumeroCelularDelVendedor;
	}

	public String getDigitoVerificadorDelVendedor() {
		return digitoVerificadorDelVendedor;
	}

	public void setDigitoVerificadorDelVendedor(String digitoVerificadorDelVendedor) {
		this.digitoVerificadorDelVendedor = digitoVerificadorDelVendedor;
	}

	public String getNumeroDeSerieDelCertificado() {
		return numeroDeSerieDelCertificado;
	}

	public void setNumeroDeSerieDelCertificado(String numeroDeSerieDelCertificado) {
		this.numeroDeSerieDelCertificado = numeroDeSerieDelCertificado;
	}

	public String getSelloDigital() {
		return selloDigital;
	}

	public void setSelloDigital(String selloDigital) {
		this.selloDigital = selloDigital;
	}

	public String getStrSpeiOperationDate() {
		return strSpeiOperationDate;
	}

	public void setStrSpeiOperationDate(String strSpeiOperationDate) {
		this.strSpeiOperationDate = strSpeiOperationDate;
	}

	public String getStrCalenderDatePayment() {
		return strCalenderDatePayment;
	}

	public void setStrCalenderDatePayment(String strCalenderDatePayment) {
		this.strCalenderDatePayment = strCalenderDatePayment;
	}

	public Date getSpeiOperationDateFormat() {
		return speiOperationDateFormat;
	}

	public void setSpeiOperationDateFormat(Date speiOperationDateFormat) {
		this.speiOperationDateFormat = speiOperationDateFormat;
	}

	public String getOriginalString() {
		return originalString;
	}

	public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}

	public String getCDA() {
		return CDA;
	}

	public void setCDA(String cDA) {
		CDA = cDA;
	}

	public Integer getCalenderTimePaymentMS() {
		return calenderTimePaymentMS;
	}

	public void setCalenderTimePaymentMS(Integer calenderTimePaymentMS) {
		this.calenderTimePaymentMS = calenderTimePaymentMS;
	}
	
	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	
	
	public void setFolioCDA(long folioCDA) {
		this.folioCDA = folioCDA;
	}

	public Integer getOriginalPaymentFolio() {
		return originalPaymentFolio;
	}

	public void setOriginalPaymentFolio(Integer originalPaymentFolio) {
		this.originalPaymentFolio = originalPaymentFolio;
	}

	public Integer getOriginalFolioPackage() {
		return originalFolioPackage;
	}

	public void setOriginalFolioPackage(Integer originalFolioPackage) {
		this.originalFolioPackage = originalFolioPackage;
	}
	
	
	

}
