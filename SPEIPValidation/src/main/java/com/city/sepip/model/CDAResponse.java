package com.city.sepip.model;

public class CDAResponse {
	String cda;
	String certificateSerialNumber;
	String originalString;
	String digitalStamp;
	
	public CDAResponse() {
		super();

	}

	public CDAResponse(String cda, String certificateSerialNumber, String originalString, String digitalStamp) {
		super();
		this.cda = cda;
		this.certificateSerialNumber = certificateSerialNumber;
		this.originalString = originalString;
		this.digitalStamp = digitalStamp;
	}

	public String getCda() {
		return cda;
	}

	public void setCda(String cda) {
		this.cda = cda;
	}

	public String getCertificateSerialNumber() {
		return certificateSerialNumber;
	}

	public void setCertificateSerialNumber(String certificateSerialNumber) {
		this.certificateSerialNumber = certificateSerialNumber;
	}

	public String getOriginalString() {
		return originalString;
	}

	public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}

	public String getDigitalStamp() {
		return digitalStamp;
	}

	public void setDigitalStamp(String digitalStamp) {
		this.digitalStamp = digitalStamp;
	}

	@Override
	public String toString() {
		return "CDAResponse [cda=" + cda + ", certificateSerialNumber=" + certificateSerialNumber + ", originalString="
				+ originalString + ", digitalStamp=" + digitalStamp + "]";
	}
	
	
	
	

}
