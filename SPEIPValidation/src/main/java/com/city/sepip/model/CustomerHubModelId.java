package com.city.sepip.model;

import java.io.Serializable;

public class CustomerHubModelId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6372917585236133919L;

	private String trackingKey;
	
	private Integer speiOperationDate;
	
	

	public CustomerHubModelId() {

	}

	public CustomerHubModelId(String trackingKey, Integer speiOperationDate) {
		this.trackingKey = trackingKey;
		this.speiOperationDate = speiOperationDate;
	}

	public String getTrackingKey() {
		return trackingKey;
	}

	public void setTrackingKey(String trackingKey) {
		this.trackingKey = trackingKey;
	}

	public Integer getSpeiOperationDate() {
		return speiOperationDate;
	}

	public void setSpeiOperationDate(Integer speiOperationDate) {
		this.speiOperationDate = speiOperationDate;
	}
	
	
	
	

	
}
