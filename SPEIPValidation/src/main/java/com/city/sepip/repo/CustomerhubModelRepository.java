package com.city.sepip.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.city.sepip.model.CustomerHubModelEntity;
import com.city.sepip.model.CustomerHubModelId;

@Repository
public interface CustomerhubModelRepository extends CrudRepository<CustomerHubModelEntity, CustomerHubModelId> {
	
}
