
package com.city.sepip.util;

import com.city.sepip.model.CustomerHubModel;
import com.city.sepip.model.SepipModel;

/**
 * @author Wipro Digital for Citi Banamex
 * Sukanta Misra, Carlos de la Rosa, Santiago Montesinos
 */
public class ValidateOutputField {

    public static String validateOutputFieldValues(SepipModel sepipModel, CustomerHubModel customerHubModel) {
        String validateCuentaBeneficiarioStr = validateCuentaBeneficiario(sepipModel.getTypeOfBeneficiaryAccount(), customerHubModel.getCuentaBeneficiario());

        String validateRfcOCurpBeneficiario2Str = validateRfcOCurpBeneficiario2(customerHubModel.getrFCOCurpBeneficiario2(), sepipModel.getPaymentType());

        if (validateRfcOCurpBeneficiario2Str != null) {
            if (validateCuentaBeneficiarioStr == null) {
                validateCuentaBeneficiarioStr = validateRfcOCurpBeneficiario2Str;
            } else {
                validateCuentaBeneficiarioStr += " ," + validateRfcOCurpBeneficiario2Str;
            }
        }

        String validateRfcOCurpBeneficiarioStr = validateRfcOCurpBeneficiario(customerHubModel.getrFCOCurpBeneficiario(), sepipModel.getPaymentType());

        if (validateRfcOCurpBeneficiarioStr != null) {
            validateCuentaBeneficiarioStr += " ," + validateRfcOCurpBeneficiarioStr;
        }

        return validateCuentaBeneficiarioStr;
    }

    private static String validateCuentaBeneficiario(int typeOfBeneficiaryAccount, String cuentaBeneficiario) {
        if (typeOfBeneficiaryAccount == 40 && cuentaBeneficiario.length() != 18) {
            return "Valor inválido, el Tipo de cuenta Beneficiario Clabe no corresponde a la Cuenta Beneficiario recibida";
        }

        if (typeOfBeneficiaryAccount == 3 && cuentaBeneficiario.length() != 16) {
            return "Valor inválido, el Tipo de cuenta Beneficiario TD no corresponde a la Cuenta Beneficiario recibida";
        }

        if (typeOfBeneficiaryAccount == 10 && cuentaBeneficiario.length() != 10) {
            return "Valor inválido, el Tipo de cuenta beneficicario línea telefónica móvil no corresponde a la Cuenta beneficiario recibida";
        }

        return null;
    }

    private static String validateRfcOCurpBeneficiario(String rFCOCurpBeneficiario, String paymentType) {
        boolean atLeastOneAlpha = paymentType.matches(".*[a-zA-Z]+.*");

        if (atLeastOneAlpha) {
            return null;
        } else {

            if (rFCOCurpBeneficiario.length() == 0) {
                return "Valor inválido, RFC O CURP Ordenante es un dato obligatorio, no puede estar vacío";
            }
        }

        return null;
    }

    private static String validateRfcOCurpBeneficiario2(String rFCOCurpBeneficiario2, String paymentType) {
        boolean atLeastOneAlpha = paymentType.matches(".*[a-zA-Z]+.*");

        if (atLeastOneAlpha) {
            return null;
        } else {
            if (rFCOCurpBeneficiario2 != null) {
                if (rFCOCurpBeneficiario2.length() == 0) {
                    return "Valor inválido, RFC O CURP Ordenante es un dato obligatorio, no puede estar vacío";
                }
            }
        }

        return null;
    }
}
