
package com.city.sepip.util;

import java.math.BigDecimal;
import java.util.StringTokenizer;

/**
 * @author SU20053232
 *
 */
public class SepipUtil {
	
	public static int getNoOfDigits(BigDecimal value) {		
		String valueStr=String.valueOf(value);
		StringTokenizer t=new StringTokenizer(valueStr,".");
		
		String beforeDigit=t.nextToken();
		String afterDigit=t.nextToken();
		
		System.out.println("beforeDigit : " + beforeDigit);
		System.out.println("afterDigit : " + afterDigit);
		
		return afterDigit.length();
	}

}
