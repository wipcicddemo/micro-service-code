
package com.city.sepip.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.city.sepip.model.SepipDB;
import com.city.sepip.model.SepipModel;

/**
 * @author Wipro Digital for Citi Banamex
 * Sukanta Misra, Carlos de la Rosa, Santiago Montesinos
 */
public class ValidationUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationUtil.class);

    /**
     * Util class that validates sepipModel fields. Returns validation error if there are any, otherwise returns null
     * @param sepipModel sepipModel received in REST request
     * @return String concatenated with errors (if any
     */
    public static String validateInputFields(SepipModel sepipModel) {
        LOGGER.info("Started Validating the Request JSON");

        String validationResult = ValidationUtil.validateAllPaymentTypeInfo(sepipModel.getPaymentType());
        String validatePaymentOfTheTransferFeeStr = ValidationUtil
                .validatePaymentOfTheTransferFee(sepipModel.getPaymentOfTheTransferFee());

        LOGGER.info("Validation of Payment of Transfer Fee " + validatePaymentOfTheTransferFeeStr);

        if (validatePaymentOfTheTransferFeeStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validatePaymentOfTheTransferFeeStr;
            } else {
                validationResult = validatePaymentOfTheTransferFeeStr;
            }
        }

        String validateBeneficiaryAccountType2Str = ValidationUtil
                .validateBeneficiaryAccountType2(sepipModel.getBeneficiaryAccountType2());

        LOGGER.info("Validation of Beneficiary Account Type 2 " + validateBeneficiaryAccountType2Str);

        if (validateBeneficiaryAccountType2Str != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateBeneficiaryAccountType2Str;
            } else {
                validationResult = validateBeneficiaryAccountType2Str;
            }
        }

        String validateCalenderTimePaymentStr = ValidationUtil
                .validateCalenderTimePayment(sepipModel.getCalenderTimePayment());

        LOGGER.info("Validation of Calender Time of Payment " + validateCalenderTimePaymentStr);

        if (validateCalenderTimePaymentStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateCalenderTimePaymentStr;
            } else {
                validationResult = validateCalenderTimePaymentStr;
            }
        }

        String validateOrderingAccountStr = ValidationUtil.validateOrderingAccount(sepipModel.getOrderingAccount(), sepipModel.getOrdererAccountType());

        LOGGER.info("Validation of Ordering Account " + validateOrderingAccountStr);

        if (validateOrderingAccountStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateOrderingAccountStr;
            } else {
                validationResult = validateOrderingAccountStr;
            }
        }

        String validateRfc = ValidationUtil.validateRfc(sepipModel.getRfcOrCurpPayer());

        LOGGER.info("Validation of RFC " + validateRfc);

        if (validateRfc != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateRfc;
            } else {
                validationResult = validateRfc;
            }
        }

        String dateCalenderDatePaymentStr = ValidationUtil
                .validateProperDate(Integer.toString(sepipModel.getCalenderDatePayment()), "ddMMyyyy");

        LOGGER.info("Validation of Calender Date of Payment " + dateCalenderDatePaymentStr);


        if (dateCalenderDatePaymentStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + dateCalenderDatePaymentStr;
            } else {
                validationResult = dateCalenderDatePaymentStr;
            }
        }

        String dateSpeiOperationDateStr = ValidationUtil
                .validateProperDate(Integer.toString(sepipModel.getSpeiOperationDate()), "ddmmyyyy");

        LOGGER.info("Validation of SPEI Operation Date " + dateSpeiOperationDateStr);


        if (dateSpeiOperationDateStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + dateSpeiOperationDateStr;
            } else {
                validationResult = dateSpeiOperationDateStr;
            }
        }

        String validateAmountOfPaymentStr = ValidationUtil.validateAmountOfPayment(sepipModel.getAmountOfPayment());

        LOGGER.info("Validation of Amount of Payment " + validateAmountOfPaymentStr);

        if (validateAmountOfPaymentStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateAmountOfPaymentStr;
            } else {
                validationResult = validateAmountOfPaymentStr;
            }
        }

        String validateConceptOfPaymentStr = ValidationUtil.validateConceptOfPayment(sepipModel.getConceptOfPayment());

        LOGGER.info("Validation of Concept of Payment " + validateConceptOfPaymentStr);


        if (validateConceptOfPaymentStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateConceptOfPaymentStr;
            } else {
                validationResult = validateConceptOfPaymentStr;
            }
        }

        String validateTypeOfBeneficiaryAccountStr = ValidationUtil
                .validateTypeOfBeneficiaryAccount(sepipModel.getTypeOfBeneficiaryAccount());

        LOGGER.info("Validation of type of Benefeciary Acocunt " + validateTypeOfBeneficiaryAccountStr);


        if (validateTypeOfBeneficiaryAccountStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateTypeOfBeneficiaryAccountStr;
            } else {
                validationResult = validateTypeOfBeneficiaryAccountStr;
            }
        }


        String validateNameOfTransferOrderIssuerParticipantStr = ValidationUtil
                .validateNameOfTransferOrderIssuerParticipant(sepipModel.getNameOfTransferOrderIssuerParticipant());

        LOGGER.info("Validation of Name of Transfer Order Issuer Participient " + validateTypeOfBeneficiaryAccountStr);


        if (validateNameOfTransferOrderIssuerParticipantStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateNameOfTransferOrderIssuerParticipantStr;
            } else {
                validationResult = validateNameOfTransferOrderIssuerParticipantStr;
            }
        }

        String validateOrderingNameStr = ValidationUtil.validateOrderingName(sepipModel.getOrderingName());

        LOGGER.info("Validation of Ordering Name " + validateOrderingNameStr);
        if (validateOrderingNameStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateOrderingNameStr;
            } else {
                validationResult = validateOrderingNameStr;
            }
        }

        String validateTypeOfAccountOrd = ValidationUtil.typeOfAccountOrd(sepipModel.getOrdererAccountType());
        LOGGER.info("Validation of Type of Ordering Account " + validateTypeOfAccountOrd);

        if (validateTypeOfAccountOrd != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateTypeOfAccountOrd;
            } else {
                validationResult = validateTypeOfAccountOrd;
            }
        }

        String validationSPEIKey = ValidationUtil.validateSPEIKey(sepipModel.getCdaParticipientSPEIKey());
        LOGGER.info("Validation of SPEI Key " + validationSPEIKey);

        if (validationSPEIKey != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validationSPEIKey;
            } else {
                validationResult = validationSPEIKey;
            }
        }

        String valitationPaymentOfFee = ValidationUtil.validatePaymentOfFee(sepipModel.getPaymentType(), sepipModel.getPaymentOfTheTransferFee());

        LOGGER.info("Validation of Payment of Fee " + valitationPaymentOfFee);
        if (valitationPaymentOfFee != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + valitationPaymentOfFee;
            } else {
                validationResult = valitationPaymentOfFee;
            }
        }

        String valitationTransferFeeAmt = ValidationUtil.validateTransferFeeAmt(sepipModel.getPaymentType(), sepipModel.getPaymentOfTheTransferFee(), sepipModel.getTransferFeeAmount());
        LOGGER.info("Validation of Transfer Fee Amount " + valitationTransferFeeAmt);
        if (valitationTransferFeeAmt != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + valitationTransferFeeAmt;
            } else {
                validationResult = valitationTransferFeeAmt;
            }
        }

        String valitationCellAlias = ValidationUtil.validateCellAlias(sepipModel.getPaymentType(), sepipModel.getBuyerCellNumberAliases());
        LOGGER.info("Validation of Cell Alias " + valitationCellAlias);
        if (valitationCellAlias != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + valitationCellAlias;
            } else {
                validationResult = valitationCellAlias;
            }
        }

        String valitationDigitalBuyer = ValidationUtil.validateDigitalBuyer(sepipModel.getDigitBuyersVerifier());
        LOGGER.info("Validation of Digital Buyer " + valitationDigitalBuyer);

        if (valitationDigitalBuyer != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + valitationDigitalBuyer;
            } else {
                validationResult = valitationDigitalBuyer;
            }
        }

        String validateIvaAmountStr = ValidationUtil.validateIvaAmount(sepipModel.getIvaAmount());
        LOGGER.info("Validation of IVA Amount " + validateIvaAmountStr);

        if (validateIvaAmountStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateIvaAmountStr;
            } else {
                validationResult = validateIvaAmountStr;
            }
        }

        String validateDigitalCollectionSchemeFolioStr = ValidationUtil.validateDigitalCollectionSchemeFolio(sepipModel.getDigitalCollectionSchemeFolio());
        LOGGER.info("Validation of Digital Collection Scheme Folio " + validateDigitalCollectionSchemeFolioStr);

        if (validateDigitalCollectionSchemeFolioStr != null) {
            if (validationResult != null) {
                validationResult = validationResult + " ," + validateDigitalCollectionSchemeFolioStr;
            } else {
                validationResult = validateDigitalCollectionSchemeFolioStr;
            }
        }

        if (validationResult != null) {
            LOGGER.info("Finished Validating the Request JSON" + validationResult);
        } else {
            LOGGER.info("Finished Validating the Request JSON With out any Failure");
        }

        return validationResult;
    }

    /**
     * Validación para Folio del Esquema Cobro Digital
     * Validation for Digital Collection Scheme Folio
     * @param digitalCollectionSchemeFolio Digital Collection Scheme folio to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateDigitalCollectionSchemeFolio(String digitalCollectionSchemeFolio) {
        if (digitalCollectionSchemeFolio.length() > 20) {
            return "Valor Inválido, Folio del Esquema Cobro Digital, no puede ser mayor a 20 posiciones";
        }
        return null;
    }

    /**
     * Validación para Tipo de Pago
     * Validation for Payment Type
     * @param paymentType Payment Type to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    public static String validateAllPaymentTypeInfo(String paymentType) {
        int count = 0;
        boolean atleastOneAlpha = paymentType.matches(".*[a-zA-Z]+.*");
        
        if(paymentType.matches("^[0-9]*$")) {
	        if (atleastOneAlpha || paymentType != "") {
	            int paymentTypeConv = Integer.parseInt(paymentType);
	            int paymentTypeNew = paymentTypeConv;
	            for (; paymentTypeNew != 0; paymentTypeNew /= 10, ++count) {
	            }
	
	            if (count == 0) {
	                return "Tipo de Pago diferente a tercero a tercero, no puede estar vacío";
	            }
	
	            if (String.valueOf(paymentType).length() > 2) {
	                return "Valor inválido, el Tipo de pago  tiene un longitud máxima de 2 digitos.";
	            }
	
	            boolean validationPaymentTypeList = ValidationUtil.validatePaymentType(paymentTypeConv);
	
	            if (!validationPaymentTypeList) {
	                return "Tipo de Pago diferente a tercero a tercero";
	            }
	            return null;
	        } else {
	            return "Tipo de Pago diferente a tercero a tercero, no puede estar vacío";
	        }
        }else {
        	return "Tipo de Pago diferente a tercero a tercero";
        }
    }

    /**
     * Validación para Fecha de Operación del SPEI
     * Validation for SPEI Operation Date
     * @param speiOperationDate SPEI operation date to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    @Deprecated
    public static String validateAllSpeiOperationDate(int speiOperationDate) {
        int length = String.valueOf(speiOperationDate).length();

        if (length == 0) {
            return "Fecha de operación de SPEI no puede estar vacía";
        }

        if (length > 8) {
            return "Fecha inválida, mayor 8 dígitos";
        }

        if (length < 8) {
            return "Fecha inválida, menor 8 dígitos";
        }
        return null;
    }

    /**
     * Validación para Alias del número celular del comprador
     * Validation for Alias of the buyer's cell number
     * @param buyerCellNumberAliases Buyer's cell number alias to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    @Deprecated
    public static String validateBuyerCellNumberAliases(Long buyerCellNumberAliases) {
        int length = String.valueOf(buyerCellNumberAliases).length();

        if (length == 0) {
            return "El campo Alias del número celular del vendedor no puede estar vacío";
        }

        if (length > 10) {
            return "Valor inválido, el campo Alias del número celular del vendedor no debe ser mayor a 10 dígitos";
        }

        return null;
    }

    /**
     * Validación para Cuenta Ordenante
     * Validation for Ordering Account
     * @param orderingAccount Ordering account to be validated
     * @param orderingAccountType  Ordering account type
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateOrderingAccount(String orderingAccount, int orderingAccountType) {
        Pattern specialCharacters = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher match = specialCharacters.matcher(orderingAccount);
        boolean containsSpecialCharacters = match.find();

        if (containsSpecialCharacters) {
            return "Valor inválidoa, Cuenta Ordenante no puede tener caracteres alfabéticos o especiales";
        }

        if (checkAlphaNumeric(orderingAccount)) {
            return "Valor inválidoa, Cuenta Ordenante no puede tener caracteres alfabéticos o especiales";
        }

        if (orderingAccount.length() < 1) {
            return "Valor inválido, Cuenta Ordenante no puede estar vacío es un dato obligatorio";
        }

        if (orderingAccount.length() > 20) {
            return "Valor inválido, Cuenta Ordenante no puede ser mayor a 20 dígitos";
        }

        if (orderingAccountType == 40 && orderingAccount.length() != 18) {
            return "Valor inválido, el Tipo de cuenta Ordenante  Clabe no corresponde a la cuenta Ordenante recibida";
        }

        if (orderingAccountType == 3 && orderingAccount.length() != 16) {
            return "Valor inválido, el Tipo de cuenta Ordenante  TD no corresponde a la cuenta Ordenante recibida";
        }

        if (orderingAccountType == 10 && orderingAccount.length() != 10) {
            return "Valor inválido, el Tipo de cuenta Ordenante  linea telefonica movil no corresponde a la cuenta Ordenante recibida";
        }
        return null;
    }

    /**
     * Validación para Tipo de Cuenta Beneficiario 2
     * Validation for Beneficiary Account Type 2
     * @param beneficiaryAccountType2 Beneficiary Account Type 2 to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateBeneficiaryAccountType2(int beneficiaryAccountType2) {
        int length = String.valueOf(beneficiaryAccountType2).length();

        int count = 0;
        int newBeneficiaryAccountType2 = beneficiaryAccountType2;
        for (; newBeneficiaryAccountType2 != 0; newBeneficiaryAccountType2 /= 10, ++count) {
        }
        if (count == 0) {
            return "Valor inválido, el Tipo de cuenta Beneficiario 2 es obligatorio no puede estar vacío.";
        }

        if (length > 2) {
            return "Valor inválido, Tipo de Cuenta Beneficiario 2 no puede ser mayor a dos dígitos";
        }

        Integer[] arrayAccounts = new Integer[]{40, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> accountList = Arrays.asList(arrayAccounts);

        if (!accountList.contains(beneficiaryAccountType2)) {
            return "Valor inválido, Tipo de Cuenta Beneficiario 2 no está catalogado";
        }

        return null;
    }

    /**
     * Validación para Dígito verificador del comprador
     * Validation for Buyer Verifier Digit
     * @param digitBuyersVerifier Buyer verifier digit to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    @Deprecated
    public static String validateDigitBuyersVerifier(int digitBuyersVerifier) {
        int length = String.valueOf(digitBuyersVerifier).length();

        if (length > 3) {
            return "Valor inválido, el campo Dígitc Verificador del comprador  o debe ser mayor a 3 dígitos";
        }
        return null;
    }

    private static String validatePaymentOfTheTransferFee(int paymentOfTheTransferFee) {
        // int length = String.valueOf(paymentOfTheTransferFee).length();
        /*int count = 0;
        int paymentOfTheTransferFeeNew = paymentOfTheTransferFee;
        for (; paymentOfTheTransferFeeNew != 0; paymentOfTheTransferFeeNew /= 10, ++count) {
        }	*/	
		/*if(count == 0) {
			return "Valor inválido para campo Pago de la comisión por la transferencial, el valor  está vacío.";
		}*/

		/*
        if (length > 3) {
            return "Valor inválido para campo Pago de la Comisión por la Transferencia, el valor no puede ser mayor a 1 dígito.";
        }*/

        return null;
    }

    /**
     * Validación para Hora Calendario del Abono
     * Validation for Calendar Time Payment
     * @param calendarTimePayment Calendar Time Payment to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateCalenderTimePayment(String calendarTimePayment) {
        calendarTimePayment = calendarTimePayment.replaceAll("\\s+", "");
        calendarTimePayment = calendarTimePayment.trim();

        int length = String.valueOf(calendarTimePayment).length();

        if (length == 0) {
            return "Valor inválido, la Hora Calendario de Abono es obligatoria, no puede estar el dato vacío";
        }

        //tal vez esto no se use
        Pattern specialCharacters = Pattern.compile("^[0-9]*$", Pattern.CASE_INSENSITIVE);
        Matcher match = specialCharacters.matcher(calendarTimePayment);
        boolean containsSpecialCharacters = match.find();

        if (!calendarTimePayment.matches("^[0-9]*$")) {
            return "Hora inválida, no se permiten caracteres alfabéticos o especiales";
        }

        if (calendarTimePayment.length() > 6) {
            return "Hora inválida, mayor a 6 dígitos";
        }

        if (calendarTimePayment.length() < 6) {
            return "Hora inválida, menor a 6 dígitos";
        }

        if (calendarTimePayment.length() == 6) {
            boolean validTime = Pattern.matches("^([0-1]\\d|2[0-3])([0-5]\\d)([0-5]\\d)$", calendarTimePayment);
            int hours = Integer.parseInt(calendarTimePayment.substring(0, 2));
            int minutes = Integer.parseInt(calendarTimePayment.substring(2, 4));
            int seconds = Integer.parseInt(calendarTimePayment.substring(4, 6));

            if (hours < 0 || hours > 24) {
                return "Hora inválida, dígitos de hora fuera de rango";
            }

            if (minutes < 0 || minutes > 59) {
                return "Hora inválida, dígitos de minutos fuera de rango";
            }

            if (seconds < 0 || seconds > 59) {
                return "Hora inválida, dígitos de segundos fuera de rango";
            }

            if (!validTime) {
                return "El formato del campo tiempo tiene un formato hhmmss no es válido";
            }
        } else {
            return "El formato del campo tiempo tiene un formato hhmmss no es válido";
        }

        return null;
    }

    /**
     * Validación para Tipo de pago
     * Validation for Payment Type
     * @param paymentType Payment Type to be validated
     * @return true or false
     */
    private static boolean validatePaymentType(int paymentType) {
        Integer[] arrayPayment = new Integer[]{1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                24, 25, 26, 27, 28, 29};
        List<Integer> paymentList = Arrays.asList(arrayPayment);

        return paymentList.contains(paymentType);
    }

    /**
     * Validación del formato de hora
     * Validation for time format
     * @param timeToValidate Input time to be validated
     * @return true or false
     */
    @Deprecated
    public static String validateTimeFormat(String timeToValidate) {
        if (timeToValidate == null)
            return "El campo tiempo no puede estar vacío";

        Pattern specialCharacters = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher match = specialCharacters.matcher(timeToValidate);
        boolean containsSpecialCharacters = match.find();

        if (containsSpecialCharacters) {
            return "El campo tiempo contiene caracteres especiales";
        }

        if (timeToValidate.length() == 6) {
            Boolean validTime = Pattern.matches("^([0-1]\\d|2[0-3])([0-5]\\d)([0-5]\\d)$", timeToValidate);
            int hours = Integer.valueOf(timeToValidate.substring(0, 2));
            int minutes = Integer.valueOf(timeToValidate.substring(2, 4));
            int seconds = Integer.valueOf(timeToValidate.substring(4, 6));

            if (hours < 0 || hours > 24) {
                return "Las horas deben de ser menor a 24";
            }

            if (minutes < 0 || minutes > 59) {
                return "Los minutos deben de ser menor o igual a 59";
            }

            if (seconds < 0 || seconds > 59) {
                return "Los segundos deben de ser menor o igual a  59";
            }

            if (!validTime) {
                return "El formato del campo tiempo tiene un formato hhmmss no es válido";
            }
        } else {
            return "El formato del campo tiempo tiene un formato hhmmss no es válido";
        }
        return null;
    }

    /**
     * Validación del RFC o CURP con base en longitud
     * Validation for RFC or CURP depending on string length
     * @param rfcToValidate RFC to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateRfc(String rfcToValidate) {
        rfcToValidate = rfcToValidate.replaceAll("\\s+", "");
        rfcToValidate = rfcToValidate.trim();

        if (rfcToValidate == null || rfcToValidate.length() == 0)
            return "Valor inválido, RFC o CURP Ordenante es un dato obligatorio, no puede estar vacío";

        if (rfcToValidate.length() > 18)
            return "Valor inválido, RFC o CURP Ordenante no puede ser mayor a 18 posiciones";

        // Remove whitespace
        rfcToValidate = rfcToValidate.replaceAll("\\s+", "");

        // Validate RFC
        /*if (rfcToValidate.length() == 12 || rfcToValidate.length() == 13) {
            boolean validRfc = Pattern.matches(
                    "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$",
                    rfcToValidate);

            if (!validRfc) {
                return "El formato del RFC Ordenante no es válido";
            }
            // Validate CURP
        } else */
       /*
        *  if (rfcToValidate.length() == 18) {
        	
            boolean validCurp = Pattern.matches(
                    "/^([A-Z][AEIOUX][A-Z]{2}\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\\d])(\\d)$/",
                    rfcToValidate);
            if (!validCurp) {
                return "El formato del CURP Ordenante no es válido";
            }
        } else {
            return "El formato del RFC/CURP Ordenante no es válido";
        }
        */

        return null;
    }

    /**
     * This method validates a string length
     *
     * @param stringToValidate String to be validated
     * @param minLength        Min allowed length of string
     * @param maxLength        Max allowed length of string
     * @param paymentType      Payment type, used for additional validations. If
     *                         none enter 0
     * @return Returns if string length is between range
     */
    @Deprecated
    public static String validateInputLength(String stringToValidate, int minLength, int maxLength, int paymentType) {
        if (stringToValidate != null) {
            if (paymentType == 22
                    && (stringToValidate.compareTo("NA") == 0 || stringToValidate.compareTo("N/A") == 0)) {
                return "El campo Nombre Ordenante debe de ser 'NA'";
            }
            if (!(stringToValidate.length() >= minLength && stringToValidate.length() <= maxLength)) {
                return "El campo debe tener más de " + Integer.toString(minLength) + " caracteres y menos de "
                        + Integer.toString(minLength) + " caracteres";
            }
        }
        return "El campo Nombre Ordenante no puede estar vacío";
    }

    /**
     * Validación para Fecha de operación del SPEI
     * Validation for SPEI Operation Date
     * @param dateToValidate Date to be validated
     * @param dateFormat  Date format to be used in validation
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateProperDate(String dateToValidate, String dateFormat) {
        if (Integer.parseInt(dateToValidate) == 0) {
            return "Valor inválido, la fecha de Operación de SPEI o la fecha calendario de Abono es obligatoria, no puede estar el dato vacío.";
        }

        int length = dateToValidate.length();

        if (length == 0) {
            return "Fecha de operación de SPEI no puede estar vacía";
        }

        if (length > 8) {
            return "Fecha inválida, mayor 8 dígitos";
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);

        if (dateToValidate.length() == 7 || dateToValidate.length() == 8) {
            String day = "";
            String month = "";
            String year = "";
            //1010201 1012201 1109201
            if (dateToValidate.length() == 7) {
                day = dateToValidate.substring(0, 1);
                month = dateToValidate.substring(1, 3);
                year = dateToValidate.substring(3);
            } else {
                day = dateToValidate.substring(0, 2);
                month = dateToValidate.substring(2, 4);
                year = dateToValidate.substring(4);
            }

            if (year.length() < 4) {
                return "Fecha inválida, formato de año no válido";
            }

            //int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            if (Integer.parseInt(year) < 1000 || Integer.parseInt(year) > 3000) {
                return "Fecha inválida, año no válido";
            }

            if (Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {
                return "Fecha inválida, mes incorrecto";
            }

            if (Integer.parseInt(day) > 31) {
                return "Fecha inválida, día no válido";
            }

            if (Integer.parseInt(month) == 2) {
                if (isLeapYear(Integer.parseInt(year))) {
                    if (!(Integer.parseInt(day) <= 29)) {
                        return "Fecha inválida, día no válido";
                    }
                } else {
                    if (!(Integer.parseInt(day) <= 28)) {
                        return "Fecha inválida, día no válido";
                    }
                }
            } else if (Integer.parseInt(month) == 1 || Integer.parseInt(month) == 3 || Integer.parseInt(month) == 5
                    || Integer.parseInt(month) == 7 || Integer.parseInt(month) == 8 || Integer.parseInt(month) == 10
                    || Integer.parseInt(month) == 12) {
                if (Integer.parseInt(day) > 31) {
                    return "Fecha inválida, día no válido";
                }
            } else if (Integer.parseInt(month) == 4 || Integer.parseInt(month) == 6 || Integer.parseInt(month) == 9
                    || Integer.parseInt(month) == 11) {
                if (Integer.parseInt(day) > 30) {
                    return "Fecha inválida, día no válido";
                }
            }

        } else {
            return "Fecha inválida, menor 8 dígitos";
        }

        return null;
    }

    /**
     * Método auxuliar que determina si un año es bisiesto
     * Helper method that validates if a year is leap year or not
     * @param year Year to be validated
     * @return true or false
     */
    private static boolean isLeapYear(int year) {
        return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
    }

    /**
     * Validación para Monto del pago
     * Validation for Amount of Payment
     * @param amountOfPayment Amounf of Payment to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateAmountOfPayment(BigDecimal amountOfPayment) {
        if (amountOfPayment != null) {
            long length = String.valueOf(amountOfPayment).length();

            if (length == 0) {
                return "Monto Inválido, no puede estar vacío";
            }

            if (amountOfPayment.compareTo(BigDecimal.ZERO) == 0) {
                return "Monto Inválido, no puede ser 0";
            }

            if (amountOfPayment.toString().indexOf(".") > 12) {
                return "Monto Inválido, no puede ser  mayor a 999,999,999,999.99";
            } else if (!amountOfPayment.toString().contains(".") && length > 12) {
                return "Monto Inválido, no puede ser  mayor a 999,999,999,999.99";
            }
        } else {
            return "Monto Inválido, no puede estar vacío";
        }
        return null;
    }

    /**
     * Validación para Concepto del Pago
     * Validation for Concept of Payment
     * @param conceptOfPayment Concept of Payment to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateConceptOfPayment(String conceptOfPayment) {
        if (conceptOfPayment.length() > 40) {
            return "Valor inválido, Concepto del Pago no puede ser mayor a 40 posiciones";
        }
        return null;
    }

    /**
     * Validación para Nombre Ordenante
     * Validation for Ordering Name
     * @param orderingName Ordering name to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateOrderingName(String orderingName) {
        if (orderingName.length() > 40) {
            return "Valor inválido en el campo  Nombre Ordenante, la longitud máxima es de 40 posiciones.";
        }
        return null;
    }

    /**
     * Validación para Tipo de Cuenta Beneficiario
     * Validation for Beneficiary Account Type
     * @param typeOfBeneficiaryAccount Beneficiary Account Type to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateTypeOfBeneficiaryAccount(int typeOfBeneficiaryAccount) {
        long length = String.valueOf(typeOfBeneficiaryAccount).length();
        int count = 0;
        int typeOfBeneficiaryAccountNew = typeOfBeneficiaryAccount;
        for (; typeOfBeneficiaryAccountNew != 0; typeOfBeneficiaryAccountNew /= 10, ++count) {
        }

        if (count == 0) {
            return "Valor inválido, el Tipo de cuenta Beneficiario es obligatorio no puede estar vacío.";
        }

        if (length > 20) {
            return "Valor inválido, Cuenta Beneficiario no puede ser mayor a 20 digitos";
        }

        if (length > 2) {
            return "Valor inválido, Tipo de Cuenta Beneficiario no puede ser mayor a dos digitos";
        }

        Integer[] arrayAccounts = new Integer[]{40, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> accountList = Arrays.asList(arrayAccounts);

        if (!accountList.contains(typeOfBeneficiaryAccount)) {
            return "Valor inválido, Tipo de Cuenta Beneficiario  no está catalogado";
        }

        return null;
    }

    /**
     * Validación para Nombre del Participante Emisor de la Orden de Transferencia
     * Validation for Transfer Order Issuer Participant Name
     * @param nameOfTransferOrderIssuerParticipant Order Issuer Participant Name to be validated
     * @return Cadena de mensaje de error de validación si hay un error, de lo contrario nulo
     */
    private static String validateNameOfTransferOrderIssuerParticipant(String nameOfTransferOrderIssuerParticipant) {
        if (nameOfTransferOrderIssuerParticipant.length() > 40) {
            return "Valor inválido en el campo  Nombre del Participante Emisor de la Orden de Transferencia, la longitud máxima es de 40 posiciones.";
        }

        return null;
    }

    /**
     * Validación para Tipo de Cuenta Ordenante
     * Validation for Ordering Account Type
     * @param accountType Ordering Account Type to be validated
     * @return Cadena de mensaje de error de validación si hay un error, de lo contrario nulo
     */
    private static String typeOfAccountOrd(int accountType) {
        Integer[] arrayAccounts = new Integer[]{40, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> accountList = Arrays.asList(arrayAccounts);
        int len = String.valueOf(accountType).length();
        int count = 0;
        int accountTypeNew = accountType;

        for (; accountTypeNew != 0; accountTypeNew /= 10, ++count) {
        }
        if (count < 1) {
            return "Valor inválido, el Tipo de Cuenta Ordenante es obligatorio, no puede estar vacío.";
        }
        if (len > 2) {
            return "Valor inválido, Tipo de Cuenta Ordenante no puede ser mayor a dos digitos";
        }
        if (!accountList.contains(accountType)) {
            return "Valor inválido, Tipo de Cuenta Ordenante  no está catalogado";
        }

        return null;
    }

    /**
     * Validación para clave SPEI del Participante Emisor
     * Validation for SPEI Key from issuing participant
     * @param cdaParticipantSPEIKey SPEI key to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateSPEIKey(int cdaParticipantSPEIKey) {
        Integer[] speiKeyValidValue = new Integer[]{40002, 40012, 40014, 40019, 40021, 40030, 40032, 40036, 40037, 40042, 40044, 40058, 40059, 40062, 40072, 102, 40103, 40106, 40132};
        List<Integer> speiKeyValidValueList = Arrays.asList(speiKeyValidValue);
        int len = String.valueOf(cdaParticipantSPEIKey).length();

        if (len == 1) {
            return "Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta  debe ser un valor numérico, no puede estár vacío";
        }
        if (len > 5) {
            return "Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta  debe ser un valor numérico de longitud máxima de 5 dígitos";
        }

        if (!speiKeyValidValueList.contains(cdaParticipantSPEIKey)) {
            return "Valor inválido, Clave SPEI del Participante Emisor de la CDA incorrecta de acuerdo con catálogo.";
        }
        return null;
    }

    /**
     * Validación para el campo Pago de la Comisión por Transferencia
     * Validation Transfer for Payment Fee
     * @param paymentType PaymentType, necessary to perform proper validation
     * @param paymentOfFee Payment Fee to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validatePaymentOfFee(String paymentType, int paymentOfFee) {
        int len = String.valueOf(paymentOfFee).length();
        if (len > 1) {
            return "Valor inválido, para el Campo de la Comisión por la transferencia, el valor no puede ser mayor a un dígito";
        }

        int count = 0;
        int paymentOfTheTransferFeeNew = paymentOfFee;
        for (; paymentOfTheTransferFeeNew != 0; paymentOfTheTransferFeeNew /= 10, ++count) {
        }

        if (count == 0) {
            return "Valor inválido para Campo Pago de la Comisión por la transferencia, el valor está vacío";
        }

        if (!paymentType.equals("")) {
            if (!(paymentOfFee == 1 || paymentOfFee == 2)) {
                return "Valor inválido para Campo Pago de la Comision por la transferencia, el valor debe ser 0";
            }
        }

        return null;
    }

    /**
     * Validación para el campo Monto de la Comisión por Transferencia
     * Validation for Transfer Amount
     * @param paymentType PaymentType, necessary to perform proper validation
     * @param paymentOfFee Payment Fee, necessary to perform proper validation
     * @param feeAmount Fee amount to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateTransferFeeAmt(String paymentType, int paymentOfFee, BigDecimal feeAmount) {
        if (!paymentType.equals("")) {
            long length = String.valueOf(feeAmount).length();

            if (length > 19) {
                return "Valor inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido de acuerdo al monto máximo que se pude recibir.";
            }

            System.out.println("feeAmount.toString().indexOf" + feeAmount.toString().indexOf("."));
            System.out.println("length " + length);
            if (paymentOfFee == 2 && feeAmount.toString().indexOf(".") > 12) {
                return "Valor inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido de acuerdo al monto máximo que se pude recibir.";
            }

            if (feeAmount.toString().indexOf(".") > 12) {
                return "Valor inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido de acuerdo al monto máximo que se pude recibir.";
            } else if (!feeAmount.toString().contains(".") && length > 12) {
                return "Valor inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido de acuerdo al monto máximo que se pude recibir.";
            }

            //int paymentTypeInt = Integer.parseInt(paymentType);
            if (paymentType.equalsIgnoreCase("18") || paymentType.equalsIgnoreCase("19") || paymentType.equalsIgnoreCase("20") || paymentType.equalsIgnoreCase("21") || paymentType.equalsIgnoreCase("22")) {
                if (paymentOfFee == 2) {
                    if (feeAmount.compareTo(BigDecimal.ZERO) == 0) {
                        return "Monto Inválido, no puede ser 0.";
                    } else if (feeAmount.toString().indexOf(".") > 12) {
                        return "Monto Inválido, el campo Monto de la Comisión por la Transferencia trae un valor inválido al monto máximo que se puede recibir";
                    }
                } else if (paymentOfFee == 1) {
                    if (feeAmount.toString() != "0.00") {
                        return "ERROR {1}, error message does not exist yet";
                    }
                }
            } else {
                if (feeAmount.toString() != "0.00") {
                    //return "ERROR {2}, error message does not exist yet";
                }
            }
        }

        return null;
    }

    /**
     * Validación para el Alias del Número Celular
     * Validation for Cellphone Number Alias
     * @param paymentType PaymentType, necessary to perform proper validation
     * @param numAlias Cellphone Number Alias to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateCellAlias(String paymentType, String numAlias) {
        numAlias = numAlias.trim();
        Pattern specialCharacters = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher match = specialCharacters.matcher(numAlias);
        boolean containsSpecialCharacters = match.find();

        if (containsSpecialCharacters) {
            return "Valor inválido, el Campo Alias del Número Celular del Comprador debe ser numérico";
        }

        if (numAlias.length() == 0) {
            return "Valor inválido, el Campo Alias del Número Celular del Comprador está vacío o es diferente de 0";
        }

        if (numAlias.length() > 10) {
            return "Valor inválido, el Campo Alias del Número Celular del Comprador no debe ser mayor a 10 dígitos";
        }

        if (checkAlphaNumeric(numAlias)) {
            return "Valor inválido, el Campo Alias del Número Celular del Comprador debe ser numérico";
        }

        if (!(paymentType.equalsIgnoreCase("19") || paymentType.equalsIgnoreCase("20") || paymentType.equalsIgnoreCase("21") || paymentType.equalsIgnoreCase("22"))) {
            if (!numAlias.equals("") || numAlias.equalsIgnoreCase("0")) {
                //return "Valor inválido, el Campo Alias del Número Celular del Comprador está vacío o es diferente de cero";
                return null;
            }
        }
        return null;
    }

    /**
     * Validación para el Dígito Verificador del Comprador
     * Validation for Verifier Digit
     * @param digBuyer Verifier Digit to be validated
     * @return Cadena de mensaje de error de validación si hay un error
     */
    private static String validateDigitalBuyer(String digBuyer) {
        Pattern specialCharacters = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        digBuyer = digBuyer.trim();
        Matcher match = specialCharacters.matcher(digBuyer);
        boolean containsSpecialCharacters = match.find();

        if (digBuyer.length() == 0) {
            return "Valor inválido, el campo está vacío o es diferente de 0";
        }

        if (containsSpecialCharacters) {
            return "Valor inválido, el campo Digito Verificador del Comprador debe ser numérico";
        }

        if (checkAlphaNumeric(digBuyer)) {
            return "Valor inválido, el campo Dígito Verificador del Comprador debe ser numérico";
        }

        if (digBuyer.length() > 3) {
            return "Valor Inválido, el campo Digito Verificador del comprador no debe ser mayor a 3 digitos";
        }
        return null;
    }

    /**
     *
     * @param ivaAmount Tax amount to be validated
     * @return Cadena de mensaje de error de validación si hay un error, de lo contrario nulo
     */
    private static String validateIvaAmount(BigDecimal ivaAmount) {
        if (ivaAmount != null) {
            long length = String.valueOf(ivaAmount).length();

            if (length == 0) {
                return "Monto Inválido, no puede estar vacío";
            }

            if (ivaAmount.compareTo(BigDecimal.ZERO) == 0) {
                return "Monto Inválido, no puede ser 0";
            }

            if (ivaAmount.toString().indexOf(".") > 12) {
                return "Monto Inválido, no puede ser mayor a 999,999,999,999.99";
            } else if (!ivaAmount.toString().contains(".") && length > 12) {
                return "Monto Inválido, no puede ser mayor a 999,999,999,999.99";
            }
        } else {
            return "Monto Inválido, no puede estar vacío";
        }

        return null;
    }

    /**
     * Método auxiliar que determina si una cadena contiene solo letras
     * Helper method that validates if a string contains only letters
     * @param value String to be validated
     * @return true or false
     */
    private static boolean checkAlphaNumeric(String value) {
        char[] chars = value.toCharArray();
        ArrayList<Boolean> list = new ArrayList<>();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                list.add(false);
            } else {
                list.add(true);
            }
        }

        return list.contains(true);
    }

    @Deprecated
    public static boolean checkNumber(String value) {
        char[] chars = value.toCharArray();
        ArrayList<Boolean> list = new ArrayList<>();

        for (char c : chars) {
            if (!Character.isDigit(c)) {
                list.add(false);
            } else {
                list.add(true);
            }
        }

        if (list.contains(true)) {
            return true;
        }
        return false;
    }
    
    

}
