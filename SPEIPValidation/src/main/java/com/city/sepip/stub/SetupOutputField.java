
package com.city.sepip.stub;

import com.city.sepip.model.CustomerHubModel;

/**
 * @author Wipro Digital for Citi Banamex
 * Sukanta Misra, Carlos de la Rosa, Santiago Montesinos
 */
public class SetupOutputField {

    CustomerHubModel model = new CustomerHubModel();

    /**
     * Test data used for outPut fields
     *
     * @param testSet Number that indicates the test set that will be used
     * @return Test customer data
     */
    public static CustomerHubModel setOutputField(String testSet) {
        CustomerHubModel model = new CustomerHubModel();

        if (testSet.equalsIgnoreCase("1")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("1st NombreBenefic2");
            model.setrFCOCurpBeneficiario2("1st  rFCOCurpBene2");
            model.setCuentaBeneficiario2("12345");
            model.setAliasDelNumeroCelularDelVendedor("123");
            model.setDigitoVerificadorDelVendedor("234");
            model.setNumeroDeSerieDelCertificado("1271238askdjfhkdeert");
            model.setSelloDigital("12121212121212121212121212121212121212121212121");
        }

        if (testSet.equalsIgnoreCase("2")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("2nd NombreBenefic2");
            model.setrFCOCurpBeneficiario2("2nd  rFCOCurpBene2");
            model.setCuentaBeneficiario2("56789");
            model.setAliasDelNumeroCelularDelVendedor("678");
            model.setDigitoVerificadorDelVendedor("873");
            model.setNumeroDeSerieDelCertificado("1271238askdjfhkdeert");
            model.setSelloDigital("21212121212121212121212121212121212121212121212");
        }

        if (testSet.equalsIgnoreCase("3")) {
            //Output Fields
            //To test if Trype of Account is 3 and CuentaBeneficiario is not 16 digits
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("3rd NombreBenefic2");
            model.setrFCOCurpBeneficiario2("3rd  rFCOCurpBene2");
            model.setCuentaBeneficiario2("987654");
            model.setAliasDelNumeroCelularDelVendedor("987");
            model.setDigitoVerificadorDelVendedor("561");
            model.setNumeroDeSerieDelCertificado("1271238askdjfhkdeert");
            model.setSelloDigital("321321321321321321321321321321321321321321321");
        }

        if (testSet.equalsIgnoreCase("4")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("4th rFCOCurp");
            model.setNombreBeneficiario2("4th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("4th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("409823");
            model.setAliasDelNumeroCelularDelVendedor("456");
            model.setDigitoVerificadorDelVendedor("409");
            model.setNumeroDeSerieDelCertificado("1271238askdjfhkdeert");
            model.setSelloDigital("432143214321432143214321432143214321432143214321");
        }

        if (testSet.equalsIgnoreCase("5")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("5th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("5th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("509267");
            model.setAliasDelNumeroCelularDelVendedor("519");
            model.setDigitoVerificadorDelVendedor("572");
            model.setNumeroDeSerieDelCertificado("5th numeroDeSerieDe");
            model.setSelloDigital("54321543215432154321543214321543215432154321543215432154321");
        }

        if (testSet.equalsIgnoreCase("6")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("6th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("5th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("609491");
            model.setAliasDelNumeroCelularDelVendedor("678");
            model.setDigitoVerificadorDelVendedor("605");
            model.setNumeroDeSerieDelCertificado("6th numeroDeSerieDe");
            model.setSelloDigital("6789678967896789678967896789AWER6167TEVRTYH");
        }

        if (testSet.equalsIgnoreCase("7")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1p");
            model.setNombreBeneficiario2("7th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("7th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("78156734");
            model.setAliasDelNumeroCelularDelVendedor("783");
            model.setDigitoVerificadorDelVendedor("750");
            model.setNumeroDeSerieDelCertificado("7th numeroDeSerieDe");
            model.setSelloDigital("7812341234578967549098765656ERQANJTGSOPBXFR");
        }

        if (testSet.equalsIgnoreCase("8")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("8th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("8th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("8756234");
            model.setAliasDelNumeroCelularDelVendedor("893");
            model.setDigitoVerificadorDelVendedor("832");
            model.setNumeroDeSerieDelCertificado("8th numeroDeSerieDe");
            model.setSelloDigital("89123456098465765556774233566AITHNFTHSDRTHGOKIUT5677642467");
        }

        if (testSet.equalsIgnoreCase("9")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("9th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("9th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("9234567");
            model.setAliasDelNumeroCelularDelVendedor("931");
            model.setDigitoVerificadorDelVendedor("981");
            model.setNumeroDeSerieDelCertificado("9th numeroDeSerieDe");
            model.setSelloDigital("61783456AKDHASLKDJ8975674233566FGTHIJUWHFTNSH812340FHKSHDSKHDAK");
        }

        if (testSet.equalsIgnoreCase("10")) {
            //Output Fields
            //To test if Trype of Account is 10 and CuentaBeneficiario is not 16 digits
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("10th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("10th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("109786");
            model.setAliasDelNumeroCelularDelVendedor("109");
            model.setDigitoVerificadorDelVendedor("896");
            model.setNumeroDeSerieDelCertificado("10th numeroDeSerieDe");
            model.setSelloDigital("9374239472394729347321047R8DJKFHKSJHDFKNKSDJHFIWUEHFDKNX82479874917");
        }

        if (testSet.equalsIgnoreCase("11")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("11th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("11th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("109786");
            model.setAliasDelNumeroCelularDelVendedor("109");
            model.setDigitoVerificadorDelVendedor("896");
            model.setNumeroDeSerieDelCertificado("11th numeroDeSerieDe");
            model.setSelloDigital("109897343894239472394728JHDGAJKGHDUASCBWUEGYFUYSDCBSDHUIERYGFISDH");
        }

        if (testSet.equalsIgnoreCase("12")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("12th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("12th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("12th numeroDeSerieDe");
            model.setSelloDigital("128923749ASKJDHASIHDIDKFGYIERUHSCDBNIEURFGIUWSHVI783648237649236482SDKJHFSKJ");
        }

        if (testSet.equalsIgnoreCase("15")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("15th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("15th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("137567");
            model.setAliasDelNumeroCelularDelVendedor("135");
            model.setDigitoVerificadorDelVendedor("129");
            model.setNumeroDeSerieDelCertificado("15th numeroDeSerieDe");
            model.setSelloDigital("AKSJHDIUHAEIRUQWERU98RUQ9R7Q9W8789Q749QW7489Q7R9YAIOH");
        }

        if (testSet.equalsIgnoreCase("16")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("16th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("16th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127437");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("16th numeroDeSerieDe");
            model.setSelloDigital("SDKFHWEUIRYUIWEYR9823Y8ROIAEHJFKSHRIW8Y9RIFHS");
        }

        if (testSet.equalsIgnoreCase("17")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("17th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("17th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("1274517");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("17th numeroDeSerieDe");
            model.setSelloDigital("DSFJKHEIURYHUIWEY478236459387849278492785943725649478239RUSOFDHZMD");
        }

        if (testSet.equalsIgnoreCase("18")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("18th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("18th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("18914");
            model.setAliasDelNumeroCelularDelVendedor("927");
            model.setDigitoVerificadorDelVendedor("422");
            model.setNumeroDeSerieDelCertificado("18th numeroDeSerieDe");
            model.setSelloDigital("SDFHKWRY78345682376235968491274923784928YRSIHXJBJKSRFE");
        }

        if (testSet.equalsIgnoreCase("19")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("19th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("19th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("65433223");
            model.setAliasDelNumeroCelularDelVendedor("5434");
            model.setDigitoVerificadorDelVendedor("3554");
            model.setNumeroDeSerieDelCertificado("19th numeroDeSerieDe");
            model.setSelloDigital("AKDJFHWEUIGR782365482376491378419073492749825792R8HASKFHK");
        }

        if (testSet.equalsIgnoreCase("20")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("20th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("20th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("434");
            model.setAliasDelNumeroCelularDelVendedor("4534523");
            model.setDigitoVerificadorDelVendedor("3454");
            model.setNumeroDeSerieDelCertificado("20th numeroDeSerieDe");
            model.setSelloDigital("AKLRHWEIURYQIFUASNCKASJHF78WYR28935623987492URAOFJIO");
        }

        if (testSet.equalsIgnoreCase("21")) {
            //Output Fields
            //To test if Trype of Account is 10 and CuentaBeneficiario is not 10 digits
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("21th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("21th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("21th numeroDeSerieDe");
            model.setSelloDigital("ASJKDHFWEUIYR782348QHDIAUHF87YW38RY7AEIFHAKJHEFIASUF");
        }

        if (testSet.equalsIgnoreCase("22")) {
            //Output Fields
            //To test if payment type is 22
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("22th NombreBenef");
            model.setrFCOCurpBeneficiario2("22th rFCOCu");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("22th numeroDeSerieDe");
            model.setSelloDigital("ADKSJFRWEUIRY7823428364523764823YR8QWHFUWEIGFAIU");
        }

        if (testSet.equalsIgnoreCase("23")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("23th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("23th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("23th numeroDeSerieDe");
            model.setSelloDigital("AKJHR23UT74782RGYAGUIFGW48IEFHAJZBFYW8TYR8AEFUIH");
        }

        if (testSet.equalsIgnoreCase("24")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("24th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("24th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("24th numeroDeSerieDe");
            model.setSelloDigital("EWJKRHF2347856R7823QWGEFQAUG8W74TGEFAUIGBIUSA");
        }

        if (testSet.equalsIgnoreCase("25")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("25th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("12th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("25th numeroDeSerieDe");
            model.setSelloDigital("KASHDKAHDIQYWI9AUHIHCW8E7YR8ASIHFACI");
        }

        if (testSet.equalsIgnoreCase("26")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("26th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("26th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("26th numeroDeSerieDe");
            model.setSelloDigital("32847398QIHDAIDHQ378YRQIAWHBBCVIASIBVHIB");
        }

        if (testSet.equalsIgnoreCase("27")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("27th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("27th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("27th numeroDeSerieDe");
            model.setSelloDigital("2397842394789QHDSAKHDFAKHDFIUQWEYR8Q7UIDFQIAGFIUEQG7");
        }

        if (testSet.equalsIgnoreCase("28")) {
            //Output Fields
            //To test if Trype of Account is 40 and CuentaBeneficiario is not 18 digits
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("0121800011351240");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("28th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("28th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("28th numeroDeSerieDe");
            model.setSelloDigital("HUWIERYQWIUQAFAIUH782641864817649174892RSKJFBASJK");
        }

        if (testSet.equalsIgnoreCase("29")) {
            //Output Fields
            model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
            model.setNombreBeneficiario("FRANCISCO MORALES GARCIA");
            model.setCuentaBeneficiario("012180001135124014");
            model.setrFCOCurpBeneficiario("AYSA871101YH1");
            model.setNombreBeneficiario2("29th NombreBenefic2");
            model.setrFCOCurpBeneficiario2("29th  rFCOCurpBene2");
            model.setCuentaBeneficiario2("127567");
            model.setAliasDelNumeroCelularDelVendedor("127");
            model.setDigitoVerificadorDelVendedor("122");
            model.setNumeroDeSerieDelCertificado("29th numeroDeSerieDe");
            model.setSelloDigital("45289748923749174912KSFJHGSUIDHZIFJKBVSDFUHGI837656276");
        }

        return model;

    }
}
