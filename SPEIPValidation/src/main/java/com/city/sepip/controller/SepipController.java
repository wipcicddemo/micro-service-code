/**
 *
 */
package com.city.sepip.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.sql.Timestamp;
import java.util.Random;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerMapping;

import com.citi.sepip.service.CustomerHubModelService;
import com.city.sepip.model.CDAResponse;
import com.city.sepip.model.CustomerHubModel;
import com.city.sepip.model.CustomerHubModelEntity;
import com.city.sepip.model.CustomerHubModelId;
import com.city.sepip.model.ErrorDetail;
import com.city.sepip.model.ErrorResponse;
import com.city.sepip.model.SepipDB;
import com.city.sepip.model.SepipModel;
//import com.city.sepip.repo.SepipRepo;
import com.city.sepip.stub.SetupOutputField;
import com.city.sepip.util.ValidateOutputField;
import com.city.sepip.util.ValidationUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

import org.json.JSONObject;


/**
 * @author Wipro Digital for Citi Banamex Sukanta Misra, Carlos de la Rosa,
 *         Santiago Montesinos, TCS Daniel Huerta, Juan Carlos Cocoletzi, Hugo Mejia
 */
@RestController
@RequestMapping("/v1")
@Api(value="CDA Generator")
public class SepipController {
	private static final Logger LOGGER = LoggerFactory.getLogger(SepipController.class);

//	@Autowired
//	SepipRepo sepipRepo;

	@Autowired
	private Environment env;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	CustomerHubModelService customerHubModelService;

	/**
	 * REST service that validates CDA structure. If valid, saves into a MongoDB
	 * instance, else returns an list of errors
	 * 
	 * @param sepipModel Model that defines input parameters for CDA validation
	 * @return list of errors (if any)
	 */
    //@ExceptionHandler(value = Exception.class)
	@ApiOperation(value = "Input method to create a CDA", response = CDAResponse.class)
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Successfully created CDA"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
	    @ApiResponse(code = 422, message = "The information introduced is not correct")
	})
	@RequestMapping(value = "/sepip", method = RequestMethod.POST)
	public ResponseEntity<String> getSepipValues(@Valid @RequestBody SepipModel sepipModel) {
		LOGGER.info("Start Service /sepip for Validation of as {} ", sepipModel.toString());
		String validatationResult = null;
		CustomerHubModel model = new CustomerHubModel();
		// Validate input fields
		validatationResult = ValidationUtil.validateInputFields(sepipModel);

		// If errors are found, return them
		if (validatationResult != null) {
			return ResponseEntity
					.unprocessableEntity()
		            .header(HttpHeaders.CONTENT_TYPE, "application/json")
		            .body("{\"message\":" +   "\"|| " + validatationResult  + "||\" }");


			//return "|| " + validatationResult + " ||";
		} else {
			// Create customerHubModel
			String validateCuentaBeneficiarioString = createCustomerHubModel(sepipModel, model);
			
				boolean modelExists = modelExist(sepipModel.getTrackingKey(), sepipModel.getSpeiOperationDate());
				System.out.println(">>>>>>>>>>>>>>>>>>>>>><MExico: "+customerHubModelService.findById(new CustomerHubModelId(sepipModel.getTrackingKey(), sepipModel.getSpeiOperationDate())).toString());
				if(modelExists) {
					return ResponseEntity
							.unprocessableEntity()
				            .header(HttpHeaders.CONTENT_TYPE, "application/json")
				            .body("{\"message\": \"El registro ya se encuentra en Base de Datos\"}");

				}
				System.out.println(modelExists);
		
				boolean isAValidDigitalStamp = false; 
				if (getSelloDigital(model) != "Error getting digital certificate") 
					isAValidDigitalStamp = true;
				
				if(!isAValidDigitalStamp) {
					return ResponseEntity
							.unprocessableEntity()
				            .header(HttpHeaders.CONTENT_TYPE, "application/json")
		            		.body("{\"message\": \"Hubo problemas al generar sello digital, por lo que no es  posible generar CDA\"}");

				}
				
			
			// Save record to MongoDB instance
			if (validateCuentaBeneficiarioString == null && modelExists == false && isAValidDigitalStamp) {
				model.setOriginalString(buildOriginalString(model));
				model.setSelloDigital(getSelloDigital(model));
				model.setCDA(buildCdaOutputString(model));
				saveCdaToDb(model); 
				customerHubModelService.addCustomerHubModel(model);
			} else {
				model.setCDA("||" + validateCuentaBeneficiarioString + "||");
			}

			if (validateCuentaBeneficiarioString == null) {
				return ResponseEntity
						.ok()
			            .header(HttpHeaders.CONTENT_TYPE, "application/json")
	            		.body("{\"cda\":" +   "\"" + buildCdaOutputString(model)  + "\","
	            				+ "\"certificateSerialNumber\":" +   "\"" + model.getNumeroDeSerieDelCertificado()  + "\","
	    	            		+ "\"originalString\":" +   "\"" + model.getOriginalString()  + "\","
	    	    	            + "\"digitalStamp\":" +   "\"" + model.getSelloDigital()  + "\""
	            				+ " }");
				

			            //.body(buildCdaOutputString(model));
				
				//return buildCdaOutputString(model);
			} else {
				return ResponseEntity
						.ok()
			            .header(HttpHeaders.CONTENT_TYPE, "application/json")
	            		.body("{\"message\":" +   "\"" + validateCuentaBeneficiarioString  + "\" }");			            

			            //.body("||" + validateCuentaBeneficiarioString + "||");
				//return "||" + validateCuentaBeneficiarioString + "||";
			}
		}
	}



	//@GetMapping("/{trackingKey}/{calenderDatePayment}")
	public SepipDB findSepipModelBytrackingKey(@PathVariable String trackingKey, @PathVariable Integer calenderDatePayment){
		SepipDB retorno = null;
		System.out.println("Inicia");
		System.out.println("trackingKey: "+trackingKey);
		System.out.println("calenderDatePayment: "+calenderDatePayment);
		try{
			//sepipRepo.findCustomerHubModelBytrackingKey("21");
			//retorno = sepipRepo.findSepipModelByTrackingKeyAndcalenderDatePayment(trackingKey,calenderDatePayment);
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("sale");
		return retorno;
	}

	/**
	 * Builds the output String based on CustomerHubModel structure
	 * 
	 * @param model Customer Model built previously
	 * @return Output string to be shown as REST response
	 */
	private String buildCdaOutputString(CustomerHubModel model) {
		return "||" + model.getPaymentType() + "|" + model.getStrSpeiOperationDate() + "|"
				+ model.getStrCalenderDatePayment() + "|" + model.getCalenderTimePayment() + "|"
				+ model.getCdaParticipientSPEIKey() + "|" + model.getNameOfTransferOrderIssuerParticipant() + "|"
				+ model.getOrderingName() + "|" + model.getOrdererAccountType() + "|" + model.getOrderingAccount() + "|"
				+ model.getRfcOrCurpPayer() + "|" + model.getMombreDelParticipanteReceptor() + "|"
				+ model.getNombreBeneficiario() + "|" + model.getTypeOfBeneficiaryAccount() + "|"
				+ model.getCuentaBeneficiario() + "|" + model.getrFCOCurpBeneficiario() + "|"
				+ model.getConceptOfPayment() + "|" + model.getIvaAmount() + "|" + model.getAmountOfPayment() + "|"
				+ model.getNombreBeneficiario2() + "|" + model.getrFCOCurpBeneficiario2() + "|"
				+ model.getBeneficiaryAccountType2() + "|" + model.getCuentaBeneficiario2() + "|"
				+ model.getDigitalCollectionSchemeFolio() + "|" + model.getPaymentOfTheTransferFee() + "|"
				+ model.getTransferFeeAmount() + "|" + model.getBuyerCellNumberAliases() + "|"
				+ model.getDigitBuyersVerifier() + "|" + model.getAliasDelNumeroCelularDelVendedor() + "|"
				+ model.getDigitoVerificadorDelVendedor() + "|" + model.getNumeroDeSerieDelCertificado() + "|"
				+ model.getSelloDigital() + "||";
	}

	/**
	 * Builds the originalString String based on CustomerHubModel structure
	 * 
	 * @param model Customer Model built previously
	 * @return Output string to be shown as REST response
	 */
	private String buildOriginalString(CustomerHubModel model) {
		return "||" + model.getPaymentType() + "|" + model.getStrSpeiOperationDate() + "|"
				+ model.getStrCalenderDatePayment() + "|" + model.getCalenderTimePayment() + "|"
				+ model.getCdaParticipientSPEIKey() + "|" + model.getNameOfTransferOrderIssuerParticipant() + "|"
				+ model.getOrderingName() + "|" + model.getOrdererAccountType() + "|" + model.getOrderingAccount() + "|"
				+ model.getRfcOrCurpPayer() + "|" + model.getMombreDelParticipanteReceptor() + "|"
				+ model.getNombreBeneficiario() + "|" + model.getTypeOfBeneficiaryAccount() + "|"
				+ model.getCuentaBeneficiario() + "|" + model.getrFCOCurpBeneficiario() + "|"
				+ model.getConceptOfPayment() + "|" + model.getIvaAmount() + "|" + model.getAmountOfPayment() + "|"
				+ model.getNombreBeneficiario2() + "|" + model.getrFCOCurpBeneficiario2() + "|"
				+ model.getBeneficiaryAccountType2() + "|" + model.getCuentaBeneficiario2() + "|"
				+ model.getDigitalCollectionSchemeFolio() + "|" + model.getPaymentOfTheTransferFee() + "|"
				+ model.getTransferFeeAmount() + "|" + model.getBuyerCellNumberAliases() + "|"
				+ model.getDigitBuyersVerifier() + "|" + model.getAliasDelNumeroCelularDelVendedor() + "|"
				+ model.getDigitoVerificadorDelVendedor() + "|" + model.getNumeroDeSerieDelCertificado() + "||";
	}

	/**
	 * Creates an instance of a Cda based on input fields
	 * 
	 * @param sepipModel Sepip Model received in REST service
	 * @param model      Customer model built based on sepipModel fields
	 * @return Beneficiary account (cuenta beneficiario)
	 */
	private String createCustomerHubModel(@RequestBody @Valid SepipModel sepipModel, CustomerHubModel model) {
		CustomerHubModel customerHubModel;
		customerHubModel = new CustomerHubModel();

		customerHubModel.setTrackingKey(sepipModel.getTrackingKey());
		customerHubModel.setPaymentType(sepipModel.getPaymentType());
		customerHubModel.setAmountOfPayment(sepipModel.getAmountOfPayment());
		customerHubModel.setBeneficiaryAccountType2(sepipModel.getBeneficiaryAccountType2());

		customerHubModel.setCalenderDatePayment(sepipModel.getCalenderDatePayment());
		customerHubModel.setCalenderTimePayment(sepipModel.getCalenderTimePayment());
		customerHubModel.setCdaParticipientSPEIKey(sepipModel.getCdaParticipientSPEIKey());
		customerHubModel.setConceptOfPayment(sepipModel.getConceptOfPayment());
		customerHubModel.setNameOfTransferOrderIssuerParticipant(sepipModel.getNameOfTransferOrderIssuerParticipant());
		customerHubModel.setOrderingName(sepipModel.getOrderingName());
		customerHubModel.setOrdererAccountType(sepipModel.getOrdererAccountType());
		customerHubModel.setOrderingAccount(sepipModel.getOrderingAccount());
		customerHubModel.setRfcOrCurpPayer(sepipModel.getRfcOrCurpPayer());
		customerHubModel.setTypeOfBeneficiaryAccount(sepipModel.getTypeOfBeneficiaryAccount());
		customerHubModel.setConceptOfPayment(sepipModel.getConceptOfPayment());
		customerHubModel.setIvaAmount(sepipModel.getIvaAmount());
		customerHubModel.setAmountOfPayment(sepipModel.getAmountOfPayment());
		customerHubModel.setBeneficiaryAccountType2(sepipModel.getBeneficiaryAccountType2());
		customerHubModel.setDigitalCollectionSchemeFolio(sepipModel.getDigitalCollectionSchemeFolio());
		customerHubModel.setPaymentOfTheTransferFee(sepipModel.getPaymentOfTheTransferFee());

		LOGGER.info("sepipModel.getTransferFeeAmount() : " + sepipModel.getTransferFeeAmount());
		LOGGER.info("sepipModel.getTransferFeeAmount().setScale(2) : " + sepipModel.getTransferFeeAmount().setScale(2));

		customerHubModel.setTransferFeeAmount(sepipModel.getTransferFeeAmount().setScale(2));
		customerHubModel.setBuyerCellNumberAliases(sepipModel.getBuyerCellNumberAliases());
		customerHubModel.setDigitBuyersVerifier(sepipModel.getDigitBuyersVerifier());
		customerHubModel.setSpeiOperationDate(sepipModel.getSpeiOperationDate());
		customerHubModel.setIdTransaccion(sepipModel.getIdTransaccion());
		
		//Se agrega folio a cada CDA en formato timestamp
		Timestamp folioCDA = new Timestamp(System.currentTimeMillis());
        customerHubModel.setFolioCDA(folioCDA.getTime());
        
		
		// Input Fields
		model.setTrackingKey(customerHubModel.getTrackingKey());
		model.setPaymentType(customerHubModel.getPaymentType());
		model.setAmountOfPayment(customerHubModel.getAmountOfPayment());

		if (!sepipModel.getPaymentType().equalsIgnoreCase("22")) {
			model.setBeneficiaryAccountType2(0);
		} else {
			model.setBeneficiaryAccountType2(customerHubModel.getBeneficiaryAccountType2());
		}

		model.setBuyerCellNumberAliases(customerHubModel.getBuyerCellNumberAliases());
		model.setCalenderDatePayment(customerHubModel.getCalenderDatePayment());
		model.setCalenderTimePayment(customerHubModel.getCalenderTimePayment());
		model.setCdaParticipientSPEIKey(customerHubModel.getCdaParticipientSPEIKey());
		model.setConceptOfPayment(customerHubModel.getConceptOfPayment());
		model.setNameOfTransferOrderIssuerParticipant(customerHubModel.getNameOfTransferOrderIssuerParticipant());
		model.setOrderingName(customerHubModel.getOrderingName());
		model.setOrdererAccountType(customerHubModel.getOrdererAccountType());
		model.setOrderingAccount(customerHubModel.getOrderingAccount());
		model.setRfcOrCurpPayer(customerHubModel.getRfcOrCurpPayer());
		model.setTypeOfBeneficiaryAccount(customerHubModel.getTypeOfBeneficiaryAccount());
		model.setConceptOfPayment(customerHubModel.getConceptOfPayment());
		model.setIvaAmount(sepipModel.getIvaAmount().setScale(2));
		model.setAmountOfPayment(customerHubModel.getAmountOfPayment().setScale(2));
		model.setDigitalCollectionSchemeFolio(customerHubModel.getDigitalCollectionSchemeFolio());

		if (sepipModel.getPaymentType().equalsIgnoreCase("19") || sepipModel.getPaymentType().equalsIgnoreCase("20")
				|| sepipModel.getPaymentType().equalsIgnoreCase("21")
				|| sepipModel.getPaymentType().equalsIgnoreCase("22")) {
			model.setPaymentOfTheTransferFee(customerHubModel.getPaymentOfTheTransferFee());
		} else {
			model.setPaymentOfTheTransferFee(0);
		}

		if (!(sepipModel.getPaymentType().equalsIgnoreCase("19") || sepipModel.getPaymentType().equalsIgnoreCase("20")
				|| sepipModel.getPaymentType().equalsIgnoreCase("21")
				|| sepipModel.getPaymentType().equalsIgnoreCase("22"))) {
			model.setBuyerCellNumberAliases("NA");
		} else {
			model.setBuyerCellNumberAliases(customerHubModel.getBuyerCellNumberAliases());
		}

		if (!(sepipModel.getPaymentType().equalsIgnoreCase("19") || sepipModel.getPaymentType().equalsIgnoreCase("20")
				|| sepipModel.getPaymentType().equalsIgnoreCase("21")
				|| sepipModel.getPaymentType().equalsIgnoreCase("22"))) {
			model.setTransferFeeAmount(new BigDecimal("0.00"));
		} else {
			model.setTransferFeeAmount(customerHubModel.getTransferFeeAmount().setScale(2));
		}

		if (!(sepipModel.getPaymentType().equalsIgnoreCase("19") || sepipModel.getPaymentType().equalsIgnoreCase("20")
				|| sepipModel.getPaymentType().equalsIgnoreCase("21")
				|| sepipModel.getPaymentType().equalsIgnoreCase("22"))) {
			model.setDigitBuyersVerifier("NA");
		} else {
			model.setDigitBuyersVerifier(customerHubModel.getDigitBuyersVerifier());
		}

		model.setSpeiOperationDate(customerHubModel.getSpeiOperationDate());
		model.setFolioCDA(folioCDA.getTime());

		/*
		 * //Output Fields model.setMombreDelParticipanteReceptor("BBVA BANCOMER");
//		 * model.setNombreBeneficiario("NombreBeneficiario");
//		 * model.setCuentaBeneficiario("012180001135124014");
//		 * model.setrFCOCurpBeneficiario("rFCOCurpBeneficiario");
//		 * model.setNombreBeneficiario2("NombreBeneficiario2");
//		 * model.setrFCOCurpBeneficiario2("setrFCOCurpBeneficiario2");
//		 * model.setCuentaBeneficiario2(1234556);
//		 * model.setAliasDelNumeroCelularDelVendedor(234);
//		 * model.setDigitoVerificadorDelVendedor(234);
//		 * model.setNumeroDeSerieDelCertificado("numeroDeSerieDelCertificado");
//		 * model.setSelloDigital("12346678899763qeekjhdiqwudhiasnxkanhduiqwaxnKDJNiqwuhI");
		 */
		
		//CustomerHubModel model1 = SetupOutputField.setOutputField(sepipModel.getPaymentType());

		//CustomerHubModel model1 = customerHubModel;
		model.setMombreDelParticipanteReceptor(sepipModel.getMombreDelParticipanteReceptor());
		model.setNombreBeneficiario(sepipModel.getNombreBeneficiario());
		model.setCuentaBeneficiario(sepipModel.getCuentaBeneficiario());
		model.setrFCOCurpBeneficiario(sepipModel.getrFCOCurpBeneficiario());

		if (sepipModel.getPaymentType().equalsIgnoreCase("22")) {
			model.setNombreBeneficiario2(sepipModel.getNombreBeneficiario2());
		} else {
			model.setNombreBeneficiario2("NA");
		}

		if (sepipModel.getPaymentType().equalsIgnoreCase("22")) {
			model.setrFCOCurpBeneficiario2(sepipModel.getrFCOCurpBeneficiario2());
		} else {
			model.setrFCOCurpBeneficiario2("NA");
		}

		if (!sepipModel.getPaymentType().equalsIgnoreCase("22")) {
			model.setCuentaBeneficiario2("0");
		} else {
			model.setCuentaBeneficiario2(sepipModel.getCuentaBeneficiario2());
		}

		if (!(sepipModel.getPaymentType().equalsIgnoreCase("19") || sepipModel.getPaymentType().equalsIgnoreCase("20")
				|| sepipModel.getPaymentType().equalsIgnoreCase("21")
				|| sepipModel.getPaymentType().equalsIgnoreCase("22"))) {
			model.setAliasDelNumeroCelularDelVendedor("NA");
		} else {
			model.setAliasDelNumeroCelularDelVendedor(sepipModel.getAliasDelNumeroCelularDelVendedor());
		}

		if (!(sepipModel.getPaymentType().equalsIgnoreCase("19") || sepipModel.getPaymentType().equalsIgnoreCase("20")
				|| sepipModel.getPaymentType().equalsIgnoreCase("21")
				|| sepipModel.getPaymentType().equalsIgnoreCase("22"))) {
			model.setDigitalCollectionSchemeFolio("NA");
		} else {
			model.setDigitalCollectionSchemeFolio(model.getDigitalCollectionSchemeFolio());
		}

		// model.setAliasDelNumeroCelularDelVendedor(model1.getAliasDelNumeroCelularDelVendedor());

		if (!(sepipModel.getPaymentType().equalsIgnoreCase("19") || sepipModel.getPaymentType().equalsIgnoreCase("20")
				|| sepipModel.getPaymentType().equalsIgnoreCase("21")
				|| sepipModel.getPaymentType().equalsIgnoreCase("22"))) {
			model.setDigitoVerificadorDelVendedor("NA");
		} else {
			model.setDigitoVerificadorDelVendedor(sepipModel.getDigitoVerificadorDelVendedor());
		}

		model.setNumeroDeSerieDelCertificado(sepipModel.getNumeroDeSerieDelCertificado());
		//model.setSelloDigital(model1.getSelloDigital());

		String validateCuentaBeneficiarioString = ValidateOutputField.validateOutputFieldValues(sepipModel, model);

		if (Integer.toString(model.getSpeiOperationDate()).length() == 7) {
			model.setStrSpeiOperationDate(String.format("%08d", model.getSpeiOperationDate()));
		} else {
			model.setStrSpeiOperationDate(Integer.toString(model.getSpeiOperationDate()));
		}

		if (Integer.toString(model.getCalenderDatePayment()).length() == 7) {
			model.setStrCalenderDatePayment(String.format("%08d", model.getCalenderDatePayment()));
		} else {
			model.setStrCalenderDatePayment(Integer.toString(model.getCalenderDatePayment()));
		}
		
		model.setIdTransaccion(customerHubModel.getIdTransaccion());

		// se agrega fecha en formato de fecha, CDA y cadena original

		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyy");
		try {
			String dateString = new String();
			if (Integer.toString(customerHubModel.getCalenderDatePayment()).length() == 7) {
				dateString = "0" + Integer.toString(customerHubModel.getCalenderDatePayment());
			} else {
				dateString = Integer.toString(customerHubModel.getCalenderDatePayment());
			}
			Date operDate = dt.parse(dateString);
			model.setSpeiOperationDateFormat(operDate);

			//LOGGER.info("fecha-------------> " + customerHubModel.getCalenderDatePayment());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Convertir horas a mili segundos
		try {
			String hour = customerHubModel.getCalenderTimePayment();
			
			Integer hoursMS = Integer.parseInt(hour.substring(0,2)) * 3600000;
			
			Integer minutsMS = Integer.parseInt(hour.substring(2,4)) * 60000;
			
			Integer secondsMS = Integer.parseInt(hour.substring(4,6)) * 1000;
			
			Integer miliSec = hoursMS + minutsMS + secondsMS;
			
			customerHubModel.setCalenderTimePaymentMS(miliSec);
			model.setCalenderTimePaymentMS(miliSec);
			
			//LOGGER.info("HORA-------------> " + "hora" + hour.substring(0,2) + "minuto" +hour.substring(2,4)+ "segundo" + hour.substring(4,6) + " "+customerHubModel.getCalenderTimePaymentMS());
		} catch (Exception e) {
			customerHubModel.setCalenderTimePaymentMS(0);
			model.setCalenderTimePaymentMS(0);
		}
		
		model.setOriginalFolioPackage(sepipModel.getOriginalFolioPackage());
		model.setOriginalPaymentFolio(sepipModel.getOriginalPaymentFolio());
		
		

		return validateCuentaBeneficiarioString;
	}

	/**
	 * Saves CustomerHubModel in MongoDB
	 * 
	 * @param model Customer Model built previously
	 */
	private void saveCdaToDb(CustomerHubModel model) {
		SepipDB sepipDb;
		// Store the value got from External System to Mongo DB -- UnComment the bellow
		// 3 lines to store the data in mongo DB
		// First setup Mongo DB in the Deployment environment

		LOGGER.info("Inserting in Mongo DB");

		// Setup DB from Environment
		LOGGER.info("Mongo DB infor from Env Variabe " + System.getenv("MONGO_URL"));

		sepipDb = new SepipDB();
		sepipDb.setTrackingKey(model.getTrackingKey());
		sepipDb.setCustomerHubModel(model);


		/*
		 * db.getCollection('sepipDB').find({ $and: [
         {"_id" : "21"}, {"customerHubModel.calenderDatePayment":16112019}
      ]}).count()
		 */


		/*for(int i = 0; i<=100; i ++) {
			int id = getRandomNumberInRange(1, 1000000);
			int contador = i + id;
			sepipDb.setTrackingKey(String.valueOf(contador));
			sepipRepo.save(sepipDb);
        
		}
		*/	
//		sepipRepo.save(sepipDb);
	}
	
	/*
	 * TEST
	 * Metodo para pruebas, solo se usa para generar id`s fake 
	 * 
	 * */
	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	
	
	public String getSelloDigital(CustomerHubModel model) {

		// final String uri = "http://127.0.0.1:8001/sellodigital";
		final String uri = env.getProperty("spring.data.selloDigital.url");

		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);

		String reqNody = "{\"original_string\": \"" + model.getOriginalString() + "\"}";


		try {
			HttpEntity<String> entity = new HttpEntity<>(reqNody, headers);
			ResponseEntity<String> result = restTemplate.exchange(uri,HttpMethod.POST, entity,String.class);
			//String result = restTemplate.postForObject(uri, entity, String.class);
		    JSONObject json = new JSONObject(result.getBody());
			String digitalStamp = (String) json.get("digital_stamp");
			return digitalStamp;
		} catch (Exception e) {
			String result = "Error getting digital certificate";
			return result;
		}

	}
	
	
	/*
	 * Se valida si un registro ya se encuentra en BD
	 * This method validates if a model exists in database
	 * Parameter:
	 * 			Input:  @trackingKey, @calendarDatePayment
	 * 			Output: false if the record doesnt exists 
	 * */
	public boolean modelExist (String trackingKey, Integer calenderDatePayment ) {
//		SepipDB findSepipModelBytrackingKey = findSepipModelBytrackingKey(trackingKey, calenderDatePayment); 
//		boolean result = false; 
//		if (findSepipModelBytrackingKey == null ) result = false;
//		else result = true; 
		
		//Implementación en Oracle
		Optional<CustomerHubModelEntity> findCHM = customerHubModelService.findById(new CustomerHubModelId(trackingKey, calenderDatePayment));
		return findCHM.isPresent();
		
		
		
//		if (result)
//			throw new IllegalArgumentException("El registro ya se encuentra en Base de Datos");
		//return result; 
		
	}
	
	@ExceptionHandler(Exception.class)
	  public ResponseEntity<ErrorDetail> handleException(HttpServletRequest request, Exception e) {
	      ErrorDetail response = new ErrorDetail();
	      Map<String,String> parameters = new HashMap<String,String>();
	      Map<String,String> aditionalInformation = new HashMap<String,String>();
	      StringWriter errors = new StringWriter();
	      e.printStackTrace(new PrintWriter(errors));
	
	      //aditionalInformation.put("trace", errors.toString());
	      response.setUuid("79de05d7-ca3b-4885-85b4-aa80f78a94ef");
	      response.setType("ERROR");
	      
	      if(e instanceof MethodArgumentNotValidException) {
	          StringBuilder sb = new StringBuilder(); 
	          List<FieldError> fieldErrors = ((MethodArgumentNotValidException) e).getBindingResult().getFieldErrors();
	          for(FieldError fieldError: fieldErrors){
	              sb.append(fieldError.getDefaultMessage());
	              sb.append(";");
	          }
	          response.setMessage(sb.toString());
	          
	      }else{
	    	  response.setMessage(e.getMessage());
	      }
	      
	      if(e instanceof HttpMessageNotReadableException) {
	    	  response.setMessage("Error en formato de JSON de entarda");
	      }
	      
	      if(e instanceof NumberFormatException) {
	    	  response.setMessage("Error en tipo de dato de entrada");
	      }
	      
	      

	      response.setParams(parameters);
	      response.setBussinesCode("COOLVIBES");
	      response.setLocation("CDA GENERATION");
	      response.setTimestamp(LocalDateTime.now());
	      response.setAditionalInformation(aditionalInformation);
	      request.removeAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE);
	
	      return new ResponseEntity<ErrorDetail>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	  }


//	@ExceptionHandler(value = Exception.class)
//    public ResponseEntity<ErrorResponse> handleException(HttpServletRequest request, Exception e) {
//        ErrorResponse response = new ErrorResponse();
//        response.setErrorMsg("Error de duplicidad: el registro ya existe en la Base de Datos " + e); // or whatever you want
//        response.setErrorCode("ERROR_POR_DEFINIR"); // or whatever you want
//        request.removeAttribute(
//                  HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE);
//
//        return new ResponseEntity<ErrorResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
//    }
}
