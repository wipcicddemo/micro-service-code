'''
Mejia Regalado Hugo Alejandro, TATA Consultancy Services
'''

import re
import logging
import os
from pymongo import MongoClient
from datetime import date
from datetime import datetime
import time
import json
import sys
import os.path
import requests
import yaml
from datetime import timedelta


def handler(event, context):
    """
    AWS Lambda Handler
    """
    logging.log(logging.INFO, context)
    return post(event)


def post(body, context = None):

    logging.log(logging.INFO, "||||||||||Entrada||||||||||")
    logging.log(logging.INFO, body)
    logging.log(logging.INFO, "||||||||||Entrada||||||||||")

    try:
        with open(r'./api/config.yaml') as file:
            properties_list = yaml.load(file, Loader=yaml.FullLoader)
        connectTo =  properties_list['mongoDBConn']
        conn = MongoClient( connectTo )
        db = conn.admin
        col = db.sepipDB
        mycol = db.paquetesCDA
        print("****Created MongoClient****")

    except Exception as exception:
        return{ 'message': str(exception)}

    return query_mongodb(conn, db, col, mycol,body)

def query_mongodb(conn, db, col, mycol,body):

        DATA = json.loads(body)
        for ELEMENTS in DATA:
            #Variables
            packageFolio = ELEMENTS['packageFolio']
            packageDate = ELEMENTS['packageDate']

            SEARCH_ARRAY = (mycol.find_one({"packageFolio":packageFolio,
                                                  "packageDate":packageDate},
                                                 {"_id":0,
                                                  "package":1}))
            #if SEARCH_ARRAY:
                #Update a BD de paquetes
            mycol.update({"packageFolio":packageFolio,
                        "packageDate":packageDate},
                        {"$set" : {"packageStatus" : "Paquete Exitoso"}})

                #Update a BD de CDAs
            col.update({"customerHubModel.folioPackage":packageFolio},
                            {"$set" : {"customerHubModel.statusCDA" : "CDA Exitoso"}}, multi=True)
            queryMongo(conn,mycol, col, packageFolio,packageDate)

        return { 'message': 'CDA Exitoso, Paquete Exitoso'}



def queryMongo(conn,mycol, col, packageFolio,packageDate):

            SEARCH_ = (mycol.find({"packageFolio":packageFolio,
                                    "packageDate":packageDate},
                                         {"_id":0,
                                          "packageStatus":1,
                                          "packageFolio":1}))

            print("-------Consulta a BD de paquetes----")
            for DICT_ELEM in SEARCH_:
                print(DICT_ELEM)
                packageStatus = DICT_ELEM["packageStatus"]
                print("Status -------->",packageStatus)

            SEARCH_A = (col.find({"customerHubModel.folioPackage":packageFolio, "customerHubModel.statusCDA" : "CDA Exitoso"},
                                  {"_id":0,
                                   "customerHubModel.statusCDA":1,
                                    "customerHubModel.CDA":1,
                                    "customerHubModel.folioPackage":1}))

            print("-----Consulta a BD de CDAs")
            for i in SEARCH_A:
                print(i)
                customerHubModel = i['customerHubModel']
                statusCDA = customerHubModel["statusCDA"]
                print("Status------>",statusCDA)










#
