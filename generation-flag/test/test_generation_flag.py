import re
import logging
import os
from pymongo import MongoClient
import json
import sys
import requests
from api.generationflag import query_mongodb, post

def test_post():
    body=b'[{"packageDate":"13-05-2020","packageFolio":100000018}]'
    conn = MongoClient( "mongodb://localhost:27017/")
    db = conn.paquetes
    col = db.cda
    mycol = db.paquetesCDA
    assert post(body) == query_mongodb(conn, db, col, mycol,body)

def test_query_mongodb():
    body=b'[{"packageDate":"13-05-2020","packageFolio":100000018}]'
    conn = MongoClient( "mongodb://localhost:27017/")
    db = conn.paquetes
    col = db.cda
    mycol = db.paquetesCDA
    assert query_mongodb(conn, db, col, mycol, body) == {"message": "CDA Exitoso, Paquete Exitoso"}
