class Const(object):
    
    @property
    def TIME_IN_SECONDS(self):
        return 86399999

    @property
    def RFC_CURP_MAX_SIZE(self):
        return 18

    @property
    def ACCOUNT_MAX_SIZE(self):
        return 20
    
    @property
    def YEAR_MAX(self):
        return 9999
    
    @property
    def YEAR_MIN(self):
        return 1900
    
    @property
    def TRACKING_KEY_MAX_SIZE(self):
        return 30
    
    @property
    def AMMOUNT_MAX_SIZE(self):
        return 20

    @property
    def EMPTY_FIELDS(self):
        return 4 
    
    @property 
    def DAYS_MAX_ALLOWED(self):
        return 31
    
    @property 
    def DAYS_MIN_ALLOWED(self):
        return 1
        
    @property 
    def MONTH_MAX_ALLOWED(self):
        return 12
    
    @property 
    def MONTH_MIN_ALLOWED(self):
        return 1