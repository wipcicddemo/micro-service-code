from pymongo import MongoClient
import configDB


################################################################################
#-------------------------BASE DE DATOS TABLA COMPLETA-------------------------#
################################################################################

def getStatus(code):
    client = MongoClient('mongodb://localhost:27017/')
    db = client.citi
    col = db.CUT_SPEI_STATUS
    SEARCH_ARRAY = (col.find({"code":code},{"_id":0,"code":1,"status":1}))
    for DICT_ELEM in SEARCH_ARRAY:
        status = (DICT_ELEM['status'])
    return(status)

def getCodes(status):
    client = MongoClient('mongodb://localhost:27017/')
    db = client.citi
    col = db.CUT_SPEI_STATUS
    SEARCH_ARRAY = (col.find({"status":status},{"_id":0,"code":1,"status":1}))
    codesOut = []
    for DICT_ELEM in SEARCH_ARRAY:
        codesOut.append({"code": DICT_ELEM['code']})
    return(codesOut)