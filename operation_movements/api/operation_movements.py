import logging
import operator


class OperationMovements(object):
    def __init__(self,
                 payment_auth_number,  # Aplica en todos
                 beneficiary_payment_account,  # Aplica en todos
                 payment_date,  # Aplica en todos
                 payment_date_end, #Aplica en todos 
                 numeric_reference,  # Aplica en todos
                 s500_auth_number,  # operaciones
                 status,  # operaciones
                 payment_type,  # operaciones
                 tracking_key,  # operaciones
                 bank,  # operaciones
                 customer_number,  # operaciones
                 transaction_invoice,  # operaciones
                 package_invoice  # operaciones
                 ):
        self.payment_auth_number = payment_auth_number
        self.beneficiary_payment_account = beneficiary_payment_account
        self.payment_date = payment_date  # Aplica en todos
        self.payment_date_end = payment_date_end  # Aplica en todos
        self.numeric_reference = numeric_reference  # Aplica en todos
        self.s500_auth_number = s500_auth_number  # operaciones
        self.status = status  # operaciones
        self.payment_type = payment_type  # operaciones
        self.tracking_key = tracking_key  # operaciones
        self.bank = bank  # operaciones
        self.customer_number = customer_number  # operaciones
        self.transaction_invoice = transaction_invoice  # operaciones
        self.package_invoice = package_invoice  # operaciones

    @property
    def payment_auth_number(self):
        return self._payment_auth_number

    @payment_auth_number.setter
    def payment_auth_number(self, payment_auth_number):
        if not payment_auth_number:
           ## self.coun = self.coun +1
            payment_auth_number = 0
            # raise Exception(
            #     "|| Número de autorización de pago es requerido ||")
        if len(str(payment_auth_number)) > 8:
            raise Exception(
                "|| Número de autorización no puede exceder de 8 posiciones ||")
        self._payment_auth_number = payment_auth_number

    @property
    def beneficiary_payment_account(self):
        return self._beneficiary_payment_account

    @beneficiary_payment_account.setter
    def beneficiary_payment_account(self, beneficiary_payment_account):
        if not beneficiary_payment_account:
           ## self.coun = self.coun +1
            beneficiary_payment_account = 0
            # raise Exception(
            #     "|| Cuenta de abono del beneficiario es requerido ||")
        if len(str(beneficiary_payment_account)) > 20:
            raise Exception(
                "|| Cuenta de abono del beneficiario no puede exceder de 20 posiciones ||")
        self._beneficiary_payment_account = beneficiary_payment_account

    @property
    def payment_date(self):
        return self._payment_date

    @payment_date.setter
    def payment_date(self, payment_date):
        if not payment_date:
            raise Exception("Criterio invàlido de fecha, se requiere digitar una fecha inicial y una fecha final") 
            # raise Exception("Criterio búsqueda Inválido, se debe indicar siempre la fecha")
        if len(str(payment_date)) > 10:
            raise Exception(
                "Fecha de abono no puede exceder de 6 posiciones")
        self._payment_date = payment_date

    @property
    def payment_date_end(self):
        return self._payment_date_end

    @payment_date_end.setter
    def payment_date_end(self, payment_date_end):
        if not payment_date_end:
            raise Exception("Criterio invàlido de fecha, se requiere digitar una fecha inicial y una fecha final") 
            # raise Exception("Criterio búsqueda Inválido, se debe indicar siempre la fecha")
            #payment_date_end = self._payment_date
        if len(str(payment_date_end)) > 10:
            raise Exception(
                "Fecha de abono fin no puede exceder de 6 posiciones")
        self._payment_date_end = payment_date_end

    @property
    def numeric_reference(self):
        return self._numeric_reference

    @numeric_reference.setter
    def numeric_reference(self, numeric_reference):
        if not numeric_reference:
           ## self.coun = self.coun +1
            # raise Exception("|| Referencia númerica es requerida ||")
            numeric_reference = 0
        if len(str(numeric_reference)) > 7:
            raise Exception(
                "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._numeric_reference = numeric_reference

    @property
    def s500_auth_number(self):
        return self._s500_auth_number

    @s500_auth_number.setter
    def s500_auth_number(self, s500_auth_number):
        if not s500_auth_number:
           ## self.coun = self.coun +1
            s500_auth_number = "n/a"
        # if len(str(s500_auth_number)) > 7:
        #     raise Exception(
        #         "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._s500_auth_number = s500_auth_number

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        if not status:
           ## self.coun = self.coun +1
            status = "n/a"
        # if len(str(s500_auth_number)) > 7:
        #     raise Exception(
        #         "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._status = status

    @property
    def payment_type(self):
        return self._payment_type

    @payment_type.setter
    def payment_type(self, payment_type):
        if not payment_type:
           ## self.coun = self.coun +1
            payment_type = "n/a"
        # if len(str(s500_auth_number)) > 7:
        #     raise Exception(
        #         "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._payment_type = payment_type

    @property
    def tracking_key(self):
        return self._tracking_key

    @tracking_key.setter
    def tracking_key(self, tracking_key):
        if not tracking_key:
           ## self.coun = self.coun +1
            tracking_key = 0
        if len(str(tracking_key)) > 30:
            raise Exception(
                "||Clave de rastreo no puede exceder de 30 posiciones ||")
        self._tracking_key = tracking_key

    @property
    def bank(self):
        return self._bank

    @bank.setter
    def bank(self, bank):
        if not bank:
           ## self.coun = self.coun +1
            bank = "n/a"
        # if len(str(s500_auth_number)) > 7:
        #     raise Exception(
        #         "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._bank = bank

    @property
    def customer_number(self):
        return self._customer_number

    @customer_number.setter
    def customer_number(self, customer_number):
        if not customer_number:
           ## self.coun = self.coun +1
            customer_number = "n/a"
        # if len(str(s500_auth_number)) > 7:
        #     raise Exception(
        #         "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._customer_number = customer_number

    @property
    def transaction_invoice(self):
        return self._transaction_invoice

    @transaction_invoice.setter
    def transaction_invoice(self, transaction_invoice):
        if not transaction_invoice:
           ## self.coun = self.coun +1
            transaction_invoice = "n/a"
        # if len(str(s500_auth_number)) > 7:
        #     raise Exception(
        #         "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._transaction_invoice = transaction_invoice

    @property
    def package_invoice(self):
        return self._package_invoice

    @package_invoice.setter
    def package_invoice(self, package_invoice):
        if not package_invoice:
            package_invoice = "n/a"
           ## self.coun = self.coun +1
        # if len(str(s500_auth_number)) > 7:
        #     raise Exception(
        #         "|| Referencia númerica no puede exceder de 7 posiciones ||")
        self._package_invoice = package_invoice

    # @property
    # def coun(self):
    #     return self._coun
    
    # @coun.setter
    # def coun(self, coun):
    #     self._coun = coun 

    def get(self):
        return {
            "payment_auth_number": self.payment_auth_number,
            "beneficiary_payment_account": self.beneficiary_payment_account,
            "payment_date": self.payment_date,
            "payment_date_end": self.payment_date_end,
            "numeric_reference": self.numeric_reference,
            "s500_auth_number": self.s500_auth_number,
            "status": self.status,
            "payment_type": self.payment_type,
            "tracking_key": self.tracking_key,
            "bank": self.bank,
            "customer_number": self.customer_number,
            "transaction_invoice": self.transaction_invoice,
            "package_invoice": self.package_invoice
            #"count": self.coun
        }
    
