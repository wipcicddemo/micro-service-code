from sqlalchemy import Column, String, Integer, ForeignKey, create_engine, and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, mapper
# import cx_Oracle
import yaml 


with open(r'./api/properties.yaml') as file:
    properties_list = yaml.load(file, Loader=yaml.FullLoader)

Base = declarative_base()

host=properties_list['oracledb']['host']
port=properties_list['oracledb']['port']
sid=properties_list['oracledb']['sid']
user=properties_list['oracledb']['user']
password=properties_list['oracledb']['password']
# sid = cx_Oracle.makedsn(host, port, sid=sid)

cstr = 'oracle://{user}:{password}@{host}:{port}/{sid}'.format(
# cstr = 'oracle+cx_oracle://{user}:{password}@{sid}'.format(
user=user,
password=password,
host=host,
port=port,
sid=sid
)

engine =  create_engine(
    cstr,
    convert_unicode=False,
    pool_recycle=10,
    pool_size=50,
    echo=True
)

Session = sessionmaker()
Session.configure(bind=engine)
