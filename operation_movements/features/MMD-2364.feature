Feature: MMD-2364 Operation Movements 
#2174 General

    Scenario: Get a Movement 
        Given a dictionary
        """
        {
            "payment_auth_number": 0,
            "beneficiary_payment_account": 1,
            "payment_date": "2020-02-23",
            "numeric_reference": 1,
            "bank": "",
            "customer_number": "",
            "package_invoice": "",
            "payment_type": "",
            "s500_auth_number": "ww",
            "status": "",
            "tracking_key": 0,
            "transaction_invoice": ""
        }
        """
        When the service is called
        Then the status code is 200

        
    Scenario: Insufficient DATA
        Given a dictionary
        """
        {
            "payment_auth_number": 0,
            "beneficiary_payment_account": 0,
            "payment_date": "2020-02-23",
            "numeric_reference": 0,
            "bank": "",
            "customer_number": "",
            "package_invoice": "",
            "payment_type": "",
            "s500_auth_number": "",
            "status": "",
            "tracking_key": 0,
            "transaction_invoice": ""
        }
        """
        When the service is called
        Then the status code is 400 
        and the message is Consulta incorrecta, se debe tener al menos dos criterios de búsqueda la fecha y otro criterio


    Scenario: Invalid DATA
        Given a dictionary
        """
        {
            "payment_auth_number": 1,
            "beneficiary_payment_account": 0,
            "payment_date": "2020-02",
            "numeric_reference": 0,
            "bank": "",
            "customer_number": "",
            "package_invoice": "",
            "payment_type": "",
            "s500_auth_number": "",
            "status": "",
            "tracking_key": 0,
            "transaction_invoice": ""
        }
        """
        When the service is called
        Then the status code is 400 
        and the message is Formato de fecha inválido, validar debe ser aaaa-mm-dd

    Scenario: Invalid Searching DATE
        Given a dictionary
        """
        {
            "payment_auth_number": 1,
            "beneficiary_payment_account": 0,
            "payment_date": "2018-02-12",
            "numeric_reference": 0,
            "bank": "",
            "customer_number": "",
            "package_invoice": "",
            "payment_type": "",
            "s500_auth_number": "",
            "status": "",
            "tracking_key": 0,
            "transaction_invoice": ""
        }
        """
        When the service is called
        Then the status code is 400 
        and the message is Datos inválidos, la fecha no puede exceder 120 días
