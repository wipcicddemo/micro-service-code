"""
Realiza el envio de paquetes al aplicativo encargado de la orquestación

"""


import requests
import re
import logging
import datetime
import os
import json
import sys
import http.client
from http import HTTPStatus

from datetime import datetime

class packageSender:

    def postPackage(self, url, data):
        newData = {
                    "_id" : "1",
                    "horaPaquete" : "15:38:04.791",
                    "fechaPaquete" : "02-04-2020",
                    "fechaOperacionSistema" : "",
                    "cdaParticipientSPEIKey" : 4002,
                    "cantidadCDAS" : 5,
                    "arreglos" : [ 
                        {
                            "trackingKey" : [ "45", "45", "45", "45", "45"]
                        }, 
                        {
                            "folioCDA" : [ "1", "2", "3", "4", "5"]
                        }, 
                        {
                            "cdaParticipientSPEIKey" : [ 40002, 40002, 40002, 40002, 40002]
                        }, 
                        {
                            "CDA_size" : [ 344, 344, 344, 344, 344]
                        }
                    ],
                    "folioPaquete" : "1585863484100000001",
                    "pesoPaquete" : 1544,
                    "paquete" : [ 
                        "||9|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||", 
                        "||9|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||", 
                        "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||", 
                        "||4|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||", 
                        "||5|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"
                    ]
}
        response = requests.post(url, json=data.stamp1)
        logging.log(logging.INFO, response)
        return response

