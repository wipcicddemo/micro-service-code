import pymongo
import random
import time
import sys
import requests
import json
import logging
import yaml
import os 
from datetime import datetime,timedelta

##from .Tools.packageSender import packageSender
logging.basicConfig(level=logging.INFO)
with open(r'./properties.yaml') as file:
    properties_list = yaml.load(file, Loader=yaml.FullLoader)
#os.system('cls')

def checkFolioIdCollection(db,colup):
    try:
        mongoIdPackage(colup)
        getPackageFolio(colup)
    except Exception as ex:
        colup = db['idandfolio']
        createPackageFolio = colup.insert_one({"_id": "1" ,"folioPackage" : 100000000})
        createPackageID = colup.insert_one({"_id": "2" ,"packageID" : "1"})
        print("Colup Created")

def mongoConn():
    try:
        connectTo =  properties_list['mongoDBConn']
        conn = pymongo.MongoClient( connectTo )
        db = conn.admin
        col = db.sepipDB
        mycol = db.paquetesCDA
        colup = db.idandfolio
    except Exception as ex:
        print("Error----->",ex)
    return (conn,db,col,mycol,colup)

def mongoIdPackage(colup):
    queryPackageID = colup.find({"_id": "2"},{"packageID":1})
    for i in queryPackageID:
        idPackage = i['packageID']
    return idPackage

def startIdPackage(colup):
    queryIdPackage = colup.find({"_id": "2"},{"packageID":1})
    for i in queryIdPackage:
        packageID = i['packageID']
    return packageID

def lastIdPackage(packageID,colup):
    queryIdPackage = colup.find({"_id": "2"},{"packageID":1})
    for i in queryIdPackage:
        packageID = int(i['packageID'])
    packageID += 1
    updatePackageID = colup.update({"_id": "2" }, {"$set" : {"packageID" : str(packageID)}},True,True)

def idPackage(mycol):
    idPackageQuery = mycol.find_one({"$query": {}, "$orderby": {"$natural" : -1}})
    idPackage = int(list(idPackageQuery.values())[0])
    print(idPackage)
    return idPackage
        
def updateExistPackage(updateFolioPackage):
    for i in updateFolioPackage:
        existPackage = col.update({"_id": str(i) }, {"$set" : {"customerHubModel.existPackage" : True}},True,True)

def getPackageFolio(colup):
    idPackageMongo = colup.find_one({"_id":"1"})
    packageFolio = idPackageMongo["folioPackage"]
    return packageFolio

def updatePackageFolio(updateFolioPackage,colup):
    idPackageMongo = colup.find_one({"_id":"1"})
    packageFolio = idPackageMongo["folioPackage"]
    for i in updateFolioPackage:
        updateFolio = col.update({"_id": str(i) }, {"$set" : {"customerHubModel.folioPackage" : packageFolio}},True,True)

def resetPackageFolio(colup):
    resetPackageFolio = colup.update({"_id": "1" }, {"$set" : {"folioPackage" : 100000000}},True,True)

def updateMongoPackageFolio(colup):
    idPackageMongo = colup.find_one({"_id":"1"})
    packageFolio = int(idPackageMongo["folioPackage"])
    packageFolio += 1
    updatePackageFolio = colup.update({"_id": "1" }, {"$set" : {"folioPackage" : packageFolio}},True,True)

def forElements(conn,db,col,mycol,colup):
    checkFolioIdCollection(db,colup)
    maximumTime = 35  #in seconds
    start = 0
    startTime = time.time()
    endTime = startTime + maximumTime
    timeTillSearch =  5
    time.sleep(1)
    while (True):
        updateFolioPackage = []
        trackingKeyArray = []
        cdaFolioArray = []
        originalFolioPackageArray = []
        cdaArray = []
        cdaParticipientSPEIKeyArray = []
        originalPaymentFolioArray = []
        cdaSizeArray = []
        cdaFolioDict = {}
        packageOriginalFolioDict = {}
        originalPaymentDict = {}
        cdaParticipientSPEIKeyDict ={}
        trackingKeyDict = {}
        cdaSizeDict = {}
        now = datetime.now()
        resetTime = 170000
        currentTime = now.strftime("%H%M%S")
        currentTimeFull = now.strftime("%H:%M:%S.%f")[:-3]
        currentDate = now.strftime("%d-%m-%Y")
        cdaCounter = 0
        startTime = time.time()
        endTime = startTime + maximumTime
        if int(currentTime) == resetTime:
            resetPackageFolio(colup)
        updatePackageFolio(updateFolioPackage,colup)
        print("                                          SEARCHING")
        print("                                         ",now.strftime("%H:%M:%S"))
        try:
            queryMongoDB = col.find({"customerHubModel.existPackage": {"$exists": False}}, {"customerHubModel.trackingKey":1,
                                     "customerHubModel.folioCDA":1,
                                     "customerHubModel.originalPaymentFolio":1,
                                     "customerHubModel.originalFolioPackage":1,
                                     "customerHubModel.CDA":1,
                                     "customerHubModel.cdaParticipientSPEIKey":1})
                            
        except Exception as ex:
            print ("------>",ex)
        for i in queryMongoDB:
            elements  = i['_id']
            customerHubModel = i['customerHubModel']
            trackingKey = customerHubModel['trackingKey']
            folioCDA = customerHubModel['folioCDA']
            originalFolioPackage = customerHubModel['originalFolioPackage']
            CDA = customerHubModel['CDA']
            cdaParticipientSPEIKey = customerHubModel['cdaParticipientSPEIKey']
            originalPaymentFolio = customerHubModel['originalPaymentFolio']
            cdaSize = sys.getsizeof(CDA)
            cdaCounter += 1
            endTime -= 1
            #print("--------------------------------")
            #print("maxTime ----------------------->",maximumTime)
            #print("startTime --------------------->",startTime)
            #print("endTime ----------------------->",endTime)
            #print("timer ------------------------->",endTime - startTime)
            #print("--------------------------------")
            #print("cda Count --------------------->",cdaCounter)
            #print("id ---------------------------->",elements)
            #print("customerHubModel -------------->",customerHubModel)
            #print("Tracking Key ------------------>",trackingKey)
            #print("Folio CDA --------------------->",folioCDA)
            #print("Original Folio package -------->",originalFolioPackage)
            #print("CDA --------------------------->",CDA)
            #print("SPEI Key ---------------------->",cdaParticipientSPEIKey)
            #print("Original Payment Folio -------->",originalPaymentFolio)
            #print("Tamano CDA -------------------->",cdaSize)
            updateFolioPackage.append(elements)
            trackingKeyArray.append(trackingKey)
            cdaFolioArray.append(folioCDA)
            originalFolioPackageArray.append(str(originalFolioPackage))
            cdaArray.append(CDA)
            cdaParticipientSPEIKeyArray.append(cdaParticipientSPEIKey)
            originalPaymentFolioArray.append(str(originalPaymentFolio))
            cdaSizeArray.append(cdaSize)
            packageSize = sys.getsizeof(str(cdaArray))           
            if cdaCounter == 35 or packageSize >=30000:
                print("                                          SIZE OR COUNT\n")
                #print("updateFolioPackage ----------------------->",updateFolioPackage)
                #print("trackingKeyArray ------------------------->",trackingKeyArray)
                #print("cdaFolioArray ---------------------------->",cdaFolioArray)
                #print("originalFolioPackageArray ---------------->",originalFolioPackageArray)
                #print("cdaArray --------------------------------->",cdaArray)
                #print("cdaParticipientSPEIKeyArray -------------->",cdaParticipientSPEIKeyArray)
                #print("originalPaymentFolioArray ---------------->",originalPaymentFolioArray)
                #print("cdaSizeArray ----------------------------->",cdaSizeArray)
                #print("packageSize ------------------------------>",packageSize,"\n")
                updateExistPackage(updateFolioPackage)
                updatePackageFolio(updateFolioPackage,colup)
                packageFolio = updatePackageFolio(updateFolioPackage,colup)
                break
            else:
                if endTime == startTime and cdaCounter !=0:
                    print("                                          TIME EXPIRED\n")
                    #print("updateFolioPackage ----------------------->",updateFolioPackage)
                    #print("trackingKeyArray ------------------------->",trackingKeyArray)
                    #print("cdaFolioArray ---------------------------->",cdaFolioArray)
                    #print("originalFolioPackageArray ---------------->",originalFolioPackageArray)
                    #print("cdaArray --------------------------------->",cdaArray)
                    #print("cdaParticipientSPEIKeyArray -------------->",cdaParticipientSPEIKeyArray)
                    #print("originalPaymentFolioArray ---------------->",originalPaymentFolioArray)
                    #print("cdaSizeArray ----------------------------->",cdaSizeArray)
                    #print("packageSize ------------------------------>",packageSize,"\n")
                    updateExistPackage(updateFolioPackage)
                    updatePackageFolio(updateFolioPackage,colup)
                    packageFolio = updatePackageFolio(updateFolioPackage,colup)
                    break
        else:
            if cdaCounter !=0:
                print("                                          TIME EXPIRED NOT MORE ELEMENTS FOUND\n")
                #print("updateFolioPackage ----------------------->",updateFolioPackage)
                #print("trackingKeyArray ------------------------->",trackingKeyArray)
                #print("cdaFolioArray ---------------------------->",cdaFolioArray)
                #print("originalFolioPackageArray ---------------->",originalFolioPackageArray)
                #print("cdaArray --------------------------------->",cdaArray)
                #print("cdaParticipientSPEIKeyArray -------------->",cdaParticipientSPEIKeyArray)
                #print("originalPaymentFolioArray ---------------->",originalPaymentFolioArray)
                #print("cdaSizeArray ----------------------------->",cdaSizeArray)
                #print("packageSize ------------------------------>",packageSize,"\n")
                updateExistPackage(updateFolioPackage)
                updatePackageFolio(updateFolioPackage,colup)
                packageFolio = updatePackageFolio(updateFolioPackage,colup)     
        if cdaCounter !=0:
            trackingKeyDict['trackingKey'] = (trackingKeyArray)
            cdaFolioDict['cdaFolio'] = (cdaFolioArray)
            packageOriginalFolioDict['originalPackageFolio'] = (originalFolioPackageArray)
            cdaSizeDict['cdaSize'] = (cdaSizeArray)
            cdaParticipientSPEIKeyDict['cdaParticipientSPEIKey'] = (cdaParticipientSPEIKeyArray)
            originalPaymentDict['originalPaymentFolio'] = (originalPaymentFolioArray)
            packageDict = json.dumps(cdaArray, indent=4, sort_keys=True, default=str)
            fullArray = json.dumps([cdaFolioDict,packageOriginalFolioDict,originalPaymentDict,cdaParticipientSPEIKeyDict,trackingKeyDict,cdaSizeDict])
            dictionaryArraysJSON = (fullArray)
            dictionaryPackage = json.loads(str(packageDict))
            cdaCounter = len(cdaArray)
            packageSize = sys.getsizeof(str(dictionaryArraysJSON)+str(dictionaryArraysJSON))


            print("                                          ELEMENTS TO JSON\n")
            print(dictionaryArraysJSON,"\n")
            print(dictionaryPackage,"\n")
            packageID = startIdPackage(colup)
            packageFolio = getPackageFolio(colup)
            dtNow = datetime.now()
            today = datetime.now()
            expire = timedelta(days=5)
            expireDB = today + expire
            
            dataRequestMongo =  {
                "_id": (str(packageID)),
                "packageTime": (currentTimeFull), 
                "packageDate": (currentDate),
                "dateOperationSystem": (dtNow),
                "cdaParticipientSPEIKey": 4002,
                "cdaQuantity": (cdaCounter), 
                "arrays": (dictionaryArraysJSON),
                "packageFolio": (packageFolio), 
                "packageWeight":(packageSize),
                "package": (dictionaryPackage),
                "expireAt" : (expireDB)
            }
            dataRequest =  {
                "_id": (str(packageID)),
                "packageTime": (currentTimeFull), 
                "packageDate": (currentDate),
                "dateOperationSystem": str(dtNow),
                "cdaParticipientSPEIKey": 4002,
                "cdaQuantity": (cdaCounter), 
                "arrays": (dictionaryArraysJSON),
                "packageFolio": (packageFolio), 
                "packageWeight":(packageSize),
                "package": (dictionaryPackage)
            }
            updateMongoPackageFolio(colup)
            lastIdPackage(packageID,colup)
            dataRequest['arrays'] = json.loads(str(dictionaryArraysJSON))
            dataRequestMongo['arrays'] = json.loads(str(dictionaryArraysJSON))
            mycol.insert_one(dataRequestMongo)
            #del dataRequestMongo['dateOperationSystem']
            dataJSON = json.dumps(dataRequest)
            
            
            try:
                url = properties_list['machineStateEndPoint']
                msg = timeRequest = properties_list['timeOutRequest']
                print("Data: ", dataJSON)
                postPackage(url, dataJSON, timeRequest , 0)
            except Exception as ee:
                print("---------------------------------------------------------------------   ", ee)
                response = str(ee)
                    
        print("                                          NEXT SEARCH IN:",timeTillSearch,"SECONDS")
        print("                                         ",now.strftime("%H:%M:%S"),"\n")
        
        print("---------------------------------------------------------------------------------------------------------")
        time.sleep(timeTillSearch)
        
def postPackage(url, payload, timeoutCount, retriesCount):
    headers = {
        'Content-Type': 'application/json'
    }
    print("RETRIES ---------------------------------->", retriesCount)
    print("URL -------------------------------------->", url)
    print("DATA -------------------------------------> ",payload)
    print("                                          END OF DATA")
    try:
        now= datetime.now()
        current_date = now.strftime("%d-%m-%Y")
        current_time = now.strftime("%H:%M:%S")
        print("                                          START REQUEST")
        response = requests.request("POST", url, headers=headers, data = payload, timeout = timeoutCount )
        if response.status_code != 200:
            raise Exception('Invalid Answer')
        else:
            status = "Received"
            
            print("                                          LAUNCH MONGO")
            mongoUpdate(payload, retriesCount, current_date, current_time, status)
            print("REQUEST ANSWER --------------------------->",response.status_code)
            print("STATUS ----------------------------------->",status)
            print("RETRIES ---------------------------------->",retriesCount)
            print("DATE ------------------------------------->",current_date)
            print("TIME ------------------------------------->",current_time)
            print("STATUS ----------------------------------->",status)
            print("                                          END REQUEST")
            print("                                          UPDATING MONGO")
    except Exception as ee:
        retriesCount += 1
        timeoutCount += 1
        now= datetime.now()
        current_date = "N/A"
        current_time = "N/A"
        status = "In process to retry"
        print("                                          IN PROCESS TO RETRY")
        
        print("TIME ------------------------------------->",timeoutCount)
        print("                                          FAILED REQUEST")
        print("CACHED EXCEPTION ------------------------->",ee)
        print("                                          END FAILED REQUEST")
        if retriesCount < properties_list['retries']:
            print("STATUS ----------------------------------->",status)
            print("RETRIES ---------------------------------->",retriesCount)
            print("DATE ------------------------------------->",current_date)
            print("TIME ------------------------------------->",current_time)
            mongoUpdate(payload, retriesCount, current_date, current_time, status)
            postPackage(url, payload, timeoutCount, retriesCount)
        else:
            status = "Retries are over"
            current_date = "N/A"
            current_time = "N/A"
            print("                                          RETRIES ARE OVER")
            mongoUpdate(payload, retriesCount, current_date, current_time, status)
        response = str(ee)
    return response

def mongoUpdate(payload,retriesCount, current_date, current_time, status):
    mongoData = json.loads(payload)
    print("                                          INSIDE MONGO\n")
    mycol.update(
        {"_id": mongoData["_id"]},
        {
            "$set" :
            {
                "retries" : retriesCount,
                "deliveryDate": current_date,
                "deliveryTime": current_time,
                "status": status
            }
        },False,False
    )
    #return response    


mongoConn = mongoConn()
conn = mongoConn[0]
db = mongoConn[1]
col = mongoConn[2]
mycol = mongoConn[3]
colup = mongoConn[4]
forElements = forElements(conn,db,col,mycol,colup)
