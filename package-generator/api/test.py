import doctest
import pymongo
import random
import time
import sys
import requests
import json
import logging
import yaml
import os 
from datetime import datetime
with open(r'C:/Users/52556/Desktop/micro-service-code/package-generator/properties.yaml') as file:
    properties_list = yaml.load(file, Loader=yaml.FullLoader)

def mongoConn():
    '''
    mongoConn () devuelve conexion a la base de datos
    >>> mongoConn ()
    ''
    '''
    conn = pymongo.MongoClient( 'mongodb://localhost:27017/' )
    db = conn.cda
    col = db.sepipDB
    mycol = db.paquetesCDA
    colup = db.idandfolio
    return (conn,db,col,mycol,colup)

#def startIdPackage(colup):
#    '''
#    startIdPackage (colup) devuelve folio del paquete
#    >>> startIdPackage(colup)
#    '2'
#    '''
#    queryIdPackage = colup.find({"_id": "2"},{"packageID":1})
#    for i in queryIdPackage:
#        packageID = i['packageID']
#    return packageID
#
#def mongoIdPackage(colup):
#    '''
#    mongoIdPackage (colup) devuelve ID del paquete
#    >>> mongoIdPackage(colup)
#    '2'
#    '''
#    queryPackageID = colup.find_one({"_id": "2"})
#    idPackage = queryPackageID['packageID']
#    return idPackage
#
#def getPackageFolio(colup):
#    '''
#    getPackageFolio (colup) devuelve folio del paquete
#    >>> getPackageFolio(colup)
#    100000000
#    '''
#    idPackageMongo = colup.find_one({"_id":"1"})
#    folioPackage = idPackageMongo["folioPackage"]
#    return folioPackage
#
#def idPackage(mycol):
#    '''
#    idPackage (mycol) devuelve ID del paquete
#    >>> idPackage(mycol)
#    20
#    '''
#    idPackageQuery = mycol.find_one({"$query": {}, "$orderby": {"$natural" : -1}})
#    idPackage = int(list(idPackageQuery.values())[0])
#    return idPackage
#
#def updateExistPackage(n):
#    '''
#    updateExistPackage (n) devuelve si ya esta empaquetado el cda
#    >>> updateExistPackage(1)
#    True
#    '''
#    existPackage = col.update({"_id": str(n) }, {"$set" : {"customerHubModel.existPackage" : True}},True,True)
#    existPackage = col.find_one({"_id": str(n) })
#    for i in existPackage:
#        customerHubModel = existPackage['customerHubModel']
#        exist = customerHubModel['existPackage']
#    return exist

#
#def updatePackageFolio(n,colup):
#    '''
#    updatePackageFolio (n,colup) devuelve folio del paquete
#    >>> updatePackageFolio(1,colup)
#    100000000
#    '''
#    idPackageMongo = colup.find_one({"_id":"1"})
#    packageFolio = idPackageMongo["folioPackage"]
#    updateFolio = col.update({"_id": str(n) }, {"$set" : {"customerHubModel.folioPackage" : 100000000}},True,True)
#    updatedFolio = col.find_one({"_id": str(n) })
#    for i in updatedFolio:
#        customerHubModel = updatedFolio['customerHubModel']
#        exist = customerHubModel['folioPackage']
#    return exist
#
#def resetPackageFolio(colup):
#    '''
#    resetPackageFolio (colup) reinicia el folio por tiempo  
#    >>> resetPackageFolio(colup)
#    999999999
#    '''
#    now = datetime.now()
#    resetTime = 170000
#    currentTime = now.strftime("%H%M%S")
#    if resetTime > int(currentTime):
#        resetPackageFolio = colup.update({"_id": "1" }, {"$set" : {"folioPackage" : 999999999}},True,True)
#    updatedFolio = colup.find_one({"_id": str(1) })
#    updatedFolio = updatedFolio["folioPackage"]
#    return updatedFolio
#
#def checkFolioIdCollection(db,colup):
#    '''
#    checkFolioIdCollection (db,colup) devuelve el id y folio si no existe la base de datos la crea
#    >>> checkFolioIdCollection (db,colup)
#    ('1', 100000000)
#    '''
#    try:
#        mongoIdPackage(colup)
#        getPackageFolio(colup)
#    except Exception as ex:
#        colup = db['idandfolio']
#        createPackageFolio = colup.insert_one({"_id": "1" ,"folioPackage" : 100000000})
#        createPackageID = colup.insert_one({"_id": "2" ,"packageID" : "1"})
#        print("Colup Created")
#    return (mongoIdPackage(colup), getPackageFolio(colup))
#
#def updateMongoPackageFolio(colup):
#    '''
#    updateMongoPackageFolio(colup) +1 al folio para el proximo paquete
#    >>> updateMongoPackageFolio(colup)
#    100000001
#    '''
#    setFolio = colup.update({"_id": "1" }, {"$set" : {"folioPackage" : 100000000}},True,True)
#    idPackageMongo = colup.find_one({"_id":"1"})
#    packageFolio = int(idPackageMongo["folioPackage"])
#    packageFolio += 1
#    updatePackageFolio = colup.update({"_id": "1" }, {"$set" : {"folioPackage" : packageFolio}},True,True)
#    return packageFolio
#
#def lastIdPackage(colup):
#    '''
#    lastIdPackage(colup) busca el ultimo paquete generado si se llega a caer el servicio 
#    >>> lastIdPackage(colup)
#    2
#    '''
#    queryIdPackage = colup.find({"_id": "2"},{"packageID":1})
#    for i in queryIdPackage:
#        packageID = int(i['packageID'])
#    packageID += 1
#    updatePackageID = colup.update({"_id": "2" }, {"$set" : {"packageID" : str(packageID)}},True,True)
#    return packageID

def forElements(conn,db,col,mycol,colup):
    '''
    forElements(conn,db,col,mycol,colup) genera el paquete
    >>> forElements(conn,db,col,mycol,colup)
    {'_id': '1', 'cdaParticipientSPEIKey': 4002, 'cdaQuantity': 5, 'arrays': [{'cdaFolio': [1, 2, 3, 4, 5]}, {'originalPackageFolio': ['1', '2', '3', '4', '5']}, {'originalPaymentFolio': ['PF1', 'PF2', 'PF3', 'PF4', 'PF5']}, {'cdaParticipientSPEIKey': [40002, 40002, 40002, 40002, 40002]}, {'trackingKey': ['1', '2', '3', '4', '5']}, {'cdaSize': [345, 345, 345, 345, 345]}], 'packageFolio': 100000000, 'packageWeight': 641, 'package': ['||01|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||', '||02|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||', '||03|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||', '||04|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||', '||05|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||']}
    '''
    maximumTime = 20  #in seconds
    start = 0
    startTime = time.time()
    endTime = startTime + maximumTime
    timeTillSearch =  5
    time.sleep(1)
    while (True):
        updateFolioPackage = []
        trackingKeyArray = []
        cdaFolioArray = []
        originalFolioPackageArray = []
        cdaArray = []
        cdaParticipientSPEIKeyArray = []
        originalPaymentFolioArray = []
        cdaSizeArray = []
        cdaFolioDict = {}
        packageOriginalFolioDict = {}
        originalPaymentDict = {}
        cdaParticipientSPEIKeyDict ={}
        trackingKeyDict = {}
        cdaSizeDict = {}
        now = datetime.now()
        resetTime = 170000
        currentTime = now.strftime("%H%M%S")
        currentTimeFull = now.strftime("%H:%M:%S.%f")[:-3]
        currentDate = now.strftime("%d-%m-%Y")
        cdaCounter = 0
        startTime = time.time()
        endTime = startTime + maximumTime
        
        try:
            queryMongoDB = col.find({"customerHubModel.existPackage": {"$exists": False}}, {
                                     "customerHubModel.trackingKey":1,
                                     "customerHubModel.folioCDA":1,
                                     "customerHubModel.originalPaymentFolio":1,
                                     "customerHubModel.originalFolioPackage":1,
                                     "customerHubModel.CDA":1,
                                     "customerHubModel.cdaParticipientSPEIKey":1})
                            
        except Exception as ex:
            print ("------>",ex)
        for i in queryMongoDB:
            elements  = i['_id']
            customerHubModel = i['customerHubModel']
            trackingKey = customerHubModel['trackingKey']
            folioCDA = customerHubModel['folioCDA']
            originalFolioPackage = customerHubModel['originalFolioPackage']
            CDA = customerHubModel['CDA']
            cdaParticipientSPEIKey = customerHubModel['cdaParticipientSPEIKey']
            originalPaymentFolio = customerHubModel['originalPaymentFolio']
            cdaSize = sys.getsizeof(CDA)
            cdaCounter += 1
            endTime -= 1
            updateFolioPackage.append(elements)
            trackingKeyArray.append(trackingKey)
            cdaFolioArray.append(folioCDA)
            originalFolioPackageArray.append(str(originalFolioPackage))  
            cdaArray.append(CDA)
            cdaParticipientSPEIKeyArray.append(cdaParticipientSPEIKey)
            originalPaymentFolioArray.append(str(originalPaymentFolio))
            cdaSizeArray.append(cdaSize)
            packageSize = sys.getsizeof(str(cdaArray))           
            if cdaCounter == 5 or packageSize >=30000:
                break
            else:
                if endTime == startTime and cdaCounter !=0:
                    break
        else:
            if cdaCounter !=0:
                print("                                          TIME EXPIRED NOT MORE ELEMENTS FOUND\n")    
        if cdaCounter !=0:
            trackingKeyDict['trackingKey'] = (trackingKeyArray)
            cdaFolioDict['cdaFolio'] = (cdaFolioArray)
            packageOriginalFolioDict['originalPackageFolio'] = (originalFolioPackageArray)
            cdaSizeDict['cdaSize'] = (cdaSizeArray)
            cdaParticipientSPEIKeyDict['cdaParticipientSPEIKey'] = (cdaParticipientSPEIKeyArray)
            originalPaymentDict['originalPaymentFolio'] = (originalPaymentFolioArray)
            packageDict = json.dumps(cdaArray, indent=4, sort_keys=True, default=str)
            fullArray = json.dumps([cdaFolioDict,packageOriginalFolioDict,originalPaymentDict,cdaParticipientSPEIKeyDict,trackingKeyDict,cdaSizeDict])
            dictionaryArraysJSON = (fullArray)
            dictionaryPackage = json.loads(str(packageDict))
            cdaCounter = len(cdaArray)
            packageSize = sys.getsizeof(str(dictionaryArraysJSON)+str(dictionaryArraysJSON))


            
            dtNow = datetime.now()
            dataRequestMongo =  {
                "_id": (str(1)),
                "cdaParticipientSPEIKey": 4002,
                "cdaQuantity": (cdaCounter), 
                "arrays": (dictionaryArraysJSON),
                "packageFolio": (100000000), 
                "packageWeight":(packageSize),
                "package": (dictionaryPackage)
            }
            dataRequest =  {
                "_id": (str(1)),
                "packageTime": (currentTimeFull), 
                "packageTime": (currentTimeFull), 
                "packageTime": (currentTimeFull), 
                "packageDate": (currentDate),
                "dateOperationSystem": str(dtNow),
                "cdaParticipientSPEIKey": 4002,
                "cdaQuantity": (cdaCounter), 
                "arrays": (dictionaryArraysJSON),
                "packageFolio": (100000000), 
                "packageWeight":(packageSize),
                "package": (dictionaryPackage)
            }
            dataRequest['arrays'] = json.loads(str(dictionaryArraysJSON))
            dataRequestMongo['arrays'] = json.loads(str(dictionaryArraysJSON))
            mycol.insert_one(dataRequestMongo)
            dataJSON = json.dumps(dataRequest)
            
        time.sleep(timeTillSearch)
        return dataRequestMongo



mongoConn = mongoConn()
conn = mongoConn[0]
db = mongoConn[1]
col = mongoConn[2]
mycol = mongoConn[3]
colup = mongoConn[4]


if __name__ == '__main__':
    import doctest
    doctest.testmod()



