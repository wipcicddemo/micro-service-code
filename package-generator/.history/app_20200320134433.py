import pymongo
import random
import time
import sys
import requests

import json
import logging
import yaml

from datetime import datetime
##from .Tools.packageSender import packageSender
logging.basicConfig(level=logging.INFO)


conn = pymongo.MongoClient( "mongodb://localhost:27017/")
db = conn.citi
col = db.sepipDB
mycol = db.paquetesCDA

paquetes = []
paquete = []
inicio = 0
enum = 1
tamano_paquete = 0
contador = 0
id = 0
contador_paquetes = 0
tiempo_max = 5
start_folio = 100000000

start_time = time.time()
end_time = 0

while (True):
    now= datetime.now()
    current_time = now.strftime("%H%M%S")
    current_timefull = now.strftime("%H:%M:%S.%f")[:-3]
    current_date = now.strftime("%d-%m-%Y")
    time_to_reset = int(current_time)
    timestamp = datetime.timestamp(now)
    folio_number = int(timestamp)

    folio_number_str = str(folio_number)
    folio = start_folio
    folio_id = int(folio_number_str + str(folio))
    end_time = time.time()
    if time_to_reset==(170000):
        start_folio=100000000
    if(inicio==0):
        start_time = time.time()
    array = []
    #Version Mongo v4.0
    #array = col.find({ "$expr": {"$lte": [{ "$toDouble": "$_id" }, str(id)]}}, {"customerHubModel.CDA":1})
    #Version Mongo v3.6 and below
    array = col.find({ "$where": "parseInt(this._id) > " + str(id) }, {"customerHubModel.CDA":1})
    contador = 0
    contador_paquetes = len(paquetes)
    print(contador)
    while(id < array.count()):
        if(inicio==0):
            start_time = time.time()

        paquete.append(array[id])
        id+=1
        if (abs(end_time-start_time)<tiempo_max):
                contador+=1
                tamano_paquete = sys.getsizeof(str(paquete))
                end_time = time.time()
                print(end_time-start_time)

                inicio=1

                if len(paquete)%35==0 or tamano_paquete >=30000:
                    paquetes.append(paquete)
                    end_time = time.time()
                    inicio=0
                    paquete = []
                    print("PAQUETE COMPLETO")
        else:
            print("TIEMPO CUMPLIDO")
            paquetes.append(paquete)
            inicio=0
            paquete = []
    else:
        if (abs(end_time-start_time)>tiempo_max and len(paquete)!=0):
            print("TIEMPO CUMPLIDO")
            paquetes.append(paquete)
            inicio=0
            paquete = []

    for paq in range(contador_paquetes,len(paquetes)):
        start_folio+=1
        folio = start_folio
        folio_id = int(folio_number_str + str(folio))
        print(current_timefull)
        print(current_date)
        # print(paquetes[paq])
        #print(tamano_paquete)
        count = str(paquetes[paq]).count('_id')
        tam_paquete = sys.getsizeof(str(paquetes[paq]))
        print(count)
        print(tam_paquete)

        #Se agrega para el envio de paquete a la state machine 
        #Cambio agregado 17 Marzo 2020 
        #Juan Carlos Cocoletzi Lopez, TATA Consultancy Services 
        # 

        dtNow = datetime.now()

        dataRequest =  {
            "_id": (paq),
            "folioPaquete": folio_id, 
            "paquete": str(paquetes[paq]),
            "horaPaquete": (current_timefull), 
            "fechaPaquete": (current_date),
            "cantidadCDAS": (count), 
            "pesoPaquete":(tam_paquete)
            ,"createdAt": dtNow
        }


        package = {}
        package = json.dumps(paquetes[paq])
        # print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        # print("Paquete: ", package)

        ##Se manda un JSON a la state machine y a la BD
        dataRequest['paquete']= json.loads(package)
        # dataRequest Se inserta en la Base de Datos
        mycol.save(dataRequest)
        #Se elimina la propiedad del TTL en mongo para la 
        #state machine
        del dataRequest['createdAt']

        # print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        # print("Paquete dumps: ", dataRequest['paquete'])

        dataJSON = json.dumps(dataRequest)
        #print("Data: ", dataJSON)
        

        # dataRequest se hace el envio al servicio expuesto
        try:
            url = 'http://localhost:8084/packageService'
            # print("Data: ", dataJSON)
            print("---------------------------------------------------------------------   M   E   N   S   A   J   E      D   E      M   E   T   O   D   O   ---------------------------------------------------------------------")

            msg = postPackage(url, dataJSON, 1, 0)
            print("---------------------------------------------------------------------   M   E   N   S   A   J   E      D   E      M   E   T   O   D   O   ---------------------------------------------------------------------")
            print('Respuesta: ', msg)
            print("---------------------------------------------------------------------   F   I   N      M   E   N   S   A   J   E      D   E      M   E   T   O   D   O   ---------------------------------------------------------------------")

        except Exception as ee:
            logging.log(logging.ERROR, ee)
            response = str(ee)
        


        for pa in paquetes[paq]:
            #for p in pa:
            col.update({"_id": pa["_id"]}, {"$set" : {"customerHubModel.folioPaquete" : folio_id}},True,True)




    time.sleep(1)


    def postPackage(url, payload, time, trying):
        headers = {
            'Content-Type': 'application/json'
        }
        print("---------------------------------------------------------------------   I   N   T   E   N   T   O   S   :   ", trying)
        print("---------------------------------------------------------------------   D   A   T   A      P   A   R   A      E   N   V   I   O   ---------------------------------------------------------------------")
        print("---------------------------------------------------------------------   A      L   A   ---------------------------------------------------------------------")
        print("---------------------------------------------------------------------   U   R   L   ---------------------------------------------------------------------", url)
        print("---------------------------------------------------------------------   D   A   T   A   :    ",payload)
        print("---------------------------------------------------------------------   F   I   N      D   A   T   A   ---------------------------------------------------------------------")

        try:
            # response = requests.post(url, json=data)
            print("---------------------------------------------------------------------   I   N   I   C   I   A      P   E   T   I   C   I   O   N   ---------------------------------------------------------------------")
            response = requests.request("POST", url, headers=headers, data = payload, timeout = time )
            print("---------------------------------------------------------------------   R   E   S   P   U   E   S   T   A      P   E   T   I   C   I   O   N   ---------------------------------------------------------------------")
            logging.log(logging.INFO, response)
            print("---------------------------------------------------------------------   F   I   N      R   E   S   P   U   E   S   T   A      P   E   T   I   C   I   O   N   ---------------------------------------------------------------------")
            print("---------------------------------------------------------------------   I   N   I   C   I   A      A   C   T   U   A   L   I   Z   A   C   I   O   N      E   N      M   O   N   G   O   ---------------------------------------------------------------------")
            print("---------------------------------------------------------------------   F   I   N   A   L   I   Z   A      A   C   T   U   A   L   I   Z   A   C   I   O   N      E   N      M   O   N   G   O   ---------------------------------------------------------------------")

        except Exception as ee:
            trying += 1
            time += 1
            now= datetime.now()
            current_date = now.strftime("%d-%m-%Y")
            current_time = now.strftime("%H:%M:%S")



            print("---------------------------------------------------------------------   T   I   E   M   P   O   :   ", time)
            print("---------------------------------------------------------------------   F   A   L   L   O      P   E   T   I   C   I   O   N   ---------------------------------------------------------------------")
            logging.log(logging.ERROR, ee)
            print("---------------------------------------------------------------------   F   I   N      F   A   L   L   O      P   E   T   I   C   I   O   N   ---------------------------------------------------------------------")
            if trying <= 3:
                print("---------------------------------------------------------------------   E   S   T   A   T   U   S:      R   E   I   N   T   E   N   T   O   ")
                print("---------------------------------------------------------------------   R   E   I   N   T   E   N   T   O   S   :   ", trying)
                print("---------------------------------------------------------------------   D   A   T   E   :   ", current_date)
                print("---------------------------------------------------------------------   H   O   U   R   :   ", current_time)
                
                postPackage(url, payload, time, trying)
            else:
                print("---------------------------------------------------------------------   S   E      A   G   O   T   A   R   O   N      L   O   S      R   E   I   N   T   E   N   T   O   S   ---------------------------------------------------------------------")
            print("------------------------------------------------------------------------------------------------------------------------------------------")
            print("------------------------------------------------------------------------------------------------------------------------------------------")
            print("------------------------------------------------------------------------------------------------------------------------------------------")
            print("------------------------------------------------------------------------------------------------------------------------------------------")
            print (ee)
            response = str(ee)

        return response

    def mongoUpdate():
        mycol.update({},true,true )
        return response


