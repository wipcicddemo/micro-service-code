import pymongo
import random
import time
import sys

import json
import logging

from datetime import datetime
##from .Tools.packageSender import PackageSender

conn = pymongo.MongoClient( "mongodb://localhost:27017/")
db = conn.citi
col = db.sepipDB
mycol = db.paquetesCDA

paquetes = []
paquete = []
inicio = 0
enum = 1
tamano_paquete = 0
contador = 0
id = 0
contador_paquetes = 0
tiempo_max = 5
start_folio = 100000000

start_time = time.time()
end_time = 0

while (True):
    now= datetime.now()
    current_time = now.strftime("%H%M%S")
    current_timefull = now.strftime("%H:%M:%S.%f")[:-3]
    current_date = now.strftime("%d-%m-%Y")
    time_to_reset = int(current_time)
    timestamp = datetime.timestamp(now)
    folio_number = int(timestamp)

    folio_number_str = str(folio_number)
    folio = start_folio
    folio_id = int(folio_number_str + str(folio))
    end_time = time.time()
    if time_to_reset==(170000):
        start_folio=100000000
    if(inicio==0):
        start_time = time.time()
    array = []
    #Version Mongo v4.0
    #array = col.find({ "$expr": {"$lte": [{ "$toDouble": "$_id" }, str(id)]}}, {"customerHubModel.CDA":1})
    #Version Mongo v3.6 and below
    array = col.find({ "$where": "parseInt(this._id) > " + str(id) }, {"customerHubModel.CDA":1})
    contador = 0
    contador_paquetes = len(paquetes)
    print(contador)
    while(id < array.count()):
        if(inicio==0):
            start_time = time.time()

        paquete.append(array[id])
        id+=1
        if (abs(end_time-start_time)<tiempo_max):
                contador+=1
                tamano_paquete = sys.getsizeof(str(paquete))
                end_time = time.time()
                print(end_time-start_time)

                inicio=1

                if len(paquete)%35==0 or tamano_paquete >=30000:
                    paquetes.append(paquete)
                    end_time = time.time()
                    inicio=0
                    paquete = []
                    print("PAQUETE COMPLETO")
        else:
            print("TIEMPO CUMPLIDO")
            paquetes.append(paquete)
            inicio=0
            paquete = []
    else:
        if (abs(end_time-start_time)>tiempo_max and len(paquete)!=0):
            print("TIEMPO CUMPLIDO")
            paquetes.append(paquete)
            inicio=0
            paquete = []

    for paq in range(contador_paquetes,len(paquetes)):
        start_folio+=1
        folio = start_folio
        folio_id = int(folio_number_str + str(folio))
        print(current_timefull)
        print(current_date)
        print(paquetes[paq])
        #print(tamano_paquete)
        count = str(paquetes[paq]).count('_id')
        tam_paquete = sys.getsizeof(str(paquetes[paq]))
        print(count)
        print(tam_paquete)

        #Se agrega para el envio de paquete a la state machine 
        #Cambio agregado 17 Marzo 2020 
        #Juan Carlos Cocoletzi Lopez, TATA Consultancy Services  
        dataRequest =  {
            "_id": (paq),
            "folioPaquete": folio_id, 
            "paquete": str(paquetes[paq]),
            "horaPaquete": (current_timefull), 
            "fechaPaquete": (current_date),
            "cantidadCDAS": (count), 
            "pesoPaquete":(tam_paquete)
        }

        
        print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        package = {}
        package = json.dumps(paquetes[paq])
        print("Paquete dumps: ", package)

       
        dataJSON = json.dumps(dataRequest)
        ##Se manda un JSON a la state machine 
        dataRequest['paquete']= package
        print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        print("Paquete dumps: ", dataRequest['paquete'])



        print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        print("Data: ", dataJSON)

        # Se inserta en la Base de Datos
        mycol.save(dataRequest)
        

        for pa in paquetes[paq]:
            #for p in pa:
            col.update({"_id": pa["_id"]}, {"$set" : {"customerHubModel.folioPaquete" : folio_id}},True,True)




    time.sleep(1)
