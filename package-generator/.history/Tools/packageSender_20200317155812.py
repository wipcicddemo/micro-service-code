"""
Realiza el envio de paquetes al aplicativo encargado de la orquestación

"""


import requests


import re
import logging
import datetime
import os
import json
import sys

import http.client
from http import HTTPStatus

from datetime import datetime


def postPackage(url, data):
    response = requests.post(url, json=data.stamp1)
    logging.log(logging.INFO, response)
    return response

