import pymongo
import random
import time
import sys
import requests

import json
import logging

from datetime import datetime
##from .Tools.packageSender import packageSender
logging.basicConfig(level=logging.INFO)


conn = pymongo.MongoClient( "mongodb://localhost:27017/")
db = conn.citi
col = db.sepipDB
mycol = db.paquetesCDA

paquetes = []
paquete = []
inicio = 0
enum = 1
tamano_paquete = 0
contador = 0
id = 0
contador_paquetes = 0
tiempo_max = 5
start_folio = 100000000

start_time = time.time()
end_time = 0

while (True):
    now= datetime.now()
    current_time = now.strftime("%H%M%S")
    current_timefull = now.strftime("%H:%M:%S.%f")[:-3]
    current_date = now.strftime("%d-%m-%Y")
    time_to_reset = int(current_time)
    timestamp = datetime.timestamp(now)
    folio_number = int(timestamp)

    folio_number_str = str(folio_number)
    folio = start_folio
    folio_id = int(folio_number_str + str(folio))
    end_time = time.time()
    if time_to_reset==(170000):
        start_folio=100000000
    if(inicio==0):
        start_time = time.time()
    array = []
    #Version Mongo v4.0
    #array = col.find({ "$expr": {"$lte": [{ "$toDouble": "$_id" }, str(id)]}}, {"customerHubModel.CDA":1})
    #Version Mongo v3.6 and below
    array = col.find({ "$where": "parseInt(this._id) > " + str(id) }, {"customerHubModel.CDA":1})
    contador = 0
    contador_paquetes = len(paquetes)
    print(contador)
    while(id < array.count()):
        if(inicio==0):
            start_time = time.time()

        paquete.append(array[id])
        id+=1
        if (abs(end_time-start_time)<tiempo_max):
                contador+=1
                tamano_paquete = sys.getsizeof(str(paquete))
                end_time = time.time()
                print(end_time-start_time)

                inicio=1

                if len(paquete)%35==0 or tamano_paquete >=30000:
                    paquetes.append(paquete)
                    end_time = time.time()
                    inicio=0
                    paquete = []
                    print("PAQUETE COMPLETO")
        else:
            print("TIEMPO CUMPLIDO")
            paquetes.append(paquete)
            inicio=0
            paquete = []
    else:
        if (abs(end_time-start_time)>tiempo_max and len(paquete)!=0):
            print("TIEMPO CUMPLIDO")
            paquetes.append(paquete)
            inicio=0
            paquete = []

    for paq in range(contador_paquetes,len(paquetes)):
        start_folio+=1
        folio = start_folio
        folio_id = int(folio_number_str + str(folio))
        print(current_timefull)
        print(current_date)
        print(paquetes[paq])
        #print(tamano_paquete)
        count = str(paquetes[paq]).count('_id')
        tam_paquete = sys.getsizeof(str(paquetes[paq]))
        print(count)
        print(tam_paquete)

        #Se agrega para el envio de paquete a la state machine 
        #Cambio agregado 17 Marzo 2020 
        #Juan Carlos Cocoletzi Lopez, TATA Consultancy Services 
        # 

        dtNow = datetime.now()

        dataRequest =  {
            "_id": (paq),
            "folioPaquete": folio_id, 
            "paquete": str(paquetes[paq]),
            "horaPaquete": (current_timefull), 
            "fechaPaquete": (current_date),
            "cantidadCDAS": (count), 
            "pesoPaquete":(tam_paquete)
            ,"createdAt": dtNow
        }


        package = {}
        package = json.dumps(paquetes[paq])
        # print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        # print("Paquete: ", package)

        ##Se manda un JSON a la state machine y a la BD
        dataRequest['paquete']= json.loads(package)
        # dataRequest Se inserta en la Base de Datos
        mycol.save(dataRequest)
        #Se elimina la propiedad del TTL en mongo para la 
        #state machine
        del dataRequest['createdAt']

        # print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        # print("Paquete dumps: ", dataRequest['paquete'])

        dataJSON = json.dumps(dataRequest)
        print("--------------------------------------------------------------------------------------------------------------------------------------------------------")
        #print("Data: ", dataJSON)
        
        
        # dataRequest se hace el envio al servicio expuesto
        try:
            url = 'http://rgfhg?'
            # print("Data: ", dataJSON)

            msg = postPackage(url, dataJSON, 1)

            print('Respuesta: ', msg)
        except Exception as ee:
            logging.log(logging.ERROR, ee)
            response = str(ee)
        
        print("---------------------------------------------------------------------F   I   N      P   E   T   I   C   I   O   N---------------------------------------------------------------------")



        for pa in paquetes[paq]:
            #for p in pa:
            col.update({"_id": pa["_id"]}, {"$set" : {"customerHubModel.folioPaquete" : folio_id}},True,True)




    time.sleep(1)


    def postPackage(url, data, time):
        newData = {
            "_id" : 1,
            "folioPaquete" : 1582510955100000001,
            "paquete" : [{"_id": "42", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "43", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "22", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "25", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "29", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "89", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "90", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "92", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "93", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "94", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "95", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "96", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}, {"_id": "97", "customerHubModel": {"CDA": "||3|13122019|22112019|123456|40002|BANORTE|Juan Carlos Cocoletzi|40|400000400000400000|MOPS870925AI1|Citi Banamex|FRANCISCO MORALES GARCIA|40|012180001135124014|AYSA871101YH1|Concepto del Pago|100.09|100.09|NA|NA|0|0|NA|0|0.00|NA|NA|NA|NA|1271238askdjfhkdeert|Error getting digital certificate||"}}],
            "horaPaquete" : "20:22:35.193",
            "fechaPaquete" : "23-02-2020",
            "cantidadCDAS" : 13,
            "pesoPaquete" : 9672
        }
        print("---------------------------------------------------------------------D   A   T   A      E   N   V   I   A   D   A---------------------------------------------------------------------")

        print("data: ",data)
        try:
            response = requests.post(url, json=data, timeout=time)
            logging.log(logging.INFO, response)
        except Exception as ee:
            logging.log(logging.ERROR, ee)
            response = str(ee)

        return response


