Feature: 1590 CDA package generator
  
  @GoodScenariosQuantity
  Scenario Outline: Generate package with different number of CDA ok
   Given <number> of CDAs in the database
   When i have <number> and i want <quantity> CDAs for the package
   Then package created: <message>

   Examples: CDAs in Database
   | number | quantity | message                                                     |
   | 1      | 1        | package created with one element                            |
   | 50     | 34       | package created with more than one element but less than 35 |
   | 50     | 35       | package created with exact number of elements               |

  @BadScenariosQuantity  
  Scenario Outline: Generate package with different number of CDA fail
   Given CDAs in Database: <CDAS>
   When try to create a package with <element> element
   Then send user <notification>

   Examples: No CDAs in database
   | CDAS   | element  | notification                                                           |
   | 0      | 0        | package cannot be created ERROR! no elements requested                 |
   | 0      | 1        | package cannot be created ERROR! no elements in database               |
   | 1      | 0        | package cannot be created ERROR! element in database but not requested |
   | 1      | 1        | package can be created                                                 |

  @RetreivingDataFromDBGood
  Scenario Outline: Retreiving size of desired package
   Given a package in the database small
   When the size is not bigger than 32000, my package: <size>
   Then response <message>

   Examples: Size of the package OK
   | size  | message                         |
   | 1     | package with elements           |
   | 31999 | package size is under requested |
  
  @RetreivingDataFromDBBAD
  Scenario Outline: Retreiving size of bigger package 
   Given a package in the database big
   When the size is bigger than 32000, my package: <size>
   Then bad response <message>

   Examples: Size of the package not OK
   | size  | message                               |
   | 32000 | package size limit                    |
   | 32001 | package size is bigger than requested |

