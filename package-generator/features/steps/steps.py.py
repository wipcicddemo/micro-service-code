from behave import given, when, then, step
from hamcrest import assert_that, equal_to, less_than_or_equal_to, greater_than, is_, less_than, greater_than_or_equal_to
import pymongo

#Good Scenarios for quantity
@given('{number} of CDAs in the database')
def step_impl(context,number):
    conn = pymongo.MongoClient( "mongodb://localhost:27017/")
    db = conn.cda
    col = db.paquetes
    mycol = db.paquetesCDA
    array = col.find({ "$expr": {"$lte": [{ "$toDouble": "$_id" }, str(id)]}}, {"customerHubModel.CDA":1, "customerHubModel.idTransaccion":1,"customerHubModel.folioCDA":1}).limit(int(number))
    for CDAs in array:
        print(CDAs)
    assert_that(int(number), is_(greater_than(0)))

@when('i have {number} and i want {quantity} CDAs for the package')
def step_impl(context,quantity,number):
        enum = 0
        CDA_package = []
        conn = pymongo.MongoClient( "mongodb://localhost:27017/")
        db = conn.cda
        col = db.paquetes
        mycol = db.paquetesCDA
        array = col.find({ "$expr": {"$lte": [{ "$toDouble": "$_id" }, str(id)]}}, {"customerHubModel.CDA":1, "customerHubModel.idTransaccion":1,"customerHubModel.folioCDA":1}).limit(int(number))
        for CDAs in array:
            print(CDAs)
        array_2 = col.find({ "$expr": {"$lte": [{ "$toDouble": "$_id" }, str(id)]}}, {"customerHubModel.CDA":1, "customerHubModel.idTransaccion":1,"customerHubModel.folioCDA":1}).limit(int(quantity))
        for CDAs in array_2:
            enum += 1
            CDA_package.append(CDAs)
        print(CDA_package)
        assert_that (enum, less_than_or_equal_to(int(quantity)))
    
@then('package created: {message}')
def step_impl(context,message):
    pass

#Bad Scenarios for quantity
@given('CDAs in Database: {CDAS}')
def step_impl(context, CDAS):
    conn = pymongo.MongoClient( "mongodb://localhost:27017/")
    db = conn.cda
    col = db.paquetes
    mycol = db.paquetesdrop
    array = col.find({ "$expr": {"$lte": [{ "$toDouble": "$_id" }, str(id)]}}, {"customerHubModel.CDA":1, "customerHubModel.idTransaccion":1,"customerHubModel.folioCDA":1}).limit(int(CDAS))
    for CDAs in array:
        print(CDAs)
    assert_that(int(CDAS), is_(less_than_or_equal_to(1))) 

@when('try to create a package with {element} element')
def step_impl(context,element):
    assert_that(int(element),is_(less_than_or_equal_to(1)))

@then('send user {notification}')
def step_impl(contex,notification):
    print(notification)
    pass

#Good Scenarios for size
@given('a package in the database small')
def step_impl(context):
    conn = pymongo.MongoClient( "mongodb://localhost:27017/")
    db = conn.cda
    col = db.paquetes
    mycol = db.paquetesCDA
    mongo_01 = mycol.find({},{"paquete":1,"_id":0}).limit(1)
    for t in mongo_01:
        print(t)
@when('the size is not bigger than 32000, my package: {size}')
def step_impl(context,size):
    assert_that(int(size),is_(less_than_or_equal_to(32000)))
@then('response {message}')
def step_impl(context,message):
    pass

#Bad Scenarios for size
@given('a package in the database big')
def step_impl(context):
    conn = pymongo.MongoClient( "mongodb://localhost:27017/")
    db = conn.cda
    col = db.paquetes
    mycol = db.paquetesCDA
    mongo_01 = mycol.find({},{"paquete":1,"_id":0}).limit(1)
    for t in mongo_01:
        print(t)

@when('the size is bigger than 32000, my package: {size}')
def step_impl(context,size):
    assert_that(int(size),is_(greater_than_or_equal_to(32000)))

@then('bad response {message}')
def step_impl(context,message):
    pass
    