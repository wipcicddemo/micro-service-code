class QueryParams:
    def __init__(self, clave_rastreo='', rfc_curp=None, fecha_spei=None):
        self.clave_rastreo = clave_rastreo
        self.rfc_curp = rfc_curp
        self.fecha_spei = fecha_spei

        if self.fecha_spei and self.clave_rastreo:
            pass
        if self.fecha_spei:
            pass
        if not self.fecha_spei and self.clave_rastreo:
            raise Exception("|| Búsqueda únicamente por clave de rastreo no es válido ||")

    @property
    def clave_rastreo(self):
        return self._clave_rastreo

    @clave_rastreo.setter
    def clave_rastreo(self, clave):
        if not clave:
            pass
            # raise Exception("|| Clave de rastreo no puede estar vacío ||")
        self._clave_rastreo = clave


    @property
    def fecha_spei(self):
        return self._fecha_spei

    @fecha_spei.setter
    def fecha_spei(self, fecha_spei):
        self._fecha_spei = fecha_spei
