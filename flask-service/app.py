import datetime
import yaml
import os
from flask import Flask
from flask import request
from flask import jsonify
from flask_cors import CORS, cross_origin
from cda import Cda
from query_params_mongo import QueryParams
from pymongo import MongoClient
from sshtunnel import SSHTunnelForwarder
from error import InvalidUsage


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
def hello_world():
    return 'El cielo sabe a chilaquiles.'


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/speip', methods=['POST'])
def speip():
    if request.method == 'POST' and request.is_json:
        try:
            datos_cda = request.json
            cda_enviado = build_cda(datos_cda)

            return cda_enviado.imprimir_cda()
        except Exception as exception:
            return json.loads(str(exception))


@app.route('/getspeip', methods=['POST'])
@cross_origin()
def get_speip():
    config = get_config()
    if request.method == 'POST' and request.is_json:
        try:
            datos_params = request.json
            query_params = build_query_parameters(datos_params)

            if bool(config.get('usessh')):
                virtual_folder = os.path.dirname(os.path.abspath(__file__))
                key_address = os.path.join(virtual_folder, config.get('sshkey'))

                with SSHTunnelForwarder(
                        (config.get('host')),
                        ssh_username=config.get('user'),
                        ssh_pkey=key_address,
                        remote_bind_address=(config.get('remoteaddress'), int(config.get('port')))
                ) as tunnel:
                    print("****SSH Tunnel Established****")
                    client = MongoClient(config.get('remoteaddress'), tunnel.local_bind_port)
                    print("****Created MongoClient****")

                    with client:
                        return query_mongodb(client, config, query_params)

            else:
                client = MongoClient('mongodb://{host}:{port}/'.format(
                    host=config.get('host'),
                    port=config.get('port')
                ))
                print("****Created MongoClient****")

                with client:
                    return query_mongodb(client, config, query_params)

        except Exception as exception:
            return jsonify({"error": str(exception)})


def query_mongodb(client, config, query_params):
    db = client[config.get('database')]
    collection = db[config.get('collection')]
    consulta = build_query(query_params)
    resultados = collection.find_one(consulta, {"_id": 0, "_class": 0})

    if resultados:
        return resultados['customerHubModel']
    else:
        return 'No se encontraron registros'


def build_query(query_params):
    consulta = {}

    if query_params.clave_rastreo:
        consulta["customerHubModel.trackingKey"] = query_params.clave_rastreo
    if query_params.rfc_curp:
        consulta["customerHubModel.rFCOCurpBeneficiario"] = query_params.rfc_curp
    if query_params.fecha_spei:
        split_date = query_params.fecha_spei.split("-")
        start_date = datetime.datetime(int(split_date[0]), int(split_date[1]), int(split_date[2]))

        consulta["customerHubModel.speiOperationDateFormat"] = {"$gte": start_date}
    return consulta


def build_cda(datos_cda):
    cda_enviado = Cda(datos_cda['Clave de Rastreo'],
                      datos_cda['Tipo de pago'],
                      datos_cda['Fecha de operación del SPEI'],
                      datos_cda['Fecha calendario del abono'],
                      datos_cda['Hora calendario del abono'],
                      datos_cda['Clave SPEI del Participante Emisor de la CDA'],
                      datos_cda['Nombre del Participante Emisor de la Orden de Transferencia'],
                      datos_cda['Nombre Ordenante'],
                      datos_cda['Tipo de Cuenta Ordenante'],
                      datos_cda['Cuenta Ordenante'],
                      datos_cda['RFC o CURP Ordenante'],
                      datos_cda['Tipo de Cuenta Beneficiario'],
                      datos_cda['Concepto del Pago'],
                      datos_cda['Importe del IVA'],
                      datos_cda['Tipo de Cuenta Beneficiario 2'],
                      datos_cda['Folio del Esquema Cobro Digital'],
                      datos_cda['Pago de la comisión por la transferencia'],
                      datos_cda['Monto de la comisión por la transferencia'],
                      datos_cda['Alias del número celular del comprador'],
                      datos_cda['Digito verificador del comprador'])
    return cda_enviado


def get_config():
    virtual_folder = os.path.dirname(os.path.abspath(__file__))
    config_address = os.path.join(virtual_folder, 'config.yml')
    with open(config_address, 'r') as yml_file:
        config = yaml.load(yml_file, Loader=yaml.FullLoader)
        return config['mongodb']


def build_query_parameters(datos_query):
    query_params = QueryParams(datos_query['claveRastreo'] if 'claveRastreo' in datos_query else '',
                               datos_query["rfcCurp"] if 'rfcCurp' in datos_query else None,
                               datos_query['speiOperationDate'] if 'speiOperationDate' in datos_query else None)

    return query_params

