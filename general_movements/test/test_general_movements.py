from api.getMovements import getMovement,dateValidator, getAllMovementsByTimeRange, addFilters,newMovement
import datetime
import os
from api.conn import Session
from api.data_base import CB_CUTSPEI_RACTGCCT, Base
from sqlalchemy import func




def test_getMovement():
    body = {'payment_auth_number': 1.0, 'beneficiary_payment_account': 1.0, 'payment_date': '2020-04-07', 'numeric_reference': 1.0}
    response = {'Code': 200, 'value': [{'legend': 'Abono Interbancario', 'query_criteria': 'T', 'settlement_date': datetime.datetime(2020, 4, 7, 14, 2, 30), 'settlement_time': None, 'ammount': 1, 'beneficiary_payment_account': 1, 'beneficiary_name': 'Juan ', 'tracking_key': 'Juan', 'numeric_reference': 1, 'payment_concept': 'Juan\n', 'issuing_institution': 'a', 'issuing_account': 1, 'issuer_name': 'Juan ', 'payment_auth_number': 1, 's500_auth_number': 1, 'status': 'a', 'payment_type': 1, 'customer_number': 1, 'bank': None, 'beneficiary_bank': 'a', 'currency': 1, 'transaction_invoice': 'a', 'package_invoice': 'a'}]}
    assert getMovement(body) == response

def test_dateValidator():
    date = "2020-04-07"
    response = {'date': datetime.datetime(2020, 4, 7, 0, 0), 'result': True, 'msg': ''}
    assert dateValidator(date) == response 

def test_getAllMovementsByTimeRange():
    data = {'payment_auth_number': 1.0, 'beneficiary_payment_account': 1.0, 'payment_date': '2020-04-07', 'numeric_reference': 1.0, 'status': 'n/a'}
    startDate = datetime.datetime(2020, 4, 7, 0, 0)
    endDate = datetime.datetime(2020, 4, 7, 0, 0)
    queryResponse = [{'legend': 'Abono Interbancario', 'query_criteria': 'T', 'settlement_date': datetime.datetime(2020, 4, 7, 14, 2, 30), 'settlement_time': None, 'ammount': 1, 'beneficiary_payment_account': 1, 'beneficiary_name': 'Juan ', 'tracking_key': 'Juan', 'numeric_reference': 1, 'payment_concept': 'Juan\n', 'issuing_institution': 'a', 'issuing_account': 1, 'issuer_name': 'Juan ', 'payment_auth_number': 1, 's500_auth_number': 1, 'status': 'a', 'payment_type': 1, 'customer_number': 1, 'bank': None, 'beneficiary_bank': 'a', 'currency': 1, 'transaction_invoice': 'a', 'package_invoice': 'a'}]
    assert getAllMovementsByTimeRange(data,startDate,endDate) == queryResponse

def test_newMovement():
    session = Session()
    data = {'payment_auth_number': 1.0, 'beneficiary_payment_account': 1.0, 'payment_date': '2020-04-07', 'numeric_reference': 1.0, 'status': 'n/a'}
    startDate = datetime.datetime(2020, 4, 7, 0, 0)
    endDate = datetime.datetime(2020, 4, 7, 0, 0)
    filters = addFilters(data, startDate, endDate)
    session.commit()
    queryResult = session.query(CB_CUTSPEI_RACTGCCT).filter(*filters).all()
    queryResponse = [{'legend': 'Abono Interbancario', 'query_criteria': 'T', 'settlement_date': datetime.datetime(2020, 4, 7, 14, 2, 30), 'settlement_time': None, 'ammount': 1, 'beneficiary_payment_account': 1, 'beneficiary_name': 'Juan ', 'tracking_key': 'Juan', 'numeric_reference': 1, 'payment_concept': 'Juan\n', 'issuing_institution': 'a', 'issuing_account': 1, 'issuer_name': 'Juan ', 'payment_auth_number': 1, 's500_auth_number': 1, 'status': 'a', 'payment_type': 1, 'customer_number': 1, 'bank': None, 'beneficiary_bank': 'a', 'currency': 1, 'transaction_invoice': 'a', 'package_invoice': 'a'}]
    assert newMovement(queryResult) == queryResponse

def test_addFilters():
    fields = {'payment_auth_number': 1.0, 'beneficiary_payment_account': 1.0, 'payment_date': '2020-04-07', 'numeric_reference': 1.0, 'status': 'n/a'}
    startDate = datetime.datetime(2020, 4, 7, 0, 0)
    endDate = datetime.datetime(2020, 4, 7, 0, 0)
    filters = [] 
    filters.append(func.trunc(CB_CUTSPEI_RACTGCCT.ACCOUNTINGDATE).between(startDate, endDate))
    filters.append( CB_CUTSPEI_RACTGCCT.payment_auth_number == 1.0)
    filters.append( CB_CUTSPEI_RACTGCCT.BENEFICIARYACCOUNTNUMBER == 1.0)
    filters.append( CB_CUTSPEI_RACTGCCT.NUMERICREFERENCE == 1.0)
    testFilters = addFilters(fields, startDate, endDate)
    assert str(testFilters[0]) == str(filters[0])
    assert str(testFilters[1]) == str(filters[1])
    assert str(testFilters[2]) == str(filters[2])
    assert str(testFilters[3]) == str(filters[3])

    


