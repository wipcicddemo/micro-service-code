from behave import given, when, then
import requests
import json
from compare import expect


@given(u'a dictionary')
def set_movement(context):
    # print (json.loads(context.text))
    context.dictionary = json.loads(context.text)


@when(u'the service is called')
def call_mock(context):
    request_data = context.dictionary
    url = "http://localhost:8081/getMovements"
    context.response = requests.post(url, json=request_data)

@then(u'the status code is {status_code}')
def assert_status_code(context, status_code):
    expect(status_code).to_equal(str(context.response.status_code))

@then(u'the message is {message}')
def assert_transfer_account(context, message):
    print("RESPUESTA: ------", context.response.json())
    print("MENSAJE: ------", message)
    expect(message).to_equal(context.response.json())

@then(u'the status of the transacction is {status}')
def assert_status_aplicada(context, status):
    x = (context.response.json())
    print(x)
    y = x['status']
    expect(status).to_equal(y)
