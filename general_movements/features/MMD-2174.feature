#Feature: MMD-2174 General Movements
Feature: MDD-2467 Ajuste para el Mapeo GENERAL_MOVEMENTS

    #Scenario: Get a Movement 
    #    Given a dictionary
    #    """
    #    {
    #        "payment_auth_number": 1,
    #        "beneficiary_payment_account": 10,
    #        "payment_date": "2020-02-12",
    #        "numeric_reference": 10
    #    }
    #    """
    #    When the service is called
    #    Then the status code is 200
#
    #    
    #Scenario: Insufficient DATA
    #    Given a dictionary
    #    """
    #    {
    #        "payment_auth_number": 1,
    #        "beneficiary_payment_account": 0,
    #        "payment_date": "2020-02-12",
    #        "numeric_reference": 10
    #    }
    #    """
    #    When the service is called
    #    Then the status code is 400 
    #    and the message is || Cuenta de abono del beneficiario es requerido ||
#
#
    #Scenario: Invalid DATA
    #    Given a dictionary
    #    """
    #    {
    #        "payment_auth_number": 1,
    #        "beneficiary_payment_account": 10,
    #        "payment_date": "2020-02",
    #        "numeric_reference": 10
    #    }
    #    """
    #    When the service is called
    #    Then the status code is 400 
    #    and the message is Formato de fecha inválido, validar debe ser aaaa-mm-dd

    #Scenario: Invalid Searching DATE
    #    Given a dictionary
    #    """
    #    {
    #        "payment_auth_number": 1,
    #        "beneficiary_payment_account": 10,
    #        "payment_date": "2018-02-12",
    #        "numeric_reference": 10
    #    }
    #    """
    #    When the service is called
    #    Then the status code is 400 
    #    and the message is Datos inválidos, la fecha no puede exceder 120 días
    
    Scenario Outline: Status Aplicada
        Given a dictionary
        """
        {
            "payment_auth_number": 1,
            "beneficiary_payment_account": 10,
            "payment_date": "2020-03-23",
            "numeric_reference": 10
        }
        """
        When the service is called
        Then the status of the transacction is <status>

        Examples:
        | status   |
        | Aplicada |

