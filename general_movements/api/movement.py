import logging
import operator


class Movement(object):
    def __init__(self,
                 legend,  # leyenda
                 query_criteria,  # criterio de busqueda
                 settlement_date,  # fecha de liquidacion
                 settlement_time,  #  hora de liquidacion
                 ammount,  # cantidad
                 beneficiary_payment_account,  # cuenta de abono beneficiario
                 beneficiary_name,  #  nombre del beneficiario
                 tracking_key,  # clave de rastreo
                 numeric_reference,  #  referencia numerica
                 payment_concept,  #  concepto de pago
                 issuing_institution,  # institucion emisora
                 issuing_account,  # cuenta emisora
                 issuer_name,  # nombre del emisor
                 payment_auth_number,  # Aplica en todos
                 s500_auth_number,  # número de autorizacion s500
                 status,  # estatus
                 payment_type,  # tipo de pago
                 customer_number,  # número de cliente
                 bank,  # Banco Emisor
                 beneficiary_bank,  #  Banco beneficiario
                 currency,  # moneda
                 # payment_date,  # Aplica en todos
                 transaction_invoice,  # folio de transacción
                 package_invoice  # folio de paquete
                 ):
        self.legend = "Abono Interbancario"
        self.query_criteria = "T"
        self.settlement_date = settlement_date
        self.settlement_time = settlement_time
        self.ammount = ammount
        self.beneficiary_payment_account = beneficiary_payment_account
        self.beneficiary_name = beneficiary_name
        self.tracking_key = tracking_key
        self.numeric_reference = numeric_reference
        self.payment_concept = payment_concept
        self.issuing_institution = issuing_institution
        self.issuing_account = issuing_account
        self.issuer_name = issuer_name
        self.payment_auth_number = payment_auth_number
        self.s500_auth_number = s500_auth_number
        self.status = status
        self.payment_type = payment_type
        self.customer_number = customer_number
        self.bank = bank
        self.beneficiary_bank = beneficiary_bank
        self.currency = currency
        self.transaction_invoice = transaction_invoice
        self.package_invoice = package_invoice
    
    def get(self):
        return {
            "legend": "Abono Interbancario",
            "query_criteria": "T",
            "settlement_date": self.settlement_date,
            "settlement_time": self.settlement_time,
            "ammount": self.ammount,
            "beneficiary_payment_account": self.beneficiary_payment_account,
            "beneficiary_name": self.beneficiary_name,
            "tracking_key": self.tracking_key,
            "numeric_reference": self.numeric_reference,
            "payment_concept": self.payment_concept,
            "issuing_institution": self.issuing_institution,
            "issuing_account": self.issuing_account,
            "issuer_name": self.issuer_name,
            "payment_auth_number": self.payment_auth_number,
            "s500_auth_number": self.s500_auth_number,
            "status": self.status,
            "payment_type": self.payment_type,
            "customer_number": self.customer_number,
            "bank": self.bank,
            "beneficiary_bank": self.beneficiary_bank,
            "currency": self.currency,
            "transaction_invoice": self.transaction_invoice,
            "package_invoice": self.package_invoice
        }


@property
def legend(self):
    return self._legend


@property
def query_criteria(self):
    return self._query_criteria  # criterio de busqueda


@property
def settlement_date(self):
    return self._settlement_date  # fecha de liquidacion


@property
def settlement_time(self):
    return self._settlement_time  #  hora de liquidacion


@property
def ammount(self):
    return self._ammount  # cantidad


@property
def beneficiary_payment_account(self):
    return self._beneficiary_payment_account  # cuenta de abono beneficiario


@property
def beneficiary_name(self):
    return self._beneficiary_name  #  nombre del beneficiario


@property
def tracking_key(self):
    return self._tracking_key  # clave de rastreo


@property
def numeric_reference(self):
    return self._numeric_reference  #  referencia numerica


@property
def payment_concept(self):
    return self._payment_concept  #  concepto de pago


@property
def issuing_institution(self):
    return self._issuing_institution  # institucion emisora


@property
def issuing_account(self):
    return self._issuing_account  # cuenta emisora


@property
def issuer_name(self):
    return self._issuer_name  # nombre del emisor


@property
def _payment_auth_number(self):
    return self._payment_auth_number  # Aplica en todos


@property
def s500_auth_number(self):
    return self._s500_auth_number  # número de autorizacion s500


@property
def status(self):
    return self._status  # estatus


@property
def payment_type(self):
    return self._payment_type  # tipo de pago


@property
def customer_number(self):
    return self._customer_number  # número de cliente


@property
def bank(self):
    return self._bank  # Banco Emisor


@property
def beneficiary_bank(self):
    return self._beneficiary_bank  #  Banco beneficiario


@property
def currency(self):
    return self._currency  # moneda


@property
def transaction_invoice(self):
    return self._transaction_invoice  # folio de transacción


@property
def package_invoice(self):
    return self._package_invoice  # folio de paquete

