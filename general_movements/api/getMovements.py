"""
Valida los parametros del modelo cda_fields
"""
import re
import logging
import datetime
import os
import yaml
import json
import sys
# modelo de parametros de movimientos 
from .operation_movements import OperationMovements
# modelo de parametros de movimientos 
from .movement import Movement
# constantes
from .const import Const
# mensajes
from .i18n.messages import Messages

from .data_base import CB_CUTSPEI_RACTGCCT, Base

from .conn import Session


#Para mock
import random

# BD Alchemy
from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.orm import sessionmaker

#servico de status
from .status_cutspei import getCode, getStatus



def handler(event, context):
    # logging.log(#logging.INFO, context)
    return post(event)


def post(body):
    # try:
    #     movement = getMovement(body)
    #     # logging.log(#logging.INFO,"Validaciones: ***************************************")
    #     print('****************************************************************')
    #     return movement['value'], movement['Code']
    #     # return json.dumps(fieldsValidation.__dict__), fieldsValidation['Code']
    try:
        movement = getMovement(body)
        # return arr, movement['Code']
        if movement['Code'] != 200:
            #'{ "detail":"' + movement['value'] + '", "status":"' + str(movement['Code']) + '", "title":"Bad--Request", "type":"about: blank"}'
            err = {
                "detail": movement['value'],
                "status": movement['Code'],
                "title": "Bad Request",
                "type":"about: blank"
            }
            #res = json.loads(err)
            res = json.dumps(err)
            res = json.loads(res)

        else:
            res = movement['value']
        return res , movement['Code']
    except Exception as ex:
        logging.log(logging.ERROR, ex)
        msg = str(ex)
        return msg, 400


def getMovement(body):
    response = {}
    try:
        isCorrect = dateValidator(body['payment_date'])
        if isCorrect['result']:
            opM = OperationMovements(
                payment_auth_number=body['payment_auth_number'],
                beneficiary_payment_account=body['beneficiary_payment_account'],
                payment_date=body['payment_date'],
                numeric_reference=body['numeric_reference'],
                status=getCode("Aplicada")
            )
            print(opM.get())
            res = opM.get()
            print('-------------------Inicio------------------------------')
            print(res)
            print('-------------------Fin------------------------------')
            response['Code'] = 200
            # response['value'] = res
            arr = []
            arr = getAllMovementsByTimeRange(res, isCorrect['date'], isCorrect['date'])
            response['value'] = arr
            # response['value'] = cretateMovementMock()
        else:
            print ("--------------_Error")
            raise Exception(isCorrect['msg'])
    except Exception as ex:
        logging.log(logging.ERROR, ex)
        msg = str(ex)
        response['value'] = msg
        response['Code'] = 400
    return response


"""
Valida los campos de hora, si alguno de los dos viene  entonces se convierten 
en requeridos, de lo contrario no son requeridos 
"""
def timeFields(datos_cda):
    start = 0
    end = 0
    result = 0
    results = {}
    msg = Messages()
    errors = ''
    try:
        start = datos_cda["hora_abono_inicio"]
        result += 1
    except:
        errors = msg.START_HOUR_REQUIRED
    try:
        end = datos_cda["hora_abono_fin"]
        result += 1
    except:
        errors += msg.END_HOUR_REQUIRED
    try:
        timediff = end - start
        if timediff < 0:
            raise Exception(msg.DIFF_FIELDS_TIME)
    except:
        result = 3
        errors += msg.DIFF_FIELDS_TIME

    results['fields'] = result
    results["error"] = errors
    return results


"""
Valida los campos de fecha
"""


def dateValidator(date):
    print("Fechas -------------",date)
    const = Const()
    newDate = None
    result = False
    msg = Messages()
    returnMsg = ''
    results = {}
    try:
        try:
            if not date:
                print("No hay fecha ------- zzzzz",date)
                raise Exception("Criterio de búsqueda inválido de fecha, se debe indicar siempre la fecha") 
        except :
            returnMsg = ("Criterio de búsqueda inválido de fecha, se debe indicar siempre la fecha")
            raise Exception("Criterio de búsqueda inválido de fecha, se debe indicar siempre la fecha") 
        try:
            print("Validacion 2 fecha ------- zzzzz",date)
            newDate = date.split("-")
            if len(newDate) < 3:
                returnMsg = msg.INVALID_FORMAT_DATE
                raise Exception(msg.INVALID_FORMAT_DATE)
        except:
            print("Fallo Validacion 2 fecha ------- zzzzz",date)
            returnMsg = msg.INVALID_FORMAT_DATE
            raise Exception(msg.INVALID_FORMAT_DATE)
        try:
            print("Validacion 3 fecha ------- zzzzz",date)
            year = int(newDate[0])
            month = int(newDate[1])
            days = int(newDate[2])
        except:
            print("Fallo Validacion 3 fecha ------- zzzzz",date)
            result = False
            returnMsg = msg.INVALID_FORMAT_DATE_CHARACTERS
            raise Exception(msg.INVALID_FORMAT_DATE_CHARACTERS)
        if (int(newDate[0]) < const.YEAR_MAX) and (int(newDate[0]) > const.YEAR_MIN):
            result = True
        else:
            returnMsg = msg.WRONG_DATE
            result = False
            raise Exception(msg.WRONG_DATE)

        if (int(newDate[1]) <= const.MONTH_MAX_ALLOWED) and (int(newDate[1]) >= const.MONTH_MIN_ALLOWED):
            result = True
        else:
            returnMsg = msg.WRONG_DATE
            result = False
            raise Exception(msg.WRONG_DATE)

        if (int(newDate[2]) <= const.DAYS_MAX_ALLOWED) and (int(newDate[2]) >= const.DAYS_MIN_ALLOWED):
            result = True
        else:
            returnMsg = msg.WRONG_DATE
            result = False
            raise Exception(msg.WRONG_DATE)
        try:
            datetime.datetime(year, month, days)
            results['date'] = datetime.datetime(year, month, days)
        except:
            result = False
            returnMsg = msg.INVALID_FORMAT_DATE
            raise Exception(msg.INVALID_FORMAT_DATE)
        try:
            mdate = datetime.datetime.today()
            rdate = datetime.datetime(year, month, days)
            delta =  (mdate - rdate).days
            print("dias de diferencia: ", delta)
            if delta > 120:
                raise Exception(msg.DIFF_FIELDS_TIME)
            if delta < 0 :
                raise Exception(" No se pueden hacer busquedas con fechas posteriores a la actual")
        except Exception as ex:
            result = False
            returnMsg = str(ex)
    except Exception as ex:
        print ('--------------------------: Error Final ',str(ex))
        newDate = None
        result = False
        returnMsg = str(ex)

    results['result'] = result
    results['msg'] = returnMsg
    return results
    # const = Const()
    # newDate = None
    # result = False
    # msg = Messages()
    # returnMsg = ''
    # results = {}
    # try:
    #     try:
    #         newDate = date.split("-")
    #         if len(newDate) < 3:
    #             returnMsg = msg.INVALID_FORMAT_DATE
    #             raise Exception(msg.INVALID_FORMAT_DATE)
    #     except:
    #         returnMsg = msg.INVALID_FORMAT_DATE
    #         raise Exception(msg.INVALID_FORMAT_DATE)
    #     try:
    #         year = int(newDate[0])
    #         month = int(newDate[1])
    #         days = int(newDate[2])
    #     except:
    #         result = False
    #         returnMsg = msg.INVALID_FORMAT_DATE_CHARACTERS
    #     if (int(newDate[0]) < const.YEAR_MAX) and (int(newDate[0]) > const.YEAR_MIN):
    #         result = True
    #     else:
    #         returnMsg = msg.WRONG_DATE
    #         result = False
    #         raise Exception(msg.WRONG_DATE)

    #     if (int(newDate[1]) <= const.MONTH_MAX_ALLOWED) and (int(newDate[1]) >= const.MONTH_MIN_ALLOWED):
    #         result = True
    #     else:
    #         returnMsg = msg.WRONG_DATE
    #         result = False
    #         raise Exception(msg.WRONG_DATE)

    #     if (int(newDate[2]) <= const.DAYS_MAX_ALLOWED) and (int(newDate[2]) >= const.DAYS_MIN_ALLOWED):
    #         result = True
    #     else:
    #         returnMsg = msg.WRONG_DATE
    #         result = False
    #         raise Exception(msg.WRONG_DATE)
    #     try:
    #         datetime.datetime(year, month, days)
    #     except:
    #         result = False
    #         returnMsg = msg.INVALID_FORMAT_DATE
    #     try:
    #         mdate = datetime.datetime.today()
    #         rdate = datetime.datetime(year, month, days)
    #         delta =  (mdate - rdate).days
    #         if delta > 120 :
    #             raise Exception(msg.DIFF_FIELDS_TIME)
    #     except Exception as ex:
    #         print (str(ex))
    #         result = False
    #         returnMsg = msg.DIFF_FIELDS_TIME
    # except:
    #     newDate = None
    #     result = False
    #     # returnMsg = msg.INVALID_FORMAT_DATE
    #     # returnMsg = returnMsg

    # results['result'] = result
    # results['msg'] = returnMsg
    # return results

# Validate if is an alphanumeric string or not
# Params: text to validate
# Return: True if is a valid string
#         False if is an invalids string
def alphanumericValidator(text):
    regex = r"^[A-Za-z0-9]+$"
    matches = re.match(regex, text)
    result = False
    if matches:
        result = True

    return result

def cretateMovementMock():
    now = datetime.datetime.now()
    dtm = datetime.datetime.timestamp(now)
    random.seed(dtm) 

    mvm = Movement(
        legend = "Abono Interbancario",
        query_criteria = "T",
        settlement_date = datetime.datetime.now(),
        settlement_time = datetime.datetime.now(),
        ammount = random.randint(2456,597889),
        beneficiary_payment_account = random.randint(2456,597889),
        beneficiary_name = "Beneficiario",
        tracking_key = random.randint(1455,597889),
        numeric_reference = random.randint(245,597889),
        payment_concept = "Pago ",
        issuing_institution = "Citi Banamex",
        issuing_account = random.randint(450,597889),
        issuer_name = "Usuario",
        payment_auth_number = random.randint(345,597889),
        s500_auth_number = random.randint(764,597889),
        status = getStatus("code"),
        payment_type = "Interbancario",
        customer_number = random.randint(987,597889),
        bank = "Citi Banamex",
        beneficiary_bank = "Citi BANAMEX",
        currency = "MX",
        transaction_invoice = random.randint(765,597889),
        package_invoice = random.randint(653,597889),
    )

    return {
        "legend": mvm.legend,
        "query_criteria": mvm.query_criteria,
        "settlement_date": mvm.settlement_date,
        "settlement_time": mvm.settlement_time,
        "ammount": mvm.ammount,
        "beneficiary_payment_account": mvm.beneficiary_payment_account,
        "beneficiary_name": mvm.beneficiary_name,
        "tracking_key": mvm.tracking_key,
        "numeric_reference": mvm.numeric_reference,
        "payment_concept": mvm.payment_concept,
        "issuing_institution": mvm.issuing_institution,
        "issuing_account": mvm.issuing_account,
        "issuer_name": mvm.issuer_name,
        "payment_auth_number": mvm.payment_auth_number,
        "s500_auth_number": mvm.s500_auth_number,
        "status": mvm.status,
        "payment_type": mvm.payment_type,
        "customer_number": mvm.customer_number,
        "bank": mvm.bank,
        "beneficiary_bank": mvm.beneficiary_bank,
        "currency": mvm.currency,
        "transaction_invoice": mvm.transaction_invoice,
        "package_invoice": mvm.package_invoice
    }
    # return json.dumps(mvm.__dict__)


def getAllMovementsByTimeRange(data, startDate, endDate):
    session = Session()
    queryResponse=[]
    filters = []
    filters = addFilters(data, startDate, endDate)
    session.commit()
    queryResult = session.query(CB_CUTSPEI_RACTGCCT).filter(*filters).all()
    session.close()
    queryResponse=newMovement(queryResult)
    return queryResponse




def newMovement(queryResult):
    queryResponse=[]
    if queryResult:
        print('--------------------Data-------------',queryResult)
        for res in queryResult:
            result = Movement(
                legend= "Abono Interbancario",
                query_criteria= "T",
                settlement_date= res.ACCOUNTINGDATE,
                settlement_time= res.settlement_time,
                ammount = res.AMOUNT,
                beneficiary_payment_account = res.BENEFICIARYACCOUNTNUMBER,
                beneficiary_name= res.FULLNAMEBENEFICIARY,
                tracking_key = res.TRACKINGKEY,
                numeric_reference = res.NUMERICREFERENCE,
                payment_concept= res.PAYMENTDESCRIPTION,
                issuing_institution= res.issuing_institution,
                issuing_account = res.ISSUERACCOUNTNUMBER,
                issuer_name= res.FULLNAMEISSUER,
                payment_auth_number = res.payment_auth_number,
                s500_auth_number = res.s500_auth_number,
                status= res.status,
                payment_type= res.PAYMENTTYPE,
                customer_number = res.customer_number,
                bank= res.bank,
                beneficiary_bank= res.beneficiary_bank,
                currency= res.CURRENCYCODE,
                transaction_invoice = res.transaction_invoice,
                package_invoice = res.package_invoice
                )
            queryResponse.append(result.get())
    else: 
        print ("no hay informacion")
        queryResponse.append({
                                "detail": "No hubo información con los criterios de búsqueda enviados",
                                "status": 200,
                                "title": "No Result",
                                "type": "about: blank"
                            })
    return queryResponse




def addFilters(fields, startDate, endDate):
    filters = []


    #Default Filter
    filters.append(func.trunc(CB_CUTSPEI_RACTGCCT.ACCOUNTINGDATE).between(startDate, startDate))

    for field in fields: 
        if fields[field] != 'n/a' and fields[field] != 0 and field != 'payment_date' and field != 'payment_date_end':
            if field == 'payment_auth_number':
                filters.append( CB_CUTSPEI_RACTGCCT.payment_auth_number == fields[field])
            if field == 'beneficiary_payment_account':
                filters.append( CB_CUTSPEI_RACTGCCT.BENEFICIARYACCOUNTNUMBER == fields[field])
            if field == 'numeric_reference':
                filters.append( CB_CUTSPEI_RACTGCCT.NUMERICREFERENCE == fields[field])
            if field == 'status':
                filters.append( CB_CUTSPEI_RACTGCCT.status == fields[field])

    return filters
