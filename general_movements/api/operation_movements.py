import logging
import operator


class OperationMovements(object):
    def __init__(self,
                 payment_auth_number,  # Aplica en todos
                 beneficiary_payment_account,  # Aplica en todos
                 payment_date,  # Aplica en todos
                 numeric_reference, #,  # Aplica en todos
                 status  # operaciones
                 ):
        self.payment_auth_number = payment_auth_number
        self.beneficiary_payment_account = beneficiary_payment_account
        self.payment_date = payment_date  # Aplica en todos
        self.numeric_reference = numeric_reference  # Aplica en todos
        self.status = status  # operaciones


    @property
    def payment_auth_number(self):
        return self._payment_auth_number

    @payment_auth_number.setter
    def payment_auth_number(self, payment_auth_number):
        if not payment_auth_number:
            raise Exception(
                "Número de autorización de pago es requerido")
        if len(str(payment_auth_number)) > 8:
            raise Exception(
                "Número de autorización no puede exceder de 8 posiciones")
        self._payment_auth_number = payment_auth_number

    @property
    def beneficiary_payment_account(self):
        return self._beneficiary_payment_account

    @beneficiary_payment_account.setter
    def beneficiary_payment_account(self, beneficiary_payment_account):
        if not beneficiary_payment_account:
            raise Exception(
                "Cuenta de abono del beneficiario es requerido")
        if len(str(beneficiary_payment_account)) > 20:
            raise Exception(
                "Cuenta de abono del beneficiario no puede exceder de 20 posiciones")
        self._beneficiary_payment_account = beneficiary_payment_account

    @property
    def payment_date(self):
        return self._payment_date

    @payment_date.setter
    def payment_date(self, payment_date):
        if not payment_date:
            raise Exception("Fecha de abono es requerido")
        if len(str(payment_date)) > 10:
            raise Exception(
                "Fecha de abono no puede exceder de 6 posiciones")
        self._payment_date = payment_date

    @property
    def numeric_reference(self):
        return self._numeric_reference

    @numeric_reference.setter
    def numeric_reference(self, numeric_reference):
        if not numeric_reference:
            raise Exception("Referencia númerica es requerida")
        if len(str(numeric_reference)) > 7:
            raise Exception(
                "Referencia númerica no puede exceder de 7 posiciones")
        self._numeric_reference = numeric_reference

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        if not status:
            status = "n/a"
        self._status = status

    
    def get(self):
        return {
            "payment_auth_number": self.payment_auth_number,
            "beneficiary_payment_account": self.beneficiary_payment_account,
            "payment_date": self.payment_date,
            "numeric_reference": self.numeric_reference,
            "status": self.status
        }
