/**
 * 
 */
package com.city.customerhub.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.city.customerhub.model.CustomerHubModel;



/**
 * @author SU20053232
 *
 */

@RestController
@RequestMapping(value="/v1")
public class CustomerHubStubController {
	
	@RequestMapping(value="/customerhub", method = RequestMethod.POST)
	public CustomerHubModel returnCustoemrHubInfo(@RequestBody CustomerHubModel customerHubModel) {	
		
		System.out.println("Customer Hub Model Received the Request");
		
		
		CustomerHubModel customerHubModelLocal = new CustomerHubModel();		
		
		//Input Fields
		customerHubModelLocal.setTrackingKey(customerHubModel.getTrackingKey());
		customerHubModelLocal.setPaymentType(customerHubModel.getPaymentType());
		customerHubModelLocal.setAmountOfPayment(customerHubModel.getAmountOfPayment());
		customerHubModelLocal.setBeneficiaryAccountType2(customerHubModel.getBeneficiaryAccountType2());
		customerHubModelLocal.setBuyerCellNumberAliases(customerHubModel.getBuyerCellNumberAliases());
		customerHubModelLocal.setCalenderDatePayment(customerHubModel.getCalenderDatePayment());
		customerHubModelLocal.setCalenderTimePayment(customerHubModel.getCalenderTimePayment());
		customerHubModelLocal.setCdaParticipientSPEIKey(customerHubModel.getCdaParticipientSPEIKey());
		customerHubModelLocal.setConceptOfPayment(customerHubModel.getConceptOfPayment());
		customerHubModelLocal.setNameOfTransferOrderIssuerParticipant(customerHubModel.getNameOfTransferOrderIssuerParticipant());
		customerHubModelLocal.setOrderingName(customerHubModel.getOrderingName());
		customerHubModelLocal.setOrdererAccountType(customerHubModel.getOrdererAccountType());
		customerHubModelLocal.setOrderingAccount(customerHubModel.getOrderingAccount());
		customerHubModelLocal.setRfcOrCurpPayer(customerHubModel.getRfcOrCurpPayer());
		customerHubModelLocal.setTypeOfBeneficiaryAccount(customerHubModel.getTypeOfBeneficiaryAccount());
		customerHubModelLocal.setConceptOfPayment(customerHubModel.getConceptOfPayment());
		customerHubModelLocal.setIvaAmount(customerHubModel.getIvaAmount());
		customerHubModelLocal.setAmountOfPayment(customerHubModel.getAmountOfPayment());
		customerHubModelLocal.setBeneficiaryAccountType2(customerHubModel.getBeneficiaryAccountType2());
		customerHubModelLocal.setDigitalCollectionSchemeFolio(customerHubModel.getDigitalCollectionSchemeFolio());
		customerHubModelLocal.setPaymentOfTheTransferFee(customerHubModel.getPaymentOfTheTransferFee());
		customerHubModelLocal.setTransferFeeAmount(customerHubModel.getTransferFeeAmount());
		customerHubModelLocal.setBuyerCellNumberAliases(customerHubModel.getBuyerCellNumberAliases());
		customerHubModelLocal.setDigitBuyersVerifier(customerHubModel.getDigitBuyersVerifier());
		customerHubModelLocal.setSpeiOperationDate(customerHubModel.getSpeiOperationDate());
		
		
		//Output Fields
		customerHubModelLocal.setMombreDelParticipanteReceptor("MombreDelParticipanteReceptor");
		customerHubModelLocal.setNombreBeneficiario("NombreBeneficiario");		
		customerHubModelLocal.setCuentaBeneficiario(123);
		customerHubModelLocal.setrFCOCurpBeneficiario("rFCOCurpBeneficiario");
		customerHubModelLocal.setNombreBeneficiario2("NombreBeneficiario2");
		customerHubModelLocal.setrFCOCurpBeneficiario2("setrFCOCurpBeneficiario2");	
		customerHubModelLocal.setCuentaBeneficiario2(1234556);
		customerHubModelLocal.setAliasDelNumeroCelularDelVendedor(234);
		customerHubModelLocal.setDigitoVerificadorDelVendedor(234);
		customerHubModelLocal.setNumeroDeSerieDelCertificado("numeroDeSerieDelCertificado");
		customerHubModelLocal.setSelloDigital("12346678899763qeekjhdiqwudhiasnxkanhduiqwaxnKDJNiqwuhI");
		
		return customerHubModelLocal ;		
		
	}

}
