Feature: MMD-1851 Generacion de archivo de baja de CDA

    Scenario: Generate file
        Given a dictionary
        """
        [
          {
              "trackingKey": "58",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "59",
              "speiOperationDate": 13122019
          }
        ]
        """
        When the service is called
        Then generate file with 2 CDA with Operation date, Tracking key, Payment sender bank, Beneficiary account and Amount

    Scenario: Generate 2 files
        Given a dictionary
        """
        [
            {
                "trackingKey": "54",
                "speiOperationDate": 13122019
            },
            {
                "trackingKey": "55",
                "speiOperationDate": 13122019
            },
            {
                "trackingKey": "56",
                "speiOperationDate": 13122019
            },
            {
                "trackingKey": "57",
                "speiOperationDate": 13122019
            },
            {
                "trackingKey": "58",
                "speiOperationDate": 13122019
            }
        ]
        """
        When the service is called
        Then generate 2 files with 5 CDA with Operation date, Tracking key, Payment sender bank, Beneficiary account and Amount


    Scenario: Generate one file
        Given a dictionary
        """
        [
          {
              "trackingKey": "54",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "55",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "56",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "57",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "58",
              "speiOperationDate": 13122019
          }
        ]
        """
        When the service is called
        Then generate file with 5 CDA with Operation date, Tracking key, Payment sender bank, Beneficiary account, Amount and CDA status and name format XXXXXYYYYMMDDXXX.txt

    Scenario: Invalid DATA
        Given a dictionary
        """
        [
          {
              "trackingKey": "",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "47",
              "speiOperationDate": 13122019
          },
          {
              "trackingKey": "51",
              "speiOperationDate": 13122019
          }
        ]
        """
        When the service is called
        Then the status code is 400
