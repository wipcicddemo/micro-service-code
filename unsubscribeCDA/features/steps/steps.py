from behave import given, when, then
import requests
import json
import os
import pymongo
from compare import expect

@given(u'a dictionary')
def set_movement(context):
    context.dictionary = json.loads(context.text)


@when(u'the service is called')
def call_mock(context):
    request_data = context.dictionary
    url = "http://localhost:8080/unsubscribeCDA"
    context.response = requests.post(url, json=request_data)

@then('generate file with 2 CDA with Operation date, Tracking key, Payment sender bank, Beneficiary account and Amount')
def assert_status_code(context):
    print("********************** FILE GENERATED ***************")
    with open(":/4000220200310002.txt", "r") as archivo:
        for linea in archivo:
            print(linea)

    print("********************* FILE PATH AND NAME FILE**************************")
    ruta = ":/4000220200310003.txt"
    print(os.path.split(ruta))

@then('generate 2 files with 5 CDA with Operation date, Tracking key, Payment sender bank, Beneficiary account and Amount')
def assert_status_code(context):
    print("*************** FILE ONE **************")
    with open(":/4000220200310004.txt", "r") as file:
        for x in file:
            print(x)
    print("*************** FILE TWO ****************")
    with open(":/4000220200310005.txt", "r") as file_two:
        for y in file_two:
            print(y)

    print("********************* FILE PATH AND NAME FILE ONE**************************")
    ruta = ":/4000220200310004.txt"
    print(os.path.split(ruta))
    print("********************* FILE PATH AND NAME FILE TWO**************************")
    ruta = ":/4000220200310005.txt"
    print(os.path.split(ruta))
    client = pymongo.MongoClient('mongodb://localhost:27017/')
    db = client.paquetes
    col = db.cda
    SEARCH_ARRAY = col.find_one({"customerHubModel.trackingKey":"54",
                                          "customerHubModel.speiOperationDate":13122019},
                                         {"_id":0,
                                          "customerHubModel.CDAStatus":1,"customerHubModel.HoraEnvio":1, "customerHubModel.FechaEnvio":1})
    for DICT_ELEM in SEARCH_ARRAY:
        DICT_VAR = (SEARCH_ARRAY[DICT_ELEM])
        CDAStatus = DICT_VAR["CDAStatus"]
        HoraEnvio = DICT_VAR["HoraEnvio"]
        FechaEnvio = DICT_VAR["FechaEnvio"]
        print("STATUS CDA:",CDAStatus)
        print("Hora envio:",HoraEnvio)
        print("Fecha Envio:",FechaEnvio)
@then('generate file with 5 CDA with Operation date, Tracking key, Payment sender bank, Beneficiary account, Amount and CDA status and name format XXXXXYYYYMMDDXXX.txt')
def assert_status_code(context):
    print("********************* FILE GENERATED *****************")
    with open(":/4000220200310005.txt", "r") as file:
        for z in file:
            print(z)
    print("********************* FILE PATH AND NAME FILE**************************")
    ruta = ":/4000220200310005.txt"
    print(os.path.split(ruta))

    client = pymongo.MongoClient('mongodb://localhost:27017/')
    db = client.paquetes
    col = db.cda

    print("********************* STATUS OF CDA IN DB **************************")
    SEARCH_ARRAY = col.find_one({"customerHubModel.trackingKey":"54",
                                          "customerHubModel.speiOperationDate":13122019},
                                         {"_id":0,
                                          "customerHubModel.CDAStatus":1,"customerHubModel.HoraEnvio":1, "customerHubModel.FechaEnvio":1})
    for DICT_ELEM in SEARCH_ARRAY:
        DICT_VAR = (SEARCH_ARRAY[DICT_ELEM])
        CDAStatus = DICT_VAR["CDAStatus"]
        HoraEnvio = DICT_VAR["HoraEnvio"]
        FechaEnvio = DICT_VAR["FechaEnvio"]
        print("STATUS CDA:",CDAStatus)
        print("Hora envio:",HoraEnvio)
        print("Fecha Envio:",FechaEnvio)



@then(u'the status code is {status_code}')
def assert_status_code(context, status_code):
    print("Estatus: ------", context.response.status_code)
    expect(status_code).to_equal(str(context.response.status_code))
